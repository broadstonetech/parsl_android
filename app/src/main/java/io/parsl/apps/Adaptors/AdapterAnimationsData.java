package io.parsl.apps.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.squareup.picasso.Picasso;

import io.parsl.apps.ModelClass.ModelsDataClass;
import io.parsl.apps.R;

public class AdapterAnimationsData extends RecyclerView.Adapter<AdapterAnimationsData.MyViewHolder> {
    private static ClickListener clickListener;
    ModelsDataClass dataList;
    private LayoutInflater inflater;
    private Context c;
    private int selectedModelPosition = -1;

    public AdapterAnimationsData(Context c, ModelsDataClass data, boolean extras) {
        this.c = c;
        this.dataList = data;
        inflater = LayoutInflater.from(c);
    }

    public int getSelectedModelPosition() {
        return selectedModelPosition;
    }

    public void setSelectedModelPosition(int selectedModelPosition) {
        this.selectedModelPosition = selectedModelPosition;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = inflater.inflate(R.layout.adaptor_extras_models, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if (selectedModelPosition == position) {
            holder.cvMain.setChecked(true);
        } else {
            holder.cvMain.setChecked(false);
        }


        Picasso.get().load(dataList.getAnimations().get(position).getAnimation_thumbnail()).placeholder(R.drawable.parsl_place_holder).into(holder.thumbnail);
        holder.name.setVisibility(View.GONE);
        holder.tvShadePrice.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return dataList.getAnimations().size();
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        AdapterAnimationsData.clickListener = clickListener;
    }


    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name, vendorName, tvShadePrice;
        ImageView thumbnail;
        MaterialCardView cvMain;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.tvName);
            vendorName = view.findViewById(R.id.tvVendor);
            thumbnail = view.findViewById(R.id.iv_thumb_adapter_filters);
            tvShadePrice = view.findViewById(R.id.tvShadePrice);
            cvMain = view.findViewById(R.id.cv_adaptor_data);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

}
