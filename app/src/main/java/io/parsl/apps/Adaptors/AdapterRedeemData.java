package io.parsl.apps.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.parsl.apps.ModelClass.ModelsDataClass;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Util.Utils;

public class AdapterRedeemData extends RecyclerView.Adapter<AdapterRedeemData.MyViewHolder> {
    private static ClickListener clickListener;
    private LayoutInflater inflater;
    private Context c;
    ModelsDataClass dataList;
    ModelsDataClass searchAbledataList;
    List<ModelsDataClass.Data> filteredList;
    private int selectedModelPosition = -1;
    boolean isExtras = false;
    String vendorName = "";


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView thumbnail;
        MaterialCardView cvMain;

        public MyViewHolder(View view) {
            super(view);
            thumbnail = view.findViewById(R.id.iv_thumb_adapter_filters);
            cvMain = view.findViewById(R.id.cv_adaptor_data);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public AdapterRedeemData(Context c, ModelsDataClass data) {
        this.c = c;
        this.dataList = data;
        this.searchAbledataList = data;
        inflater = LayoutInflater.from(c);
    }


    public int getSelectedModelPosition() {
        return selectedModelPosition;
    }

    public void setSelectedModelPosition(int selectedModelPosition) {
        this.selectedModelPosition = selectedModelPosition;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = inflater.inflate(R.layout.adaptor_redeem_data, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Picasso.get().load(searchAbledataList.getData().get(position).getModel_files().getThumbnail()).placeholder(R.drawable.parsl_place_holder).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return searchAbledataList.getData().size();
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        AdapterRedeemData.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

}
