package io.parsl.apps.Adaptors;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import io.parsl.apps.Activities.CartActivity;
import io.parsl.apps.Activities.CartActivity;
import io.parsl.apps.ModelClass.ShoppingCartItem;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Util.Utils;

import org.json.JSONArray;

import java.util.List;

import static io.parsl.apps.Activities.ARActivities.GltfActivity.shoppingCartProductList;
import static io.parsl.apps.Activities.CartActivity.commissioned;
import static io.parsl.apps.Activities.CartActivity.idList;
import static io.parsl.apps.Activities.CartActivity.rvDataInText;


public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.ViewHolder> {
    private Activity context;

    List<ShoppingCartItem> objList;
    int conter = 1;
    private String itemPrice;
    public static double totalPrice;
    double totalPriceWithCommission;
    private double price;
    private AdapterItemTextData adapterTextData;


    public CartListAdapter(Activity context, List<ShoppingCartItem> obj) {
        this.context = context;
        this.objList = obj;
        totalPrice = objList.get(0).getObj().getPrice_related().getPrice() * objList.get(0).getQuantity();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.adator_cart_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.itemname.setText(objList.get(position).getObj().getBasic_info().getName());
        Picasso.get().load(objList.get(position).getObj().getModel_files().getThumbnail()).placeholder(R.drawable.parsl_place_holder).into(holder.imgThumbnail);
        holder.itemprice.setText("$" + " " + Utils.getRoundedDoubleForMoney(objList.get(position).getObj().getPrice_related().getPrice()));
        holder.tv_quantity.setText(String.valueOf(objList.get(position).getQuantity()));


        if (objList.get(position).getDisCounts() > 0) {
            holder.tvDiscountedRate.setVisibility(View.VISIBLE);
            holder.tvDiscountedRate.setText("$" + " " + Utils.getRoundedDoubleForMoney(objList.get(position).getDisCounts()));
            holder.itemprice.setPaintFlags(holder.itemprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.tvDiscountedRate.setVisibility(View.GONE);
            holder.itemprice.setPaintFlags(0);
        }
        holder.cart_plus_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conter = objList.get(position).getQuantity();
                conter++;
                objList.get(position).setQuantity(conter);
                holder.tv_quantity.setText(String.valueOf(conter));


                adapterTextData = new AdapterItemTextData(context, objList);
                rvDataInText.setAdapter(adapterTextData);
                totalPrice = 0;
                if (objList.get(position).getDisCounts() > 0) {
                    for (int x = 0; x < objList.size(); x++) {

                        totalPrice += objList.get(x).getDisCounts() * objList.get(x).getQuantity();
                    }
                } else {
                    for (int x = 0; x < objList.size(); x++) {

                        totalPrice += objList.get(x).getObj().getPrice_related().getPrice() * objList.get(x).getQuantity();
                    }
                }

                commissioned = totalPrice * 0.15;
                totalPriceWithCommission = totalPrice + commissioned;
                CartActivity.tv_total.setText("$" + " " + Utils.getRoundedDoubleForMoney(totalPriceWithCommission));
                CartActivity.tvTranscationFee.setText("$" + " " + Utils.getRoundedDoubleForMoney(commissioned));
            }
        });

        holder.cart_minus_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conter = objList.get(position).getQuantity();
                if (conter > 0) {
                    conter--;
                    objList.get(position).setQuantity(conter);
                    holder.tv_quantity.setText(String.valueOf(conter));

                    adapterTextData = new AdapterItemTextData(context, objList);
                    rvDataInText.setAdapter(adapterTextData);
                    totalPrice = 0;
                    if (objList.get(position).getDisCounts() > 0) {
                        for (int x = 0; x < objList.size(); x++) {

                            totalPrice += objList.get(x).getDisCounts() * objList.get(x).getQuantity();
                        }
                    } else {
                        for (int x = 0; x < objList.size(); x++) {

                            totalPrice += objList.get(x).getObj().getPrice_related().getPrice() * objList.get(x).getQuantity();
                        }
                    }

                    commissioned = totalPrice * 0.15;
                    totalPriceWithCommission = totalPrice + commissioned;
                    CartActivity.tv_total.setText("$" + " " + Utils.getRoundedDoubleForMoney(totalPriceWithCommission));
                    CartActivity.tvTranscationFee.setText("$" + " " + Utils.getRoundedDoubleForMoney(commissioned));
                } else {
                    conter = 0;
                    itemPrice = String.valueOf(conter * objList.get(position).getObj().getPrice_related().getPrice());
                }


            }
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objList.remove(position);
                notifyDataSetChanged();
                adapterTextData = new AdapterItemTextData(context, objList);
                rvDataInText.setAdapter(adapterTextData);
                idList = new JSONArray();
                totalPrice = 0;
                for (int x = 0; x < objList.size(); x++) {

                    totalPrice += objList.get(x).getObj().getPrice_related().getPrice() * objList.get(x).getQuantity();
                    idList.put(objList.get(x).getObj().getProduct_id());
                }

                double commissioned = totalPrice * 0.15;
                double totalPriceWithCommisin = totalPrice + commissioned;
                CartActivity.tv_total.setText("$" + " " + Utils.getRoundedDoubleForMoney(totalPriceWithCommisin));
                CartActivity.tvTranscationFee.setText("$" + " " + Utils.getRoundedDoubleForMoney(commissioned));
            }
        });

    }


    @Override
    public int getItemCount() {
        return objList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView itemprice, itemname, tv_quantity, tvDiscountedRate;
        ImageView cart_minus_img, cart_plus_img;
        ImageView imgDelete, imgThumbnail;

        public ViewHolder(View itemView) {
            super(itemView);
            itemname = itemView.findViewById(R.id.tv_name);
            itemprice = itemView.findViewById(R.id.tv_rate);
            tv_quantity = itemView.findViewById(R.id.tv_total);
            tvDiscountedRate = itemView.findViewById(R.id.tvDiscountedRate);
            cart_plus_img = itemView.findViewById(R.id.imgAddQuantity);
            cart_minus_img = itemView.findViewById(R.id.imgSubtractQuantity);
            imgDelete = itemView.findViewById(R.id.imgDelete);
            imgThumbnail = itemView.findViewById(R.id.imgThumbnail);

        }
    }


}

