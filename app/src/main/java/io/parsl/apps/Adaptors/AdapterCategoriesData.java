package io.parsl.apps.Adaptors;

import static io.parsl.apps.Network.Parser.CategoriesDataParser.categoriesDatatObj;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;

import io.parsl.apps.ModelClass.ModelsDataClass;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Util.Utils;

import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class AdapterCategoriesData extends RecyclerView.Adapter<AdapterCategoriesData.MyViewHolder> implements Filterable {
    private static ClickListener clickListener;
    private LayoutInflater inflater;
    private Context c;
    ModelsDataClass dataList;
    ModelsDataClass searchAbledataList;
    List<ModelsDataClass.Data> filteredList;
    private int selectedModelPosition = -1;
    boolean isExtras = false;
    String vendorName = "";

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    try {
                        searchAbledataList = (ModelsDataClass) categoriesDatatObj.clone();
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                } else {
                    filteredList = new ArrayList<>();
                    for (ModelsDataClass.Data row : searchAbledataList.getData()) {
                        if (row.getBasic_info().getName().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getBasic_info().getName().toLowerCase().contains(charString.toLowerCase()) || row.getBasic_info().getManufacturer().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    searchAbledataList.setData(filteredList);

                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = searchAbledataList.getData();
                return filterResults;
            }


            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                searchAbledataList.setData((ArrayList<ModelsDataClass.Data>) filterResults.values);
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name, vendorName, tvShadePrice;
        ImageView thumbnail;
        MaterialCardView cvMain;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.tvName);
            vendorName = view.findViewById(R.id.tvVendor);
            thumbnail = view.findViewById(R.id.iv_thumb_adapter_filters);
            tvShadePrice = view.findViewById(R.id.tvShadePrice);
            cvMain = view.findViewById(R.id.cv_adaptor_data);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public AdapterCategoriesData(Context c, ModelsDataClass data) {
        this.c = c;
        this.searchAbledataList = data;
        this.dataList = data;
        inflater = LayoutInflater.from(c);
    }

    public int getSelectedModelPosition() {
        return selectedModelPosition;
    }

    public void setSelectedModelPosition(int selectedModelPosition) {
        this.selectedModelPosition = selectedModelPosition;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = inflater.inflate(R.layout.adaptor_categories_data, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (!isExtras) {
            if (selectedModelPosition == position) {
                holder.cvMain.setChecked(true);
            }
        } else {
            holder.cvMain.setChecked(selectedModelPosition == position);
        }
        if (vendorName.length() > 1) {
            if (searchAbledataList.getData().get(position).getCategory().equalsIgnoreCase(vendorName)) {
                holder.name.setText(searchAbledataList.getData().get(position).getBasic_info().getName());
                holder.vendorName.setText(searchAbledataList.getData().get(position).getBasic_info().getManufacturer());
                holder.tvShadePrice.setText("$" + Utils.getRoundedDoubleForMoney(searchAbledataList.getData().get(position).getPrice_related().getPrice()));
                Picasso.get().load(searchAbledataList.getData().get(position).getModel_files().getThumbnail()).placeholder(R.drawable.parsl_place_holder).into(holder.thumbnail);
            }
        } else {
            holder.name.setText(searchAbledataList.getData().get(position).getBasic_info().getName());
            holder.vendorName.setText(searchAbledataList.getData().get(position).getBasic_info().getManufacturer());
            holder.tvShadePrice.setText("$" + Utils.getRoundedDoubleForMoney(searchAbledataList.getData().get(position).getPrice_related().getPrice()));
            Picasso.get().load(searchAbledataList.getData().get(position).getModel_files().getThumbnail()).placeholder(R.drawable.parsl_place_holder).into(holder.thumbnail);
        }
    }

    @Override
    public int getItemCount() {
        return searchAbledataList.getData().size();
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        AdapterCategoriesData.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

}
