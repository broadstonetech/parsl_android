package io.parsl.apps.Adaptors;

import android.app.Activity;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;

import java.util.List;

import io.parsl.apps.ModelClass.ShoppingCartItem;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Util.Utils;

import static io.parsl.apps.Activities.ARActivities.GltfActivity.shoppingCartProductList;
import static io.parsl.apps.Activities.CartActivity.idList;
import static io.parsl.apps.Activities.CartActivity.rvDataInText;


public class TotalSubtalListAdapter extends RecyclerView.Adapter<TotalSubtalListAdapter.ViewHolder> {
    private Activity context;

    List<ShoppingCartItem> objList;
    int conter = 1;
    private String itemPrice;
    double totalPriceWithCommission;
    private double price;
    private AdapterItemTextData adapterTextData;


    public TotalSubtalListAdapter(Activity context, List<ShoppingCartItem> obj) {
        this.context = context;
        this.objList = obj;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.adaptor_total_sub_total, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemname.setText(objList.get(position).getObj().getBasic_info().getName());
        holder.itemprice.setText("$" + " " + Utils.getRoundedDoubleForMoney(objList.get(position).getObj().getPrice_related().getPrice()));
        holder.itemsize.setText("Large");
        holder.tv_quantity.setText(String.valueOf(objList.get(position).getQuantity()));


        if (objList.get(position).getDisCounts() > 0) {
        } else {
        }

    }


    @Override
    public int getItemCount() {
        return objList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView itemname, itemsize, tv_quantity, itemprice;
        ImageView cart_minus_img, cart_plus_img;
        ImageView imgDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            itemname = itemView.findViewById(R.id.tv_name);
            itemsize = itemView.findViewById(R.id.tv_size);
            tv_quantity = itemView.findViewById(R.id.tv_total);
            itemprice = itemView.findViewById(R.id.tv_rate);
            imgDelete = itemView.findViewById(R.id.imgDelete);

        }
    }


}

