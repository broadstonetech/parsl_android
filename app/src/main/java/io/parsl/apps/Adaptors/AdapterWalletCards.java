package io.parsl.apps.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.parsl.apps.ModelClass.ModelsDataClass;
import io.parsl.apps.R;

public class AdapterWalletCards extends RecyclerView.Adapter<AdapterWalletCards.MyViewHolder> {
    private static ClickListener clickListener;
    private LayoutInflater inflater;
    private Context c;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imgCard;

        public MyViewHolder(View view) {
            super(view);
            imgCard = view.findViewById(R.id.imgCard);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public AdapterWalletCards(Context c, ModelsDataClass data) {
        this.c = c;
        inflater = LayoutInflater.from(c);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = inflater.inflate(R.layout.adaptor_wallet_cards, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return 2;
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        AdapterWalletCards.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

}
