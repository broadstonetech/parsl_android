package io.parsl.apps.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.util.List;

import io.parsl.apps.ModelClass.CategoriesModelClass;
import io.parsl.apps.ModelClass.ModelsDataClass;
import io.parsl.apps.R;

public class CustomSpinnerAdapter extends BaseAdapter {
    Context context;
    CategoriesModelClass categoryNames;
    LayoutInflater inflter;

    public CustomSpinnerAdapter(Context applicationContext, CategoriesModelClass categoryNames) {
        this.context = applicationContext;
        this.categoryNames = categoryNames;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return categoryNames.getData().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_item, null);

        TextView names = view.findViewById(R.id.tvSpinneritem);
        names.setText(categoryNames.getData().get(i).getCategory_title());
        return view;
    }
}
