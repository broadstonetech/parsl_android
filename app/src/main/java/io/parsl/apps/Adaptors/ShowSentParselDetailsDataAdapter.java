package io.parsl.apps.Adaptors;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;

import io.parsl.apps.ModelClass.GetPArselDetailsModelClass;

import io.parsl.apps.R;

import com.squareup.picasso.Picasso;

public class ShowSentParselDetailsDataAdapter extends RecyclerView.Adapter<ShowSentParselDetailsDataAdapter.MyViewHolder> {
    private static ClickListener clickListener;
    private LayoutInflater inflater;
    GetPArselDetailsModelClass dataList;
    private int selectedModelPosition = -1;
    boolean isExtras = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView thumbnail;
        MaterialCardView cvMain;
        TextView tvProductName;

        public MyViewHolder(View view) {
            super(view);
            thumbnail = view.findViewById(R.id.iv_thumb_adapter_filters);
            cvMain = view.findViewById(R.id.cv_adaptor_data);
            tvProductName = view.findViewById(R.id.tvName);
        }


    }

    public ShowSentParselDetailsDataAdapter(Context c, GetPArselDetailsModelClass data) {
        this.dataList = data;
        inflater = LayoutInflater.from(c);
    }


    public int getSelectedModelPosition() {
        return selectedModelPosition;
    }

    public void setSelectedModelPosition(int selectedModelPosition) {
        this.selectedModelPosition = selectedModelPosition;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = inflater.inflate(R.layout.adaptor_redeem_data, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if (selectedModelPosition == position) {
            holder.cvMain.setChecked(true);
        }
        Picasso.get().load(dataList.getData().getParsl_models().get(position).getModel_files().getThumbnail()).placeholder(R.drawable.parsl_place_holder).into(holder.thumbnail);

        holder.tvProductName.setText(dataList.getData().getParsl_models().get(position).getBasic_info().getName());
        holder.cvMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(position, v);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.getData().getParsl_models().size();
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        ShowSentParselDetailsDataAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

}
