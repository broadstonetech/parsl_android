package io.parsl.apps.Adaptors;

import android.app.Activity;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import io.parsl.apps.ModelClass.ShoppingCartItem;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Util.Utils;

import java.util.List;


public class AdapterItemTextData extends RecyclerView.Adapter<AdapterItemTextData.ViewHolder> {
    private Activity context;

    List<ShoppingCartItem> objList;


    public AdapterItemTextData(Activity context, List<ShoppingCartItem> obj) {
        this.context = context;
        this.objList = obj;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.adaptor_text_data_items_layout, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemname.setText(objList.get(position).getObj().getBasic_info().getName());
        holder.tvQuantity.setText("X" + " " + String.valueOf(objList.get(position).getQuantity()));

        if (objList.get(position).getDisCounts() > 0) {
            holder.itemprice.setText("$" + " " + Utils.getRoundedDoubleForMoney(objList.get(position).getDisCounts()));
        } else {
            holder.itemprice.setText("$" + " " + Utils.getRoundedDoubleForMoney(objList.get(position).getObj().getPrice_related().getPrice()));
        }
    }


    @Override
    public int getItemCount() {
        return objList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView itemprice, itemname, tvQuantity;


        public ViewHolder(View itemView) {
            super(itemView);
            itemname = itemView.findViewById(R.id.tvItemName);
            tvQuantity = itemView.findViewById(R.id.tvItemQuantity);
            itemprice = itemView.findViewById(R.id.tvItemPrice);

        }
    }


}

