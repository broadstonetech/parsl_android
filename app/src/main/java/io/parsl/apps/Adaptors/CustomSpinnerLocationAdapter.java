package io.parsl.apps.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import io.parsl.apps.ModelClass.CategoriesModelClass;
import io.parsl.apps.ModelClass.CategoryVendorsClass;
import io.parsl.apps.R;

public class CustomSpinnerLocationAdapter extends BaseAdapter {
    Context context;
    List<String> cityNames;
    LayoutInflater inflter;
    CategoryVendorsClass mObj;
    boolean isFetchVendors = false;

    public CustomSpinnerLocationAdapter(Context applicationContext, List<String> cities) {
        this.context = applicationContext;
        this.cityNames = cities;
        inflter = (LayoutInflater.from(applicationContext));
    }

    public CustomSpinnerLocationAdapter(Context applicationContext, CategoryVendorsClass obj, boolean vendors) {
        this.context = applicationContext;
        this.mObj = obj;
        this.isFetchVendors = vendors;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        if (isFetchVendors) {
            return mObj.getData().getVendors_list().size();
        } else
            return cityNames.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_item, null);
        TextView names = view.findViewById(R.id.tvSpinneritem);
        if (!isFetchVendors) {
            names.setText(cityNames.get(i));
        } else {
            names.setText(mObj.getData().getVendors_list().get(i).getVendor_name());
        }
        return view;
    }
}
