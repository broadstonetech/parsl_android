package io.parsl.apps.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.security.PrivateKey;
import java.util.List;

import io.parsl.apps.Activities.Wallet.PARSLWalletActivity;
import io.parsl.apps.ModelClass.GetUserCardsModelClass;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Util.Utils;

public class AdapterCreditCard extends RecyclerView.Adapter<AdapterCreditCard.cardViewHolder> {
    List<GetUserCardsModelClass.Data.Cards> cardList;
    private static AdapterAvatarsData.ClickListener clickListener;
    Context context;
    int pos = -1;

    public AdapterCreditCard(Context con, List<GetUserCardsModelClass.Data.Cards> objList) {
        this.cardList = objList;
        this.context = con;
    }

    @NonNull
    @Override
    public cardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.adapter_credit_card, parent, false);
        return new cardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull cardViewHolder holder, int position) {
        String amount = cardList.get(position).getBalance();
        holder.creditCardAmount.setText("$" + Utils.getRoundedDoubleForMoney(Double.parseDouble(amount)));
        if (pos == position) {
            holder.cardBackGround.setBackground(ContextCompat.getDrawable(context, R.drawable.crdit_card_curve_corner));
        } else {
            holder.cardBackGround.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }
        holder.cardBackGround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = holder.getAdapterPosition();
                clickListener.onItemClick(holder.getAdapterPosition(), holder.cardBackGround);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }

    public void setOnItemClickListener(AdapterAvatarsData.ClickListener clickListener) {
        AdapterCreditCard.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    public class cardViewHolder extends RecyclerView.ViewHolder {
        TextView creditCardAmount;
        ConstraintLayout cardBackGround;

        public cardViewHolder(@NonNull View itemView) {
            super(itemView);
            creditCardAmount = itemView.findViewById(R.id.credit_card_amount);
            cardBackGround = itemView.findViewById(R.id.cardBackGround);
        }
    }
}
