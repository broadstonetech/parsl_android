package io.parsl.apps.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.squareup.picasso.Picasso;

import io.parsl.apps.ModelClass.ModelsDataClass;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Util.Utils;

public class AdapterExtrasModelsData extends RecyclerView.Adapter<AdapterExtrasModelsData.MyViewHolder> {
    private static ClickListener clickListener;
    private LayoutInflater inflater;
    private Context c;
    ModelsDataClass dataList;
    private int selectedModelPosition = -1;
    boolean isExtras = false;
    int animations = 0;
    String vendorName = "";

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name, vendorName, tvShadePrice;
        ImageView thumbnail;
        MaterialCardView cvMain;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.tvName);
            vendorName = view.findViewById(R.id.tvVendor);
            thumbnail = view.findViewById(R.id.iv_thumb_adapter_filters);
            tvShadePrice = view.findViewById(R.id.tvShadePrice);
            cvMain = view.findViewById(R.id.cv_adaptor_data);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public AdapterExtrasModelsData(Context c, ModelsDataClass data, String vendor) {
        this.c = c;
        this.dataList = data;
        this.vendorName = vendor;
        inflater = LayoutInflater.from(c);
    }

    public AdapterExtrasModelsData(Context c, ModelsDataClass data) {
        this.c = c;
        this.dataList = data;
        inflater = LayoutInflater.from(c);
    }

    public AdapterExtrasModelsData(Context c, ModelsDataClass data, boolean extras) {
        this.c = c;
        this.dataList = data;
        this.isExtras = extras;
        inflater = LayoutInflater.from(c);
    }

    public AdapterExtrasModelsData(Context c, ModelsDataClass data, int animations) {
        this.c = c;
        this.dataList = data;
        this.animations = animations;
        inflater = LayoutInflater.from(c);
    }

    public int getSelectedModelPosition() {
        return selectedModelPosition;
    }

    public void setSelectedModelPosition(int selectedModelPosition) {
        this.selectedModelPosition = selectedModelPosition;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = inflater.inflate(R.layout.adaptor_extras_models, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (!isExtras) {
            if (selectedModelPosition == position) {
                holder.cvMain.setChecked(true);
            }
        } else if (!isExtras && animations == 0) {
            holder.cvMain.setChecked(selectedModelPosition == position);
        } else if (animations == 1) {
            holder.cvMain.setChecked(selectedModelPosition == position);
        } else {
            holder.cvMain.setChecked(selectedModelPosition == position);
        }
        if (vendorName.length() > 1) {
            if (dataList.getData().get(position).getCategory().equalsIgnoreCase(vendorName)) {
                holder.name.setText(dataList.getData().get(position).getBasic_info().getName());
                holder.vendorName.setText(dataList.getData().get(position).getBasic_info().getManufacturer());
                holder.tvShadePrice.setText("$" + Utils.getRoundedDoubleForMoney(dataList.getData().get(position).getPrice_related().getPrice()));
                Picasso.get().load(dataList.getData().get(position).getModel_files().getThumbnail()).placeholder(R.drawable.parsl_place_holder).into(holder.thumbnail);
            }
        } else if (!isExtras && animations == 0) {
            Picasso.get().load(dataList.getAvatars().get(position).getChracter_thumbnail()).placeholder(R.drawable.parsl_place_holder).into(holder.thumbnail);
            holder.name.setVisibility(View.GONE);
        } else if (animations == 1) {
            Picasso.get().load(dataList.getAnimations().get(position).getAnimation_thumbnail()).placeholder(R.drawable.parsl_place_holder).into(holder.thumbnail);
            holder.name.setVisibility(View.GONE);
        } else {
            holder.name.setText(dataList.getData().get(position).getBasic_info().getName());
            holder.vendorName.setText(dataList.getData().get(position).getBasic_info().getManufacturer());
            holder.tvShadePrice.setText("$" + Utils.getRoundedDoubleForMoney(dataList.getData().get(position).getPrice_related().getPrice()));
            Picasso.get().load(dataList.getData().get(position).getModel_files().getThumbnail()).placeholder(R.drawable.parsl_place_holder).into(holder.thumbnail);
        }
    }

    @Override
    public int getItemCount() {
        if (!isExtras && animations > 0) {
            return dataList.getAvatars().size();
        } else if (animations == 1) {
            return dataList.getAnimations().size();
        } else
            return dataList.getData().size();
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        AdapterExtrasModelsData.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

}
