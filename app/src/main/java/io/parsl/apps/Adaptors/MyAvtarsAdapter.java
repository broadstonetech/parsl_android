package io.parsl.apps.Adaptors;

import static io.parsl.apps.Activities.CartActivity.commissioned;
import static io.parsl.apps.Activities.CartActivity.fragmentLogin;
import static io.parsl.apps.Activities.CartActivity.idList;
import static io.parsl.apps.Activities.CartActivity.rvDataInText;
import static io.parsl.apps.Network.Parser.AvatarDetailModelParser.avatarDetailsDataObj;
import static io.parsl.apps.Network.Parser.EditAvatarModelClassDataParser.editAvatarDataObj;
import static io.parsl.apps.Utilities.Util.Utils.convertDate;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import io.parsl.apps.Activities.BottomSheetFragment.BottomSheetEditAvatar;
import io.parsl.apps.Activities.BottomSheetFragment.BottomSheetLogin;
import io.parsl.apps.Activities.CartActivity;
import io.parsl.apps.Activities.ReadyPlayerMe.MyAvatarsActivity;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.ModelClass.AvatarDetailsClass;
import io.parsl.apps.ModelClass.ShoppingCartItem;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;


public class MyAvtarsAdapter extends RecyclerView.Adapter<MyAvtarsAdapter.ViewHolder> {
    private Activity context;
    private static ClickListener clickListener;
    AvatarDetailsClass objList;
    private BottomSheetEditAvatar fragmentEditAvatar;
    public static String avatarID;
    public static int avatarPos;

    public MyAvtarsAdapter(Activity context, AvatarDetailsClass obj) {
        this.context = context;
        this.objList = obj;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.adaptor_my_avtars, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.itemname.setText(objList.getData().get(position).getTitle());
        holder.tvStatus.setText(objList.getData().get(position).getStatus());
        holder.tvDescription.setText(objList.getData().get(position).getDescription());

        holder.tvDate.setText(objList.getData().get(position).getStatus());

        holder.card_myevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(holder.getAdapterPosition(), v);
            }
        });

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                avatarPos = position;
                avatarID = objList.getData().get(position).getAvatar_id();
                fragmentEditAvatar = new BottomSheetEditAvatar();
                fragmentEditAvatar.show(((MyAvatarsActivity) context).getSupportFragmentManager(), "");
            }
        });

        holder.imgDelet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                avatarPos = position;
                avatarID = objList.getData().get(position).getAvatar_id();
                deleteAvatarDialog();
            }
        });
    }


    @Override
    public int getItemCount() {
        return objList.getData().size();
    }


    private JSONObject getParams() {
        JSONObject data = new JSONObject();
        try {
            data.put("avatar_id", avatarID);
            data.put("namespace", UserPrefs.getNameSpace(context));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;
    }

    AsyncTaskListener deletAvatarListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            ((MyAvatarsActivity) context).dismissProgressDialog();
            if (editAvatarDataObj.isSuccess()) {
                avatarDetailsDataObj.getData().remove(avatarPos);
                ((MyAvatarsActivity) context).updateUI();
            } else {
                Utils.showCenteredToastmessage(context, "Please Try Again!!!");
            }
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDate, itemname, tvDescription, tvStatus;
        ImageView imgThumbnail, imgEdit, imgDelet;
        MaterialCardView card_myevent;

        public ViewHolder(View itemView) {
            super(itemView);
            itemname = itemView.findViewById(R.id.tv_name);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            card_myevent = itemView.findViewById(R.id.card_myevent);
            imgEdit = itemView.findViewById(R.id.imgEdit);
            imgDelet = itemView.findViewById(R.id.imgDelet);

        }

    }

    public void setOnItemClickListener(ClickListener clickListener) {
        MyAvtarsAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    private void deleteAvatarDialog() {
        AlertDialog.Builder builderAddInfo = new AlertDialog.Builder(context);
        LayoutInflater inflater = context.getLayoutInflater();
        View dialogViewAddInfo = inflater.inflate(R.layout.clear_screen_dialog, null);
        builderAddInfo.setView(dialogViewAddInfo);
        AlertDialog dialogAddInfo = builderAddInfo.create();
        Objects.requireNonNull(dialogAddInfo.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogAddInfo.show();
        dialogAddInfo.setCancelable(true);


        TextView btnYes = dialogViewAddInfo.findViewById(R.id.btnYes);
        TextView btnNo = dialogViewAddInfo.findViewById(R.id.btnNo);
        TextView tvMessage = dialogViewAddInfo.findViewById(R.id.tvMessage);


        tvMessage.setText("Are you sure you want to delete?");
        btnYes.setText("Confirm");
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAddInfo.dismiss();
            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MyAvatarsActivity) context).showProgressDialog(context);
                Api.deleteAvatar(context, getParams(), deletAvatarListner);
                dialogAddInfo.dismiss();
            }
        });

    }
}

