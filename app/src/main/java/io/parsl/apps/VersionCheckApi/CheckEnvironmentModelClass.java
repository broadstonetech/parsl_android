package io.parsl.apps.VersionCheckApi;

public class CheckEnvironmentModelClass {

    /**
     * message : Success.
     * data : {"environment":"testflight"}
     */

    private String message;
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        /**
         * environment : testflight
         */

        private String environment;

        public String getEnvironment() {
            return environment;
        }

        public void setEnvironment(String environment) {
            this.environment = environment;
        }
    }
}
