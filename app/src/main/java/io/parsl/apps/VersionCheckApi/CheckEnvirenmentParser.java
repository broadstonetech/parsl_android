package io.parsl.apps.VersionCheckApi;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.BaseParser;


public class CheckEnvirenmentParser implements BaseParser {
    public static CheckEnvironmentModelClass environemntObj = new CheckEnvironmentModelClass();
    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        result.code = httpCode;
        try {
            JSONObject jsonResponse = new JSONObject(response);
            result.code = httpCode;
            if (httpCode == 200 || httpCode == 2) {
                result.setData(jsonResponse);
                result.success(true);

                Gson gson = new Gson();
                environemntObj =  gson.fromJson(String.valueOf(jsonResponse), CheckEnvironmentModelClass.class);
            }else if (httpCode == 400){
                Gson gson = new Gson();
            }
        }catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}
