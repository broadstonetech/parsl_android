package io.parsl.apps.VersionCheckApi;


import static io.parsl.apps.VersionCheckApi.CheckEnvirenmentParser.environemntObj;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import io.parsl.apps.Network.API.HttpAsyncRequest;
import io.parsl.apps.Network.AsyncTaskListener;


public class VersionCheck {

    private static final String url = "https://api.parsl.io/check/version";
    private static final String urlCheckEnvironment = "https://api.parsl.io/change/environment";
    private static PackageInfo pInfo;

    public static void getVersionCheck(Context context, AsyncTaskListener callback) {
        versionCheck(context, getParams(context), callback);
    }


    public static void versionCheck(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, url,
                HttpAsyncRequest.RequestType.JSONDATA, new VersionCheckParser(), callback);
        req.setJsonData(data);
        req.execute();
    }


    private static JSONObject getParams(Context con) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            pInfo = con.getPackageManager().getPackageInfo(con.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        try {
            data.put("app_id", "parsl");
            data.put("platform", "android");
            data.put("version_no", pInfo.versionName);
            data.put("environment", environemntObj.getData().getEnvironment());
            data.put("build_no", pInfo.versionCode);
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }
}
