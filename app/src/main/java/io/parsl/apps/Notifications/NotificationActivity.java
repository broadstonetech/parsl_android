package io.parsl.apps.Notifications;

import static io.parsl.apps.Network.Parser.NotificationClassParser.notiObjList;

import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;


import org.json.JSONException;
import org.json.JSONObject;

import io.parsl.apps.BaseActivity;
import io.parsl.apps.ModelClass.NotificationsClass;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;

public class NotificationActivity extends BaseActivity {
    RecyclerView rvNotifications;
    public static NotificationsClass dataObj;
    TextView errorText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rvNotifications = findViewById(R.id.rvNotifications);
        errorText = findViewById(R.id.errorText);


        showProgressDialog(this);
        Api.getNotifications(this, getParms(), notificationListner);

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_notification;
    }

    public JSONObject getParms() {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            js.put("device_id", Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));
            js.put("email", UserPrefs.getEmail(NotificationActivity.this));
            js.put("phone_no", UserPrefs.getPhone(NotificationActivity.this));
            data.put("data", js);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;
    }

    private AsyncTaskListener notificationListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();


            if (result.code == 200 || result.code == 2) {
                if (notiObjList.getData() != null || notiObjList.getData().size() > 0) {
                    errorText.setVisibility(View.VISIBLE);
                } else {
                    StaggeredGridLayoutManager sglm = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                    rvNotifications.setLayoutManager(sglm);
                    NotificationAdaptor notiAdaptor = new NotificationAdaptor(NotificationActivity.this, notiObjList);
                    rvNotifications.setAdapter(notiAdaptor);

                    notiAdaptor.setOnItemClickListener(new NotificationAdaptor.ClickListener() {
                        @Override
                        public void onItemClick(int position, View v) {
                        }
                    });
                    errorText.setVisibility(View.GONE);
                }

            } else {
                showCenteredToastmessage(NotificationActivity.this, result.message);
            }

        }
    };
}
