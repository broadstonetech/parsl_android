package io.parsl.apps.Notifications;

import static io.parsl.apps.Utilities.Util.Utils.convertDate;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.parsl.apps.ModelClass.NotificationsClass;
import io.parsl.apps.R;

public class NotificationAdaptor extends RecyclerView.Adapter<NotificationAdaptor.MyViewHolder> {
    private static ClickListener clickListener;
    private NotificationsClass metaDataobjList;
    private LayoutInflater inflater;
    private Context c;
    de.hdodenhof.circleimageview.CircleImageView btnProject;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle, tvNotiDate, tvNotiDetail;

        public MyViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            tvTitle = view.findViewById(R.id.tvNotiTitle);
            tvNotiDetail = view.findViewById(R.id.tvNotiDetail);
            tvNotiDate = view.findViewById(R.id.tvNotiDate);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public NotificationAdaptor(Context c, NotificationsClass objList) {
        this.c = c;
        this.metaDataobjList = objList;
        inflater = LayoutInflater.from(c);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = inflater.inflate(R.layout.adaptor_notification_layout, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        String txtTitle = metaDataobjList.getData().get(position).getNot_title() + " " + metaDataobjList.getData().get(position).getNotification_reciever_name();
        String detail = metaDataobjList.getData().get(position).getNot_message();
        holder.tvTitle.setText(txtTitle);
        holder.tvNotiDetail.setText(detail);
        String time = convertDate(metaDataobjList.getData().get(position).getSent_time());
        holder.tvNotiDate.setText(convertDate(metaDataobjList.getData().get(position).getSent_time()));

    }

    @Override
    public int getItemCount() {
        return metaDataobjList.getData().size();
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        NotificationAdaptor.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }


}