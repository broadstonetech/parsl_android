package io.parsl.apps.AgentChatActivity.model;

/*
 * Created by troy379 on 04.04.17.
 */
public class MessageFromFirebase  /*and this one is for custom content type (in this case - voice message)*/ {


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSenderName() {
        return sender_name;
    }

    public void setSenderName(String senderName) {
        this.sender_name = senderName;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    private String sender_id;
    private String content;
    private String sender_name;
    private Long created;


}
