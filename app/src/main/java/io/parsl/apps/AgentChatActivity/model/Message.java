package io.parsl.apps.AgentChatActivity.model;

import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.MessageContentType;

import java.util.Date;

public class Message implements IMessage,
        MessageContentType {

    private String id;
    private String text;
    private Date createdAt;
    private User user;
    private String dateTime;

    public Message(User user, String text) {
        this(user, text, new Date());
    }

    public Message(User user, String text, Long milis) {
        this(user, text, new Date(milis * 1000L));
    }

    public Message(String id, User user, String text, Date createdAt) {
        this.id = id;
        this.text = text;
        this.user = user;
        this.createdAt = createdAt;
    }

    public Message(User user, String text, Date createdAt) {
        this.text = text;
        this.user = user;
        this.createdAt = createdAt;


    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public User getUser() {
        return this.user;
    }


    public String getStatus() {
        return "Sent";
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
