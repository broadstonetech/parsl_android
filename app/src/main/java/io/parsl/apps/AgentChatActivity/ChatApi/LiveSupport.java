package io.parsl.apps.AgentChatActivity.ChatApi;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;


import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.HttpAsyncRequest;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.Utilities.Preferences.UserPrefs;


public class LiveSupport {

    private static final String url = Api.BASE_URL + "/start/chat";
    private static final String urlEndChat = Api.BASE_URL + "/save/chat";
    private static final String URL_PUSH_USER_MSG = Api.BASE_URL + "/push/user/msg";
    private static final String URL_GET_USER_MSG = Api.BASE_URL + "/get/user/msgs";
    private static PackageInfo pInfo;


    public static void startChat(Context context, String creater, String name, String email,
                                 String nameSpace, AsyncTaskListener callback) {

        startChat(context, getParams(context, creater, name, email, nameSpace), callback);
    }

    public static void endChat(Context context, String id, AsyncTaskListener callback) {

        endChat(context, endChatParms(id), callback);
    }


    public static void getUserChat(Context context, String id, AsyncTaskListener callback) {

        getUserMsg(context, getUserMessageParams(id), callback);
    }

    public static void pushChat(Context context, String id, String msg, String nameSpace, String email, AsyncTaskListener callback) {

        pushChatMsg(context, pushMsgParams(id, msg, nameSpace, email), callback);
    }

    private static void startChat(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, url,
                HttpAsyncRequest.RequestType.JSONDATA, new StartChatParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    private static void endChat(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, urlEndChat,
                HttpAsyncRequest.RequestType.JSONDATA, new EndChatParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    private static void pushChatMsg(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, URL_PUSH_USER_MSG,
                HttpAsyncRequest.RequestType.JSONDATA, new PushUserChatParser(), callback);
        req.setJsonData(data);
        req.execute();
    }


    private static void getUserMsg(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, URL_GET_USER_MSG,
                HttpAsyncRequest.RequestType.JSONDATA, new GetUserMessagesParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    private static JSONObject getParams(Context con, String creater, String name, String email,
                                        String nameSpace) {

        try {
            pInfo = con.getPackageManager().getPackageInfo(con.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String type;
        if (isTablet(con)) {
            type = "tablet";
        } else {
            type = "mobile";
        }

        JSONObject creater_device_info = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            data.put("app_id", "parsl");
            data.put("creator", creater);
            data.put("creator_name", name);
            data.put("phone", "");
            data.put("creator_email", email);
            data.put("creator_namespace", nameSpace);

            creater_device_info.put("device_version", pInfo.versionName);
            creater_device_info.put("device_version", pInfo.versionName);
            creater_device_info.put("device_os", "Android");
            creater_device_info.put("device_name", Build.MODEL);
            creater_device_info.put("device_manufacturar", Build.MANUFACTURER);
            creater_device_info.put("device_type", type);

            data.put("creator_device_token", UserPrefs.getFCMUserToken(con));
            data.put("creater_device_info", creater_device_info);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;
    }

    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }

    private static JSONObject endChatParms(String documentID) {
        JSONObject js = new JSONObject();

        try {
            js.put("firebase_id", documentID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    private static JSONObject getUserMessageParams(String documentID) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            js.put("firebase_id", documentID);
            data.put("data", js);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;
    }

    private static JSONObject pushMsgParams(String documentID, String senderTxtMsg, String nameSpace, String email) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            js.put("sender_id", nameSpace);
            js.put("sender_name", email);
            js.put("firebase_id", documentID);
            js.put("sender_msg", senderTxtMsg);
            js.put("send_by", "client");
            js.put("msg_timestamp", System.currentTimeMillis() / 1000L);
            data.put("data", js);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }
}
