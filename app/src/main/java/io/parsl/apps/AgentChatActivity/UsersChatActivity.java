package io.parsl.apps.AgentChatActivity;


import static io.parsl.apps.Activities.ARActivities.GltfActivity.emailLiveSupport;
import static io.parsl.apps.AgentChatActivity.ChatApi.GetUserMessagesParser.getUserChatObj;
import static io.parsl.apps.AgentChatActivity.ChatApi.PushUserChatParser.pushUserMsgObj;
import static io.parsl.apps.AgentChatActivity.ChatApi.StartChatParser.startChatObj;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.parsl.apps.AgentChatActivity.ChatApi.GetUserMessagesModelCLass;
import io.parsl.apps.AgentChatActivity.ChatApi.LiveSupport;
import io.parsl.apps.AgentChatActivity.model.Message;
import io.parsl.apps.AgentChatActivity.model.User;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.R;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.Network.API.TaskResult;


@SuppressWarnings("ALL")
public class UsersChatActivity extends BaseActivity {

    MessagesListAdapter<Message> adapter;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    TextView tvEndChat;
    private MessageInput messageInput;
    private MessagesList messagesList;
    private FirebaseAuth mAuth;
    private String mCurrentUserId;
    private String agentID = "agent2479";
    private boolean isFromSendBtn = false;
    private String documentId = "";
    private ImageLoader imageLoader;
    private String msg;
    public static boolean isChatOpen = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        messageInput = findViewById(R.id.input);
        messagesList = findViewById(R.id.messagesList);
        tvEndChat = findViewById(R.id.tvEndChat);


        messageInput.setInputListener(input -> {
            msg = input.toString().trim();
            sendMessageTofireBase(input.toString().trim());
            return true;
        });


        tvEndChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endChatDialog();
            }
        });

        mAuth = FirebaseAuth.getInstance();
        mCurrentUserId = emailLiveSupport;

        imageLoader = (imageView, url, payload) -> Picasso.get().load(url).into(imageView);
        adapter = new MessagesListAdapter<>(mCurrentUserId, imageLoader);
        messagesList.setAdapter(adapter);

        loadMessages();

        final Handler ha = new Handler();
        ha.postDelayed(new Runnable() {

            @Override
            public void run() {
                loadFirebaseMessages();
                ha.postDelayed(this, 10000);
            }
        }, 10000);

    }

    @Override
    public int getLayoutId() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.activity_default_messages;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isChatOpen = true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isChatOpen = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isChatOpen = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isChatOpen = false;
    }

    private void endChatDialog() {
        AlertDialog.Builder optionDialog = new AlertDialog.Builder(UsersChatActivity.this);

        optionDialog.setTitle(getResources().getString(R.string.app_name));
        optionDialog.setMessage("Are you sure you want to exit?");

        // Specifying a listener allows you to take an action before dismissing the dialog.
        // The dialog is automatically dismissed when a dialog button is clicked.
        optionDialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                showProgressDialog(UsersChatActivity.this);
                LiveSupport.endChat(UsersChatActivity.this, documentId, endChatListner);
            }
        })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private void addMessages(List<Message> baseMessages) {
        adapter.addToEnd(baseMessages, true);
    }

    private void addMessage1(String textMessage) {
        adapter.addToStart(new Message(getMessageUser(), textMessage), true);
    }

    private User getMessageUser() {
        return new User(emailLiveSupport, emailLiveSupport, true);
    }

    private static User getUser(GetUserMessagesModelCLass.Data.Chat.Agent obj) {
        return new User(obj.getSender_id(), obj.getSender_name(), true);
    }

    private void sendMessageTofireBase(String message) {
        LiveSupport.pushChat(UsersChatActivity.this, documentId, message, emailLiveSupport, emailLiveSupport, pushMsglistner);

    }

    private void loadFirebaseMessages() {
        LiveSupport.getUserChat(UsersChatActivity.this, documentId, getUserChatListner);
    }

    private void checkAndInsertUserinFireBase() {

        showProgressDialog(UsersChatActivity.this);

        loadMessages();
    }

    private void loadMessages() {
        LiveSupport.startChat(UsersChatActivity.this, emailLiveSupport, emailLiveSupport
                , emailLiveSupport, emailLiveSupport, startChatListner);
    }

    final AsyncTaskListener startChatListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if (startChatObj.isStatus()) {
                agentID = startChatObj.getData().getReciever_info().getReciever_id();
                documentId = startChatObj.getData().getFirebase_id();
                loadFirebaseMessages();
            } else {
                Toast.makeText(UsersChatActivity.this, "All support officer are busy please try again after sometime !", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    };

    final AsyncTaskListener endChatListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            finish();
        }
    };

    AsyncTaskListener getUserChatListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (getUserChatObj.isStatus()) {
                adapter.clear();
                adapter.notifyDataSetChanged();
                List<GetUserMessagesModelCLass.Data.Chat.Agent> chatList = new ArrayList<>();
                chatList.clear();
                chatList.addAll(getUserChatObj.getData().getChat().getAgent());
                chatList.addAll(getUserChatObj.getData().getChat().getClient());
                Collections.sort(chatList, new Comparator<GetUserMessagesModelCLass.Data.Chat.Agent>() {
                    public int compare(GetUserMessagesModelCLass.Data.Chat.Agent obj1, GetUserMessagesModelCLass.Data.Chat.Agent obj2) {
                        return obj1.getCreated().compareTo(obj2.getCreated());
                    }
                });


                List<Message> messageWrappers = new ArrayList<>();

                messageWrappers.clear();
                for (GetUserMessagesModelCLass.Data.Chat.Agent message : chatList) {
                    if (message.getCreated() != 0L)
                        messageWrappers.add(new Message(getUser(message), message.getContent(), message.getCreated()));

                }
                addMessages(messageWrappers);
            } else {
                Toast.makeText(UsersChatActivity.this, "Tesingt", Toast.LENGTH_LONG).show();
            }
        }
    };

    AsyncTaskListener pushMsglistner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (result.code == 200 || result.code == 2) {
                if (pushUserMsgObj.isStatus()) {
                    addMessage1(msg);
                    Toast.makeText(UsersChatActivity.this, "Message sent", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(UsersChatActivity.this, "Message not sent", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(UsersChatActivity.this, "Something went wrong Please try again !", Toast.LENGTH_LONG).show();
            }
        }
    };

}
