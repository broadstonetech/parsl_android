package io.parsl.apps.AgentChatActivity.ChatApi;

import java.util.List;

public class GetUserMessagesModelCLass {

    /**
     * message : Success
     * data : {"chat":{"client":[],"agent":[{"created":1624551257,"sender_name":"support@api.vesgo.io","sender_id":"agent2499","content":"Hello, How may i help you?"},{"sender_id":"agent2499","content":"1st time","created":1624633829,"sender_name":"support@api.vesgo.io"},{"created":1624633829,"sender_name":"support@api.vesgo.io","sender_id":"agent2499","content":"2nd time"}]}}
     * status : true
     */

    private String message;
    private Data data;
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        /**
         * chat : {"client":[],"agent":[{"created":1624551257,"sender_name":"support@api.vesgo.io","sender_id":"agent2499","content":"Hello, How may i help you?"},{"sender_id":"agent2499","content":"1st time","created":1624633829,"sender_name":"support@api.vesgo.io"},{"created":1624633829,"sender_name":"support@api.vesgo.io","sender_id":"agent2499","content":"2nd time"}]}
         */

        private Chat chat;

        public Chat getChat() {
            return chat;
        }

        public void setChat(Chat chat) {
            this.chat = chat;
        }

        public static class Chat {
            private List<Agent> client;
            private List<Agent> agent;

            public List<Agent> getClient() {
                return client;
            }

            public void setClient(List<Agent> client) {
                this.client = client;
            }

            public List<Agent> getAgent() {
                return agent;
            }

            public void setAgent(List<Agent> agent) {
                this.agent = agent;
            }

            public static class Agent {
                /**
                 * created : 1624551257
                 * sender_name : support@api.vesgo.io
                 * sender_id : agent2499
                 * content : Hello, How may i help you?
                 */

                private Long created;
                private String sender_name;
                private String sender_id;
                private String content;

                public Long getCreated() {
                    return created;
                }

                public void setCreated(Long created) {
                    this.created = created;
                }

                public String getSender_name() {
                    return sender_name;
                }

                public void setSender_name(String sender_name) {
                    this.sender_name = sender_name;
                }

                public String getSender_id() {
                    return sender_id;
                }

                public void setSender_id(String sender_id) {
                    this.sender_id = sender_id;
                }

                public String getContent() {
                    return content;
                }

                public void setContent(String content) {
                    this.content = content;
                }
            }
        }
    }
}
