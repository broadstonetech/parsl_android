package io.parsl.apps.AgentChatActivity.ChatApi;

public class StartChatModelClass {

    /**
     * message : Success
     * data : {"reciever_info":{"reciever_name":"support@api.vesgo.io","reciever_id":"agent2499"},"firebase_id":"z3i2CJGTbabbPqaFOqpo"}
     * status : true
     */

    private String message;
    private Data data;
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        /**
         * reciever_info : {"reciever_name":"support@api.vesgo.io","reciever_id":"agent2499"}
         * firebase_id : z3i2CJGTbabbPqaFOqpo
         */

        private RecieverInfo reciever_info;
        private String firebase_id;

        public RecieverInfo getReciever_info() {
            return reciever_info;
        }

        public void setReciever_info(RecieverInfo reciever_info) {
            this.reciever_info = reciever_info;
        }

        public String getFirebase_id() {
            return firebase_id;
        }

        public void setFirebase_id(String firebase_id) {
            this.firebase_id = firebase_id;
        }

        public static class RecieverInfo {
            /**
             * reciever_name : support@api.vesgo.io
             * reciever_id : agent2499
             */

            private String reciever_name;
            private String reciever_id;

            public String getReciever_name() {
                return reciever_name;
            }

            public void setReciever_name(String reciever_name) {
                this.reciever_name = reciever_name;
            }

            public String getReciever_id() {
                return reciever_id;
            }

            public void setReciever_id(String reciever_id) {
                this.reciever_id = reciever_id;
            }
        }
    }
}
