package io.parsl.apps.AgentChatActivity.ChatApi;

public class PushChaModelClass {

    /**
     * message : Success
     * data : {}
     * status : true
     */

    private String message;
    private Data data;
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
    }
}
