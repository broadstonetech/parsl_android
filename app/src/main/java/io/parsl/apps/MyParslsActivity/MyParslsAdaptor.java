package io.parsl.apps.MyParslsActivity;

import static io.parsl.apps.Utilities.Util.Utils.convertDate;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;

import io.parsl.apps.ModelClass.GetMyParslsModelClass;
import io.parsl.apps.ModelClass.NotificationsClass;
import io.parsl.apps.R;

public class MyParslsAdaptor extends RecyclerView.Adapter<MyParslsAdaptor.MyViewHolder> {
    private static ClickListener clickListener;
    private GetMyParslsModelClass metaDataobjList;
    private LayoutInflater inflater;
    private Context c;
    de.hdodenhof.circleimageview.CircleImageView btnProject;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvNotiDate, tvNotiDetail, tvStatus;
        MaterialCardView cvMyParsl;

        public MyViewHolder(View view) {
            super(view);

            tvTitle = view.findViewById(R.id.tvNotiTitle);
            tvNotiDetail = view.findViewById(R.id.tvNotiDetail);
            tvNotiDate = view.findViewById(R.id.tvNotiDate);
            tvStatus = view.findViewById(R.id.tvStatus);
            cvMyParsl = view.findViewById(R.id.cvMyParsl);
        }

    }

    public MyParslsAdaptor(Context c, GetMyParslsModelClass objList) {
        this.c = c;
        this.metaDataobjList = objList;
        inflater = LayoutInflater.from(c);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = inflater.inflate(R.layout.my_parsl_adaptor_layout, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        String txtTitle = metaDataobjList.getData().get(position).getSender_name() + " " + "sent you a PARSL" + " " + metaDataobjList.getData().get(position).getParsl_otp();
        holder.tvTitle.setText(txtTitle);

        holder.tvNotiDate.setText(convertDate(metaDataobjList.getData().get(position).getTimestamp()));
        if (metaDataobjList.getData().get(position).isRedeemed_status()) {
            holder.tvStatus.setText("Redeemed");
            holder.tvStatus.setTypeface(Typeface.DEFAULT_BOLD);
            ;
            holder.tvStatus.setTextColor(ResourcesCompat.getColor(c.getResources(), R.color.colorPrimary, null));
        } else {
            holder.tvStatus.setText("Not redeemed");
        }


        holder.cvMyParsl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(holder.getAdapterPosition(), v);
            }
        });
    }

    @Override
    public int getItemCount() {
        return metaDataobjList.getData().size();
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        MyParslsAdaptor.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }


}