package io.parsl.apps.MyParslsActivity;

import static io.parsl.apps.Activities.ARActivities.GltfActivity.url;
import static io.parsl.apps.Network.Parser.GetMyParslsModelClassDataParser.getMyParslsListDataObj;
import static io.parsl.apps.Network.Parser.NotificationClassParser.notiObjList;

import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import org.json.JSONException;
import org.json.JSONObject;

import io.parsl.apps.Activities.ARActivities.GltfActivity;
import io.parsl.apps.Activities.ARActivities.ProfileActivity;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.ModelClass.NotificationsClass;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;

public class MyParslsActivity extends BaseActivity {
    RecyclerView rvMyParsls;
    public static boolean isFromMyParsl = false;
    TextView errorText, tvDetails, tvTapToview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rvMyParsls = findViewById(R.id.rvNotifications);
        errorText = findViewById(R.id.errorText);
        tvDetails = findViewById(R.id.tvDetails);
        tvTapToview = findViewById(R.id.tvTapToview);


        tvTapToview.setVisibility(View.VISIBLE);
        tvDetails.setText(getResources().getString(R.string.my_parsls));
        showProgressDialog(this);
        Api.getMyParsls(this, getParms(), myParslsListner);

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_notification;
    }

    @Override
    public void onBackPressed() {
        AddActivityAndRemoveIntentTransition(ProfileActivity.class);
    }

    public JSONObject getParms() {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            js.put("app_id", GeneralConstants.APP_ID);
            data.put("namespace", UserPrefs.getNameSpace(this));
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    private AsyncTaskListener myParslsListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();


            if (result.code == 200 || result.code == 2) {
                if (getMyParslsListDataObj.isStatus()) {
                    errorText.setVisibility(View.GONE);
                    StaggeredGridLayoutManager sglm = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                    rvMyParsls.setLayoutManager(sglm);
                    MyParslsAdaptor myParslsAdaptor = new MyParslsAdaptor(MyParslsActivity.this, getMyParslsListDataObj);
                    rvMyParsls.setAdapter(myParslsAdaptor);

                    myParslsAdaptor.setOnItemClickListener(new MyParslsAdaptor.ClickListener() {
                        @Override
                        public void onItemClick(int position, View v) {
                            Utils.showCenteredToastmessage(MyParslsActivity.this, "button call");
                            url = getMyParslsListDataObj.getData().get(position).getParsl_otp();
                            isFromMyParsl = true;
                            finish();
                        }
                    });
                } else {
                    errorText.setVisibility(View.VISIBLE);
                }
            } else {
                showCenteredToastmessage(MyParslsActivity.this, result.message);
            }

        }
    };
}
