package io.parsl.apps;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import static io.parsl.apps.Activities.ARActivities.GltfActivity.isVideo;

public class VideoActivity extends BaseActivity {

    VideoView videoPlayer;
    MediaController mediaC;
    Uri uri;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(isVideo){
             uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.send_parsl);
        }else {
             uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.redeem_parsl);
        }
        videoPlayer = findViewById(R.id.videoView);
        MediaController mediaController= new MediaController(this);
        mediaController.setAnchorView(videoPlayer);
        videoPlayer.setMediaController(mediaController);
        videoPlayer.setVideoURI(uri);
        videoPlayer.seekTo( 1 );
        videoPlayer.start();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_video;
    }
}