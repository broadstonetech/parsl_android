package io.parsl.apps.Activities.BottomSheetFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerListener;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.tabs.TabLayout;
import com.vikktorn.picker.City;
import com.vikktorn.picker.CityPicker;
import com.vikktorn.picker.Country;
import com.vikktorn.picker.CountryPicker;
import com.vikktorn.picker.OnCityPickerListener;
import com.vikktorn.picker.OnCountryPickerListener;
import com.vikktorn.picker.OnStatePickerListener;
import com.vikktorn.picker.State;
import com.vikktorn.picker.StatePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import io.parsl.apps.Activities.ARActivities.GltfActivity;
import io.parsl.apps.Adaptors.CustomSpinnerLocationAdapter;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.ModelClass.AllVendorListModelClass;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.Network.Parser.CategoriesModelParser;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Constants.GeneralConstants;

import static io.parsl.apps.Network.Parser.AllVendorListModelClassDataParser.allVendorListDataObj;

public class SearchBottomSheetFragment extends BottomSheetDialogFragment implements View.OnClickListener,MultiSpinnerListener, OnCountryPickerListener, OnStatePickerListener, OnCityPickerListener,AdapterView.OnItemSelectedListener {

    private Spinner spStates, spCity;
    MultiSpinnerSearch spVendors, spCategories;
    private String country;
    private CountryPicker countryPicker;
    private StatePicker statePicker;
    public   List<State> stateObject;
    Country countryObj;
    List<String> stateNames = new ArrayList<>();
    List<String> cityNames = new ArrayList<>();
    private double latitude,longitude;
    public  List<City> cityObject;
    HashMap<String,Integer> map = new HashMap<>();
    List<String> cityNamesList = new ArrayList<>();
    TextView tvVendors,tvApplySearch;
    TabLayout tabsCategories;
    LinearLayout ll_filter_activity_landing_screen,llLocation,llSpVendors;
    List<String> catlist = new ArrayList<>();
    List<String> vendorList = new ArrayList<>();
    HashMap<String,List<AllVendorListModelClass.Data.DataObj>> tempVendorList = new HashMap<>();
    HashMap<String,String> tempVendorListKeyWithID = new HashMap<>();
    CheckBox chkLocation;
    List<KeyPairBoolData> listArray2 = new ArrayList<>();
    List<String> keysList = new ArrayList<>();
    List<String> tempKeysList = new ArrayList<>();
    List<List<AllVendorListModelClass.Data.DataObj>> tempVendorsKeyList = new ArrayList<>();
    int isHaveLocation = 0;
    EditText etCountry,etStates,etCities,etZipCode;
    public int countryID;
    List<State> stateDataObj = new ArrayList<>();
    private CityPicker cityPicker;
    boolean isCity = false;
    List<KeyPairBoolData> listArray1;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment_bottom_sheet, container, false);

        spCategories = view.findViewById(R.id.spCategories);
        spStates = view.findViewById(R.id.spStates);
        spCity = view.findViewById(R.id.spCity);
        spVendors = view.findViewById(R.id.spVendors);
        tvVendors = view.findViewById(R.id.tvVendors);
        tvApplySearch = view.findViewById(R.id.tvApplySearch);
        tabsCategories = view.findViewById(R.id.tabCategories);
        RecyclerView rvCategoriesData = view.findViewById(R.id.rvCategoriesData);
        ll_filter_activity_landing_screen = view.findViewById(R.id.ll_filter_activity_landing_screen);
        llLocation = view.findViewById(R.id.llLocation);
        llLocation = view.findViewById(R.id.llLocation);
        llSpVendors = view.findViewById(R.id.llSpVendors);
        chkLocation = view.findViewById(R.id.chkLocation);
        etCountry = view.findViewById(R.id.etCountry);
        etStates = view.findViewById(R.id.etStates);
        etCities = view.findViewById(R.id.etCities);
        etZipCode = view.findViewById(R.id.etZipCode);

        stateObject = new ArrayList<>();
        cityObject = new ArrayList<>();

        countryPicker = new CountryPicker.Builder().with(getActivity()).listener(this::onSelectCountry).build();

        getAllVendorsList();

        try {
            getStateJson();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            getCityJson();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        tvApplySearch.setOnClickListener(this::onClick);
        etCountry.setOnClickListener(this::onClick);
        etStates.setOnClickListener(this::onClick);
        etCities.setOnClickListener(this::onClick);

        etCountry.setFocusable(false);
        tvVendors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getVendorsLocationBased();
            }
        });

        chkLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    isHaveLocation = 1;
                    llLocation.setVisibility(View.VISIBLE);
                }
                else {
                    isHaveLocation = 0;
                    llLocation.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {
       if(v.getId() == tvApplySearch.getId()){
           getSearchResult();
       }else if(v.getId() == etCountry.getId()){
           countryPicker.showDialog(getActivity().getSupportFragmentManager());
       }else if(v.getId() == etStates.getId()){
           if(!etStates.getText().toString().equalsIgnoreCase("")) {
               statePicker.showDialog(getActivity().getSupportFragmentManager());
           }else {
               ((BaseActivity)getActivity()).showCenteredToastmessage(getActivity(),"Please select country");
           }
       }else if(v.getId() == etCities.getId()){
           if(!etStates.getText().toString().equalsIgnoreCase("")) {
               cityPicker = new CityPicker.Builder().with(getActivity()).listener(this::onSelectCity).build();
               cityPicker.showDialog(getActivity().getSupportFragmentManager());
           }else {
               ((BaseActivity)getActivity()).showCenteredToastmessage(getActivity(),"Please select state");
           }
       }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        int stateID = stateDataObj.get(position).getStateId();
        getCityNames(stateID);
        CustomSpinnerLocationAdapter cityAdapter = new CustomSpinnerLocationAdapter(getActivity(), cityNamesList);
        spCity.setAdapter(cityAdapter);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    @Override
    public void onSelectCountry(Country country) {

        countryID = country.getCountryId();
        stateDataObj.clear();
        StatePicker.equalStateObject.clear();
        CityPicker.equalCityObject.clear();

        // GET STATES OF SELECTED COUNTRY
        etCountry.setText(country.getName());
        for(int i = 0; i < stateObject.size(); i++) {
            // init state picker
            statePicker = new StatePicker.Builder().with(getActivity()).listener(this::onSelectState).build();
            State stateData = new State();
            if (stateObject.get(i).getCountryId() == countryID) {

                stateData.setStateId(stateObject.get(i).getStateId());
                stateData.setStateName(stateObject.get(i).getStateName());
                stateData.setCountryId(stateObject.get(i).getCountryId());
                stateData.setFlag(country.getFlag());
                StatePicker.equalStateObject.add(stateData);

            }
        }
    }

    private void setStates(String country){
        stateNames.clear();
                for (int x = 0; x < countryPicker.getAllCountries().size(); x++) {
            if (countryPicker.getAllCountries().get(x).getName().equalsIgnoreCase(country)) {
                countryObj = countryPicker.getAllCountries().get(x);
            }
        }


        for (int i = 0; i < stateObject.size(); i++) {

            if (stateObject.get(i).getCountryId() == countryObj.getCountryId()) {
                stateNames.add(stateObject.get(i).getStateName());
                map.put(stateObject.get(i).getStateName(),stateObject.get(i).getStateId());
            }
        }
                CustomSpinnerLocationAdapter stateAdapter = new CustomSpinnerLocationAdapter(getActivity(), stateNames);
        spStates.setAdapter(stateAdapter);
        spStates.setOnItemSelectedListener(this);

    }

    private void setCategorySpinner(){
        spCategories.setSearchEnabled(true);

        spCategories.setSearchHint("Select categories");
        spCategories.setEmptyTitle("Not Data Found!");
        spCategories.setShowSelectAllButton(true);
        spCategories.setClearText("Close & Clear");


    }

    private void setVendorSpinner(List<KeyPairBoolData> items , List<AllVendorListModelClass.Data.DataObj> dataList,int position){
        spVendors.setVisibility(View.VISIBLE);
        spVendors.setSearchEnabled(true);
        spVendors.setSearchHint("Select vendors");
        spVendors.setEmptyTitle("Not Data Found!");
        spVendors.setShowSelectAllButton(true);
        spVendors.setClearText("Close & Clear");

        if(!tempVendorList.containsKey(items.get(position).getName())) {
            tempVendorList.put(items.get(position).getName(), dataList);

        }
        listArray2.clear();

        for (int i = 0; i < tempVendorList.size(); i++) {
            for(int x= 0; x< tempVendorList.get(items.get(i).getName()).size();x++) {
                KeyPairBoolData h = new KeyPairBoolData();
                h.setId(x);
                h.setName(tempVendorList.get(items.get(i).getName()).get(x).getVendor_name());
                h.setSelected(x == 0);

                tempVendorListKeyWithID.put(tempVendorList.get(items.get(i).getName()).get(x).getVendor_name(),
                        tempVendorList.get(items.get(i).getName()).get(x).getVendor_id());
                listArray2.add(h);
            }
        }

        spVendors.setItems(listArray2, new MultiSpinnerListener() {
            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).isSelected()) {
                    }
                }
                if(items.size() == 0){
                }
            }
        });
    }

    public static List<String> contains(HashMap<String, List<AllVendorListModelClass.Data.DataObj>> map, List<KeyPairBoolData> list) {

        Collection<String> values = map.keySet();
        List<String> result = new ArrayList<>();
        for (KeyPairBoolData listItem:list) {
            if (values.contains(listItem.getName())) {
                result.add(listItem.getName());
            }
        }
        return result;  // contains a collection of all list items that are values of the map
    }

    private void getVendorsLocationBased(){
        ((BaseActivity)getActivity()).showProgressDialog(getActivity());
        Api.getVendorsLocationBased(getActivity(),params(),locationBasedVendorsListner);
    }

    private JSONObject params(){
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            js.put("app_id", GeneralConstants.APP_ID);
            data.put("country", etCountry.getText());
            data.put("city", etCities.getText());
            data.put("state",etStates.getText());
            data.put("zip_code",etZipCode.getText());
            js.put("data",data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    AsyncTaskListener locationBasedVendorsListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            ((BaseActivity)getActivity()).dismissProgressDialog();

            if(allVendorListDataObj.isStatus() && (result.code == 200 || result.code == 2)) {
                tempVendorsKeyList.add(allVendorListDataObj.getData().getVendors_list().getFood());
                tempVendorsKeyList.add(allVendorListDataObj.getData().getVendors_list().getFashion());
                tempVendorsKeyList.add(allVendorListDataObj.getData().getVendors_list().getDecor());
                tempVendorsKeyList.add(allVendorListDataObj.getData().getVendors_list().getElectronics());
                tempVendorsKeyList.add(allVendorListDataObj.getData().getVendors_list().getSpecial_occasions());
                dismiss();
            }else if (!allVendorListDataObj.isStatus() && (result.code == 200 || result.code == 2)){

                ((BaseActivity)getActivity()).showCenteredToastmessage(getActivity(),allVendorListDataObj.getMessage());
            }else if(!allVendorListDataObj.isStatus() && (result.code == 500 || result.code == 5)){
                ((BaseActivity)getActivity()).showCenteredToastmessage(getActivity(),"Unexpected error occurred, please try again later");
            }
            }
    };

    public void getStateJson() throws JSONException {
        String json = null;
        try {
            InputStream inputStream = getActivity().getAssets().open("states.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }


        JSONObject jsonObject = new JSONObject(json);
        JSONArray events = jsonObject.getJSONArray("states");
        for (int j = 0; j < events.length(); j++) {
            JSONObject cit = events.getJSONObject(j);
            State stateData = new State();

            stateData.setStateId(Integer.parseInt(cit.getString("id")));
            stateData.setStateName(cit.getString("name"));
            stateData.setCountryId(Integer.parseInt(cit.getString("country_id")));
            stateObject.add(stateData);
        }
    }

    public void getCityJson() throws JSONException {
        String json = null;
        try {
            InputStream inputStream = getActivity().getAssets().open("cities.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }


        JSONObject jsonObject = new JSONObject(json);
        JSONArray events = jsonObject.getJSONArray("cities");
        for (int j = 0; j < events.length(); j++) {
            JSONObject cit = events.getJSONObject(j);
            City cityData = new City();

            cityData.setCityId(Integer.parseInt(cit.getString("id")));
            cityData.setCityName(cit.getString("name"));
            cityData.setStateId(Integer.parseInt(cit.getString("state_id")));
            cityObject.add(cityData);
        }
    }

    private void getCityNames(int id){
        cityNamesList.clear();
        for(int i = 0; i < cityObject.size(); i++) {
            City cityData = new City();
            if (cityObject.get(i).getStateId() == id) {
                cityData.setCityId(cityObject.get(i).getCityId());
                cityData.setCityName(cityObject.get(i).getCityName());
                cityData.setStateId(cityObject.get(i).getStateId());
                cityNamesList.add(cityObject.get(i).getCityName());
            }
        }
    }

    private void getSearchResult(){
        ((BaseActivity)getActivity()).showProgressDialog(getActivity());
        Api.getSearchedModel(getActivity(),searchParams(),searchResultDataListner);
    }

    private JSONObject searchParams(){
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        JSONObject price = new JSONObject();
        JSONObject location_data = new JSONObject();
        JSONArray catArr = new JSONArray();
        JSONArray vendorArr = new JSONArray();


        for(int x = 0; x < spCategories.getSelectedItems().size();x++) {
            catArr.put(spCategories.getSelectedItems().get(x).getName().toLowerCase());
        }

        for(int i = 0; i < spVendors.getSelectedItems().size();i++) {
                vendorArr.put(tempVendorListKeyWithID.get(spVendors.getSelectedItems().get(i).getName()));
        }

        try {
            js.put("app_id", GeneralConstants.APP_ID);
            js.put("namespace", "");

            data.put("category", catArr);
            data.put("vendors_list", vendorArr);

            price.put("status", "no_range");
            price.put("lowest", 0);
            price.put("highest", 0);

            location_data.put("country","Pakistan");
            location_data.put("city","Lahore");
            location_data.put("state","Punjab");
            location_data.put("have_loc",isHaveLocation);

            data.put("location_data",location_data);
            data.put("price_related", price);
            js.put("data",data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    AsyncTaskListener searchResultDataListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            ((BaseActivity)getActivity()).dismissProgressDialog();
            tempVendorsKeyList.clear();
                    tempVendorsKeyList.add(allVendorListDataObj.getData().getVendors_list().getFood());
                    tempVendorsKeyList.add(allVendorListDataObj.getData().getVendors_list().getFashion());
                    tempVendorsKeyList.add(allVendorListDataObj.getData().getVendors_list().getDecor());
                    tempVendorsKeyList.add(allVendorListDataObj.getData().getVendors_list().getElectronics());
                    tempVendorsKeyList.add(allVendorListDataObj.getData().getVendors_list().getSpecial_occasions());
            dismiss();
        }
    };


    private void getAllVendorsList(){
        ((BaseActivity)getActivity()).showProgressDialog(getActivity());
        Api.getAllVendors(getActivity(),allVendorsPrams(),allVendrosListner);
    }

    AsyncTaskListener allVendrosListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            ((BaseActivity)getActivity()).dismissProgressDialog();
            setCategorySpinner();

        }
    };

    private JSONObject allVendorsPrams(){
        return new JSONObject();
    }

    @Override
    public void onItemsSelected(List<KeyPairBoolData> selectedItems) {

        ((BaseActivity)getActivity()).showCenteredToastmessage(getActivity(),"Test Toast");
    }

    @Override
    public void onSelectState(State state) {

        etStates.setText(state.getStateName());
        CityPicker.equalCityObject.clear();
        isCity = false;
        for(int i = 0; i < cityObject.size(); i++) {

            City cityData = new City();
            if (cityObject.get(i).getStateId() == state.getStateId()) {
                cityData.setCityId(cityObject.get(i).getCityId());
                cityData.setCityName(cityObject.get(i).getCityName());
                cityData.setStateId(cityObject.get(i).getStateId());
                CityPicker.equalCityObject.add(cityData);
                isCity = true;
            }
        }
        if(!isCity){
            for(int x = 0; x < StatePicker.equalStateObject.size(); x++) {
                City cityData = new City();
                cityData.setCityId(StatePicker.equalStateObject.get(x).getStateId());
                cityData.setCityName(StatePicker.equalStateObject.get(x).getStateName());
                cityData.setStateId(StatePicker.equalStateObject.get(x).getStateId());
                CityPicker.equalCityObject.add(cityData);
            }
        }
    }

    @Override
    public void onSelectCity(City city) {

        etCities.setText(city.getCityName());
    }
}
