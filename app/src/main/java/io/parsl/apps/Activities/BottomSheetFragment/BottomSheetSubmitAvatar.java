package io.parsl.apps.Activities.BottomSheetFragment;

import static io.parsl.apps.Network.Parser.CreateAvatarParser.createAvatarObj;
import static io.parsl.apps.Network.Parser.UserLoginParser.userLoginObj;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import io.parsl.apps.Activities.ARActivities.GltfActivity;
import io.parsl.apps.Activities.ReadyPlayerMe.MyAvatarsActivity;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;

public class BottomSheetSubmitAvatar extends Fragment implements View.OnClickListener {

    EditText avatarTitle, edtDesc, edtUrl;
    RelativeLayout rlSubmit;

    @SuppressLint("RestrictedApi")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_submit_avatar, container, false);

        rlSubmit = view.findViewById(R.id.rlSubmit);
        edtDesc = view.findViewById(R.id.edtDesc);
        avatarTitle = view.findViewById(R.id.avatarTitle);
        edtUrl = view.findViewById(R.id.edtUrl);


        edtUrl.setText(readFromClipboard());
        rlSubmit.setOnClickListener(this);

        return view;

    }

    public String readFromClipboard() {
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboard.hasPrimaryClip()) {
            android.content.ClipDescription description = clipboard.getPrimaryClipDescription();
            android.content.ClipData data = clipboard.getPrimaryClip();
            if (data != null && description != null && description.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN))
                return String.valueOf(data.getItemAt(0).getText());
        }
        return null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == rlSubmit.getId()) {
            ((MyAvatarsActivity) requireActivity()).showProgressDialog(getActivity());
            Api.createAvatar(getActivity(), getParams(), createAvatarListner);
        }
    }

    private JSONObject getParams() {
        JSONObject data = new JSONObject();


        try {
            data.put("app_id", GeneralConstants.APP_ID);
            data.put("namespace", UserPrefs.getNameSpace(getActivity()));
            data.put("title", avatarTitle.getText().toString());
            data.put("description", edtDesc.getText().toString());
            data.put("url", readFromClipboard());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;
    }

    AsyncTaskListener createAvatarListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            ((BaseActivity) getActivity()).dismissProgressDialog();
            if (createAvatarObj.getMessage().equalsIgnoreCase("Success")) {
                ((BaseActivity) requireActivity()).dismissProgressDialog();
                ((MyAvatarsActivity) getActivity()).dismissSubmitAvatrFragment();
                ((MyAvatarsActivity) getActivity()).clearRecyclerView();
                ((MyAvatarsActivity) getActivity()).apiCallgetAvatars();
            } else {
                Utils.showCenteredToastmessage(getActivity(), "Please Try Again!!!");
            }
        }
    };

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
