package io.parsl.apps.Activities.ReadyPlayerMe

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import io.parsl.apps.R


class MainActivity : AppCompatActivity() {
    lateinit var updateBtn: Button
    lateinit var createBtn: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)


        updateBtn = findViewById(R.id.update_button)
        createBtn = findViewById(R.id.create_button)

        val myClipboard = this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

        if (CookieHelper(this).getUpdateState()){
            updateBtn.visibility = View.VISIBLE
        }

        createBtn.setOnClickListener {
            openWebViewPage()
        }

        updateBtn.setOnClickListener{
           // openUpdateWebView()
            val pData: ClipData? = myClipboard.getPrimaryClip()
            val item = pData?.getItemAt(0)
            val txtpaste = item?.text.toString()
            Toast.makeText(this, txtpaste, Toast.LENGTH_LONG).show()
        }
    }

    private fun openUpdateWebView() {
        val intent = Intent(this, WebViewActivity::class.java)
        intent.putExtra(WebViewActivity.IS_CREATE_NEW, false)
        startActivity(intent)
    }


    private fun openWebViewPage() {
        val intent = Intent(this, WebViewActivity::class.java)
        intent.putExtra(WebViewActivity.IS_CREATE_NEW, true)
        startActivity(intent)
    }
}