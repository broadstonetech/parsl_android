package io.parsl.apps.Activities

import android.view.View
import com.unity3d.player.UnityPlayer
import io.parsl.apps.Activities.ARActivities.GltfActivity
import io.parsl.apps.Activities.ARActivities.GltfActivity.*
import io.parsl.apps.Utilities.Util.Utils

class CommunicationBridge(val communicationCallback: CommunicationCallback) {

    var isAnimation :Boolean = false;

    fun callFromUnityWithNoParameters() {
        communicationCallback.onNoParamCall()
    }


    fun callFromUnityMessage(param: String) {
        if(!isRedeemUseCase)
            (con as GltfActivity).loadDialogAdditionalInfo(con,param)
//        communicationCallback.onOneParamCall(param)
    }

    fun callFromUnityModelProjection(param: Boolean) {
        isModelDownloadedFromUnity = param
        if(isRedeemUseCase && !avatarMethodCalled){
            (con as GltfActivity).setRedeemText("Tap to view gift box.")
        }else if(isRedeemUseCase && avatarMethodCalled) {
            (con as GltfActivity).setRedeemText("Tap to view Avatar.")
            avatarMethodCalled = false
            isRedeemUseCase = false
            isAnimation = true;
        }else if(isAnimation){
            isAnimation = false;
            (con as GltfActivity).setRedeemText("Tap to view animation.")
        }
//        (con as GltfActivity).toast()
    }

    fun ShadowSwitch(param: Boolean) {
    }
    fun shadowOn() {
        UnityPlayer.UnitySendMessage("ObjectSpawner",
            "ShadowSwitchOn", "")
    }
    fun shadowOff() {
        UnityPlayer.UnitySendMessage("ObjectSpawner",
            "ShadowSwitchOff", "")
    }
    fun enableVerticalSurface() {
        UnityPlayer.UnitySendMessage("ObjectSpawner",
            "VertMovement", "")
        Utils.showCenteredToastmessage(con,"Vertical surface detection enabled")
    }
    fun ExitAppCallbackUnity() {
        (con as GltfActivity).finish()
    }
    fun enableHorizontalSurface() {
        UnityPlayer.UnitySendMessage("ObjectSpawner",
            "HoriMovement", "")
        Utils.showCenteredToastmessage(con,"Horizontal surface detection enabled")
    }
    fun clearCache() {
        UnityPlayer.UnitySendMessage("ObjectSpawner",
            "ClearModelsCache", "")
    }
    fun removeModelsFromScreen() {
        UnityPlayer.UnitySendMessage("ObjectSpawner",
            "EmptyScreen", "")
    }

    fun resetCamera() {
        UnityPlayer.UnitySendMessage("Main Camera",
            "ResetCameraRotation", "")
    }

    fun modelProjectionDone(param: Boolean){  // only call when gift model projected
        if(redeemCount == 0) {
            (con as GltfActivity).projectAvatar(param)
            redeemCount++;
        }
    }

    fun nonARLoaded(){
        imgResetCam.visibility = View.VISIBLE
    }
    fun avatarProjectionDone(param: Boolean){
        (con as GltfActivity).callPArslIcon(param)

    }
    fun callFromUnityWithTwoParameters(param1: String, param2: Int){
        communicationCallback.onTwoParamCall(param1, param2)
    }
    fun callToUnityWithNoMessage() {
        UnityPlayer.UnitySendMessage("ObjectSpawner",
            "Activate", "")
    }
    fun parslScreenLoaded() {
        UnityPlayer.UnitySendMessage("ObjectSpawner",
            "ParslScreenLoaded", "")
    }

    fun callToUnityWithMessage(param: String) {
        UnityPlayer.UnitySendMessage("ObjectSpawner",
            "Activate", param)
    }

    fun reedeemGiftModel(param: String) {   // gift model redeem
        UnityPlayer.UnitySendMessage("ObjectSpawner",
            "Redeem", param)

    }

    fun unityReloaded() {
//        shoppingCartProductList.clear()
//        productList.clear()
//        modelDataMap.clear()
    }

    fun reloadUnityScene() {
        UnityPlayer.UnitySendMessage("ObjectSpawner",
            "ReLoadUnity", "")
    }
    fun vertMovement(boolean: Boolean ) {

    }


    fun callToUnityAddToParams(testList:List<String>) {
        UnityPlayer.UnitySendMessage("AddToCartPanel",
            "ReturnCart","")
    }

    fun callToUnityRedeemCharacter(param:String) {

        UnityPlayer.UnitySendMessage("ObjectSpawner",
            "RedeemCharacters",param)
        (con as GltfActivity).setRedeemText("Downloading Avatar")
    }

    interface CommunicationCallback {
        fun onNoParamCall()
        fun onOneParamCall(param: String)
        fun onTwoParamCall(param1: String, param2: Int)
    }


}