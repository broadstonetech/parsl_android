package io.parsl.apps.Activities;

import static io.parsl.apps.Network.Parser.ResetPasswordClassDataParser.resetPasswordDataObj;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import io.parsl.apps.BaseActivity;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;
import okhttp3.internal.Util;

public class ChangePaswordActivity extends BaseActivity implements View.OnClickListener {

    EditText etOldPass, etNewPass, etNewConfirmPass;
    RelativeLayout rlSubmit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        etOldPass = findViewById(R.id.etOldPass);
        etNewPass = findViewById(R.id.etNewPass);
        etNewConfirmPass = findViewById(R.id.etNewConfirmPass);
        rlSubmit = findViewById(R.id.rlSubmit);
        rlSubmit.setOnClickListener(this);

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_change_pasword;
    }

    //region onclicks
    @Override
    public void onClick(View v) {
        if (v.getId() == rlSubmit.getId()) {
            if (etNewPass.getText().toString().equalsIgnoreCase(etNewConfirmPass.getText().toString())) {
                if (Utils.isValidPassword(etNewPass.getText().toString()))
                    submitResetPassApiCall();
                else {
                    etNewPass.setError(getResources().getString(R.string.EMPTY_PASSWORD));
                }
            } else {
                Utils.showCenteredToastmessage(ChangePaswordActivity.this, "Your passowrd did not match");
            }
        }
    }
    //endregion

    private void submitResetPassApiCall() {
        Api.resetPassword(this, getParams(), resetPassListner);
    }

    //region api calls
    private JSONObject getParams() {
        JSONObject data = new JSONObject();

        try {
            data.put("app_id", GeneralConstants.APP_ID);
            data.put("new_password", getSHA256(etNewPass.getText().toString()));
            data.put("old_password", getSHA256(etOldPass.getText().toString()));
            data.put("namespace", UserPrefs.getNameSpace(this));

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return data;
    }

    AsyncTaskListener resetPassListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (resetPasswordDataObj.isStatus()) {
                Utils.showCenteredToastmessage(ChangePaswordActivity.this, "Password has been reset");
                finish();
            } else {
                Utils.showCenteredToastmessage(ChangePaswordActivity.this, "No user exist with this password");
            }
        }
    };
    //endregion
}