package io.parsl.apps.Activities.ReadyPlayerMe;

import static io.parsl.apps.Network.Parser.AvatarDetailModelParser.avatarDetailsDataObj;
import static io.parsl.apps.Utilities.Constants.GeneralConstants.IsFromWebView;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import io.parsl.apps.Activities.ARActivities.ProfileActivity;
import io.parsl.apps.Activities.BottomSheetFragment.BottomSheetSubmitAvatar;
import io.parsl.apps.Adaptors.MyAvtarsAdapter;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;

public class MyAvatarsActivity extends BaseActivity implements View.OnClickListener {

    public static boolean isFromMyAvatars = false;
    public static int customAvatarPos = 0;
    TextView createBtn, updateBtn, tvToolBar, tvNoDataFound;
    RecyclerView rvMyAvtars;
    private MyAvtarsAdapter avatarAdaptor;
    private BottomSheetSubmitAvatar fragmentSubmitAvatar;
    private Fragment fragment;

    //region overriden methods
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        updateBtn = findViewById(R.id.update_button);
        createBtn = findViewById(R.id.create_button);
        rvMyAvtars = findViewById(R.id.rvAvatars);
        tvToolBar = findViewById(R.id.tvToolBar);
        tvNoDataFound = findViewById(R.id.tvNoDataFound);
        rvMyAvtars.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        updateBtn.setOnClickListener(this);
        createBtn.setOnClickListener(this);
        tvToolBar.setOnClickListener(this);

        apiCallgetAvatars();
    }

    public void apiCallgetAvatars(){
        showProgressDialog(this);
        Api.getAvatars(this, getParams(), myAvatarsListner);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (IsFromWebView) {
            fragmentSubmitAvatar = new BottomSheetSubmitAvatar();
            setFragment(fragmentSubmitAvatar);
            IsFromWebView = false;

        }
    }

    @Override
    public void onBackPressed() {

        fragment = getSupportFragmentManager().findFragmentByTag(GeneralConstants.SUBMIT_AVATAR);
        if (fragment != null)
            cancelSubmitAvatarDialog();
        else {
            AddActivityAndRemoveIntentTransition(ProfileActivity.class);
        }
    }

    protected void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, fragment, GeneralConstants.SUBMIT_AVATAR);
        fragmentTransaction.commit();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == createBtn.getId() || v.getId() == tvToolBar.getId()) {
            openWebViewPage();
        } else if (v.getId() == updateBtn.getId()) {
            openUpdateWebView();
        }
    }
    //endregion

    //region Generic methods
    public void updateUI() {
        avatarAdaptor.notifyDataSetChanged();
    }

    private void openUpdateWebView() {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(WebViewActivity.IS_CREATE_NEW, false);
        startActivity(intent);
        finish();
    }

    private void openWebViewPage() {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(WebViewActivity.IS_CREATE_NEW, true);
        startActivity(intent);
        finish();
    }

    private void cancelSubmitAvatarDialog() {
        AlertDialog.Builder builderAddInfo = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogViewAddInfo = inflater.inflate(R.layout.clear_screen_dialog, null);
        builderAddInfo.setView(dialogViewAddInfo);
        AlertDialog dialogAddInfo = builderAddInfo.create();
        Objects.requireNonNull(dialogAddInfo.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogAddInfo.show();
        dialogAddInfo.setCancelable(true);

        TextView tvMessage = dialogViewAddInfo.findViewById(R.id.tvMessage);
        TextView btnYes = dialogViewAddInfo.findViewById(R.id.btnYes);
        TextView btnNo = dialogViewAddInfo.findViewById(R.id.btnNo);

        tvMessage.setText("Are you sure you want to quit? Progress lost.");

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAddInfo.dismiss();
            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().remove(fragmentSubmitAvatar).commit();
                dialogAddInfo.dismiss();
            }
        });

    }

    public void dismissSubmitAvatrFragment() {
        getSupportFragmentManager().beginTransaction().remove(fragmentSubmitAvatar).commit();
        Utils.showCenteredToastmessage(this, "Your avatar has been submitted");
    }

    public void clearRecyclerView() {
        int size = avatarDetailsDataObj.getData().size();
        avatarDetailsDataObj.getData().clear();
        if(avatarAdaptor != null){
            if (avatarAdaptor.getItemCount() != 0)
                avatarAdaptor.notifyItemRangeRemoved(0, size);

        }
    }
    //endregion

    //region Api call

    AsyncTaskListener myAvatarsListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if (avatarDetailsDataObj.isStatus()) {
                rvMyAvtars.setVisibility(View.VISIBLE);
                tvNoDataFound.setVisibility(View.GONE);
                avatarAdaptor = new MyAvtarsAdapter(MyAvatarsActivity.this, avatarDetailsDataObj);
                rvMyAvtars.setAdapter(avatarAdaptor);
                createBtn.setVisibility(View.GONE);

                avatarAdaptor.setOnItemClickListener(new MyAvtarsAdapter.ClickListener() {
                    @Override
                    public void onItemClick(int position, View v) {
                        customAvatarPos = position;
                        isFromMyAvatars = true;
                        finish();
                    }
                });
            } else {
                rvMyAvtars.setVisibility(View.GONE);
            }
        }
    };

    private JSONObject getParams() {
        JSONObject js = new JSONObject();

        try {
            js.put("app_id", GeneralConstants.APP_ID);
            js.put("namespace", UserPrefs.getNameSpace(this));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }
    //endregion
}