//package io.parsl.apps.Activities;
//
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.os.Bundle;
//import android.view.Gravity;
//import android.view.View;
//import android.widget.FrameLayout;
//import android.widget.LinearLayout;
//import android.widget.Toast;
//
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.google.android.material.tabs.TabLayout;
//import com.unity3d.player.UnityPlayerActivity;
//
//import org.jetbrains.annotations.NotNull;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import io.parsl.apps.Activities.ARActivities.GltfActivity;
//import io.parsl.apps.Adaptors.AdapterCategoriesData;
//import io.parsl.apps.Helpers.DownloadFileHelper.AsyncTaskDownload;
//import io.parsl.apps.ModelClass.AllVendorListModelClass;
//import io.parsl.apps.Network.API.Api;
//import io.parsl.apps.Network.API.TaskResult;
//import io.parsl.apps.Network.AsyncTaskListener;
//import io.parsl.apps.R;
//import io.parsl.apps.Utilities.Constants.GeneralConstants;
//import io.parsl.apps.Utilities.Util.Utils;
//
//import static io.parsl.apps.Network.Parser.AllVendorListModelClassDataParser.allVendorListDataObj;
//import static io.parsl.apps.Network.Parser.CategoriesDataParser.categoriesDatatObj;
//import static io.parsl.apps.Network.Parser.CategoriesModelParser.categoriesListObj;
//import static io.parsl.apps.Network.Parser.CategoriesVendorsDataParser.categoryVendorsObj;
//
//public class SecondMainActivity extends UnityPlayerActivity implements TabLayout.OnTabSelectedListener {
//
//    FrameLayout layout;
//    private TabLayout tabsCategories,tabVendors;
//    private LinearLayout tabsContainerLayout,tabsContainerLayout2;
//    boolean isSearchModel = false;
//    private int vendorTabPosition=-1;
//    int categoryTabPosition = 0;
//    String categoryKey;
//    List<AllVendorListModelClass.Data.DataObj> vendorsList = new ArrayList<>();
//    private int tabPosition=-1;
//    private LinearLayout tempLinearLayout,tempLinearLayout2;
//    private boolean firstime = false;
//    public ProgressDialog progressDialog;
//    private AdapterCategoriesData adapterData;
//    RecyclerView rvCategoriesData;
//    CommunicationBridge obj;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_kotlin);
//
//        layout = findViewById(R.id.layout);
//        tabsCategories = findViewById(R.id.tabCategories);
//        tabVendors = findViewById(R.id.tabVendros);
//        rvCategoriesData = findViewById(R.id.rvCategoriesData);
//
//            obj = new CommunicationBridge(new CommunicationBridge.CommunicationCallback() {
//            @Override
//            public void onNoParamCall() {
//
//            }
//
//            @Override
//            public void onOneParamCall(@NotNull String param) {
//
//            }
//
//            @Override
//            public void onTwoParamCall(@NotNull String param1, int param2) {
//
//            }
//        });
//
//
//        layout.addView(mUnityPlayer.getView());
//
//        tabsCategories.addOnTabSelectedListener(SecondMainActivity.this);
//        addCategoryTabs();
//    }
//
//    @Override
//    public void onTabSelected(TabLayout.Tab tab) {
//        if (categoriesListObj != null && !isSearchModel) {
//            vendorTabPosition = -1;
//            categoryTabPosition = tab.getPosition();
//            categoryKey = categoriesListObj.getData().get(tab.getPosition()).getCategory_key();
//            getVendors(categoriesListObj.getData().get(tab.getPosition()).getCategory_key());
//        }else {
//            vendorsList.clear();
//            categoryKey = allVendorListDataObj.getData().getCat_list().get(tab.getPosition()).getCategory_key();
//
//            if(tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Food")){
//                vendorsList.addAll(allVendorListDataObj.getData().getVendors_list().getFood());
//            }else if(tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Fashion")){
//                vendorsList.addAll(allVendorListDataObj.getData().getVendors_list().getFashion());
//            }else if(tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Decor")){
//                vendorsList.addAll(allVendorListDataObj.getData().getVendors_list().getDecor());
//            }else if(tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Electronics")){
//                vendorsList.addAll(allVendorListDataObj.getData().getVendors_list().getElectronics());
//            }else if(tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Special Occasions")){
//                vendorsList.addAll(allVendorListDataObj.getData().getVendors_list().getSpecial_occasions());
//            }
//
//            addVendorTabs(vendorsList);
//        }
//
//        if(tabPosition != tab.getPosition() || tab.getPosition() == 0) {
//            tabsContainerLayout = (LinearLayout) tabsCategories.getChildAt(0);
//            tempLinearLayout = (LinearLayout) tabsContainerLayout.getChildAt(tabsCategories.getSelectedTabPosition());
//            tempLinearLayout.setBackgroundResource(R.drawable.tab_selected_color);
//        }
//        if(tabPosition >= 0) {
//            tabsContainerLayout = (LinearLayout) tabsCategories.getChildAt(0);
//            tempLinearLayout = (LinearLayout) tabsContainerLayout.getChildAt(tabPosition);
//            tempLinearLayout.setBackgroundColor(getResources().getColor(R.color.white));
//            tabPosition = -1;
//        }
//
//        tabPosition = tab.getPosition();
//    }
//
//    @Override
//    public void onTabUnselected(TabLayout.Tab tab) {
//        tabsContainerLayout2 = (LinearLayout) tabVendors.getChildAt(0);
//        tempLinearLayout2 = (LinearLayout) tabsContainerLayout2.getChildAt(vendorTabPosition);
//        tempLinearLayout2.setBackgroundColor(getResources().getColor(R.color.white));
//    }
//
//    @Override
//    public void onTabReselected(TabLayout.Tab tab) {
//
//    }
//
//    TabLayout.OnTabSelectedListener vendorTab = new TabLayout.OnTabSelectedListener() {
//        @Override
//        public void onTabSelected(TabLayout.Tab tab) {
//
//            if(firstime) {
//                if (!isSearchModel) {
//                    getCategoryModels(tab.getPosition(), categoryVendorsObj.getData().getVendors_list().get(tab.getPosition()).getVendor_id());
//
//                } else {
//                    if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Food")) {
//                        getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFood().get(tab.getPosition()).getVendor_id());
//                    } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Fashion")) {
//                        getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFood().get(tab.getPosition()).getVendor_id());
//                    } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Decor")) {
//                        getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFood().get(tab.getPosition()).getVendor_id());
//                    } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Electronics")) {
//                        getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFood().get(tab.getPosition()).getVendor_id());
//                    } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Special Occasions")) {
//                        getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFood().get(tab.getPosition()).getVendor_id());
//                    }
//                }
//            }
//            firstime = true;
//            tabVendors.getChildAt(0).setBackgroundColor(getResources().getColor(R.color.white));
//            if(vendorTabPosition != tab.getPosition() || tab.getPosition() == 0) {
//                tabsContainerLayout2 = (LinearLayout) tabVendors.getChildAt(0);
//                tempLinearLayout2 = (LinearLayout) tabsContainerLayout2.getChildAt(tabVendors.getSelectedTabPosition());
//                tempLinearLayout2.setBackgroundResource(R.drawable.tab_selected_vendor_tav);
//
//
//            }
//            if(vendorTabPosition >= 0) {
//                tabsContainerLayout2 = (LinearLayout) tabVendors.getChildAt(0);
//                tempLinearLayout2 = (LinearLayout) tabsContainerLayout2.getChildAt(vendorTabPosition);
//                tempLinearLayout2.setBackgroundColor(getResources().getColor(R.color.white));
//                //  vendorTabPosition = -1;
//            }
//
////                tabVendors.getTabAt(tab.getPosition()).setIcon(R.drawable.plus_icon);
//            vendorTabPosition = tab.getPosition();
//
//        }
//
//        @Override
//        public void onTabUnselected(TabLayout.Tab tab) {
//            vendorTabPosition = tab.getPosition();
//            tabsContainerLayout2 = (LinearLayout) tabVendors.getChildAt(0);
//            tempLinearLayout2 = (LinearLayout) tabsContainerLayout2.getChildAt(tab.getPosition());
//            tempLinearLayout2.setBackgroundColor(getResources().getColor(R.color.white));
//
//        }
//
//        @Override
//        public void onTabReselected(TabLayout.Tab tab) {
//            if (!isSearchModel) {
//                getCategoryModels(tab.getPosition(), categoryVendorsObj.getData().getVendors_list().get(tab.getPosition()).getVendor_id());
//
//            } else {
//                if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Food")) {
//                    getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFood().get(tab.getPosition()).getVendor_id());
//                } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Fashion")) {
//                    getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFood().get(tab.getPosition()).getVendor_id());
//                } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Decor")) {
//                    getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFood().get(tab.getPosition()).getVendor_id());
//                } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Electronics")) {
//                    getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFood().get(tab.getPosition()).getVendor_id());
//                } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Special Occasions")) {
//                    getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFood().get(tab.getPosition()).getVendor_id());
//                }
//            }
//        }
//    };
//
//
//    private void getVendors(String categoryName){
//        showProgressDialog(this);
//        Api.getCategoryVendors(this, getCategoryVendros(categoryName), catgoryVendorListner);
//    }
//
//    private void addVendorTabs(List<AllVendorListModelClass.Data.DataObj> vendors) {
//
//        List<String> message = new ArrayList<>();
//        tabVendors.removeAllTabs();
//        for (int i = 0; i < vendors.size(); i++) {
//            tabVendors.addTab(tabVendors.newTab().setText(vendors.get(i).getVendor_name()));
//        }
//    }
//
//    private JSONObject getCategoryVendros(String name) {
//        JSONObject js = new JSONObject();
//        JSONObject data = new JSONObject();
//
//        try {
//            js.put("app_id", GeneralConstants.APP_ID);
//            data.put("category", name);
//            js.put("data", data);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return js;
//    }
//
//    AsyncTaskListener catgoryVendorListner = new AsyncTaskListener() {
//        @Override
//        public void onComplete(TaskResult result) {
//            dismissProgressDialog();
//            if(categoryVendorsObj.getMessage().equalsIgnoreCase("success")) {
//                tabVendors.addOnTabSelectedListener(vendorTab);
//                addVendorTabs();
//            }else {
//                showCenteredToastmessage(SecondMainActivity.this,"Unexpected error occurred, please try again later");
//            }
//
//        }
//    };
//
//    private void getCategoryModels(int pos,String vendorID) {
//        showProgressDialog(this);
//        Api.getVendorModels(this, getVendorModels(categoryKey,vendorID), catgoryModelsListner);
//    }
//
//    private JSONObject getVendorModels(String name,String vendorID) {
//        JSONObject js = new JSONObject();
//        JSONObject data = new JSONObject();
//        JSONArray arr = new JSONArray();
//
//        try {
//            js.put("app_id", GeneralConstants.APP_ID);
//            data.put("category", name);
//            arr.put(vendorID);
//            data.put("vendors", arr);
//            js.put("data", data);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return js;
//    }
//
//    AsyncTaskListener catgoryModelsListner = new AsyncTaskListener() {
//        @Override
//        public void onComplete(TaskResult result) {
//            dismissProgressDialog();
//            if(categoriesDatatObj.getMessage().equalsIgnoreCase("Success")) {
//                setCategoryModel();
//            }else {
//                showCenteredToastmessage(SecondMainActivity.this,"Unexpected error occurred, please try again later");
//            }
//        }
//    };
//
//    private void addCategoryTabs() {
//        List<String> message = new ArrayList<>();
//        if (categoriesListObj != null) {
//            for (int i = 0; i < categoriesListObj.getData().size(); i++) {
//                tabsCategories.addTab(tabsCategories.newTab().setText(categoriesListObj.getData().get(i).getCategory_title()));
//            }
//        } else {
//            message.add("food");
//            tabsCategories.addTab(tabsCategories.newTab().setText(message.get(0)));
//        }
//    }
//
//    private void addVendorTabs() {
//
//        if (tabVendors.getTabAt(0) == null) {
//            List<String> message = new ArrayList<>();
//            tabVendors.removeAllTabs();
//            if (categoryVendorsObj != null) {
//                for (int i = 0; i < categoryVendorsObj.getData().getVendors_list().size(); i++) {
//                    tabVendors.addTab(tabVendors.newTab().setText(categoryVendorsObj.getData().getVendors_list().get(i).getVendor_name()));
//                }
//            }
//        }else {
//            tabVendors.removeAllTabs();
//            if (categoriesListObj != null) {
//                for (int i = 0; i < categoryVendorsObj.getData().getVendors_list().size(); i++) {
//                    tabVendors.addTab(tabVendors.newTab().setText(categoryVendorsObj.getData().getVendors_list().get(i).getVendor_name()));
//                }
//            }
//        }
//    }
//
//    public void setCategoryModel() {
//        adapterData = new AdapterCategoriesData(this,categoriesDatatObj);
//        rvCategoriesData.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
//        rvCategoriesData.setAdapter(adapterData);
//
//        adapterData.setOnItemClickListener(new AdapterCategoriesData.ClickListener() {
//            @Override
//            public void onItemClick(int position, View v) {
//                Utils.setProjectionModelPosition(position, adapterData);
//                showCenteredToastmessage(SecondMainActivity.this,categoriesDatatObj.getData().get(position).getBasic_info().getName());
//                obj.callToUnityWithMessage("surprisebox"+position);
//            }
//        });
//    }
//
//    public void showProgressDialog(Context context) {
//        try {
//            if (progressDialog != null && progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//            progressDialog = new ProgressDialog(context, R.style.ProgressDialogTheme);
//            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    public void dismissProgressDialog() {
//        try {
//            if (progressDialog != null && progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    public void showCenteredToastmessage(Context mContext, String text) {
//        Toast toast = Toast.makeText(mContext, text, Toast.LENGTH_LONG);
//        toast.setGravity(Gravity.CENTER, 0, 0);
//        toast.show();
//    }
//}