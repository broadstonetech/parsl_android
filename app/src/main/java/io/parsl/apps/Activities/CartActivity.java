package io.parsl.apps.Activities;

import static io.parsl.apps.Activities.ARActivities.GltfActivity.modelDataMap;
import static io.parsl.apps.Utilities.Util.Utils.hasPermissions;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baoyz.actionsheet.ActionSheet;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.ParseException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.mime.HttpMultipartMode;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;
import cz.msebera.android.httpclient.entity.mime.content.FileBody;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import io.parsl.apps.Activities.ARActivities.GltfActivity;
import io.parsl.apps.Activities.BottomSheetFragment.BottomSheetLogin;
import io.parsl.apps.Adaptors.AdapterAnimationsData;
import io.parsl.apps.Adaptors.AdapterAvatarsData;
import io.parsl.apps.Adaptors.AdapterExtrasModelsData;
import io.parsl.apps.Adaptors.AdapterItemTextData;
import io.parsl.apps.Adaptors.CartListAdapter;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.Network.Parser.CoupanCodeDataParser;
import io.parsl.apps.Network.Parser.ParselClassDataParser;
import io.parsl.apps.Network.Parser.SaveParselCartParser;
import io.parsl.apps.Payments.PaymentBaseActivity;
import io.parsl.apps.Payments.PaymentMethodSelectionFragment;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;

public class CartActivity extends PaymentBaseActivity implements View.OnClickListener, ActionSheet.ActionSheetListener {

    public static TextView tv_total, tvTranscationFee;
    public static RecyclerView rvDataInText;
    public static File videoFile;
    public static String giftModelID;
    public static EditText msg, etCoupan;
    public static boolean animatedModel = false;
    public static boolean isFromCart = false;
    public static JSONArray idList = new JSONArray();
    public static EditText etEmail, etName;
    public static double commissioned;
    public static String charcter;
    public static String animation = "0";
    RecyclerView recycler_itemlist;
    CartListAdapter cartListAdapter;
    TextView btnPayment, tvAddMessage, tvVideoMessage;
    RecyclerView rvExtras, rvAvatars, rvAnimations;
    TextView tvApplyCoupan, tvLogIn;
    int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 1000;
    int REQUEST_CODE_GALLERY_VIDEO = 2000;
    String path = "";
    double totalPrice, totalPriceWithCommisin;
    ImageView imgEditVideo;
    VideoView videoView;
    LinearLayout llPersonalData, llExtras, llVideoMessage;
    public static LinearLayout llSignIn;
    TextView tvPersonalData, tvExtras;
    private AdapterExtrasModelsData adapterData;
    private AdapterItemTextData adapterTextData;
    private String video_url;
    private Uri videoUri;
    int greetingCount = 0;
    int videoMessageCount = 0;
    public static boolean isDefualtVideo = false;
    public static BottomSheetLogin fragmentLogin;
    ActivityResultLauncher<Intent> mPermissions;
    private static String[] PERMISSIONS = null;
    int PERMISSION_ALL = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isFromCart = true;
        tv_total = findViewById(R.id.tv_total);
        tvTranscationFee = findViewById(R.id.tvTranscationFee);
        btnPayment = findViewById(R.id.btnPayment);
        msg = findViewById(R.id.etMsg);
        etCoupan = findViewById(R.id.etCoupan);
        tvApplyCoupan = findViewById(R.id.tvApplyCoupan);
        tvAddMessage = findViewById(R.id.tvAddMessage);
        tvVideoMessage = findViewById(R.id.tvVideoMessage);
        tvLogIn = findViewById(R.id.tvLogIn);
        llVideoMessage = findViewById(R.id.llVideoMessage);
        llSignIn = findViewById(R.id.llSignIn);
        etEmail = findViewById(R.id.etEmail);
        etName = findViewById(R.id.etName);
        videoView = findViewById(R.id.videoView);

        rvExtras = findViewById(R.id.rvExtras);
        rvAnimations = findViewById(R.id.rvAnimations);
        rvAvatars = findViewById(R.id.rvAvatars);
        llExtras = findViewById(R.id.llExtrass);
        llPersonalData = findViewById(R.id.llPersonalData);
        imgEditVideo = findViewById(R.id.imgEditVideo);

        tvExtras = findViewById(R.id.tvExtras);
        tvPersonalData = findViewById(R.id.tvPersonalData);

        recycler_itemlist = findViewById(R.id.recycler_cart);

        rvDataInText = findViewById(R.id.recyclerItems);
        recycler_itemlist.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        cartListAdapter = new CartListAdapter(CartActivity.this, GltfActivity.shoppingCartProductList);
        recycler_itemlist.setAdapter(cartListAdapter);


        rvDataInText.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        adapterTextData = new AdapterItemTextData(CartActivity.this, GltfActivity.shoppingCartProductList);
        rvDataInText.setAdapter(adapterTextData);

        showProgressDialog(this);
        Api.getParselModels(this, getParams(), extrasListner);

        btnPayment.setOnClickListener(this);
        tvApplyCoupan.setOnClickListener(this);
        imgEditVideo.setOnClickListener(this);
        tvPersonalData.setOnClickListener(this);
        tvExtras.setOnClickListener(this);
        tvLogIn.setOnClickListener(this);
        tvAddMessage.setOnClickListener(this);
        tvVideoMessage.setOnClickListener(this);

        for (int x = 0; x < GltfActivity.shoppingCartProductList.size(); x++) {

            totalPrice += GltfActivity.shoppingCartProductList.get(x).getObj().getPrice_related().getPrice() * GltfActivity.shoppingCartProductList.get(x).getQuantity();
            idList.put(GltfActivity.shoppingCartProductList.get(x).getObj().getProduct_id());
        }

        video_url = "android.resource://" + getPackageName() + "/" + R.raw.promo;
        videoUri = Uri.parse(video_url);
        MediaController mediaController = new MediaController(this);

        videoView.setMediaController(mediaController);
        videoView.setVideoURI(videoUri);
        //videoView.requestFocus();
        videoView.seekTo(1);
        // videoView.start();
        mediaController.setAnchorView(videoView);
        commissioned = totalPrice * 0.15;
        totalPriceWithCommisin = totalPrice + commissioned;
        tv_total.setText("$" + " " + Utils.getRoundedDoubleForMoney(totalPriceWithCommisin));
        tvTranscationFee.setText("$" + " " + Utils.getRoundedDoubleForMoney(commissioned));


        etEmail.setText(UserPrefs.getEmail(this));

        if (!UserPrefs.getNameSpace(this).isEmpty()) {
            llSignIn.setVisibility(View.GONE);
        } else {
            llSignIn.setVisibility(View.VISIBLE);
        }

        PERMISSIONS = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };
        mPermissions = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if (result.getResultCode() == GltfActivity.RESULT_OK) {
                    isDefualtVideo = false;
                    Intent intent = new Intent("android.media.action.VIDEO_CAPTURE");
                    intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
                    intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
                    startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
                }

            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_cart_layout;
    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {

    }

    //region Api listners and payload

    private void setExtrasObjModel() {
        adapterData = new AdapterExtrasModelsData(this, ParselClassDataParser.parselModelObj, true);
        rvExtras.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvExtras.setAdapter(adapterData);

        Utils.setExtrasProjectionPosition(0, adapterData);
        animatedModel = true;
        giftModelID = ParselClassDataParser.parselModelObj.getData().get(0).getProduct_id();

        setAvatars();
        setAnimations();
        adapterData.setOnItemClickListener(new AdapterExtrasModelsData.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Utils.setExtrasProjectionPosition(position, adapterData);
                animatedModel = true;
                giftModelID = ParselClassDataParser.parselModelObj.getData().get(position).getProduct_id();
            }
        });
    }

    private void setAvatars() {

        AdapterAvatarsData adapterData = new AdapterAvatarsData(this, ParselClassDataParser.parselModelObj);
        rvAvatars.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvAvatars.setAdapter(adapterData);


        adapterData.setOnItemClickListener(new AdapterAvatarsData.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Utils.setExtrasProjectionPosition(position, adapterData);
                CartActivity.charcter = ParselClassDataParser.parselModelObj.getAvatars().get(position).getId();
            }
        });
    }

    private void setAnimations() {

        AdapterAnimationsData adapterData = new AdapterAnimationsData(this, ParselClassDataParser.parselModelObj, true);
        rvAnimations.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvAnimations.setAdapter(adapterData);


        adapterData.setOnItemClickListener(new AdapterAnimationsData.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Utils.setExtrasProjectionPosition(position, adapterData);
                CartActivity.animation = ParselClassDataParser.parselModelObj.getAnimations().get(position).getAnimation_key();
            }
        });
    }

    //region onclicks
    @Override
    public void onClick(View v) {
        if (v.getId() == btnPayment.getId()) {
            if (animatedModel) {
                if (validateFields()) {

                    if (UserPrefs.getEmail(CartActivity.this).equalsIgnoreCase(etEmail.getText().toString())) {
                        UserPrefs.setOldEmail(this, "");
                    } else {
                        UserPrefs.setOldEmail(this, UserPrefs.getEmail(this));
                        UserPrefs.setEmail(this, etEmail.getText().toString());
                    }
                    if (isValidEmail(etEmail.getText().toString().trim())) {
                        UserPrefs.setEmail(CartActivity.this, etEmail.getText().toString().trim());
                        startPaymentBottomSheetFragment(1000);

                    } else {
                        showCenteredToastmessage(this, "Please enter valid email address.");
                    }
                }
            } else {
                showCenteredToastmessage(this, "Please select gift from above mentioned choice.");
            }

        } else if (v.getId() == tvApplyCoupan.getId()) {
            showProgressDialog(this);
            Api.getApplyCoupan(this, getCoupanParams(etCoupan.getText().toString(), idList), getCoupanListner);
        } else if (v.getId() == imgEditVideo.getId()) {
            ActionSheet.createBuilder(CartActivity.this, getSupportFragmentManager())
                    .setCancelButtonTitle("Cancel")
                    .setOtherButtonTitles("Use default video", "Record video", "Upload from gallery")
                    .setCancelableOnTouchOutside(true)
                    .setListener(messageListner).show();
        } else if (v.getId() == tvExtras.getId()) {
            tvExtras.setBackground(getResources().getDrawable(R.drawable.right_curved_corner));
            tvPersonalData.setBackgroundDrawable(null);
            tvExtras.setTextColor(getResources().getColor(R.color.white));
            tvPersonalData.setTextColor(getResources().getColor(R.color.black));
            llPersonalData.setVisibility(View.GONE);
            llSignIn.setVisibility(View.GONE);
            llExtras.setVisibility(View.VISIBLE);

        } else if (v.getId() == tvPersonalData.getId()) {
            tvPersonalData.setBackground(getResources().getDrawable(R.drawable.rounded_corner_left_corner));
            tvExtras.setBackgroundDrawable(null);
            tvPersonalData.setTextColor(getResources().getColor(R.color.white));
            tvExtras.setTextColor(getResources().getColor(R.color.black));
            llExtras.setVisibility(View.GONE);
            llPersonalData.setVisibility(View.VISIBLE);
            if (!UserPrefs.getNameSpace(this).isEmpty()) {
                llSignIn.setVisibility(View.GONE);
            } else {
                llSignIn.setVisibility(View.VISIBLE);
            }
        } else if (v.getId() == tvAddMessage.getId()) {
            if (greetingCount == 0) {
                msg.setVisibility(View.VISIBLE);
                greetingCount++;
            } else {
                msg.setVisibility(View.GONE);
                greetingCount = 0;
            }
        } else if (v.getId() == tvVideoMessage.getId()) {
            if (videoMessageCount == 0) {
                llVideoMessage.setVisibility(View.VISIBLE);
                videoMessageCount++;
            } else {
                llVideoMessage.setVisibility(View.GONE);
                videoMessageCount = 0;
            }
        } else if (v.getId() == tvLogIn.getId()) {
            fragmentLogin = new BottomSheetLogin();
            fragmentLogin.show(getSupportFragmentManager(), "");
        }
    }
    //endregion

    //<editor-fold desc="api">
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri fileUri = data.getData();
                try {
                    path = Utils.getPath(this, fileUri);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                videoFile = new File(fileUri.getPath());
                videoView.setVideoURI(fileUri);
                videoView.seekTo(5);
                int x = 100;
                ;
            }
        } else if (requestCode == REQUEST_CODE_GALLERY_VIDEO && data != null) {
            Uri selectedVieo = data.getData();
            try {

                MediaPlayer mp = MediaPlayer.create(this, selectedVieo);
                int duration = mp.getDuration();
                mp.release();

                if ((duration / 1000) > 10) {
                    showCenteredToastmessage(this, "Video must not be greater than 10 seconds");
                } else {
                    videoView.setVideoURI(selectedVieo);
                    videoView.seekTo(5);

                    videoFile = new File(data.getData().getPath());

                    path = Utils.getPath(this, selectedVieo);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private JSONObject getParams() {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            js.put("app_id", GeneralConstants.APP_ID);
            js.put("namespace", UserPrefs.getNameSpace(this));
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    private JSONObject getCoupanParams(String coupanCode, JSONArray selectedProducts) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        try {
            js.put("app_id", GeneralConstants.APP_ID);
            data.put("coupon_code", coupanCode);
            data.put("selected_products", selectedProducts);
            data.put("date_time", currentDate + " " + currentTime);
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }
    //</editor-fold>

    public void uploadVideo() {
        showProgressDialog(this);
        new UploadFileAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void startPaymentBottomSheetFragment(double amount) {
        PaymentMethodSelectionFragment paymentMethodSelectionActivity =
                new PaymentMethodSelectionFragment();
        paymentMethodSelectionActivity.show(getSupportFragmentManager(),
                "");
    }

    ActionSheet.ActionSheetListener messageListner = new ActionSheet.ActionSheetListener() {
        @Override
        public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

        }

        @Override
        public void onOtherButtonClick(ActionSheet actionSheet, int index) {
            if (index == 0) {
                videoView.setVideoURI(videoUri);
                videoView.seekTo(1);
                videoFile = new File(videoUri.getPath());
                isDefualtVideo = true;
            } else if (index == 1) {

                if (!hasPermissions(CartActivity.this, PERMISSIONS)) {
                    Utils.requestPermission(CartActivity.this, mPermissions, PERMISSIONS, PERMISSION_ALL, CartActivity.this);
                } else {
                    isDefualtVideo = false;
                    Intent intent = new Intent("android.media.action.VIDEO_CAPTURE");
                    intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
                    intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
                    startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
                }
            } else if (index == 2) {

                isDefualtVideo = false;
                Intent intent = new Intent();
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_CODE_GALLERY_VIDEO);

            }
        }
    };

    AsyncTaskListener extrasListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            setExtrasObjModel();
        }
    };

    AsyncTaskListener getCoupanListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if (result.code == 200 || result.code == 2) {
                if (CoupanCodeDataParser.coupanCodeDatatObj.getMessage().equals("Details checked successfuly.")) {
                    for (int i = 0; i < GltfActivity.shoppingCartProductList.size(); i++) {

                        for (int x = 0; x < CoupanCodeDataParser.coupanCodeDatatObj.getData().getApproved_products().size(); x++) {
                            if (GltfActivity.shoppingCartProductList.get(i).getObj().getProduct_id().equals(CoupanCodeDataParser.coupanCodeDatatObj.getData().getApproved_products().get(x))) {
                                if (CoupanCodeDataParser.coupanCodeDatatObj.getData().getCoupon_relief_type().equals("price_relief")) {
                                    double price = GltfActivity.shoppingCartProductList.get(i).getObj().getPrice_related().getPrice() - CoupanCodeDataParser.coupanCodeDatatObj.getData().getCoupon_relief();
                                    GltfActivity.shoppingCartProductList.get(i).setDisCounts(price);
                                    GltfActivity.shoppingCartProductList.get(i).setDiscountType("price");
                                } else {
                                    double relief = CoupanCodeDataParser.coupanCodeDatatObj.getData().getCoupon_relief() / 100;
                                    double price = GltfActivity.shoppingCartProductList.get(i).getObj().getPrice_related().getPrice() * relief;
                                    double prices = GltfActivity.shoppingCartProductList.get(i).getObj().getPrice_related().getPrice() - price;
                                    double totalPRice = Double.valueOf(Utils.getRoundedDoubleForMoney(prices));
                                    GltfActivity.shoppingCartProductList.get(i).setDisCounts(totalPRice);
                                    GltfActivity.shoppingCartProductList.get(i).setDiscountType("percent");
                                }
                            }
                        }
                    }
                    cartListAdapter = new CartListAdapter(CartActivity.this, GltfActivity.shoppingCartProductList);
                    recycler_itemlist.setAdapter(cartListAdapter);

                    adapterTextData = new AdapterItemTextData(CartActivity.this, GltfActivity.shoppingCartProductList);
                    rvDataInText.setAdapter(adapterTextData);

                    totalPrice = 0;
                    for (int x = 0; x < CoupanCodeDataParser.coupanCodeDatatObj.getData().getApproved_products().size(); x++) {

                        for (int i = 0; i < GltfActivity.shoppingCartProductList.size(); i++) {
                            if (CoupanCodeDataParser.coupanCodeDatatObj.getData().getApproved_products().get(x).equals(GltfActivity.shoppingCartProductList.get(i).getObj().getProduct_id())) {
                                totalPrice += GltfActivity.shoppingCartProductList.get(i).getDisCounts() * GltfActivity.shoppingCartProductList.get(i).getQuantity();
                            }

                        }
                    }

                    double deniedProductPrice = 0;
                    if (CoupanCodeDataParser.coupanCodeDatatObj.getData().getDenied_products().size() > 0) {
                        for (int x = 0; x < CoupanCodeDataParser.coupanCodeDatatObj.getData().getDenied_products().size(); x++) {

                            for (int i = 0; i < GltfActivity.shoppingCartProductList.size(); i++) {

                                if (CoupanCodeDataParser.coupanCodeDatatObj.getData().getDenied_products().get(x).equals(GltfActivity.shoppingCartProductList.get(i).getObj().getProduct_id()))
                                    deniedProductPrice += GltfActivity.shoppingCartProductList.get(i).getObj().getPrice_related().getPrice() * GltfActivity.shoppingCartProductList.get(i).getQuantity();
                            }
                        }

                        commissioned = (totalPrice + deniedProductPrice) * 0.15;
                        totalPriceWithCommisin = totalPrice + deniedProductPrice + commissioned;
                        tv_total.setText("$" + " " + Utils.getRoundedDoubleForMoney(totalPriceWithCommisin));
                    } else {
                        commissioned = (totalPrice + deniedProductPrice) * 0.15;
                        totalPriceWithCommisin = totalPrice + deniedProductPrice + commissioned;
                        tv_total.setText("$" + " " + Utils.getRoundedDoubleForMoney(totalPriceWithCommisin));
                    }


                } else {
                    showCenteredToastmessage(CartActivity.this, "Your coupan does not valid on above products");
                }
            } else {
                showCenteredToastmessage(CartActivity.this, "Inavlid coupon");
            }
        }
    };
    //endregion

    @Override
    public void onBackPressed() {
        isFromCart = false;
        super.onBackPressed();
    }

    private void loadConfirmationDialog() {
        AlertDialog.Builder builderTour = new AlertDialog.Builder(CartActivity.this);
        LayoutInflater inflater = CartActivity.this.getLayoutInflater();
        View dialogViewTour = inflater.inflate(R.layout.selection_dialog_yes_no, null);
        builderTour.setView(dialogViewTour);
        AlertDialog dialogYesNo = builderTour.create();
        dialogYesNo.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogYesNo.show();
        dialogYesNo.setCancelable(true);

        TextView tvMessage = dialogViewTour.findViewById(R.id.tvMessage);
        TextView tvYesClick = dialogViewTour.findViewById(R.id.btnYes);
        TextView tvNoClick = dialogViewTour.findViewById(R.id.btnNo);

        tvMessage.setText("Do you want to clear canvas?");

        tvYesClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GltfActivity.shoppingCartProductList.clear();
                GltfActivity.productList.clear();
                modelDataMap.clear();
                dialogYesNo.dismiss();
                finish();
            }
        });

        tvNoClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogYesNo.dismiss();
            }
        });
    }

    private boolean validateFields() {
        if (etName.getText().toString().isEmpty() && etEmail.getText().toString().trim().isEmpty()) {
            etName.setError("Name was empty");
            etEmail.setError("Email was empty");
            return false;
        }

        if (etName.getText().toString().isEmpty()) {
            etName.setError("Name was empty");
            return false;
        }

        if (etEmail.getText().toString().trim().isEmpty()) {
            etEmail.setError("Email was empty");
            return false;
        }
        return true;
    }

    private class UploadFileAsync extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            //  int code = upLoad2Server(path);
            try {
                uploadVideo(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            dismissProgressDialog();

        }

        @Override
        protected void onPreExecute() {
        }

        private void uploadVideo(String videoPath) throws ParseException, IOException {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("https://api.parsl.io/save/parsl/video/msg");

            FileBody filebodyVideo = new FileBody(new File(videoPath));

            MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("video_msg", filebodyVideo).
                    addTextBody("parsl_id", SaveParselCartParser.saveParselCartObj.getData().getParsl_id());


            HttpEntity entity = reqEntity.build();

            httppost.setEntity(entity);

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();


            if (resEntity != null) {
                resEntity.consumeContent();
            } // end if

            httpclient.getConnectionManager().shutdown();
        }


    }
}
