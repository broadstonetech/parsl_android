package io.parsl.apps.Activities.Wallet;

import static io.parsl.apps.Network.Parser.GetUserCardModelClassDataParser.userCardDataObj;
import static io.parsl.apps.Network.Parser.WalletPublicKeyModelClassDataParser.walletPublicKeyDataObj;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import io.parsl.apps.Adaptors.AdapterAvatarsData;
import io.parsl.apps.Adaptors.AdapterCreditCard;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.Helpers.WalletCardEncryption;
import io.parsl.apps.ModelClass.GetUserCardsModelClass;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;

public class PARSLWalletActivity extends BaseActivity {

    TextInputEditText etSenderName, etRecieverrName, etCardNumber, etValidity, etValidTil, etCVV, etRedeemCode;
    TextView tvTest, tvAmount;
    LinearLayout linearLayout;
    RelativeLayout relativeLayout;
    RecyclerView rvCreditCard;
    List<GetUserCardsModelClass.Data.Cards> cardsList = new ArrayList<>();
    double totalBalance = 0;
    LinearLayoutManager layoutManager;
    private int pos = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rvCreditCard = findViewById(R.id.rvCards);
        etSenderName = findViewById(R.id.etSenderName);
        etRecieverrName = findViewById(R.id.etRecieverName);
        relativeLayout = findViewById(R.id.relative_layout);
        linearLayout = findViewById(R.id.linearlayoit1);
        etCardNumber = findViewById(R.id.etCardNumber);
        etValidity = findViewById(R.id.etValidity);
        etValidTil = findViewById(R.id.etValidTil);
        etCVV = findViewById(R.id.etCVV);
        tvAmount = findViewById(R.id.tvAmount);
        etRedeemCode = findViewById(R.id.etRedeemCode);
        tvTest = findViewById(R.id.tvTest);

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvCreditCard.setLayoutManager(layoutManager);

//        linearLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.image_background_color));
//        relativeLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.image_background_color));


        showProgressDialog(this);
        Api.getWalletPublicKey(this, Utils.getNameSpaceParams(this), publicKeyListner);

        tvTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSenderName.setText(cardsList.get(0).getSender_name());
            }
        });


    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_parsl_wallet;
    }

    private JSONObject getCardParams(String publickey) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            js.put("namespace", UserPrefs.getNameSpace(this));
            //  js.put("namespace","pr5025");
            data.put("public_key", publickey);
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    AsyncTaskListener publicKeyListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.code == 200 || result.code == 2) {

                Api.getUserCards(PARSLWalletActivity.this, getCardParams(walletPublicKeyDataObj.getData().getPublic_key()), getCardsListner);
            } else {
                dismissProgressDialog();
            }
        }
    };
    AsyncTaskListener getCardsListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if (result.code == 200 || result.code == 2) {
                String secretKey = WalletCardEncryption.getSecretKey(walletPublicKeyDataObj.getData().getPublic_key(), userCardDataObj.getData().getTimestamp());
                String sha512 = null;
                try {
                    sha512 = WalletCardEncryption.getSha512(secretKey);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (userCardDataObj.getData().getCheck_sum().equalsIgnoreCase(sha512)) {
                    for (int x = 0; x < userCardDataObj.getData().getCards().size(); x++) {
                        GetUserCardsModelClass.Data.Cards obj = new GetUserCardsModelClass.Data.Cards(WalletCardEncryption.decrypt(userCardDataObj.getData().getCards().get(x).getName_on_card(), secretKey),
                                WalletCardEncryption.decrypt(userCardDataObj.getData().getCards().get(x).getEnc_card_num(), secretKey), WalletCardEncryption.decrypt(userCardDataObj.getData().getCards().get(x).getEnc_cvc(), secretKey)
                                , WalletCardEncryption.decrypt(userCardDataObj.getData().getCards().get(x).getEnc_expiry(), secretKey), userCardDataObj.getData().getCards().get(x).getCard_network()
                                , WalletCardEncryption.decrypt(userCardDataObj.getData().getCards().get(x).getBalance(), secretKey), WalletCardEncryption.decrypt(userCardDataObj.getData().getCards().get(x).getIssue_date(), secretKey)
                                , WalletCardEncryption.decrypt(userCardDataObj.getData().getCards().get(x).getRedeem_code(), secretKey), WalletCardEncryption.decrypt(userCardDataObj.getData().getCards().get(x).getCurrency(), secretKey)
                                , userCardDataObj.getData().getCards().get(x).getSender_name());


                        cardsList.add(obj);
                        double amount = Double.parseDouble(cardsList.get(x).getBalance());
                        totalBalance += amount;
                    }
                    if (cardsList.size() > 0) {
                        setDatainView(0);

                        AdapterCreditCard adapterCreditCard = new AdapterCreditCard(PARSLWalletActivity.this, cardsList);
                        rvCreditCard.setAdapter(adapterCreditCard);
                        adapterCreditCard.setOnItemClickListener(new AdapterAvatarsData.ClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
                                linearLayout.setVisibility(View.VISIBLE);
                                setDatainView(position);
                                adapterCreditCard.notifyDataSetChanged();

                            }
                        });


                    } else {
                        tvAmount.setText("$" + "0");
                    }
                }

            }
        }
    };

    private void setDatainView(int pos) {
        tvAmount.setText("$" + Utils.getRoundedDoubleForMoney(totalBalance));
        etSenderName.setText(cardsList.get(pos).getSender_name());
        etRecieverrName.setText(cardsList.get(pos).getName_on_card());
        etCardNumber.setText(cardsList.get(pos).getEnc_card_num());
        etCVV.setText(cardsList.get(pos).getEnc_cvc());
        etRedeemCode.setText(cardsList.get(pos).getRedeem_code());
        etValidity.setText(Utils.getDate(Long.parseLong(cardsList.get(pos).getIssue_date()) * 1000));
        etValidTil.setText(cardsList.get(pos).getEnc_expiry());
    }
}