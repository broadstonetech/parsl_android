package io.parsl.apps.Activities.BottomSheetFragment;

import static io.parsl.apps.Activities.ARActivities.GltfActivity.isSaveOtp;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.rlChangePassword;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.rlMyParsls;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.rlUploadPhotos;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.tvEmail;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.tv_signout_fragments_settings;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.viewChangePass;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.viewMyparsls;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.viewUploadPhotos;
import static io.parsl.apps.Activities.CartActivity.llSignIn;
import static io.parsl.apps.Network.Parser.UserLoginParser.userLoginObj;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import io.parsl.apps.Activities.ARActivities.GltfActivity;
import io.parsl.apps.Activities.ARActivities.ProfileActivity;
import io.parsl.apps.Activities.CartActivity;
import io.parsl.apps.Activities.ForgotPassWordActivity;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;

public class BottomSheetLogin extends BottomSheetDialogFragment implements View.OnClickListener {

    AppCompatButton btn_google_login;
    EditText emailID, edtPass;
    RelativeLayout rlSignIn, rlContinueWithoutLogin;
    String socialLogin = "";
    TextView tvSkip, tvForgotPass, tvTermsAndConditions, tvPrivacyPlicy;
    CheckBox userConsent_cb, userConsent_cb1;
    Context ac;
    Dialog dialogRef;

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        //Set the custom view
        View view = LayoutInflater.from(getContext()).inflate(R.layout.bottom_sheet_login, null);
        dialog.setContentView(view);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();


        btn_google_login = view.findViewById(R.id.google_login_btn);
        rlSignIn = view.findViewById(R.id.rlSignIn);
        edtPass = view.findViewById(R.id.edtPass);
        emailID = view.findViewById(R.id.emailID);
        tvSkip = view.findViewById(R.id.tvSkip);
        tvForgotPass = view.findViewById(R.id.tvForgotPass);
        tvTermsAndConditions = view.findViewById(R.id.tvTermsAndConditions);
        tvPrivacyPlicy = view.findViewById(R.id.tvPrivacyPlicy);
        userConsent_cb = view.findViewById(R.id.userConsent_cb);
        userConsent_cb1 = view.findViewById(R.id.userConsent_cb1);
        rlContinueWithoutLogin = view.findViewById(R.id.rlContinueWithoutLogin);

        btn_google_login.setOnClickListener(this);
        rlSignIn.setOnClickListener(this);
        tvSkip.setOnClickListener(this);
        userConsent_cb.setOnClickListener(this);
        tvForgotPass.setOnClickListener(this);
        tvTermsAndConditions.setOnClickListener(this);
        tvPrivacyPlicy.setOnClickListener(this);
        rlContinueWithoutLogin.setOnClickListener(this);

        userConsent_cb.setChecked(true);
        userConsent_cb1.setChecked(true);

        dialogRef = getDialog();

    }

    @Override
    public void onStart() {
        super.onStart();
        dialogRef = getDialog();
        if (dialogRef != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialogRef.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == rlSignIn.getId()) {
            socialLogin = "";
            if (isFormValidated()) {
                if (!userConsent_cb.isChecked() || !userConsent_cb1.isChecked()) {
                    Utils.showCenteredToastmessage(getActivity(), getResources().getString(R.string.error_text_terms_condotions));
                } else {
                    if (getActivity() instanceof GltfActivity) {
                        ((GltfActivity) requireActivity()).showProgressDialog(getActivity());
                        Api.userLogin(getActivity(), getParams(), loginListner);
                    } else {
                        ((BaseActivity) requireActivity()).showProgressDialog(getActivity());
                        Api.userLogin(getActivity(), getParams(), loginListner);
                    }
                }
            } else {
                setErrors();
            }
            if (!userConsent_cb.isChecked() && !userConsent_cb1.isChecked()) {
                Utils.showCenteredToastmessage(getActivity(), getResources().getString(R.string.error_text_terms_condotions));
            }
        } else if (v.getId() == btn_google_login.getId()) {
            socialLogin = "google";
            if (userConsent_cb.isChecked() && userConsent_cb1.isChecked()) {
                if (getActivity() instanceof GltfActivity) {
                    ((GltfActivity) getActivity()).googleSignIn();
                } else {
                    //  ((BaseActivity) getActivity()).showProgressDialog(getActivity());
                    ((BaseActivity) getActivity()).googleSignIn(getActivity());

                }
            } else {
                if (!userConsent_cb.isChecked())
                    Utils.showCenteredToastmessage(getActivity(), getResources().getString(R.string.error_text_terms_condotions));
                else
                    Utils.showCenteredToastmessage(getActivity(), getResources().getString(R.string.error_text_terms_condotions));
            }
        } else if (v.getId() == tvSkip.getId()) {
            dialogRef.dismiss();
        } else if (v.getId() == tvForgotPass.getId()) {
            if (getActivity() instanceof GltfActivity)
                ((GltfActivity) getActivity()).AddActivityAndRemoveIntentTransition(ForgotPassWordActivity.class);
            else
                ((BaseActivity) getActivity()).AddActivityAndRemoveIntentTransitionWithoutFinish(ForgotPassWordActivity.class);
        } else if (v.getId() == tvPrivacyPlicy.getId()) {
            Utils.openUrlInChrome(getResources().getString(R.string.privacy_policy_url), getActivity());
        } else if (v.getId() == tvTermsAndConditions.getId()) {
            Utils.openUrlInChrome(getResources().getString(R.string.terms_conditions_url), getActivity());
        } else if (v.getId() == rlContinueWithoutLogin.getId()) {
            dialogRef = getDialog();
            dialogRef.dismiss();
        }
    }

    public void dismisFragment() {
        Dialog dialog = getDialog();
        dialog.dismiss();
    }

    private boolean isFormValidated() {
        return !emailID.getText().toString().equals("") &&
                !edtPass.getText().toString().equals("") &&
                Utils.isValidEmail(emailID.getText().toString()) &&
                Utils.isValidPassword(edtPass.getText().toString());
    }

    private JSONObject getParams() {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            js.put("app_id", GeneralConstants.APP_ID);
            js.put("sociallogin", socialLogin);
            data.put("email", emailID.getText().toString());
            data.put("old_mail", UserPrefs.getEmail(getActivity()));
            //      data.put("password", "");
            if (getActivity() instanceof GltfActivity)
                data.put("password", edtPass.getText().toString());
            else
                data.put("password", edtPass.getText().toString());


            data.put("device_token", UserPrefs.getFCMUserToken(getActivity()));
            js.put("data", data);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    private void setErrors() {
        if (emailID.getText().toString().equals("")) {
            emailID.setError(getResources().getString(R.string.EMPTY_EMAIL));
        } else if (Utils.isValidPassword(edtPass.getText().toString()) || edtPass.getText().toString().equals("")) {
            edtPass.setError(getResources().getString(R.string.EMPTY_PASSWORD));
        } else if (!userConsent_cb.isChecked()) {
            userConsent_cb.setError("You must accept terms and conditions");
        } else if (!userConsent_cb1.isChecked()) {
            userConsent_cb1.setError("You must accept our privacy policy");
        }
    }

    AsyncTaskListener loginListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (getActivity() instanceof GltfActivity) {
                ((GltfActivity) requireActivity()).dismissProgressDialog();
                if (isSaveOtp) {
                    ((GltfActivity) requireActivity()).saveParslApiCall();
                    isSaveOtp = false;
                    dismisFragment();
                }
            } else {
                ((BaseActivity) requireActivity()).dismissProgressDialog();
                dismisFragment();
            }
            if (userLoginObj.isStatus()) {
                UserPrefs.setNameSpace(getActivity(), userLoginObj.getData().getNamespace());
                UserPrefs.setEmail(getActivity(), emailID.getText().toString().trim());
                dismisFragment();
            } else {
                Utils.showCenteredToastmessage(getActivity(), "Please Try Again!!!");
            }

            if (getActivity() instanceof ProfileActivity) {
                if (!UserPrefs.getNameSpace(getActivity()).isEmpty()) {
                    Utils.showView();
                    tv_signout_fragments_settings.setText("Sign out");
                    tvEmail.setText(UserPrefs.getEmail(getActivity()));
                }
            } else if (getActivity() instanceof CartActivity) {
                llSignIn.setVisibility(View.GONE);
            }
        }
    };
}
