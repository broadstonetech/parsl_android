package io.parsl.apps.Activities.ARActivities;

import static io.parsl.apps.Activities.ARActivities.GltfActivity.communicationBridgeobj;
import static io.parsl.apps.Activities.CartActivity.fragmentLogin;
import static io.parsl.apps.Utilities.Util.Utils.hasPermissions;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import io.parsl.apps.Activities.BottomSheetFragment.BottomSheetLogin;
import io.parsl.apps.Activities.ChangePaswordActivity;
import io.parsl.apps.Activities.MainActivity;
import io.parsl.apps.Activities.ReadyPlayerMe.MyAvatarsActivity;
import io.parsl.apps.AgentChatActivity.UsersChatActivity;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.BuildConfig;
import io.parsl.apps.MyParslsActivity.MyParslsActivity;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Notifications.NotificationActivity;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;
import io.parsl.apps.VideoActivity;

public class ProfileActivity extends BaseActivity implements View.OnClickListener {

    private static final int CAMERA_PIC_REQUEST = 1001 ;
    TextView tvGeneralQueries,tvRefund,tvHowToSendParsl,tvHowToRedeemParsl,tvLegals,tvLiveSupport,tvClearCache,tvUpdates,tvVerson,
            tvHelpSupport,tvAppSettings,tvNotifications,tvMyAvatar,tvPersonal,tv_change_pass_settings_fragment,tvTerms,tvPrivacyPlicy,tvMyParsl,tvUploadPhoto;
    public static TextView tv_signout_fragments_settings,tvEmail;
    public static RelativeLayout rlChangePassword,rlMyParsls,rlUploadPhotos;
    public static boolean isVideo = true;
    boolean clickEventPersonal = false, clickEventAppSettings = false, clickEventHelp = false, clickEventLegal = false;
    public static  View viewChangePass,viewMyparsls,viewUploadPhotos;
    LinearLayout ll_child_app_fragment_settings,ll_child_personal_fragment_settings,ll_child_help_fragment_settings,ll_child_legal;
    private Intent cameraIntent;
    ActivityResultLauncher<Intent> mPermissions;
    private static String[] PERMISSIONS = null;
    int PERMISSION_ALL = 1;
    private Uri imageToUploadUri;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tvLiveSupport = findViewById(R.id.tvLiveSupport);
        tvGeneralQueries = findViewById(R.id.tvGeneralQueries);
        tvRefund = findViewById(R.id.tvRefund);
        tvHowToSendParsl = findViewById(R.id.tvHowToSendParsl);
        tvHowToRedeemParsl = findViewById(R.id.tvHowToRedeemParsl);
        tvClearCache = findViewById(R.id.tvClearCache);
        tvUpdates = findViewById(R.id.tvUpdates);
        tvVerson = findViewById(R.id.tvVerson);
        tvNotifications = findViewById(R.id.tvNotifications);
        tvMyAvatar = findViewById(R.id.tvMyAvatar);
        tvMyParsl = findViewById(R.id.tvMyParsl);
        tvUploadPhoto = findViewById(R.id.tvUploadPhoto);
        rlChangePassword = findViewById(R.id.rlChangePassword);
        rlMyParsls = findViewById(R.id.rlMyParsls);
        rlUploadPhotos = findViewById(R.id.rlUploadPhotos);
        viewChangePass = findViewById(R.id.viewChangePass);
        viewMyparsls = findViewById(R.id.viewMyparsls);
        viewUploadPhotos = findViewById(R.id.viewUploadPhotos);
        tvPersonal = findViewById(R.id.tv_profile_fragment_settings);
        tvAppSettings = findViewById(R.id.tv_app_settings_fragment_settings);
        tvHelpSupport = findViewById(R.id.tv_help_n_support_fragments_settings);
        tvLegals = findViewById(R.id.tvLegals);
        tvTerms = findViewById(R.id.tvTerms);
        tvPrivacyPlicy = findViewById(R.id.tvPrivacyPlicy);
        tvEmail = findViewById(R.id.tvEmail);
        tv_signout_fragments_settings = findViewById(R.id.tv_signout_fragments_settings);
        tv_change_pass_settings_fragment = findViewById(R.id.tv_change_pass_settings_fragment);
        ll_child_app_fragment_settings = findViewById(R.id.ll_child_app_fragment_settings);
        ll_child_personal_fragment_settings = findViewById(R.id.ll_child_personal_fragment_settings);
        ll_child_help_fragment_settings = findViewById(R.id.ll_child_help_fragment_settings);
        ll_child_legal = findViewById(R.id.ll_child_legal);

        //region ClickListeners
        tvLiveSupport.setOnClickListener(this);
        tvGeneralQueries.setOnClickListener(this);
        tvRefund.setOnClickListener(this);
        tvHowToSendParsl.setOnClickListener(this);
        tvHowToRedeemParsl.setOnClickListener(this);
        tvClearCache.setOnClickListener(this);
        tvUpdates.setOnClickListener(this);
        tvNotifications.setOnClickListener(this);
        tvMyAvatar.setOnClickListener(this);
        tvMyAvatar.setOnClickListener(this);
        tvPersonal.setOnClickListener(this);
        tvAppSettings.setOnClickListener(this);
        tvHelpSupport.setOnClickListener(this);
        tvLegals.setOnClickListener(this);
        tvTerms.setOnClickListener(this);
        tvPrivacyPlicy.setOnClickListener(this);
        tvMyParsl.setOnClickListener(this);
        tvUploadPhoto.setOnClickListener(this);
        tv_change_pass_settings_fragment.setOnClickListener(this);
        tv_signout_fragments_settings.setOnClickListener(this);
        //endregion

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            String versionCode = String.valueOf(pInfo.versionCode);

            tvVerson.setText("Version"+" "+version+"("+versionCode+")");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if(!UserPrefs.getNameSpace(this).isEmpty()){
            rlChangePassword.setVisibility(View.VISIBLE);
            rlMyParsls.setVisibility(View.VISIBLE);
        //    rlUploadPhotos.setVisibility(View.VISIBLE);
            viewChangePass.setVisibility(View.VISIBLE);
            viewMyparsls.setVisibility(View.VISIBLE);
            viewUploadPhotos.setVisibility(View.VISIBLE);
            tv_signout_fragments_settings.setText("Sign out");
            tvEmail.setText(UserPrefs.getEmail(this));
        }else {
            rlChangePassword.setVisibility(View.GONE);
            rlMyParsls.setVisibility(View.GONE);
            rlUploadPhotos.setVisibility(View.GONE);
            viewChangePass.setVisibility(View.GONE);
            viewMyparsls.setVisibility(View.GONE);
            viewUploadPhotos.setVisibility(View.GONE);
            tv_signout_fragments_settings.setText("Sign in / Sign up");
            tv_signout_fragments_settings.setTextColor(getResources().getColor(R.color.colorPrimary));

        }

        PERMISSIONS = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };

        mPermissions = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if(result.getResultCode() == GltfActivity.RESULT_OK){
                    Utils.showCenteredToastmessage(ProfileActivity.this    ,"Permission given ");
                }

            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.profile_activity_layout;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_PIC_REQUEST) {

            Utils.showCenteredToastmessage(ProfileActivity.this,"uploaded");
        }
    }

    //region on clicks
    @Override
    public void onClick(View v) {
        if(v.getId() == tvGeneralQueries.getId()){
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("plain/text");
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"apps@parsl.io"});
            intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
            startActivity(Intent.createChooser(intent, ""));
        }else if(v.getId() == tvLiveSupport.getId()){
            loadLiveSupportDialog();
        }else if(v.getId() == tvRefund.getId()){
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("plain/text");
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"apps@parsl.io"});
            intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
            startActivity(Intent.createChooser(intent, ""));
        }else if(v.getId() == tvHowToSendParsl.getId()){
            isVideo = true;
            AddActivityAndRemoveIntentTransitionWithoutFinish(VideoActivity.class);
        }else if(v.getId() == tvHowToRedeemParsl.getId()){
            isVideo = false;
            AddActivityAndRemoveIntentTransitionWithoutFinish(VideoActivity.class);
        }else if(v.getId() == tvUpdates.getId()){
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }else if(v.getId() == tvClearCache.getId()){
            //cacheClearDialog();
            Utils.showCenteredToastmessage(ProfileActivity.this,"Caches has been cleared");
        }else if(v.getId() == tvNotifications.getId()){
            AddActivityAndRemoveIntentTransitionWithoutFinish(NotificationActivity.class);
        }else if(v.getId() == tvMyAvatar.getId()){
            if(UserPrefs.getNameSpace(ProfileActivity.this).equalsIgnoreCase("")){
                fragmentLogin = new BottomSheetLogin();
                fragmentLogin.show(getSupportFragmentManager(), "");
            }else
            AddActivityAndRemoveIntentTransition(MyAvatarsActivity.class);
        }else if (v.getId() == tvPersonal.getId()){
            if(!clickEventPersonal) {
                hidellChild();
                ll_child_personal_fragment_settings.setVisibility(View.VISIBLE);
                clickEventPersonal = true;
            }else {
                hidellChild();
                clickEventPersonal = false;
            }
        }else if (v.getId() == tvAppSettings.getId()){
            if(!clickEventAppSettings) {
                hidellChild();
                ll_child_app_fragment_settings.setVisibility(View.VISIBLE);
                clickEventAppSettings = true;
            }else {
                hidellChild();
                clickEventAppSettings = false;
            }
        }else if (v.getId() == tvHelpSupport.getId()){
            if(!clickEventHelp) {
                hidellChild();
                ll_child_help_fragment_settings.setVisibility(View.VISIBLE);
                clickEventHelp = true;
            }else {
                hidellChild();
                clickEventHelp = false;
            }
        }else if (v.getId() == tvPrivacyPlicy.getId()){
            Utils.openUrlInChrome(getResources().getString(R.string.privacy_policy_url),this);
        }else if(v.getId() == tv_change_pass_settings_fragment.getId()){
            AddActivityAndRemoveIntentTransitionWithoutFinish(ChangePaswordActivity.class);
        }else if(v.getId() == tv_signout_fragments_settings.getId()){
            if(tv_signout_fragments_settings.getText().equals("Sign in / Sign up")){
                fragmentLogin = new BottomSheetLogin();
                fragmentLogin.show(getSupportFragmentManager(), "");
            }else {
                UserPrefs.setEmail(ProfileActivity.this, "");
                UserPrefs.setNameSpace(ProfileActivity.this, "");
                finish();
            }
        }else if(v.getId() == tvTerms.getId()){
            Utils.openUrlInChrome(getResources().getString(R.string.terms_conditions_url),this);
        }else if(v.getId() == tvLegals.getId()){
            if(!clickEventLegal) {
                hidellChild();
                ll_child_legal.setVisibility(View.VISIBLE);
                clickEventLegal = true;
            } else {
                hidellChild();
                clickEventLegal = false;
            }
        }else if(v.getId() == tvMyParsl.getId() ){
            AddActivityAndRemoveIntentTransition(MyParslsActivity.class);
            finish();
        }else if(v.getId() == tvUploadPhoto.getId() ){
            if (!hasPermissions(ProfileActivity.this, PERMISSIONS)) {
                Utils.requestPermission(ProfileActivity.this,mPermissions,PERMISSIONS,PERMISSION_ALL,ProfileActivity.this);
            }
                AddActivityAndRemoveIntentTransitionWithoutFinish(MainActivity.class);

        }
    }
    //endregion

    //region functions
    private void hidellChild(){
        ll_child_app_fragment_settings.setVisibility(View.GONE);
        ll_child_personal_fragment_settings.setVisibility(View.GONE);
        ll_child_help_fragment_settings.setVisibility(View.GONE);
        ll_child_legal.setVisibility(View.GONE);
    }

    private void loadLiveSupportDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Live Support");
        alertDialog.setMessage("Enter email").setPositiveButton("Submit",null).setNegativeButton("May be later",null);
        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        alertDialog.setView(input);

        final AlertDialog mAlertDialog = alertDialog.create();

        mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button positiveBtn = mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                Button negativeBtn = mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);

                positiveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String email = input.getText().toString();
                        if(isValidEmail(email)) {
                            GltfActivity.emailLiveSupport = email;
                            AddActivityAndRemoveIntentTransitionWithoutFinish(UsersChatActivity.class);
                        }else {
                            showCenteredToastmessage(ProfileActivity.this,"Invalid email");
                            input.setError("Invalid email");
                        }
                    }
                });

                negativeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAlertDialog.dismiss();
                    }
                });

            }
        });
        mAlertDialog.show();
    }
    //endregion
}