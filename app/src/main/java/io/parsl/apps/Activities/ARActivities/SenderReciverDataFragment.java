package io.parsl.apps.Activities.ARActivities;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputEditText;

import io.parsl.apps.BaseActivity;
import io.parsl.apps.ModelClass.SenderReciverDataClass;

import io.parsl.apps.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static android.app.Activity.RESULT_OK;


public class SenderReciverDataFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    private static final int REQUEST_CODE = 100;
    ImageView imgRecieverNo, imgSenderNo;
    private TextInputEditText senderEmail, senderPhoneNo, senderName, recieverEmail, recieverPhone, recieverName, etLocation, etSenderLocation;
    public static SenderReciverDataClass dataObj = new SenderReciverDataClass();
    TextView tvSubmit;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sender_reciever_layout_fragment, container, false);

        imgSenderNo = view.findViewById(R.id.imgSenderNo);
        senderEmail = view.findViewById(R.id.senderEmail);
        senderName = view.findViewById(R.id.senderName);
        senderPhoneNo = view.findViewById(R.id.senderPhoneNo);

        imgRecieverNo = view.findViewById(R.id.imgRecieverNo);
        recieverEmail = view.findViewById(R.id.email);
        recieverName = view.findViewById(R.id.name);
        recieverPhone = view.findViewById(R.id.phoneNo);
        etLocation = view.findViewById(R.id.etLocation);
        etSenderLocation = view.findViewById(R.id.etSenderLocation);

        tvSubmit = view.findViewById(R.id.tvSubmit);

        imgSenderNo.setOnClickListener(this);
        imgRecieverNo.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent intent) {
        if (requestCode == 200) {
            if (resultCode == RESULT_OK) {
                if (!getPickedData(intent).get(0).contains("+")) {
                    recieverPhone.setText(getPickedData(intent).get(0).toString().substring(1));
                } else {
                    recieverPhone.setText(getPickedData(intent).get(0).toString());
                }
                etLocation.setText(getPickedData(intent).get(2));
                recieverName.setText(getPickedData(intent).get(1).toString());
            }
        } else if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                if (!getPickedData(intent).get(0).contains("+")) {
                    senderPhoneNo.setText(getPickedData(intent).get(0).toString().substring(1));
                } else {
                    senderPhoneNo.setText(getPickedData(intent).get(0).toString());
                }
                etSenderLocation.setText(getPickedData(intent).get(2));
                senderName.setText(getPickedData(intent).get(1).toString());

            }
        }
    }

    ;

    @Override
    public void onClick(View v) {
        if (v.getId() == imgSenderNo.getId()) {
            pickContactsIntent(100);
        } else if (v.getId() == imgRecieverNo.getId()) {
            pickContactsIntent(200);
        } else if (v.getId() == tvSubmit.getId()) {
            if (validateFields()) {
                if (!senderPhoneNo.getText().toString().contains("+")) {
                    ((BaseActivity) getActivity()).showCenteredToastmessage(getActivity(), "Please add country code in sender number");
                } else if (!recieverPhone.getText().toString().contains("+")) {
                    ((BaseActivity) getActivity()).showCenteredToastmessage(getActivity(), "Please add country code in reciever number");
                } else {
                    dataObj.setRecieverEmail(recieverEmail.getText().toString());
                    dataObj.setRecieverPhone(recieverPhone.getText().toString());
                    dataObj.setRecieverName(recieverName.getText().toString());
                    dataObj.setRecieverLocation(etLocation.getText().toString());

                    dataObj.setSenderEmail(senderEmail.getText().toString());
                    dataObj.setSenderPhone(senderPhoneNo.getText().toString());
                    dataObj.setSenderName(senderName.getText().toString());
                    dataObj.setSenderLocation(etSenderLocation.getText().toString());

                    dismiss();
                }
            }
        }
    }

    private List<String> getPickedData(Intent intent) {
        Uri uri = intent.getData();
        List<String> data = new ArrayList<>();
        String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};

        Cursor cursor = getActivity().getContentResolver().query(uri, projection,
                null, null, null);
        cursor.moveToFirst();

        int numberColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        String number = cursor.getString(numberColumnIndex);
        data.add(number);
        int nameColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        String name = cursor.getString(nameColumnIndex);
        data.add(name);

        Uri postal_uri = ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI;
        Cursor postal_cursor = getActivity().getContentResolver().query(postal_uri, null, ContactsContract.Data.CONTACT_ID + "=" + getContactIDFromNumber(number), null, null);
        String cntry = "", strt = "", cty = "";
        while (postal_cursor.moveToNext()) {
            strt = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
            cty = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
            cntry = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
        }
        data.add(strt + " " + cty + " " + cntry);
        postal_cursor.close();
        return data;
    }

    public String getContactIDFromNumber(String contactNumber) {
        contactNumber = Uri.encode(contactNumber);
        int phoneContactID = new Random().nextInt();
        Cursor contactLookupCursor = getActivity().getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, contactNumber), new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID}, null, null, null);
        while (contactLookupCursor.moveToNext()) {
            phoneContactID = contactLookupCursor.getInt(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
        }
        contactLookupCursor.close();

        return String.valueOf(phoneContactID);
    }

    private void pickContactsIntent(int code) {
        Uri uri = Uri.parse("content://contacts");
        Intent intent = new Intent(Intent.ACTION_PICK, uri);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, code);
    }

    private boolean validateFields() {
        if (senderName.getText().toString().length() == 0) {
            senderName.setError("Name was empty");
            return false;
        }
        if (senderPhoneNo.getText().toString().length() == 0) {
            senderPhoneNo.setError("Phone was empty");
            return false;
        }
        if (recieverName.getText().toString().length() == 0) {
            recieverName.setError("Name was empty");
            return false;
        }
        if (recieverPhone.getText().toString().length() == 0) {
            recieverPhone.setError("Phone was empty");
            return false;
        }
        return true;
    }
}