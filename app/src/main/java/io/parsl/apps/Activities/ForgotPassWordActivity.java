package io.parsl.apps.Activities;

import static io.parsl.apps.Network.Parser.ForgotPasswordClassDataParser.forgotPasswordDataObj;
import static io.parsl.apps.Network.Parser.UserLoginParser.userLoginObj;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import io.parsl.apps.BaseActivity;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;

public class ForgotPassWordActivity extends BaseActivity implements View.OnClickListener {

    EditText etEmail;
    RelativeLayout rlSubmit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        etEmail = findViewById(R.id.etEmail);
        rlSubmit = findViewById(R.id.rlSubmit);

        rlSubmit.setOnClickListener(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_forgot_pass_word;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == rlSubmit.getId()) {
            if (isValidEmail(etEmail.getText().toString()))
                submitForgetPassApiCall();
        }
    }

    private void submitForgetPassApiCall() {
        showProgressDialog(this);
        Api.forgetPassword(ForgotPassWordActivity.this, getParams(), forgetPassListner);
    }

    //region api calls
    private JSONObject getParams() {
        JSONObject data = new JSONObject();

        try {
            data.put("app_id", GeneralConstants.APP_ID);
            data.put("user_email", etEmail.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;
    }

    AsyncTaskListener forgetPassListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if (forgotPasswordDataObj.isStatus()) {
                Utils.showCenteredToastmessage(ForgotPassWordActivity.this, forgotPasswordDataObj.getMessage());
                finish();
            } else {
                Utils.showCenteredToastmessage(ForgotPassWordActivity.this, forgotPasswordDataObj.getMessage());
            }

        }
    };
    //endregion
}