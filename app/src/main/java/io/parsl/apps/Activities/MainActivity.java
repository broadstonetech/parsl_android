
package io.parsl.apps.Activities;

import static io.parsl.apps.Utilities.Util.Utils.hasPermissions;

import android.Manifest;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.mime.HttpMultipartMode;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;
import cz.msebera.android.httpclient.entity.mime.content.FileBody;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;
import io.parsl.apps.Activities.ARActivities.GltfActivity;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.Helpers.ZipManager;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;


public class MainActivity extends AppCompatActivity {
    private static int BUFFER_SIZE = 8192;
    Button uploadImgBtn;
    ImageView imageView;

    private static final String tag = "MainActivity";
    File zipFilePath;
    String stringPathName;
    private static String[] PERMISSIONS = null;
    int PERMISSION_ALL = 1;

    List<String> s = new ArrayList<>();
    ArrayList<String> abc = new ArrayList<>();
    private long zipFileLength;
    ActivityResultLauncher<Intent> mPermissions;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_main_activity);
        uploadImgBtn = findViewById(R.id.uploadImgBtnID);
        imageView = findViewById(R.id.imageView);
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }

        PERMISSIONS = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        };

        if (!hasPermissions(MainActivity.this, PERMISSIONS)) {
            Utils.requestPermission(MainActivity.this,mPermissions,PERMISSIONS,PERMISSION_ALL,MainActivity.this);
        }

        mPermissions = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if(result.getResultCode() == MainActivity.RESULT_OK){
                    Utils.showCenteredToastmessage(MainActivity.this    ,"Permission given ");
                }

            }
        });

        uploadImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1100);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        {
            Uri selectedUri ;
            Uri UriList = Uri.parse("");
            try {
                //create a folder in internal storage
                String root = Environment.getExternalStorageDirectory().getAbsolutePath();
                File defaultFile = new File(root + "/PARSL_IMAGES");
                if (!defaultFile.exists()) {
                    defaultFile.mkdirs();
                }
                if(defaultFile.exists()){
                }

                if (requestCode == 1100 && resultCode == RESULT_OK) {
                    for(int i=0; i<data.getClipData().getItemCount();i++){
                        selectedUri = data.getClipData().getItemAt(i).getUri();
                        //ClipData is the name of an Array or an Obj containing all the data


                        //<editor-fold desc="no use code i dont want to copy image from one folder to pther">
                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())+i;
                        String imageFileName = timeStamp + ".jpg";// 123.jpg


                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedUri);
                        File file = new File(defaultFile, imageFileName);

                        try {
                            FileOutputStream out = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                            out.flush();
                            out.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //</editor-fold>
                        s.add(getPath(MainActivity.this,Uri.fromFile(file)));
                    }

                    // /sdcard/ZipDemo/textfile.txt
                    File zipImgsFolder = new File(root + "/ZIP_IMAGES");
                    if (!zipImgsFolder.exists()) {
                        zipImgsFolder.mkdirs();
                    }
                    if(zipImgsFolder.exists()){
                    }
                    String inputPath = Environment.getExternalStorageDirectory().getPath()+ "/ZIP_IMAGES/";
                    String inputFile = "Apply.zip";
                    // first parameter is d files second parameter is zip
                    // file name
                    ZipManager zipManager = new ZipManager();

                    // calling the zip function
                    zipManager.zip(s, inputPath + inputFile);

                    //delete PARSL_IMAGES folder because it is of no use anymore!
                    defaultFile.deleteOnExit();


                    //code to upload file to api
                    File zipFilePath = getLatestFilefromDir(root + "/ZIP_IMAGES");
                    stringPathName  = zipFilePath.getPath();

                    zipFileLength = zipFilePath.length();
                    Toast.makeText(this, "File Size in Bytes Is : "+zipFileLength, Toast.LENGTH_LONG).show();
                    new UploadFileAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                }
            } catch (Exception ex) {
                Toast.makeText(MainActivity.this, "Exception1",
                        Toast.LENGTH_SHORT).show();
                Log.e("Some Tag", ex.getMessage(), ex);
            }
        }
    }


    //File Upload API code
    private class UploadFileAsync extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String data = "";
            try {
                data = uploadZipFile(stringPathName);
            } catch (IOException | NoSuchAlgorithmException | JSONException e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(MainActivity.this,"Upload Result =" +result,Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onPreExecute() {
        }

        private String uploadZipFile(String zipPath) throws ParseException, IOException, NoSuchAlgorithmException, JSONException {

            JSONObject result = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("https://api.parsl.io/upload/user/model");

            FileBody fileBodyZip = new FileBody(new File(zipPath));

            String size =  getSHA256(String.valueOf(zipFileLength));
            MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("zipfile", fileBodyZip).addTextBody("app_id", "parsl_android").addTextBody("namespace","pr2326").addTextBody("size", size);

            HttpEntity entity = reqEntity.build();

            httppost.setEntity(entity);

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();


            if (resEntity != null) {
               // resEntity.consumeContent();
                String retSrc = EntityUtils.toString(resEntity);
                // parsing JSON
                 result = new JSONObject(retSrc);
            } // end if
            httpclient.getConnectionManager().shutdown();


            return result.toString();
        }
    }

    private File getLatestFilefromDir(String dirPath){
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            return null;
        }

        File lastModifiedFile = files[0];
        for (int i = 1; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
            }
        }
        return lastModifiedFile;
    }

    //file size in bytes
    public String getSHA256(String pas) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(pas.getBytes("UTF-8"));
        byte[] digest = md.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            sb.append(String.format("%02x", digest[i] & 0xFF));
        }
        return sb.toString();
    }

    //code to get Absolute path
    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = true;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{split[1]};
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

}