package io.parsl.apps.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import io.parsl.apps.BaseActivity;
import io.parsl.apps.ModelClass.LocalTestModelClass;
import io.parsl.apps.R;

public class SendGiftActvity extends BaseActivity {

    LocalTestModelClass obj = new LocalTestModelClass();
    RecyclerView rvCategoriesData;
    Button btnAddToCart;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rvCategoriesData = findViewById(R.id.rvCategoriesData);
        btnAddToCart = findViewById(R.id.btnAddToCart);
        rvCategoriesData.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        obj.setName("Pizza");
        obj.setVenderName("Domino");

        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_send_gift_actvity;
    }
}