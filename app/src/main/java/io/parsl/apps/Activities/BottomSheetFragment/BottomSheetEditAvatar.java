package io.parsl.apps.Activities.BottomSheetFragment;

import static io.parsl.apps.Adaptors.MyAvtarsAdapter.avatarID;
import static io.parsl.apps.Adaptors.MyAvtarsAdapter.avatarPos;
import static io.parsl.apps.Network.Parser.AvatarDetailModelParser.avatarDetailsDataObj;
import static io.parsl.apps.Network.Parser.CreateAvatarParser.createAvatarObj;
import static io.parsl.apps.Network.Parser.EditAvatarModelClassDataParser.editAvatarDataObj;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import io.parsl.apps.Activities.ReadyPlayerMe.MyAvatarsActivity;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;
import okhttp3.internal.Util;

public class BottomSheetEditAvatar extends BottomSheetDialogFragment implements View.OnClickListener {

    EditText avatarTitle, edtDesc, edtUrl;
    RelativeLayout rlSubmit;
    TextView tvCancel;
    Dialog dialogEditAvatar;

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.edit_avatar_fragment, null);
        dialog.setContentView(view);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        rlSubmit = view.findViewById(R.id.rlSubmit);
        edtDesc = view.findViewById(R.id.edtDesc);
        avatarTitle = view.findViewById(R.id.avatarTitle);
        edtUrl = view.findViewById(R.id.edtUrl);
        tvCancel = view.findViewById(R.id.tvCancel);


        rlSubmit.setOnClickListener(this);
        tvCancel.setOnClickListener(this);

        dialogEditAvatar = getDialog();


    }


    @Override
    public void onClick(View v) {
        if (v.getId() == rlSubmit.getId()) {
            if (!avatarTitle.getText().toString().isEmpty() || !edtDesc.getText().toString().isEmpty()) {
                ((MyAvatarsActivity) requireActivity()).showProgressDialog(getActivity());
                Api.editAvtar(getActivity(), getParams(), editAvatarListner);
            } else {
                Utils.showCenteredToastmessage(getActivity(), "You must enter title or description");
            }
        } else if (v.getId() == tvCancel.getId()) {
            dialogEditAvatar.dismiss();
        }
    }

    private JSONObject getParams() {
        JSONObject data = new JSONObject();
        try {
            data.put("avatar_id", avatarID);
            data.put("namespace", UserPrefs.getNameSpace(getActivity()));
            data.put("avatar_title", avatarTitle.getText().toString());
            data.put("avatar_description", edtDesc.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;
    }

    AsyncTaskListener editAvatarListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            ((BaseActivity) getActivity()).dismissProgressDialog();
            if (editAvatarDataObj.isSuccess()) {
                avatarDetailsDataObj.getData().get(avatarPos).setTitle(avatarTitle.getText().toString());
                avatarDetailsDataObj.getData().get(avatarPos).setDescription(edtDesc.getText().toString());
                ((MyAvatarsActivity) getActivity()).updateUI();
                dialogEditAvatar.dismiss();
            } else {
                Utils.showCenteredToastmessage(getActivity(), "Please Try Again!!!");
            }
        }
    };

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
