package io.parsl.apps.Activities.ARActivities;

import static android.os.Build.VERSION.SDK_INT;
import static io.parsl.apps.Activities.CartActivity.fragmentLogin;
import static io.parsl.apps.Activities.CartActivity.isFromCart;
import static io.parsl.apps.Activities.ReadyPlayerMe.MyAvatarsActivity.customAvatarPos;
import static io.parsl.apps.Activities.ReadyPlayerMe.MyAvatarsActivity.isFromMyAvatars;
import static io.parsl.apps.AgentChatActivity.ChatApi.LiveSupport.isTablet;
import static io.parsl.apps.MyParslsActivity.MyParslsActivity.isFromMyParsl;
import static io.parsl.apps.Network.Parser.AllVendorListModelClassDataParser.allVendorListDataObj;
import static io.parsl.apps.Network.Parser.AvatarDetailModelParser.avatarDetailsDataObj;
import static io.parsl.apps.Network.Parser.CategoriesDataParser.categoriesDatatObj;
import static io.parsl.apps.Network.Parser.CategoriesModelParser.categoriesListObj;
import static io.parsl.apps.Network.Parser.CategoriesVendorsDataParser.categoryVendorsObj;
import static io.parsl.apps.Network.Parser.SaveParslOtpModelClassDataParser.parslOtpDataObj;
import static io.parsl.apps.Network.Parser.UserLoginParser.userLoginObj;
import static io.parsl.apps.Network.Parser.VirtualCardDetailsParser.virtualCardDatatObj;
import static io.parsl.apps.Utilities.Util.Utils.showCenteredToastmessage;
import static pub.devrel.easypermissions.EasyPermissions.hasPermissions;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baoyz.actionsheet.ActionSheet;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.squareup.picasso.Picasso;
import com.stripe.android.pushProvisioning.PushProvisioningActivity;
import com.stripe.android.pushProvisioning.PushProvisioningActivityStarter;
import com.unity3d.player.UnityPlayerActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.parsl.apps.Activities.BottomSheetFragment.BottomSheetLogin;
import io.parsl.apps.Activities.CartActivity;
import io.parsl.apps.Activities.CommunicationBridge;
import io.parsl.apps.Activities.MainActivity;
import io.parsl.apps.Activities.Wallet.PARSLWalletActivity;
import io.parsl.apps.Adaptors.AdapterCategoriesData;
import io.parsl.apps.Adaptors.ShowSentParselDetailsDataAdapter;
import io.parsl.apps.ModelClass.AllVendorListModelClass;
import io.parsl.apps.ModelClass.EphermalKeysModelCLass;
import io.parsl.apps.ModelClass.ModelsDataClass;
import io.parsl.apps.ModelClass.ShoppingCartItem;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.Network.Parser.GetParselDetailsParser;
import io.parsl.apps.Payments.GooglePay.GooglePayTokenModelClass;
import io.parsl.apps.Payments.PaymentDataCallbacks;
import io.parsl.apps.Payments.PaymentUtils;
import io.parsl.apps.Payments.Stripe.DigitalWallet.CustomEphermalKeyProvider;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Analytics;
import io.parsl.apps.Utilities.Util.Utils;

public class GltfActivity extends UnityPlayerActivity implements View.OnClickListener, com.stripe.android.pushProvisioning.EphemeralKeyUpdateListener, TabLayout.OnTabSelectedListener, ActionSheet.ActionSheetListener {

    //<editor-fold desc="declarations">
    private static final String TAG = GltfActivity.class.getSimpleName();
    private static final double MIN_OPENGL_VERSION = 3.0;
    private static final float VIDEO_HEIGHT_METERS = 0.85f;
    private static final int[] ITEM_DRAWABLES = {R.drawable.ic_send_parcl, R.drawable.ic_redeem, R.drawable.ic_card_black, R.drawable.ic_baseline_settings_24};
    private static final int REQUEST_CODE_GOOGLE_LOGIN = 200;
    public static List<ModelsDataClass.Data> productList = new ArrayList<>();
    public static List<ShoppingCartItem> shoppingCartProductList = new ArrayList<>();
    public static ModelsDataClass.Data modelDataObj = new ModelsDataClass.Data();
    public static boolean isVideo = true;
    public static String emailLiveSupport = "bilawalkhan042@gmail.com";
    public static Context con;
    public static boolean isModelDownloadedFromUnity = false, isAvtarProject = false;
    public static boolean isRedeemUseCase = false;
    public static int redeemCount = 0;
    public static Map<String, ModelsDataClass.Data> modelDataMap = new HashMap<>();
    public static ImageView imgResetCam;
    static FrameLayout fl_fragment_container;
    static LinearLayout ll_filter_activity_landing_screen;
    static LinearLayout llcards;
    static int count = 0;
    static TextView tvCounter;
    static int parselPos;
    static ModelsDataClass.Data mObj = new ModelsDataClass.Data();
    static ShoppingCartItem obj;
    static int quantity = 0;
    static boolean objExistance = true;
    static com.bvapp.arcmenulibrary.ArcMenu arcMenu2;
    static CommunicationBridge communicationBridgeobj;
    public ProgressDialog progressDialog;
    RelativeLayout imgParslIcon;
    RecyclerView rvCategoriesData, rvRedeemData, rvSearch;
    TextView tvParslMsg;
    TextView tvMessageCard, tvCenetrViewGreetings,tvRedeemText,tvCreditCardAmount;
    TextView tvGetCard, tvGreetings, tvDone;
    RelativeLayout rlTabs, llVideoLayout,rlRedeemText;
    MaterialCardView cvMessage;
    int tvCount = 0;
    VideoView videoView;
    boolean isClickToPlayVideo = false;
    ImageView btnPlay, imgRefresh, imgShadow,imgCartIcon;
    boolean isDeepLink = false;
    int categoryTabPosition = 0;
    String categoryKey;
    ImageView imgSearch, imgSurface;
    boolean isSearchModel = false;
    List<AllVendorListModelClass.Data.DataObj> vendorsList = new ArrayList<>();
    CountryCodePicker ccp;
    FrameLayout layout;
    LinearLayout flCart;
    FrameLayout rlUnityCartIcon;
    Button testButton;
    int touchCount = 0;
    ModelsDataClass adaptorObj;
    ModelsDataClass.Data projectedObj;
    private LinearLayout llGreetings;
    private TabLayout tabsCategories, tabVendors;
    @Nullable
    public static String url;
    private TextInputLayout parsUrl;
    private TextInputEditText etPhone;
    private TextInputEditText etParslUrl;
    private TextInputEditText etName;
    private Intent appLinkIntent;
    private String[] str = {"Send", "Redeem", "Wallet","Settings"};
    private CustomEphermalKeyProvider ephrmalKeyProviderobj;
    private SearchView searchViewAllModels;
    private AdapterCategoriesData adapterData;
    private LinearLayout tabsContainerLayout, tabsContainerLayout2;
    private int tabPosition = -1;
    private LinearLayout tempLinearLayout, tempLinearLayout2, llCenterView;
    private int vendorTabPosition = -1;
    private boolean firstime = false;
    private PaymentDataCallbacks paymentDataCallbacks;
    private String modelName;
    private FirebaseAnalytics mFirebaseAnalytics;
    private MediaController mediacontroller;
    private boolean isVideoPlayOnDemand = false;
    private boolean isShadow = true;
    private boolean isSurface = true;
    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    private int posInList;
    private FirebaseUser user;
    private AlertDialog dialogParslOtp;
    public static boolean isSaveOtp = false;
    ActivityResultLauncher<Intent> mPermissions;
    private static String[] PERMISSIONS = null;
    int PERMISSION_ALL = 1;
    public static int dialogCount = 0;
    public static boolean avatarMethodCalled = false;
    //</editor-fold>

    //<editor-fold desc="overridden methods">

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uxx);

        if (!checkIsSupportedDeviceOrFinish(this)) {
            return;
        }

        PERMISSIONS = new String[]{
                Manifest.permission.CAMERA
        };

        if (!hasPermissions(this, PERMISSIONS)) {
            Utils.requestPermission(this,mPermissions,PERMISSIONS,PERMISSION_ALL,this);
        }


        con = this;
        tabsCategories = findViewById(R.id.tabCategories);
        tvCreditCardAmount = findViewById(R.id.credit_card_amount);
        tabVendors = findViewById(R.id.tabVendros);
        rvCategoriesData = findViewById(R.id.rvCategoriesData);
        rvRedeemData = findViewById(R.id.rvRedeemData);
        rlUnityCartIcon = findViewById(R.id.rlUnityCartIcon);
        fl_fragment_container = findViewById(R.id.fl_fragment_container);
        ll_filter_activity_landing_screen = findViewById(R.id.ll_filter_activity_landing_screen);
        llcards = findViewById(R.id.llcards);
        tvCounter = findViewById(R.id.tvCounter);
        rlTabs = findViewById(R.id.rlTabs);
        tvParslMsg = findViewById(R.id.tvParslMsg);
        tvGetCard = findViewById(R.id.tvGetCard);
        tvGreetings = findViewById(R.id.tvGreetings);
        tvMessageCard = findViewById(R.id.tvMessageCard);
        tvCenetrViewGreetings = findViewById(R.id.tvCenetrViewGreetings);
        cvMessage = findViewById(R.id.cvMessage);
        videoView = findViewById(R.id.videoView);
        btnPlay = findViewById(R.id.btnPlay);
        imgShadow = findViewById(R.id.imgShadow);
        imgRefresh = findViewById(R.id.imgRefresh);
        imgSearch = findViewById(R.id.imgSearch);
        imgSurface = findViewById(R.id.imgSurface);
        imgResetCam = findViewById(R.id.imgResetCam);
        arcMenu2 = findViewById(R.id.arcMenu2);
        layout = findViewById(R.id.layout);
        flCart = findViewById(R.id.flCart);
        searchViewAllModels = findViewById(R.id.searchView);
        rvSearch = findViewById(R.id.rvSearch);
        imgParslIcon = findViewById(R.id.imgParslIcon);
        llCenterView = findViewById(R.id.llCenterView);
        testButton = findViewById(R.id.testButton);
        tvDone = findViewById(R.id.tvDone);
        llGreetings = findViewById(R.id.llGreetings);
        llVideoLayout = findViewById(R.id.llVideoLayout);
        rlRedeemText = findViewById(R.id.rlRedeemText);
        tvRedeemText = findViewById(R.id.tvRedeemText);
        imgCartIcon = findViewById(R.id.imgCartIcon);


        //region ClickListerners
        cvMessage.setOnClickListener(this);
        tvGetCard.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        imgSearch.setOnClickListener(this);
        rlUnityCartIcon.setOnClickListener(this);
        imgParslIcon.setOnClickListener(this);
        llVideoLayout.setOnClickListener(this);
        tvDone.setOnClickListener(this);
        flCart.setOnClickListener(this);
        imgRefresh.setOnClickListener(this);
        imgShadow.setOnClickListener(this);
        imgSurface.setOnClickListener(this);
        imgResetCam.setOnClickListener(this);
        //endregion

        appLinkIntent = getIntent();
        if (appLinkIntent != null && appLinkIntent.getData() != null
                && (appLinkIntent.getData().getScheme().equals("https"))) {
            Uri data = appLinkIntent.getData();
            List<String> pathSegments = data.getPathSegments();
            if (pathSegments.size() > 0) {
                String prefix = pathSegments.get(0);
                url = Api.BASE_URL + prefix;
                showProgressDialog(GltfActivity.this);
                Api.getParselDetails(GltfActivity.this, getParselDetails(url), getParselDetailsListner);
            }
        } else {
            showProgressDialog(this);
            Api.getGuest(GltfActivity.this, guestJsonParams(), guestListner);
        }

        labelsWithArcMenu();
        initArcMenu(arcMenu2, str, ITEM_DRAWABLES);

        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        communicationBridgeobj = new CommunicationBridge(new CommunicationBridge.CommunicationCallback() {
            @Override
            public void onNoParamCall() {

            }

            @Override
            public void onOneParamCall(@NotNull String param) {

                showCenteredToastmessage(GltfActivity.this, param);
            }

            @Override
            public void onTwoParamCall(@NotNull String param1, int param2) {

            }
        });


        layout.addView(mUnityPlayer.getView());

        mUnityPlayer.getView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });


        mPermissions = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if(result.getResultCode() == GltfActivity.RESULT_OK){
                    Utils.showCenteredToastmessage(GltfActivity.this    ,"Permission given ");
                }

            }
        });

        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToAvatarScreen();
            }
        });


        communicationBridgeobj.parslScreenLoaded();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        searchModels();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == cvMessage.getId()) {
            if (tvCount == 0) {
                tvParslMsg.setText(GetParselDetailsParser.getParselDetailsObj.getData().getReciever_data().getText_msg());
                tvCount++;
            } else {
                tvCount = 0;
            }

        } else if (v.getId() == tvGetCard.getId()) {
            if(UserPrefs.getNameSpace(GltfActivity.this).isEmpty()) {
                loadDialogRedeemInfo();
            }else {
                fragmentLogin = new BottomSheetLogin();
                fragmentLogin.show(getSupportFragmentManager(), "");
            }
        } else if (v.getId() == btnPlay.getId()) {
            isClickToPlayVideo = true;
            videoView.setVisibility(View.VISIBLE);
            showProgressDialog(GltfActivity.this);
        } else if (v.getId() == imgSearch.getId()) {
            tabPosition = -1;
            vendorTabPosition = -1;
            ActionSheet.createBuilder(GltfActivity.this, getSupportFragmentManager())
                    .setCancelButtonTitle("Cancel")
                    .setOtherButtonTitles("Sort by amount (High to Low)", "Sort by amount (Low to High)", "Sort by symbol (A to Z)", "Sort by symbol (Z to A)", "Sort by location")
                    .setCancelableOnTouchOutside(true)
                    .setListener(sortListner).show();
        } else if (v.getId() == imgParslIcon.getId()) {
            llCenterView.setVisibility(View.VISIBLE);
            imgParslIcon.setVisibility(View.GONE);
            Animation slidIn = AnimationUtils.loadAnimation(GltfActivity.this, R.anim.slide_in_bottom);
            llCenterView.startAnimation(slidIn);
            tvCenetrViewGreetings.setText("Greetings from" + " " + GetParselDetailsParser.getParselDetailsObj.getData().getSender_data().getSender_name());
            if ((GetParselDetailsParser.getParselDetailsObj.getData().getReciever_data().getVideo_msg() != null &&
                    GetParselDetailsParser.getParselDetailsObj.getData().getReciever_data().getVideo_msg().length() > 0) ||
                    GetParselDetailsParser.getParselDetailsObj.getData().isDefault_video()) {
                llVideoLayout.setVisibility(View.VISIBLE);
            }
            //  projectAvatar();
        } else if (v.getId() == llVideoLayout.getId()) {
            showProgressDialog(GltfActivity.this);
            videoView.setVisibility(View.VISIBLE);
            llCenterView.setVisibility(View.GONE);
            arcMenu2.hide();
            isVideoPlayOnDemand = true;
            if (GetParselDetailsParser.getParselDetailsObj.getData().isDefault_video()) {
                playVideo("android.resource://" + getPackageName() + "/" + R.raw.promo);
            } else {
                playVideo(GetParselDetailsParser.getParselDetailsObj.getData().getReciever_data().getVideo_msg());
            }
        } else if (v.getId() == tvDone.getId()) {
            llCenterView.setVisibility(View.GONE);
            llGreetings.setVisibility(View.GONE);
            imgParslIcon.setVisibility(View.VISIBLE);
        } else if (v.getId() == flCart.getId()) {
            if (shoppingCartProductList.size() > 0) {
                AddActivityAndRemoveIntentTransition(CartActivity.class);
            } else {
                Utils.showCenteredToastmessage(GltfActivity.this, "Your cart is empty !");
            }
        } else if (v.getId() == imgRefresh.getId()) {
            clearScreenDialog();
        } else if (v.getId() == imgShadow.getId()) {
            if (isShadow) {
                imgShadow.setImageDrawable(getResources().getDrawable(R.drawable.shadow_on));
                communicationBridgeobj.shadowOn();
                isShadow = false;
            } else {
                imgShadow.setImageDrawable(getResources().getDrawable(R.drawable.shadow_off));
                communicationBridgeobj.shadowOff();
                isShadow = true;
            }
        } else if (v.getId() == imgSurface.getId()) {
            if (isSurface) {
                imgSurface.setImageDrawable(getResources().getDrawable(R.drawable.vertical));
                communicationBridgeobj.enableVerticalSurface();
                isSurface = false;
            } else {
                imgSurface.setImageDrawable(getResources().getDrawable(R.drawable.horizontal));
                communicationBridgeobj.enableHorizontalSurface();
                isSurface = true;
            }
        } else if (v.getId() == imgResetCam.getId()) {
            communicationBridgeobj.resetCamera();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isFromCart) {
            count = 0;
            tvCounter.setText("");
            imgCartIcon.setImageDrawable(ContextCompat.getDrawable(GltfActivity.this,R.drawable.cart));
            tvCounter.setVisibility(View.GONE);
            shoppingCartProductList.clear();
            modelDataMap.clear();
            productList.clear();
            communicationBridgeobj.removeModelsFromScreen();
            isFromCart = false;
        }else if(isFromMyAvatars){

            communicationBridgeobj.callToUnityWithMessage(avatarDetailsDataObj.getData().get(customAvatarPos).getAvatar_id()+",false");
            isFromMyAvatars = false;
        }else if(isFromMyParsl){
            final Handler ha=new Handler();
            ha.postDelayed(new Runnable() {

                @Override
                public void run() {
                    showProgressDialog(GltfActivity.this);
                    Api.getParselDetails(GltfActivity.this, getParselDetails(url), getParselDetailsListner);
                    isFromMyParsl = false;
                }
            }, 3000);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PushProvisioningActivityStarter.REQUEST_CODE) {
            if (resultCode == PushProvisioningActivity.RESULT_OK) {
                PushProvisioningActivityStarter.Result success = PushProvisioningActivityStarter.Result.fromIntent(data);
            } else if (resultCode == PushProvisioningActivity.RESULT_ERROR) {
                PushProvisioningActivityStarter.Error error = PushProvisioningActivityStarter.Error.fromIntent(data);
            }
        } else if (requestCode == PaymentUtils.GPAY_REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK: {
                    assert data != null;
                    PaymentData paymentData = PaymentData.getFromIntent(data);
                    assert paymentData != null;
                    handlePaymentSuccess(paymentData);

                    break;
                }
                case Activity.RESULT_CANCELED: {
                    break;
                }
                case AutoResolveHelper.RESULT_ERROR: {
                    final Status status = AutoResolveHelper.getStatusFromIntent(data);
                    assert status != null;
                    handleError(status.getStatusCode());
                    break;
                }
                default: {
                    break;
                }
            }
        }else if (requestCode == REQUEST_CODE_GOOGLE_LOGIN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                assert account != null;
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        if (categoriesListObj != null && !isSearchModel) {
            vendorTabPosition = -1;
            categoryTabPosition = tab.getPosition();
            categoryKey = categoriesListObj.getData().get(tab.getPosition()).getCategory_key();
            getVendors(categoriesListObj.getData().get(tab.getPosition()).getCategory_key());
        } else {
            vendorsList.clear();
            categoryKey = allVendorListDataObj.getData().getCat_list().get(tab.getPosition()).getCategory_key();

            if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Food")) {
                vendorsList.addAll(allVendorListDataObj.getData().getVendors_list().getFood());
            } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Fashion")) {
                vendorsList.addAll(allVendorListDataObj.getData().getVendors_list().getFashion());
            } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Decor")) {
                vendorsList.addAll(allVendorListDataObj.getData().getVendors_list().getDecor());
            } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Electronics")) {
                vendorsList.addAll(allVendorListDataObj.getData().getVendors_list().getElectronics());
            } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Special Occasions")) {
                vendorsList.addAll(allVendorListDataObj.getData().getVendors_list().getSpecial_occasions());
            }

            addVendorTabs(vendorsList);
        }

        if (tabPosition != tab.getPosition() || tab.getPosition() == 0) {
            tabsContainerLayout = (LinearLayout) tabsCategories.getChildAt(0);
            tempLinearLayout = (LinearLayout) tabsContainerLayout.getChildAt(tabsCategories.getSelectedTabPosition());
            tempLinearLayout.setBackgroundResource(R.drawable.tab_selected_color);
        }
        if (tabPosition >= 0) {
            tabsContainerLayout = (LinearLayout) tabsCategories.getChildAt(0);
            tempLinearLayout = (LinearLayout) tabsContainerLayout.getChildAt(tabPosition);
            tempLinearLayout.setBackgroundColor(getResources().getColor(R.color.white));
            tabPosition = -1;
        }

        tabPosition = tab.getPosition();

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        tabsContainerLayout2 = (LinearLayout) tabVendors.getChildAt(0);
        tempLinearLayout2 = (LinearLayout) tabsContainerLayout2.getChildAt(vendorTabPosition);
        tempLinearLayout2.setBackgroundColor(getResources().getColor(R.color.white));
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    //</editor-fold>

    //<editor-fold desc="listners and params">

    private JSONObject getParselDetails(String url) {

        PackageInfo pInfo = null;
        String type;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (isTablet(GltfActivity.this)) {
            type = "tablet";
        } else {
            type = "mobile";
        }

        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        JSONObject creater_device_info = new JSONObject();

        try {
            js.put("app_id", GeneralConstants.APP_ID);
            js.put("namespace", "");
            data.put("parsl", url);
            data.put("device_token", UserPrefs.getFCMUserToken(this));
            data.put("device_id", Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));

            creater_device_info.put("device_version", pInfo.versionName);
            creater_device_info.put("device_os", "Android");
            creater_device_info.put("device_name", Build.MODEL);
            creater_device_info.put("device_manufacturar", Build.MANUFACTURER);
            creater_device_info.put("device_type", type);

            js.put("receiver_device_info", creater_device_info);
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    private JSONObject getCategoryVendros(String name) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            js.put("app_id", GeneralConstants.APP_ID);
            data.put("category", name);
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    private JSONObject getVendorModels(String name, String vendorID) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        JSONArray arr = new JSONArray();

        try {
            js.put("app_id", GeneralConstants.APP_ID);
            data.put("category", name);
            arr.put(vendorID);
            data.put("vendors", arr);
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    private JSONObject cardJsonParams(String url, String name, CountryCodePicker phoneNo) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            if(!UserPrefs.getNameSpace(this).isEmpty()) {
                js.put("namespace", UserPrefs.getNameSpace(this));
            }
            data.put("parsl", url);
            data.put("reciever_name", name);
            data.put("reciever_phone_no", phoneNo.getNumber());
            data.put("redeemed_timestamp", String.valueOf(System.currentTimeMillis()));
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    private JSONObject guestJsonParams() {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            js.put("app_id", GeneralConstants.APP_ID);
            data.put("device_token", UserPrefs.getFCMUserToken(this));
            data.put("device_id", Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    private JSONObject saveParslParams(String otp) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            js.put("app_id", GeneralConstants.APP_ID);
            js.put("namespace", UserPrefs.getNameSpace(this));
            data.put("parsl_otp",otp);
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    public void saveParslApiCall(){
        showProgressDialog(this);
        Api.saveParslOtp(GltfActivity.this,saveParslParams(url),saveParslOtpListner);
    }

    private JSONObject googleLoginParams(FirebaseUser obj) throws NoSuchAlgorithmException {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            js.put("app_id", GeneralConstants.APP_ID);
            data.put("email",obj.getEmail());
            data.put("password",obj.getEmail());
            data.put("sociallogin", "google");
            data.put("device_token", UserPrefs.getFCMUserToken(this));
            js.put("data", data);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    ActionSheet.ActionSheetListener sortListner = new ActionSheet.ActionSheetListener() {
        @Override
        public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

        }

        @Override
        public void onOtherButtonClick(ActionSheet actionSheet, int index) {
            if (index == 0) {
                Collections.sort(categoriesDatatObj.getData(), (lhs, rhs) -> (int) (rhs.getPrice_related().getPrice() - lhs.getPrice_related().getPrice()));

            } else if (index == 1) {

                Collections.sort(categoriesDatatObj.getData(), (lhs, rhs) -> (int) (lhs.getPrice_related().getPrice() - rhs.getPrice_related().getPrice()));

            } else if (index == 2) {
                Collections.sort(categoriesDatatObj.getData(), new Comparator<ModelsDataClass.Data>() {
                    @Override
                    public int compare(ModelsDataClass.Data o1, ModelsDataClass.Data o2) {
                        return o1.getBasic_info().getName().compareToIgnoreCase(o2.getBasic_info().getName());
                    }

                });

            } else if (index == 3) {
                Collections.sort(categoriesDatatObj.getData(), new Comparator<ModelsDataClass.Data>() {
                    @Override
                    public int compare(ModelsDataClass.Data o1, ModelsDataClass.Data o2) {
                        return o2.getBasic_info().getName().compareToIgnoreCase(o1.getBasic_info().getName());
                    }

                });

            } else if (index == 4) {
                //  startSearchBottomSheetFragment();
            }

            adapterData.notifyDataSetChanged();
        }
    };

    TabLayout.OnTabSelectedListener vendorTab = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {

            if (firstime) {
                if (!isSearchModel) {
                    getCategoryModels(tab.getPosition(), categoryVendorsObj.getData().getVendors_list().get(tab.getPosition()).getVendor_id());

                } else {
                    if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Food")) {
                        getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFood().get(tab.getPosition()).getVendor_id());
                    } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Fashion")) {
                        getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFashion().get(tab.getPosition()).getVendor_id());
                    } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Decor")) {
                        getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getDecor().get(tab.getPosition()).getVendor_id());
                    } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Electronics")) {
                        getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getElectronics().get(tab.getPosition()).getVendor_id());
                    } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Special Occasions")) {
                        getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getSpecial_occasions().get(tab.getPosition()).getVendor_id());
                    }
                }
            }
            firstime = true;
            tabVendors.getChildAt(0).setBackgroundColor(getResources().getColor(R.color.white));
            if (vendorTabPosition != tab.getPosition() || tab.getPosition() == 0) {
                tabsContainerLayout2 = (LinearLayout) tabVendors.getChildAt(0);
                tempLinearLayout2 = (LinearLayout) tabsContainerLayout2.getChildAt(tabVendors.getSelectedTabPosition());
                tempLinearLayout2.setBackgroundResource(R.drawable.tab_selected_vendor_tav);


            }
            if (vendorTabPosition >= 0) {
                tabsContainerLayout2 = (LinearLayout) tabVendors.getChildAt(0);
                tempLinearLayout2 = (LinearLayout) tabsContainerLayout2.getChildAt(vendorTabPosition);
                tempLinearLayout2.setBackgroundColor(getResources().getColor(R.color.white));
            }

            vendorTabPosition = tab.getPosition();

        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            vendorTabPosition = tab.getPosition();
            tabsContainerLayout2 = (LinearLayout) tabVendors.getChildAt(0);
            tempLinearLayout2 = (LinearLayout) tabsContainerLayout2.getChildAt(tab.getPosition());
            tempLinearLayout2.setBackgroundColor(getResources().getColor(R.color.white));

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
            if (!isSearchModel) {
                getCategoryModels(tab.getPosition(), categoryVendorsObj.getData().getVendors_list().get(tab.getPosition()).getVendor_id());

            } else {
                if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Food")) {
                    getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFood().get(tab.getPosition()).getVendor_id());
                } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Fashion")) {
                    getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getFashion().get(tab.getPosition()).getVendor_id());
                } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Decor")) {
                    getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getDecor().get(tab.getPosition()).getVendor_id());
                } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Electronics")) {
                    getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getElectronics().get(tab.getPosition()).getVendor_id());
                } else if (tabsCategories.getTabAt(tabsCategories.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("Special Occasions")) {
                    getCategoryModels(tab.getPosition(), allVendorListDataObj.getData().getVendors_list().getSpecial_occasions().get(tab.getPosition()).getVendor_id());
                }
            }
        }
    };

    AsyncTaskListener googleLoginListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if(userLoginObj.isStatus()) {
                UserPrefs.setNameSpace(GltfActivity.this, userLoginObj.getData().getNamespace());
                UserPrefs.setEmail(GltfActivity.this, user.getEmail());
                fragmentLogin.dismisFragment();
                saveParslApiCall();
            }else {
                Utils.showCenteredToastmessage(GltfActivity.this,"Please Try Again!!!");
            }
        }
    };

    AsyncTaskListener catgoryModelsListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if (categoriesDatatObj.getMessage().equalsIgnoreCase("Success")) {
                setCategoryModel();
                // projectAvatar();
            } else {
                showCenteredToastmessage(GltfActivity.this, "Unexpected error occurred, please try again later");
            }
        }
    };

    AsyncTaskListener catgoryVendorListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if (categoryVendorsObj.getMessage().equalsIgnoreCase("success")) {
                tabVendors.addOnTabSelectedListener(vendorTab);
                addVendorTabs();
            } else {
                showCenteredToastmessage(GltfActivity.this, "Unexpected error occurred, please try again later");
            }

        }
    };

    AsyncTaskListener getParselDetailsListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if (result.code == 200 || result.code == 2) {
                if (GetParselDetailsParser.getParselDetailsObj.getMessage().equalsIgnoreCase("Success")) {
                  //  if(GetParselDetailsParser.getParselDetailsObj.getData().getReciever_data().UserPrefs.getNameSpace(GltfActivity.this))
                    llcards.setVisibility(View.VISIBLE);
                    isRedeemUseCase = true;
                    if ((GetParselDetailsParser.getParselDetailsObj.getData().getReciever_data().getVideo_msg() != null &&
                            GetParselDetailsParser.getParselDetailsObj.getData().getReciever_data().getVideo_msg().length() > 0) ||
                            GetParselDetailsParser.getParselDetailsObj.getData().isDefault_video()) {
                        arcMenu2.hide();
                        showProgressDialog(GltfActivity.this);
                        videoView.setVisibility(View.VISIBLE);
                        if (GetParselDetailsParser.getParselDetailsObj.getData().isDefault_video()) {
                            playVideo("android.resource://" + getPackageName() + "/" + R.raw.promo);
                        } else {
                            playVideo(GetParselDetailsParser.getParselDetailsObj.getData().getReciever_data().getVideo_msg());
                        }
                    } else {
                        setSentParselData();
                        ll_filter_activity_landing_screen.setVisibility(View.GONE);
                        showParsl();


                    }
                } else {
                    showCenteredToastmessage(GltfActivity.this, GetParselDetailsParser.getParselDetailsObj.getMessage());
                }
            } else {
                showCenteredToastmessage(GltfActivity.this, "Please verify your gift code");
            }
        }
    };

    AsyncTaskListener cardListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if (virtualCardDatatObj.getMessage().equalsIgnoreCase("Success")) {
                loadcardDialog();
            } else {
                showCenteredToastmessage(GltfActivity.this, "Unexpected error occurred, please try again later");
            }
        }
    };

    AsyncTaskListener guestListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
        }
    };

    AsyncTaskListener catgoryListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if (categoriesListObj.getMessage().equalsIgnoreCase("success")) {
                tabsCategories.addOnTabSelectedListener(GltfActivity.this);
                ll_filter_activity_landing_screen.setVisibility(View.VISIBLE);
                addCategoryTabs();
                //  AddActivityAndRemoveIntentTransitionWithoutFinish(SecondMainActivity.class);
            } else {
                showCenteredToastmessage(GltfActivity.this, "Unexpected error occurred, please try again later");
            }

        }
    };

    AsyncTaskListener saveParslOtpListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if (parslOtpDataObj.isStatus()) {
                showCenteredToastmessage(GltfActivity.this, parslOtpDataObj.getMessage());
                dialogParslOtp.dismiss();
            } else if(!parslOtpDataObj.isStatus() && (result.code == 200 || result.code == 2)) {
                showCenteredToastmessage(GltfActivity.this, parslOtpDataObj.getMessage());
            }else {
                showCenteredToastmessage(GltfActivity.this, "Unexpected error occurred, please try again");
            }

        }
    };
    //</editor-fold>


    //<editor-fold desc="Dialogs">
    private void loadPromoDialog() {
        AlertDialog.Builder builderAddInfo = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogViewAddInfo = inflater.inflate(R.layout.redeem_code_dialog_layout, null);
        builderAddInfo.setView(dialogViewAddInfo);
        dialogParslOtp = builderAddInfo.create();
        Objects.requireNonNull(dialogParslOtp.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogParslOtp.show();
        dialogParslOtp.setCancelable(true);

        TextView tvRedeemLater = dialogViewAddInfo.findViewById(R.id.tvRedeemLater);
        TextView tvRedeemNow = dialogViewAddInfo.findViewById(R.id.tvRedeemNow);
        TextInputEditText etParslCode = dialogViewAddInfo.findViewById(R.id.etParslCode);

        etParslCode.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        etParslCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
              tvRedeemLater.setTextColor(ResourcesCompat.getColor(getResources(),R.color.black, null));
                tvRedeemNow.setTextColor(ResourcesCompat.getColor(getResources(),R.color.black,null));
            }
        });
        if (url != null && !url.isEmpty()) {
            etParslCode.setText(url);
        }

        tvRedeemNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                url = etParslCode.getText().toString().toUpperCase();
               if (!url.isEmpty()) {
                            showProgressDialog(GltfActivity.this);
                            Api.getParselDetails(GltfActivity.this, getParselDetails(url), getParselDetailsListner);
                            dialogParslOtp.dismiss();
                        } else {
                            etParslCode.setError("Enter PARSL Redeem Code");
                        }
            }
        });

        tvRedeemLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                url = etParslCode.getText().toString().toUpperCase();
                if (!url.isEmpty()) {
                    if (!UserPrefs.getNameSpace(GltfActivity.this).isEmpty()) {
                        saveParslApiCall();
                    } else {
                        isSaveOtp = true;
                        fragmentLogin = new BottomSheetLogin();
                        fragmentLogin.show(getSupportFragmentManager(), "");
                    }
                }else {
                    etParslCode.setError("Enter PARSL Redeem Code");
                }

            }
        });
    }

    private void loadLiveSupportDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Live Support");
        alertDialog.setMessage("Enter email").setPositiveButton("Submit", null).setNegativeButton("May be later", null);
        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        alertDialog.setView(input);

        final AlertDialog mAlertDialog = alertDialog.create();

        mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button positiveBtn = mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                Button negativeBtn = mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);

                positiveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String email = input.getText().toString();
//                        if(isValidEmail(email)) {
//                            emailLiveSupport = email;
//                            AddActivityAndRemoveIntentTransitionWithoutFinish(UsersChatActivity.class);
//                        }else {
//                            showCenteredToastmessage(GltfActivity.this,"Invalid email");
//                            input.setError("Invalid email");
//                        }
                    }
                });

                negativeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAlertDialog.dismiss();
                    }
                });

            }
        });
        mAlertDialog.show();
    }

    private void loadcardDialog() {
        AlertDialog.Builder builderAddInfo = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogViewAddInfo = inflater.inflate(R.layout.visa_card_dialog, null);
        builderAddInfo.setView(dialogViewAddInfo);
        AlertDialog dialogAddInfo = builderAddInfo.create();
        dialogAddInfo.show();

        TextView tvCard1 = dialogViewAddInfo.findViewById(R.id.tvCard1);
        TextView tvCard2 = dialogViewAddInfo.findViewById(R.id.tvCard2);
        TextView tvCard3 = dialogViewAddInfo.findViewById(R.id.tvCard3);
        TextView tvCard4 = dialogViewAddInfo.findViewById(R.id.tvCard4);
        TextView addToVallet = dialogViewAddInfo.findViewById(R.id.addToVallet);

        TextView tvCardHolderName = dialogViewAddInfo.findViewById(R.id.tvCardHolderName);

        tvCard1.setText(virtualCardDatatObj.getData().getNumber().substring(0, 4));
        tvCard2.setText(virtualCardDatatObj.getData().getNumber().substring(4, 8));
        tvCard3.setText(virtualCardDatatObj.getData().getNumber().substring(8, 12));
        tvCard4.setText(virtualCardDatatObj.getData().getNumber().substring(12, 16));
        tvCardHolderName.setText(virtualCardDatatObj.getData().getCardholder().getName());

        addToVallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAddInfo.dismiss();
                AddActivityAndRemoveIntentTransition(PARSLWalletActivity.class);
////                showProgressDialog(GltfActivity.this);
////                ephrmalKeyProviderobj = new CustomEphermalKeyProvider(GltfActivity.this);
////                ephrmalKeyProviderobj.createEphemeralKey(etParslUrl.getText().toString(), GltfActivity.this);
//                jumpToWallet();
            }
        });


    }

    private void clearScreenDialog() {
        AlertDialog.Builder builderAddInfo = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogViewAddInfo = inflater.inflate(R.layout.clear_screen_dialog, null);
        builderAddInfo.setView(dialogViewAddInfo);
        AlertDialog dialogAddInfo = builderAddInfo.create();
        Objects.requireNonNull(dialogAddInfo.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogAddInfo.show();
        dialogAddInfo.setCancelable(true);


        TextView btnYes = dialogViewAddInfo.findViewById(R.id.btnYes);
        TextView btnNo = dialogViewAddInfo.findViewById(R.id.btnNo);


        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAddInfo.dismiss();
            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shoppingCartProductList.clear();
                productList.clear();
                modelDataMap.clear();
                dialogAddInfo.dismiss();
                tvCounter.setText("");
                imgCartIcon.setImageDrawable(ContextCompat.getDrawable(GltfActivity.this,R.drawable.cart));
                tvCounter.setVisibility(View.GONE);
                communicationBridgeobj.removeModelsFromScreen();
            }
        });

    }

    public void dismissProgressDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void showProgressDialog(Context context) {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = new ProgressDialog(context, R.style.ProgressDialogTheme);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.setCancelable(false);
            progressDialog.show();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void loadDialogRedeemInfo() {
        AlertDialog.Builder builderAddInfo = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogViewAddInfo = inflater.inflate(R.layout.dialog_redeem_info, null);
        builderAddInfo.setView(dialogViewAddInfo);
        AlertDialog dialogAddInfo = builderAddInfo.create();
        Objects.requireNonNull(dialogAddInfo.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogAddInfo.show();
        dialogAddInfo.setCancelable(true);

        ccp = (CountryCodePicker) dialogViewAddInfo.findViewById(R.id.ccp);
        etName = dialogViewAddInfo.findViewById(R.id.name);
        etPhone = dialogViewAddInfo.findViewById(R.id.phoneNo);
        parsUrl = dialogViewAddInfo.findViewById(R.id.parsUrl);
        etParslUrl = dialogViewAddInfo.findViewById(R.id.etParsUrl);
        TextView tvSubmit = dialogViewAddInfo.findViewById(R.id.tvSubmit);

        etParslUrl.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        ccp.registerPhoneNumberTextView(etPhone);
        if ((appLinkIntent != null && appLinkIntent.getData() != null)) {
            parsUrl.setVisibility(View.GONE);
        } else if (url != null && !url.isEmpty()) {
            parsUrl.setVisibility(View.VISIBLE);
            etParslUrl.setText(url);
        } else {
            parsUrl.setVisibility(View.VISIBLE);
        }

        UserPrefs.setPhone(GltfActivity.this, ccp.getNumber());

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    showProgressDialog(GltfActivity.this);
                    if (parsUrl.getVisibility() == View.VISIBLE) {
                        url = etParslUrl.getText().toString();
                        Api.getVirtualCardDetails(GltfActivity.this, cardJsonParams(etParslUrl.getText().toString(), etName.getText().toString(), ccp), cardListner);
                    } else {
                        etParslUrl.setText(url);
                        Api.getVirtualCardDetails(GltfActivity.this, cardJsonParams(url, etName.getText().toString(), ccp), cardListner);
                    }
                }
            }
        });
    }
    //</editor-fold>

    //<editor-fold desc="genric methods and payment callbacks">

    public void setCategoryModel() {
        try {
            adaptorObj = (ModelsDataClass) categoriesDatatObj.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        ModelsDataClass obj = new ModelsDataClass();

        imgSearch.setVisibility(View.VISIBLE);
        searchViewAllModels.setVisibility(View.VISIBLE);
        adapterData = new AdapterCategoriesData(this, adaptorObj);
        rvCategoriesData.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvCategoriesData.setAdapter(adapterData);

        rvSearch.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvSearch.setAdapter(adapterData);


        adapterData.setOnItemClickListener(new AdapterCategoriesData.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                quantity = 0;
                parselPos = position;
                String user;
                if(UserPrefs.getNameSpace(GltfActivity.this).isEmpty()){
                    user = "Guest";
                }else {
                    user = UserPrefs.getNameSpace(GltfActivity.this);
                }
                Analytics.sendEventTrackingData(GltfActivity.this,user,categoriesDatatObj.getData().get(position).getBasic_info().getName(),categoriesDatatObj.getData().get(position).getProduct_id());
                Utils.setProjectionModelPosition(position, adapterData);
                Log.i("object id",categoriesDatatObj.getData().get(position).getProduct_id());
                Log.i("object url",categoriesDatatObj.getData().get(position).getModel_files().get_$3d_model_file());
                modelDataMap.put(categoriesDatatObj.getData().get(position).getProduct_id(), categoriesDatatObj.getData().get(position));
                communicationBridgeobj.callToUnityWithMessage(categoriesDatatObj.getData().get(position).getProduct_id()+","+"false");
               // communicationBridgeobj.callToUnityWithMessage("assettable"+","+false);
                projectedObj = categoriesDatatObj.getData().get(position);
            }
        });
    }

    private void markSelectiveProduct() {
        if (projectedObj != null) {
            posInList = 0;
            for (ModelsDataClass.Data entry : categoriesDatatObj.getData()) {

                posInList++;
                if (entry.getModel_id().equalsIgnoreCase(projectedObj.getModel_id())) {
                    Utils.setProjectionModelPosition(posInList, adapterData);
                    break;
                }
            }

        }
    }

    private <T> T copyObject(Object object) {
        Gson gson = new Gson();
        JsonObject jsonObject = gson.toJsonTree(object).getAsJsonObject();
        return gson.fromJson(jsonObject, (Type) object.getClass());
    }

    public void downloadFail(String msg) {
        Utils.showCenteredToastmessage(this, msg);
    }

    public void callUnitytestMessage() {
        dismissProgressDialog();
        modelName = "";
        File allModelfiles;
        allModelfiles = getGLTFFile("pepsi_android");
//        file:///storage/emulated/0/.Parsel/pepsi_android.glb
    }

    private File getGLTFFile(String filename) {

        String root = Environment.getExternalStorageDirectory().toString();

        return new File(root + "/.Parsel", filename + ".glb");
    }

    public void setCategoryModel(boolean isSearch, AllVendorListModelClass.Data.VendorsList vendrList) {
        isSearchModel = isSearch;

        tabsCategories.removeAllTabs();
        tabVendors.removeAllTabs();
        addCategoryTabs(allVendorListDataObj.getData().getCat_list());
    }

    private void setSentParselData() {

        ShowSentParselDetailsDataAdapter adapterData = new ShowSentParselDetailsDataAdapter(this, GetParselDetailsParser.getParselDetailsObj);
        rvRedeemData.setLayoutManager(new GridLayoutManager(this, 3));
        rvRedeemData.setAdapter(adapterData);
        tvParslMsg.setText(GetParselDetailsParser.getParselDetailsObj.getData().getReciever_data().getText_msg());
        tvCreditCardAmount.setText("$"+Utils.getRoundedDoubleForMoney(GetParselDetailsParser.getParselDetailsObj.getData().getParsl_amount()));
        adapterData.setOnItemClickListener(new ShowSentParselDetailsDataAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                parselPos = position;
                imgParslIcon.setVisibility(View.VISIBLE);
                Utils.setProjectionModelPosition(position, adapterData);
                llCenterView.setVisibility(View.GONE);
                modelDataMap.put(GetParselDetailsParser.getParselDetailsObj.getData().getParsl_models().get(position).getModel_id(), GetParselDetailsParser.getParselDetailsObj.getData().getParsl_models().get(position));
                communicationBridgeobj.callToUnityWithMessage(GetParselDetailsParser.getParselDetailsObj.getData().getParsl_models().get(position).getProduct_id()+","+GetParselDetailsParser.getParselDetailsObj.getData().getParsl_models().get(position).isDefault_shadows());
            }
        });
    }

    public void googleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, REQUEST_CODE_GOOGLE_LOGIN);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        user = mAuth.getCurrentUser();
                        showProgressDialog(this);
                        try {
                            Api.userLogin(this, googleLoginParams(user), googleLoginListner);
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }
                    } else {
                        // If sign in fails, display a message to the user.
                    }

                    // ...
                });
    }

    public String getSHA256(String pas) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(pas.getBytes("UTF-8"));
        byte[] digest = md.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            sb.append(String.format("%02x", digest[i] & 0xFF));
        }
        return sb.toString();
    }

    public static boolean checkIsSupportedDeviceOrFinish(final Activity activity) {
        String openGlVersionString =
                ((ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE))
                        .getDeviceConfigurationInfo()
                        .getGlEsVersion();
        if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES 3.0 later");
            Toast.makeText(activity, "Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG)
                    .show();
            activity.finish();
            return false;
        }
        return true;
    }

    public void loadDialogAdditionalInfo(Context con, String modelID) {
        dialogCount++;
        AlertDialog.Builder builderAddInfo = new AlertDialog.Builder(con);
        LayoutInflater inflater = LayoutInflater.from(con);
        View dialogViewAddInfo = inflater.inflate(R.layout.info_dialog, null);
        builderAddInfo.setView(dialogViewAddInfo);
        AlertDialog dialogAddInfo = builderAddInfo.create();
        Objects.requireNonNull(dialogAddInfo.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogAddInfo.show();

       // Utils.showCenteredToastmessage(this,modelID);
        //    modelDataObj = modelDataMap.get("1619176950_4860559bab0d7ce__cat_1");
        modelDataObj = modelDataMap.get(modelID);
        ImageView img_addinfo = dialogViewAddInfo.findViewById(R.id.imgLoadAddInfoImage);
        TextView tvTitle = dialogViewAddInfo.findViewById(R.id.tvTitle);
        TextView tvAddinfoDesText = dialogViewAddInfo.findViewById(R.id.tvAddinfoDesText);
        TextView btnAction = dialogViewAddInfo.findViewById(R.id.btnAction);
        ImageView btnShadow = dialogViewAddInfo.findViewById(R.id.btnShadow);


//        if (isRedeemUseCase) {
//            btnAction.setVisibility(View.GONE);
//        } else {
//            btnAction.setVisibility(View.VISIBLE);
//        }
        tvTitle.setText(Objects.requireNonNull(modelDataObj).getBasic_info().getName());
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                Picasso.get().load(modelDataObj.getModel_files().getThumbnail()).placeholder(R.drawable.parsl_place_holder).into(img_addinfo);
            }
        });

        tvAddinfoDesText.setText(modelDataObj.getDescription());

        mObj.setProduct_id("123456");

//        if (shoppingCartProductList.size() == 0) {
//            obj = new ShoppingCartItem();
//            obj.setObj(modelDataObj);
//            obj.setQuantity(1);
//            shoppingCartProductList.add(obj);
//
//        } else {
//            for (int i = 0; i < shoppingCartProductList.size(); i++) {
//                obj = new ShoppingCartItem();
//                if (modelDataObj.equals(shoppingCartProductList.get(i).getObj())) {
//                    quantity = shoppingCartProductList.get(i).getQuantity();
//                    quantity++;
//                    shoppingCartProductList.get(i).setQuantity(quantity);
//                    objExistance = true;
//                    quantity = 0;
//                } else {
//                    objExistance = false;
//                }
//            }
//            if (!objExistance) {
//                obj.setObj(modelDataObj);
//                quantity++;
//                obj.setQuantity(quantity);
//                quantity = 0;
//                shoppingCartProductList.add(obj);
//            }
//        }
//
//        productList.add(categoriesDatatObj.getData().get(parselPos));

        btnShadow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(modelDataObj.isDefault_shadows()){
                    Utils.showCenteredToastmessage(GltfActivity.this,"This PARSL has no shadow.");
                }else {
                    if (isShadow) {
                        btnShadow.setImageDrawable(getResources().getDrawable(R.drawable.shadow_on));
                        communicationBridgeobj.shadowOn();
                        isShadow = false;
                    } else {
                        imgShadow.setImageDrawable(getResources().getDrawable(R.drawable.shadow_off));
                        communicationBridgeobj.shadowOff();
                        isShadow = true;
                    }
                }

            }
        });

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (shoppingCartProductList.size() == 0) {
                    obj = new ShoppingCartItem();
                    obj.setObj(modelDataObj);
                    obj.setQuantity(1);
                    shoppingCartProductList.add(obj);

                } else {
                    for (int i = 0; i < shoppingCartProductList.size(); i++) {
                        obj = new ShoppingCartItem();
                        if (modelDataObj.equals(shoppingCartProductList.get(i).getObj())) {
//                            quantity = shoppingCartProductList.get(i).getQuantity();
//                            quantity++;
//                            shoppingCartProductList.get(i).setQuantity(quantity);
//                            objExistance = true;
//                            quantity = 0;
                            Utils.showCenteredToastmessage(GltfActivity.this,"Item already exist");
                            return;
                        } else {
                            objExistance = false;
                        }
                    }
                    if (!objExistance) {
                        obj.setObj(modelDataObj);
                        quantity++;
                        obj.setQuantity(quantity);
                        quantity = 0;
                        shoppingCartProductList.add(obj);
                    }
                }

                productList.add(categoriesDatatObj.getData().get(parselPos));
//                llcards.setVisibility(View.GONE);
//                count++;
                Handler uiHandler2 = new Handler(Looper.getMainLooper());
                uiHandler2.post(new Runnable() {
                    @Override
                    public void run() {
                        tvCounter.setVisibility(View.VISIBLE);
                        imgCartIcon.setImageDrawable(ContextCompat.getDrawable(GltfActivity.this,R.drawable.new_app_icon));
                        tvCounter.setText(String.valueOf(shoppingCartProductList.size()));
                    }
                });

                Utils.showCenteredToastmessage(GltfActivity.this,"PARSL has been add to your cart");
            }
        });

    }

    private void getVendors(String categoryName) {
        showProgressDialog(this);
        Api.getCategoryVendors(this, getCategoryVendros(categoryName), catgoryVendorListner);
    }

    private void getCategoryModels(int pos, String vendorID) {
        showProgressDialog(this);
        Api.getVendorModels(this, getVendorModels(categoryKey, vendorID), catgoryModelsListner);
    }

    private void addCategoryTabs() {
        List<String> message = new ArrayList<>();
        if (categoriesListObj != null) {
            for (int i = 0; i < categoriesListObj.getData().size(); i++) {
                tabsCategories.addTab(tabsCategories.newTab().setText(categoriesListObj.getData().get(i).getCategory_title()));
            }
        } else {
            message.add("food");
            tabsCategories.addTab(tabsCategories.newTab().setText(message.get(0)));
        }
    }

    private void addVendorTabs() {

        if (tabVendors.getTabAt(0) == null) {
            List<String> message = new ArrayList<>();
            tabVendors.removeAllTabs();
            if (categoryVendorsObj != null) {
                for (int i = 0; i < categoryVendorsObj.getData().getVendors_list().size(); i++) {
                    tabVendors.addTab(tabVendors.newTab().setText(categoryVendorsObj.getData().getVendors_list().get(i).getVendor_name()));
                }
            }
        } else {
            tabVendors.removeAllTabs();
            if (categoriesListObj != null) {
                for (int i = 0; i < categoryVendorsObj.getData().getVendors_list().size(); i++) {
                    tabVendors.addTab(tabVendors.newTab().setText(categoryVendorsObj.getData().getVendors_list().get(i).getVendor_name()));
                }
            }
        }
    }

    private void addCategoryTabs(List<AllVendorListModelClass.Data.CatList> list) {

        for (int i = 0; i < list.size(); i++) {
            tabsCategories.addTab(tabsCategories.newTab().setText(list.get(i).getCategory_title()));
        }
    }

    private void addVendorTabs(List<AllVendorListModelClass.Data.DataObj> vendors) {
        tabVendors.removeAllTabs();
        for (int i = 0; i < vendors.size(); i++) {
            tabVendors.addTab(tabVendors.newTab().setText(vendors.get(i).getVendor_name()));
        }
    }

    // generate ephermal keys for digital wallets
    @Override
    public void onKeyUpdate(@NotNull String s) {
        dismissProgressDialog();
//        EphermalKeysModelCLass obj = new EphermalKeysModelCLass();
//        Gson gson = new Gson();
//        obj = gson.fromJson(String.valueOf(s), EphermalKeysModelCLass.class);
//
//        obj.getData().getId();  // ephermal id

     //   if (obj.getMessage().equalsIgnoreCase("Success")) {
            showCenteredToastmessage(GltfActivity.this, "Gpay callled");

            new PushProvisioningActivityStarter(
                    this, // The Activity or Fragment you are initiating the push provisioning from
                    new PushProvisioningActivityStarter.Args(
                            etName.getText().toString(), // The name that will appear on the push provisioning UI
                            ephrmalKeyProviderobj, // Your instance of EphemeralKeyProvider
                            true // If you want to enable logs or not
                    )).startForResult();

      //  }
    }

    @Override
    public void onKeyUpdateFailure(int i, @NotNull String s) {

    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {

        if (index == 0) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("plain/text");
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"apps@parsl.io"});
            intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
            startActivity(Intent.createChooser(intent, ""));
        } else if (index == 1) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("plain/text");
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"apps@parsl.io"});
            intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
            startActivity(Intent.createChooser(intent, ""));
        } else if (index == 2) {

            isVideo = true;
        } else if (index == 3) {
            isVideo = false;
        } else if (index == 4) {
            loadLiveSupportDialog();
        }
    }

    private void initArcMenu(final com.bvapp.arcmenulibrary.ArcMenu menu, final String[] str, int[] itemDrawables) {
        for (int i = 0; i < itemDrawables.length; i++) {
            com.bvapp.arcmenulibrary.widget.FloatingActionButton item = new com.bvapp.arcmenulibrary.widget.FloatingActionButton(this);  //Use internal fab as a child
            item.setSize(com.bvapp.arcmenulibrary.widget.FloatingActionButton.SIZE_MINI);  //set minimum size for fab 42dp
            item.setShadow(true); //enable to draw shadow
            item.setIcon(itemDrawables[i]); //add icon for fab
            item.setBackgroundColor(getResources().getColor(R.color.white));  //set menu button normal color programmatically
            menu.setChildSize(item.getIntrinsicHeight()); // fit menu child size exactly same as fab

            final int position = i;
            menu.addItem(item, str[i], new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        if (str[position].equalsIgnoreCase("Settings")) {
                        AddActivityAndRemoveIntentTransition(ProfileActivity.class);
                    } else if (str[position].equalsIgnoreCase("Wallet")) {
                            if(UserPrefs.getNameSpace(GltfActivity.this).isEmpty()){
                                fragmentLogin = new BottomSheetLogin();
                                fragmentLogin.show(getSupportFragmentManager(), "");
                            }else {
                                AddActivityAndRemoveIntentTransition(PARSLWalletActivity.class);
                            }
                    } else if (str[position].equalsIgnoreCase("Redeem")) {
                        sendParslButtonCall();
                        redeemCount = 0;
                        isRedeemUseCase = true;
                        ll_filter_activity_landing_screen.setVisibility(View.GONE);
                        loadPromoDialog();
                    } else if (str[position].equalsIgnoreCase("Send")) {
                            isRedeemUseCase = false;
                        if (categoriesListObj.getData() == null) {
              //              AddActivityAndRemoveIntentTransition(MainActivity.class);
                            imgParslIcon.setVisibility(View.GONE);
                            tabPosition = -1;
                            vendorTabPosition = -1;
                            firstime = false;
                            showProgressDialog(GltfActivity.this);
                            Api.getCategories(GltfActivity.this, Utils.getJSonParams(GltfActivity.this), catgoryListner);
                            if (tabsCategories.getTabAt(0) != null && tabVendors.getTabAt(0) != null) {
                                tabsCategories.removeAllTabs();
                                tabVendors.removeAllTabs();
                                isSearchModel = false;
                            }
                        } else {
                            sendParslButtonCall();
                        }
                    } else if (str[position].equalsIgnoreCase("Avatar")) {
                        moveToAvatarScreen();
                    }
                }
            });
        }
    }

    private void sendParslButtonCall() {
        isRedeemUseCase = false;
        imgParslIcon.setVisibility(View.GONE);
        llCenterView.setVisibility(View.GONE);
        rlRedeemText.setVisibility(View.GONE);
        ll_filter_activity_landing_screen.setVisibility(View.VISIBLE);
        rlTabs.setVisibility(View.VISIBLE);
        btnPlay.setVisibility(View.GONE);
        hideView();
    }

    public void setRedeemText(String text){
      //  rlRedeemText.setVisibility(View.VISIBLE);
        tvRedeemText.setText(text);
    }
    private boolean validations() {

        if (parsUrl.getVisibility() == View.VISIBLE) {
            if (etParslUrl.getText().length() == 0) {
                etParslUrl.setError("PARSL Redeem Code");
                return false;
            }
        }

        if (etName.getText().length() == 0) {
            etName.setError("Name was empty");
            return false;
        }
        if (etPhone.getText().length() == 0) {
            etPhone.setError("Phone was empty");
            return false;
        }


        return true;
    }

    private void hideView() {
        cvMessage.setVisibility(View.GONE);
        videoView.setVisibility(View.GONE);

    }


    public void projectAvatar(boolean paar) {
        if(GetParselDetailsParser.getParselDetailsObj.getData().getAvatar_chracter()!= null && !GetParselDetailsParser.getParselDetailsObj.getData().getAvatar_chracter().isEmpty()) {
            isModelDownloadedFromUnity = true;
            avatarMethodCalled = true;
            communicationBridgeobj.callToUnityRedeemCharacter(GetParselDetailsParser.getParselDetailsObj.getData().getAvatar_chracter() + "," +
                    GetParselDetailsParser.getParselDetailsObj.getData().getAvatar_animation());
        }else {
            callPArslIcon(true);
        }

        //callPArslIcon();
    }

    public void moveToAvatarScreen() {
        ll_filter_activity_landing_screen.setVisibility(View.GONE);
    }

    private void labelsWithArcMenu() {

        arcMenu2.setToolTipTextSize(12);
        arcMenu2.showTooltip(true);
        arcMenu2.setToolTipBackColor(getResources().getColor(R.color.white_80));
        arcMenu2.setToolTipCorner(6);
        arcMenu2.setToolTipPadding(8);
        arcMenu2.setToolTipTextColor(getResources().getColor(R.color.black));
        arcMenu2.setAnim(300, 300, com.bvapp.arcmenulibrary.ArcMenu.ANIM_MIDDLE_TO_RIGHT, com.bvapp.arcmenulibrary.ArcMenu.ANIM_MIDDLE_TO_RIGHT,
                com.bvapp.arcmenulibrary.ArcMenu.ANIM_INTERPOLATOR_ACCELERATE_DECLERATE, com.bvapp.arcmenulibrary.ArcMenu.ANIM_INTERPOLATOR_ACCELERATE_DECLERATE);

    }

    public void toast(){
        showCenteredToastmessage(this, String.valueOf(""));
    }

    public void showParsl() {
        llGreetings.setVisibility(View.VISIBLE);
        tvGreetings.setText(GetParselDetailsParser.getParselDetailsObj.getData().getReciever_data().getText_msg());
        rlRedeemText.setVisibility(View.VISIBLE);
        setRedeemText("PARSL is being redeemed");
        communicationBridgeobj.reedeemGiftModel(GetParselDetailsParser.getParselDetailsObj.getData().getGift_model().getProduct_id()+","+GetParselDetailsParser.getParselDetailsObj.getData().getGift_model().isDefault_shadows());

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                llGreetings.setVisibility(View.GONE);

            }
        }, 10000);
    }

    public void callPArslIcon(boolean pa) {
        //testMessage(pa);
     //   Utils.showCenteredToastmessage(con,"Tap on surface to view avatar");
     //   setRedeemText("Tap on screen to view Avatar");

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rlRedeemText.setVisibility(View.GONE);
                        imgParslIcon.setVisibility(View.VISIBLE);
                        Animation pulse = AnimationUtils.loadAnimation(con, R.anim.pulse_anim);
                        imgParslIcon.startAnimation(pulse);

                    }
                });

            }
        }, 5000);

    }

    public void AddActivityAndRemoveIntentTransition(Class activity) {
        Intent intent = new Intent(this, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    private void playVideo(String videoUrl) {
        try {
            // Start the MediaController
            mediacontroller = new MediaController(this);
            //  mediacontroller.setAnchorView(videoView);

            Uri videoUri = Uri.parse(videoUrl);
            videoView.setMediaController(mediacontroller);
            videoView.setVideoURI(videoUri);

        } catch (Exception e) {

            e.printStackTrace();
        }

        videoView.requestFocus();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                dismissProgressDialog();
                videoView.start();
                mediacontroller.setAnchorView(videoView);
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            public void onCompletion(MediaPlayer mp) {
                if (!isVideoPlayOnDemand) {
                    videoViewCompletionCall();
                    setSentParselData();
                    showParsl();
                } else {
                    videoViewCompletionCall();
                }
            }
        });

    }

    private void videoViewCompletionCall() {
        videoView.setVisibility(View.GONE);
        ll_filter_activity_landing_screen.setVisibility(View.GONE);
        arcMenu2.show();
    }

    private void handlePaymentSuccess(PaymentData paymentData) {
        final String paymentInfo = paymentData.toJson();
        if (paymentInfo == null) {
            return;
        }

        try {
            JSONObject paymentMethodData = new JSONObject(paymentInfo).getJSONObject("paymentMethodData");

            final JSONObject tokenizationData = paymentMethodData.getJSONObject("tokenizationData");
            final String token = tokenizationData.getString("token");
            Gson gson = new Gson();
            GooglePayTokenModelClass googlePayTokenModelClass = new GooglePayTokenModelClass();
            googlePayTokenModelClass = gson.fromJson(token, GooglePayTokenModelClass.class);
            if (paymentDataCallbacks != null) {
                paymentDataCallbacks.onDataReceive(googlePayTokenModelClass.getId(), googlePayTokenModelClass.getCard().getDynamic_last4());
            }

        } catch (JSONException e) {
            if (paymentDataCallbacks != null) {
                paymentDataCallbacks.onError(Arrays.toString(e.getStackTrace()));
            }
        }
    }

    private void handleError(int statusCode) {
        if (paymentDataCallbacks != null) {
            paymentDataCallbacks.onError(String.format("Error code: %d", statusCode));
        }
    }

    public PaymentDataCallbacks getPaymentDataCallbacks() {
        return paymentDataCallbacks;
    }

    public void setPaymentDataCallbacks(PaymentDataCallbacks paymentDataCallbacks) {
        this.paymentDataCallbacks = paymentDataCallbacks;
    }

    public void jumpToWallet() {
        // Check first if wallet is installed
        String packageName = "com.google.android.apps.walletnfcrel";
        Context appContext = getApplicationContext();
        PackageManager pm = appContext.getPackageManager();
        boolean installed = false;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            installed = true;

        } catch (PackageManager.NameNotFoundException e) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.walletnfcrel&hl=en&gl=US"));
            startActivity(intent);
        }
        if (installed) {
            Intent launchIntent = pm.getLaunchIntentForPackage(packageName);
            if (launchIntent != null) {
                appContext.startActivity(launchIntent);
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.walletnfcrel&hl=en&gl=US"));
                startActivity(intent);
            }
        }
    }

    void searchModels() {
        View closeButton = searchViewAllModels.findViewById(R.id.search_close_btn);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvSearch.setVisibility(View.GONE);
                searchViewAllModels.onActionViewCollapsed();
            }
        });

        searchViewAllModels.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    rvSearch.setVisibility(View.VISIBLE);
                } else {
                    rvSearch.setVisibility(View.GONE);
                    markSelectiveProduct();
                }
            }
        });

        searchViewAllModels.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (TextUtils.isEmpty(query)) {
                    markSelectiveProduct();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapterData.getFilter().filter(query);
                return false;
            }
        });

    }



    //</editor-fold>

}

