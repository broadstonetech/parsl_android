//package io.parsl.apps.Activities
//
//import android.graphics.Color
//import android.os.Bundle
//import android.widget.Button
//import android.widget.FrameLayout
//import androidx.constraintlayout.widget.ConstraintLayout
//import com.unity3d.player.UnityPlayerActivity
//import io.parsl.apps.R
//
//class KotlinActivity : UnityPlayerActivity() {
//    var toggle:Boolean = false
//    lateinit var layout:FrameLayout
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_kotlin)
//
//        layout = findViewById(R.id.layout);
//        val communicationBridge = CommunicationBridge(
//                object : CommunicationBridge.CommunicationCallback {
//                    override fun onNoParamCall() {
//
//                    }
//                    override fun onOneParamCall(param: String) {
//                    }
//                    override fun onTwoParamCall(param1: String, param2: Int) {
//                    }
//                })
//
//        communicationBridge.callToUnityWithMessage("surprisebox1")
//        val button = Button(this)
//        button.layoutParams = ConstraintLayout.LayoutParams(
//                ConstraintLayout.LayoutParams.WRAP_CONTENT,
//                ConstraintLayout.LayoutParams.WRAP_CONTENT)
//        button.text = "Call with no message"
//        button.setOnClickListener {
//            if(toggle) {
//                button.text = "Call with Message"
//                communicationBridge.callToUnityWithNoMessage()
//            } else {
//                button.text = "Call with no message"
//                communicationBridge.callToUnityWithMessage("surprisebox1")
//            }
//            toggle = !toggle
//        }
//        button.setBackgroundColor(Color.GREEN)
//        button.setTextColor(Color.RED)
//        mUnityPlayer.addView(button)
//
//        layout.addView(mUnityPlayer.view)
//    }
//}