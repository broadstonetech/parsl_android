


package io.parsl.apps.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import io.parsl.apps.Activities.ARActivities.GltfActivity;
import io.parsl.apps.Adaptors.TotalSubtalListAdapter;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.Payments.PaymentMethodSelectionFragment;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Util.Utils;

import static io.parsl.apps.Activities.CartActivity.commissioned;
import static io.parsl.apps.Activities.CartActivity.etName;
import static io.parsl.apps.Activities.CartActivity.msg;
import static io.parsl.apps.Network.Parser.SaveParselCartParser.saveParselCartObj;

public class PaymentResponseActivity extends BaseActivity implements View.OnClickListener {

    TextView tvTranscationID, tvResponse, tvShare, tvAmount, tvResponseOtp, tvRecepientMsg, tvCustmize, tvSend, tvParslUrl, tvTranscationAmount, tvParslCode;
    TextInputEditText etEmail;
    String resText;
    ImageView imgSendParsl;
    RecyclerView rvLastItems;
    Boolean isSharedParsl = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tvTranscationID = findViewById(R.id.tvTranscationID);
        tvResponse = findViewById(R.id.tvResponse);
        tvShare = findViewById(R.id.tvShare);
        tvAmount = findViewById(R.id.tvAmount);
        tvResponseOtp = findViewById(R.id.tvResponseOtp);
        tvRecepientMsg = findViewById(R.id.tvRecepientMsg);
        tvCustmize = findViewById(R.id.tvCustmize);
        tvTranscationAmount = findViewById(R.id.tvTranscationAmount);
        tvParslUrl = findViewById(R.id.parslUrl);
        tvParslCode = findViewById(R.id.parslCode);
        etEmail = findViewById(R.id.etEmail);
        rvLastItems = findViewById(R.id.rvLastItems);
        imgSendParsl = findViewById(R.id.imgSendParsl);

        String amount = Utils.getRoundedDoubleForMoney(PaymentMethodSelectionFragment.finalAmount);
        tvTranscationID.setText(saveParselCartObj.getData().getTransection_id());
        tvAmount.setText("$" + " " + amount);


        tvTranscationAmount.setText("$" + " " + Utils.getRoundedDoubleForMoney(commissioned));
        tvResponseOtp.setText(saveParselCartObj.getData().getParsl_otp());

        resText = msg.getText() + " " + etName.getText().toString();


        tvParslUrl.setText(saveParselCartObj.getData().getParsl_url());
        tvParslCode.setText(saveParselCartObj.getData().getParsl_otp());

        tvResponse.setText(saveParselCartObj.getData().getParsl_url());
        tvRecepientMsg.setText(resText);
        tvShare.setOnClickListener(this);
        tvCustmize.setOnClickListener(this);
        imgSendParsl.setOnClickListener(this);

        rvLastItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        TotalSubtalListAdapter cartListAdapter = new TotalSubtalListAdapter(PaymentResponseActivity.this, GltfActivity.shoppingCartProductList);
        rvLastItems.setAdapter(cartListAdapter);
    }

    @Override
    public int getLayoutId() {
        return R.layout.payment_base_activty_layout;
    }

    //region oclicks
    @Override
    public void onClick(View v) {

        if (v.getId() == tvShare.getId()) {
            if (!isSharedParsl) {
                shareIntent();
                isSharedParsl = true;
            } else {
                loadISharedParslDialog();
            }
        } else if (v.getId() == tvCustmize.getId()) {
            tvRecepientMsg.setEnabled(true);
            tvRecepientMsg.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(tvRecepientMsg, InputMethodManager.SHOW_IMPLICIT);
        }

    }
    //endregion

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        GltfActivity.shoppingCartProductList.clear();
        GltfActivity.productList.clear();
    }

    //region api calls
    private JSONObject params() {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            data.put("transection_id", saveParselCartObj.getData().getTransection_id());
            data.put("card_number", saveParselCartObj.getData().getCard_number());
            data.put("total_price", saveParselCartObj.getData().getTotal_price());
            data.put("parsl_url", saveParselCartObj.getData().getParsl_url());
            data.put("parsl_otp", saveParselCartObj.getData().getParsl_otp());
            data.put("user_email", etEmail.getText().toString());
            data.put("email_condition", "send_transection_and_parsl_information_to_sender");
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    AsyncTaskListener sendEmailListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            showCenteredToastmessage(PaymentResponseActivity.this, "Check your email");
        }
    };
    //endregion

    private void shareIntent() {

//                "to unwrap your PARSL";
        String text = "Download app" + " " + "https://linktr.ee/parsl" + " " + "or use code" + " " + saveParselCartObj.getData().getParsl_otp() + " " + "" +
                "to unwrap your PARSL";
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, resText + " " + text);
        sendIntent.setType("text/plain");

        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
        GltfActivity.shoppingCartProductList.clear();
        GltfActivity.productList.clear();
    }


    private void loadISharedParslDialog() {
        AlertDialog.Builder builderAddInfo = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogViewAddInfo = inflater.inflate(R.layout.dialod_is_shared_parsl, null);
        builderAddInfo.setView(dialogViewAddInfo);
        AlertDialog dialogAddInfo = builderAddInfo.create();
        Objects.requireNonNull(dialogAddInfo.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogAddInfo.show();
        dialogAddInfo.setCancelable(true);

        TextView tvYes = dialogViewAddInfo.findViewById(R.id.tvYes);
        TextView tvNo = dialogViewAddInfo.findViewById(R.id.tvNo);

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareIntent();
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}