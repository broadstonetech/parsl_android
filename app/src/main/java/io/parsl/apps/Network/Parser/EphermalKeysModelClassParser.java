package io.parsl.apps.Network.Parser;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import io.parsl.apps.ModelClass.EphermalKeysModelCLass;
import io.parsl.apps.ModelClass.SuccessMessageClassResponse;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.BaseParser;

public class EphermalKeysModelClassParser implements BaseParser {
    public static EphermalKeysModelCLass ephermalKeysObj = new EphermalKeysModelCLass();
    public static String ephermalKeysJsonString;

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        result.code = httpCode;
        try {
            JSONObject jsonResponse = new JSONObject(response);
            result.code = httpCode;
            if (httpCode == 200 || httpCode == 2) {
                result.setData(jsonResponse);
                result.success(true);

                Gson gson = new Gson();
                ephermalKeysJsonString = response;
                ephermalKeysObj = gson.fromJson(String.valueOf(jsonResponse), EphermalKeysModelCLass.class);
            } else if (httpCode == 400) {
                Gson gson = new Gson();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }

}
