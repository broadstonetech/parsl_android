package io.parsl.apps.Network.Parser;

import com.google.gson.Gson;

import io.parsl.apps.ModelClass.CoupanCodeModelClass;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.BaseParser;

import org.json.JSONException;
import org.json.JSONObject;

public class CoupanCodeDataParser implements BaseParser {
    public static CoupanCodeModelClass coupanCodeDatatObj = new CoupanCodeModelClass();

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        result.code = httpCode;
        try {
            JSONObject jsonResponse = new JSONObject(response);
            result.code = httpCode;
            if (httpCode == 200 || httpCode == 2) {
                result.setData(jsonResponse);
                result.success(true);

                Gson gson = new Gson();
                coupanCodeDatatObj = gson.fromJson(String.valueOf(jsonResponse), CoupanCodeModelClass.class);
            } else if (httpCode == 400) {
                Gson gson = new Gson();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }

}
