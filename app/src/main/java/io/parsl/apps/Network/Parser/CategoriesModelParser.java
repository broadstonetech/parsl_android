package io.parsl.apps.Network.Parser;

import com.google.gson.Gson;

import io.parsl.apps.ModelClass.CategoriesModelClass;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.BaseParser;

import org.json.JSONException;
import org.json.JSONObject;

public class CategoriesModelParser implements BaseParser {
    public static CategoriesModelClass categoriesListObj = new CategoriesModelClass();

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        result.code = httpCode;
        try {
            JSONObject jsonResponse = new JSONObject(response);
            result.code = httpCode;
            if (httpCode == 200 || httpCode == 2) {
                result.setData(jsonResponse);
                result.success(true);

                Gson gson = new Gson();
                categoriesListObj = gson.fromJson(String.valueOf(jsonResponse), CategoriesModelClass.class);
            } else if (httpCode == 400) {
                Gson gson = new Gson();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }

}
