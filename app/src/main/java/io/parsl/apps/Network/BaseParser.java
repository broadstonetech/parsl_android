package io.parsl.apps.Network;


import io.parsl.apps.Network.API.TaskResult;

public interface BaseParser {

	int SUCCESS = 200;
	String SUCCESS_TRUE = "200";

	String KEY_ERROR_CODE = "code";
	String KEY_ERROR_MESSAGE = "message";

	TaskResult parse(int httpCode, String response);
}
