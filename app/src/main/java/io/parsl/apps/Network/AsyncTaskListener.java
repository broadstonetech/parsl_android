package io.parsl.apps.Network;


import io.parsl.apps.Network.API.TaskResult;

public interface AsyncTaskListener {

	void onComplete(TaskResult result);
	
}
