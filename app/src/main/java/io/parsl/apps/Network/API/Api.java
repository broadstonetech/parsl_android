package io.parsl.apps.Network.API;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import io.parsl.apps.Activities.ForgotPassWordActivity;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.Network.Parser.AllVendorListModelClassDataParser;
import io.parsl.apps.Network.Parser.AvatarDetailModelParser;
import io.parsl.apps.Network.Parser.CategoriesDataParser;
import io.parsl.apps.Network.Parser.CategoriesModelParser;
import io.parsl.apps.Network.Parser.CategoriesVendorsDataParser;
import io.parsl.apps.Network.Parser.CoupanCodeDataParser;
import io.parsl.apps.Network.Parser.CreateAvatarParser;
import io.parsl.apps.Network.Parser.EditAvatarModelClassDataParser;
import io.parsl.apps.Network.Parser.EphermalKeysModelClassParser;
import io.parsl.apps.Network.Parser.ForgotPasswordClassDataParser;
import io.parsl.apps.Network.Parser.GetMyParslsModelClassDataParser;
import io.parsl.apps.Network.Parser.GetParselDetailsParser;
import io.parsl.apps.Network.Parser.GetUserCardModelClassDataParser;
import io.parsl.apps.Network.Parser.NotificationClassParser;
import io.parsl.apps.Network.Parser.ParselClassDataParser;
import io.parsl.apps.Network.Parser.ResetPasswordClassDataParser;
import io.parsl.apps.Network.Parser.SaveParselCartParser;
import io.parsl.apps.Network.Parser.SaveParslOtpModelClassDataParser;
import io.parsl.apps.Network.Parser.SuccessMessageClassParser;
import io.parsl.apps.Network.Parser.UserLoginParser;
import io.parsl.apps.Network.Parser.VirtualCardDetailsParser;
import io.parsl.apps.Network.Parser.WalletPublicKeyModelClassDataParser;


public class Api {
    public static final String BASE_URL = "https://api.parsl.io/";
    // private static final String BASE_URL = "https://dev.locusar.com/";
    private static final String GET_CATEGORIES = "get/categories/list";
    private static final String GET_CATEGORIES_MODELS = "get/category/models";
    private static final String GET_PARSEL_MODELS = "get/gift/models";
    private static final String SAVE_PARSEL_CART = "save/parsl/details";
    private static final String GET_PARSEL_DETAILS = "get/parsl/details";
    private static final String GET_VIRTUAL_CARD_DETAILS = "get/virtual/card/details";
    private static final String SAVE_GUETS_API = "save/guest/device/details";
    private static final String CHECK_COUPAN_DETAILS = "check/coupon/details";
    private static final String SEND_EMAIL = "send/email";
    private static final String GET_CATEGORY_VENDORS = "/get/category/vendors";
    private static final String GET_VENDOR_MODELS = "get/vendor/models";
    private static final String GET_VENDORS_LOCATION_BASED = "fetch/vendors";
    private static final String GET_SEARCHED_MODELS = "get/filtered/models";
    private static final String GET_ALL_VENDORS = "get/vendors/list";
    private static final String GET_EPHERMAL_KEYS = "get/ephemeral/key";
    private static final String CREATE_AVATAR = "get/create/avatars";
    private static final String GET_AVATARS = "get/details/avatars";
    private static final String USER_LOGIN = "login";
    private static final String FORGET_PASSWORD = "forget/password";
    private static final String RESET_PASSWORD = "reset/password";
    private static final String EDIT_AVATARS = "edit/avatars";
    private static final String DELETE_AVATARS = "delete/avatars";
    private static final String SAVE_PARSL_OTP = "save/parslotp";
    private static final String GET_MY_PARSLS = "get/myparcel";
    private static String GET_PUSH_NOTIFICATIONS = "get/user/notifications";
    private static String GET_PUBLIC_KEY = "get/public/key";
    private static String GET_USER_WALLET = "get/user/wallet";

    public static void getCategories(Context context, JSONObject data, AsyncTaskListener callback) {
        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_CATEGORIES,
                HttpAsyncRequest.RequestType.JSONDATA, new CategoriesModelParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getNotifications(Context context, JSONObject object, AsyncTaskListener callback) {
        Log.d("JSON==>", object.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_PUSH_NOTIFICATIONS, HttpAsyncRequest.RequestType.JSONDATA,
                new NotificationClassParser(), callback);
        req.setJsonData(object);
        req.execute();
    }

    // this method has been depriciated
    public static void getCategoriesModels(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.e("url", BASE_URL + GET_CATEGORIES_MODELS);
        Log.e("data", data.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_CATEGORIES_MODELS,
                HttpAsyncRequest.RequestType.JSONDATA, new CategoriesDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getParselModels(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_PARSEL_MODELS,
                HttpAsyncRequest.RequestType.JSONDATA, new ParselClassDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void saveParselCart(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + SAVE_PARSEL_CART,
                HttpAsyncRequest.RequestType.JSONDATA, new SaveParselCartParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getParselDetails(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_PARSEL_DETAILS,
                HttpAsyncRequest.RequestType.JSONDATA, new GetParselDetailsParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getVirtualCardDetails(Context context, JSONObject data, AsyncTaskListener callback) {


        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_VIRTUAL_CARD_DETAILS,
                HttpAsyncRequest.RequestType.JSONDATA, new VirtualCardDetailsParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getGuest(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + SAVE_GUETS_API,
                HttpAsyncRequest.RequestType.JSONDATA, new SuccessMessageClassParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getApplyCoupan(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + CHECK_COUPAN_DETAILS,
                HttpAsyncRequest.RequestType.JSONDATA, new CoupanCodeDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void sendEmail(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + SEND_EMAIL,
                HttpAsyncRequest.RequestType.JSONDATA, new SuccessMessageClassParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getCategoryVendors(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_CATEGORY_VENDORS,
                HttpAsyncRequest.RequestType.JSONDATA, new CategoriesVendorsDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getVendorModels(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_VENDOR_MODELS,
                HttpAsyncRequest.RequestType.JSONDATA, new CategoriesDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getVendorsLocationBased(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_VENDORS_LOCATION_BASED,
                HttpAsyncRequest.RequestType.JSONDATA, new AllVendorListModelClassDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getSearchedModel(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_SEARCHED_MODELS,
                HttpAsyncRequest.RequestType.JSONDATA, new CategoriesDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getAllVendors(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_ALL_VENDORS,
                HttpAsyncRequest.RequestType.JSONDATA, new AllVendorListModelClassDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getEphermalKeys(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_EPHERMAL_KEYS,
                HttpAsyncRequest.RequestType.JSONDATA, new EphermalKeysModelClassParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void createAvatar(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + CREATE_AVATAR,
                HttpAsyncRequest.RequestType.JSONDATA, new CreateAvatarParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getAvatars(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_AVATARS,
                HttpAsyncRequest.RequestType.JSONDATA, new AvatarDetailModelParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void userLogin(Context context, JSONObject data, AsyncTaskListener callback) {

        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + USER_LOGIN,
                HttpAsyncRequest.RequestType.JSONDATA, new UserLoginParser(), callback);
        req.setJsonData(data);
        req.execute();
    }


    public static void forgetPassword(ForgotPassWordActivity context, JSONObject data, AsyncTaskListener callback) {
        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + FORGET_PASSWORD,
                HttpAsyncRequest.RequestType.JSONDATA, new ForgotPasswordClassDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void resetPassword(Context context, JSONObject data, AsyncTaskListener callback) {
        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + RESET_PASSWORD,
                HttpAsyncRequest.RequestType.JSONDATA, new ResetPasswordClassDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void editAvtar(Context context, JSONObject data, AsyncTaskListener callback) {
        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + EDIT_AVATARS,
                HttpAsyncRequest.RequestType.JSONDATA, new EditAvatarModelClassDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void deleteAvatar(Context context, JSONObject data, AsyncTaskListener callback) {
        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + DELETE_AVATARS,
                HttpAsyncRequest.RequestType.JSONDATA, new EditAvatarModelClassDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void saveParslOtp(Context context, JSONObject data, AsyncTaskListener callback) {
        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + SAVE_PARSL_OTP,
                HttpAsyncRequest.RequestType.JSONDATA, new SaveParslOtpModelClassDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getMyParsls(Context context, JSONObject data, AsyncTaskListener callback) {
        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_MY_PARSLS,
                HttpAsyncRequest.RequestType.JSONDATA, new GetMyParslsModelClassDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getWalletPublicKey(Context context, JSONObject data, AsyncTaskListener callback) {
        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_PUBLIC_KEY,
                HttpAsyncRequest.RequestType.JSONDATA, new WalletPublicKeyModelClassDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getUserCards(Context context, JSONObject data, AsyncTaskListener callback) {
        HttpAsyncRequest req = new HttpAsyncRequest(context, BASE_URL + GET_USER_WALLET,
                HttpAsyncRequest.RequestType.JSONDATA, new GetUserCardModelClassDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }
}

