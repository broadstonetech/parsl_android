package io.parsl.apps.Network.API;




import io.parsl.apps.Network.CacheOptions;

import java.util.HashMap;

public class TempCacheManager {

	private static TempCacheManager instance = new TempCacheManager();

	private final HashMap<String, LFLCache> map;
	
	private TempCacheManager() {
		map = new HashMap<>();
	}

	public static TempCacheManager getInstance() {
		if(instance == null) {
			instance = new TempCacheManager();
		}
		return instance;
	}
	
	public String get(CacheOptions config) {
		LFLCache cache = map.get(config.getKey());
		if(cache == null) {
			return null;
		}
		long dt = System.currentTimeMillis() - cache.cacheTime;
		if(dt >= config.getCacheTimeOut()) {
			map.remove(config.getKey());
			return null;
		}
		return cache.data;
	}
	
	public void addToCache(String key, String data) {
		LFLCache cache = new LFLCache(data, System.currentTimeMillis());
		map.put(key, cache);
	}

	static class LFLCache {
		final String data;
		final long cacheTime;
		LFLCache(String data, long time) {
			this.data = data;
			this.cacheTime = time;
		}
	}

}
