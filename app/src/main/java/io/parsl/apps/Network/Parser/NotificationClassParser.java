package io.parsl.apps.Network.Parser;


import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.parsl.apps.ModelClass.LocationBasedVendorModelClass;
import io.parsl.apps.ModelClass.NotificationsClass;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.BaseParser;

public class NotificationClassParser implements BaseParser {

    public static NotificationsClass notiObjList= new NotificationsClass();
    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        result.code = httpCode;
        try {
            JSONObject jsonResponse = new JSONObject(response);
            result.code = httpCode;
            if (httpCode == 200 || httpCode == 2) {
                result.setData(jsonResponse);
                result.success(true);

                Gson gson = new Gson();
                notiObjList =  gson.fromJson(String.valueOf(jsonResponse), NotificationsClass.class);

            }
        }catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}
