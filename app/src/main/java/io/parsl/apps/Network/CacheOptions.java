package io.parsl.apps.Network;

public interface CacheOptions {

	String getKey();
	boolean shouldCache();
	int getCacheTimeOut();
	CacheType getType();
	
	
	enum CacheType {
		PREFS, TEMP
	}
	
}
