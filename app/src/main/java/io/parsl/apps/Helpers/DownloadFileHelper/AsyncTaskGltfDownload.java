package io.parsl.apps.Helpers.DownloadFileHelper;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.webkit.URLUtil;

import io.parsl.apps.Activities.ARActivities.GltfActivity;
import io.parsl.apps.ModelClass.ModelsDataClass;
import io.parsl.apps.Utilities.Util.Utils;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.NoSuchAlgorithmException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import io.parsl.apps.Helpers.RSAencryptionDecryption.EncryptionDecryption;

public class AsyncTaskGltfDownload extends AsyncTask<String, String, String> {
    ProgressDialog pd;
    int position;
    Context ctx;
    String modelName;
    String imageFilename = "";
    String textureUrl;
    File fileTexture;
    InputStream streamTexture;
    FileOutputStream fosTexture;
    ModelsDataClass.Data mObj;

    public AsyncTaskGltfDownload(Context ctxt, String itemName, ModelsDataClass.Data obj) {
        pd = new ProgressDialog(ctxt);
        ctx = ctxt;
        modelName = itemName;
        mObj = obj;

    }

    public AsyncTaskGltfDownload(Context ctxt) {
        pd = new ProgressDialog(ctxt);
        ctx = ctxt;
    }


    @Override
    protected String doInBackground(String... params) {
        position = Integer.parseInt(params[0]);

        try {
            String modelName = URLUtil.guessFileName(mObj.getModel_files().get_$3d_model_file(), null, null);
            String URL =mObj.getModel_files().get_$3d_model_file();
            SaveFile(modelName, URL);
        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }


        return "";
    }

    @Override
    protected void onCancelled() {

        super.onCancelled();
    }

    @Override
    protected void onPostExecute(String response) {

        pd.dismiss();
        String path = URLUtil.guessFileName(mObj.getModel_files().get_$3d_model_file(), null, null);
        final File file = getFile(path);
        try {
            String data = EncryptionDecryption.pasreJsonArrayAndDecryptData(mObj);
            String zipFile_SHA1 = Utils.fileSHA1(file);
            if (data.equalsIgnoreCase(zipFile_SHA1)) {
                File unzipFile = unzip(file, new File(Environment.getExternalStorageDirectory().toString() + "/.Parsel"));
                String unZipfileSHA1 = Utils.fileSHA1(unzipFile);

                if (unZipfileSHA1.equalsIgnoreCase(mObj.getCryptographic_info().getModel_sha1())) {


                } else {
                    file.delete();
                }
            }
        else {
                file.delete();
               }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void SaveFile(String filename, String Url) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/.Parsel");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }

        File file = new File(myDir, filename);
        if (!file.exists()) {
            downloadWithoutTexture(Url, file);
        }

    }

    private File getFile(String filename) {

        String root = Environment.getExternalStorageDirectory().toString();

        return new File(root + "/.Parsel", filename);
    }

    public File unzip(File zipFile, File targetDirectory) throws IOException {
        ZipInputStream zis = new ZipInputStream(
                new BufferedInputStream(new FileInputStream(zipFile)));
        File file = null;
        try {
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                file = new File(targetDirectory, ze.getName());
                File dir = ze.isDirectory() ? file : file.getParentFile();
                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Failed to ensure directory: " +
                            dir.getAbsolutePath());
                if (ze.isDirectory())
                    continue;
                FileOutputStream fout = new FileOutputStream(file);
                try {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                } finally {
                    fout.close();
                }
            }
        } finally {
            zis.close();
        }
        return file;
    }

    private void downloadWithoutTexture(String path, File outputFile) {
        int contentLength = 0;

        try {

            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            contentLength = conn.getContentLength();


            InputStream stream = new BufferedInputStream(url.openStream());


            FileOutputStream fos = new FileOutputStream(outputFile);


            byte[] buffer = new byte[1024];
            int readCount = 0;
            int readNum = 0;


            while (readCount < contentLength && readNum != -1) {
                if (isCancelled())
                    break;
                readNum = stream.read(buffer);
                if (readNum > -1) {
                    fos.write(buffer, 0, readNum);

                    readCount = readCount + readNum;

                    publishProgress("" + (int) ((readCount * 100) / contentLength));

                    int progress = (int) ((readCount * 100) / contentLength);

                    String test = String.valueOf(progress);

                }
            }

            fos.flush();
            fos.close();
            stream.close();
        } catch (FileNotFoundException e) {
            Log.d("ExceptionGPSRE1", e.toString());
            return;
        } catch (IOException e) {
            Log.d("ExceptionGPSRE2", e.toString());

            return;
        }
    }


    public void downloadFile(String url, File outputFile) {
        try {
            URL u = new URL(url);
            URLConnection conn = u.openConnection();
            int contentLength = conn.getContentLength();

            DataInputStream stream = new DataInputStream(u.openStream());

            byte[] buffer = new byte[contentLength];
            stream.readFully(buffer);
            stream.close();

            DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
            fos.write(buffer);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            return;
        } catch (IOException e) {
            return;
        }
    }

    public  void SaveVideoFile(String filename, String Url) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/.Parsel");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }

        File file = new File(myDir, filename);
        if (!file.exists())
            downloadFile(Url, file);
    }

}
