package io.parsl.apps.Helpers;

import android.content.Context;
import android.util.Base64;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import io.parsl.apps.Utilities.Constants.GeneralConstants;

public class WalletCardEncryption {

    private static SecretKeySpec secretKey;
    private static byte[] key;
    private static String encryptedMsg;

    public static String getPublicKey(String key, String cardNumer, String checkSum, Context con) throws IOException, GeneralSecurityException {

        key.replace("-", "");

        String secretKey = key.replace("-", "").replaceAll("(?s).(.)?", "$1");


        String decodedCheckSum = getSha512(secretKey);

        if (decodedCheckSum.equalsIgnoreCase(checkSum)) {
            encryptedMsg = AESCrypt.decrypt(secretKey, cardNumer);
        }
        return encryptedMsg;
    }

    public static String getSecretKey(String publicKey, long timestamp) {
        String key = publicKey + timestamp + GeneralConstants.WALLET_CARD_CONSTANT;
        key.replace("-", "");

        String secretKey = key.replace("-", "").replaceAll("(?s).(.)?", "$1");
        return secretKey;
    }

    public static String getSha512(String secretKey) throws IOException {

        final HashFunction hashFunction = Hashing.sha512();
        final HashCode hc = hashFunction
                .newHasher()
                .putString(secretKey, StandardCharsets.UTF_8)
                .hash();
        final String sha512 = hc.toString();

        return sha512;
    }

    public static String decrypt(String strToDecrypt, final String secret) {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "AES");

            Cipher cipher = Cipher.getInstance("AES/ECB/NOPADDING", "BC");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] original = cipher.doFinal(Base64.decode(strToDecrypt, Base64.DEFAULT));


            String decrypted = new String(original);
            return decrypted.replaceAll("=", "");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }
}
