package io.parsl.apps.Helpers.RSAencryptionDecryption;

import android.util.Log;

import io.parsl.apps.ModelClass.ModelsDataClass;

import java.math.BigInteger;
import java.util.List;


public class EncryptionDecryption {
    public static String decryptedString = "";
    static final int MAX = 100000;

    public static String decryptData(List<Integer> publicKey, List<Integer> encryptedData) {
        decryptedString = "";
        Integer exp = publicKey.get(0);
        Integer n = publicKey.get(1);

        for (int i = 0; i < encryptedData.size(); i++) {
            Integer result = powerMod(encryptedData.get(i), exp, n);

            int x = result;
            char y = (char) x;
            String charString = Character.toString(y);
            System.out.println(charString);
            decryptedString += charString;
            Log.d("powerResult==", result.toString());
        }
        Log.d("DECRYPTED_STRING", decryptedString);

        return decryptedString;
    }

    public static Integer powerMod(Integer base, Integer exponent, Integer modolus) {

        if (base > 0 && exponent >= 0 && modolus > 0) {

            Integer result = 1;

            BigInteger exp = new BigInteger(String.valueOf(exponent));
            BigInteger bas = new BigInteger(String.valueOf(base));
            BigInteger modulo = new BigInteger(String.valueOf(modolus));
            BigInteger result1 = bas.modPow(exp, modulo);
            Log.d("Result", String.valueOf(result));

            return Integer.parseInt(String.valueOf(result1));

        } else {
            return -1;
        }

    }


    static void power(int x, int n) {

        //printing value "1" for power = 0
        if (n == 0) {
            System.out.print("1");
            return;
        }
        int res[] = new int[MAX];
        int res_size = 0;
        int temp = x;

        // Initialize result
        while (temp != 0) {
            res[res_size++] = temp % 10;
            temp = temp / 10;
        }

        // Multiply x n times
        // (x^n = x*x*x....n times)
        for (int i = 2; i <= n; i++)
            res_size = multiply(x, res, res_size);

        System.out.print(x + "^" + n + " = ");
        for (int i = res_size - 1; i >= 0; i--)
            System.out.print(res[i]);
    }

    static int multiply(int x, int res[], int res_size) {

        // Initialize carry
        int carry = 0;

        // One by one multiply n with
        // individual digits of res[]
        for (int i = 0; i < res_size; i++) {
            int prod = res[i] * x + carry;

            // Store last digit of
            // 'prod' in res[]
            res[i] = prod % 10;

            // Put rest in carry
            carry = prod / 10;
        }

        // Put carry in res and
        // increase result size
        while (carry > 0) {
            res[res_size] = carry % 10;
            carry = carry / 10;
            res_size++;
        }
        return res_size;
    }


    public static String pasreJsonArrayAndDecryptData(ModelsDataClass.Data obj) {

        return decryptData(obj.getCryptographic_info().getPublic_key(), obj.getCryptographic_info().getDigital_sig());
    }

}
