package io.parsl.apps.ModelClass;

public class ResetPasswordModelClass {

    /**
     * message : Password updated
     * data : {}
     * status : true
     */

    private String message;
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
