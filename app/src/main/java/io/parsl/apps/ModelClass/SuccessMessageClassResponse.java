package io.parsl.apps.ModelClass;

public class SuccessMessageClassResponse {
    /**
     * message : Guest device details added successfuly.
     */

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
