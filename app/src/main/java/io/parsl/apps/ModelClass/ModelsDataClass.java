package io.parsl.apps.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelsDataClass implements Cloneable {

    /**
     * message : Success
     * data : [{"basic_info":{"name":"A2 Milk Box","manufacturer":"LocusAR","model":"A2 Milk Box"},"price_related":{"price":0,"currency":"USD","price_status":"free"},"scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"tested_status":1,"hyperlink":"https://www.amazon.com/fairlife-Ultra-Filtered-Vanilla-Natural-Packaging/dp/B01FUI2HHU/ref=redir_mobile_desktop?ie=UTF8&aaxitk=G8DLDA4zwisTMASZO3e92w&hsa_cr_id=5347013920801&ref_=sb_s_sparkle","document":"","sample_video":"","promotional_video":"","images":[],"license_source":"free3d.com","multi_texture":0,"actions":{"action_title":"Read More","action_type":"link","action_data":"https://www.amazon.com/fairlife-Ultra-Filtered-Vanilla-Natural-Packaging/dp/B01FUI2HHU/ref=redir_mobile_desktop?ie=UTF8&aaxitk=G8DLDA4zwisTMASZO3e92w&hsa_cr_id=5347013920801&ref_=sb_s_sparkle"},"languages_supported":{},"model_files":{"3d_model_file":"https://nyc3.digitaloceanspaces.com/jediar/android_models/a_two_milk_box/a_two_milk_box.zip","thumbnail":"https://nyc3.digitaloceanspaces.com/jediar/android_models/a_two_milk_box/a_two_milk_box_thumbnail.jpg","textures_type":"single","textures":[],"audio_file":"","multitexture_models_list":[]},"video":"","cryptographic_info":{"digital_sig":[514208,453697,481771,42036,286009,42036,481771,659860,453697,355727,514208,355727,514208,239178,193678,566778,239178,42036,481771,168664,332666,656757,453697,453697,193678,168664,298963,168664,332666,355727,566778,239178,286009,298963,122334,193678,239178,42036,168664,481771],"public_key":[203719,693271],"model_sha1":"58a47af90c4bfe0141b177e1a4bca2932a13c8b2","unzipped_size":517656,"zipped_size":320997},"analytics":{"shares_count":0,"download_count":3,"search_appearances":90,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"]},"category":"food","manufacturing_date":"2020-05-05","owner_id":"org6150","purchase_status":1,"format":".glb","license":"Open source","description":"YUP Scoop O' Vanilla is a deliciously flavored ready to drink milk beverage made with 2% low fat ultra filtered milk\nA ready to drink milk beverage with nutritional goodness including 16g of protein calcium and vitamin A and D","additional_info":"{'fat': 'Low Fat', 'filter': 'Ultra-Filtered Milk', 'Flavor': 'Smooth Vanilla Flavor', 'Packaging': '14 fl'}","annotations":{},"multi_node":0,"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"comment_feed":[],"meta_tags":"","model_index":3,"vision_message":"","cpg_key":1,"sub_category":[],"product_id":"5eb1a54c607f0a0787b0f2af:cat_1","model_id":"5eb1a54c607f0a0787b0f2af","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":[]}]},{"basic_info":{"name":"Coca Cola","manufacturer":"LocusAR","model":"Coca Cola"},"price_related":{"price":0,"currency":"USD","price_status":"free"},"scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"tested_status":1,"hyperlink":"https://www.coca-colacompany.com/","document":"","sample_video":"","promotional_video":"","images":[],"license_source":"free3d.com","multi_texture":0,"actions":{"action_title":"Read More","action_type":"link","action_data":"https://www.coca-colacompany.com/"},"languages_supported":{},"model_files":{"3d_model_file":"https://nyc3.digitaloceanspaces.com/jediar/android_models/coca_cola_android/coca_cola_android.zip","thumbnail":"https://nyc3.digitaloceanspaces.com/jediar/android_models/coca_cola_android/coca_cola_android_thumbnail.jpg","textures_type":"single","textures":[],"audio_file":"","multitexture_models_list":[]},"video":"","cryptographic_info":{"digital_sig":[299063,152045,577674,11167,295051,577674,86867,176391,36218,295051,152045,73464,176391,254147,13800,176391,73464,369303,36218,176391,36218,13800,577674,299063,299063,254147,36218,295051,577674,295051,369303,404532,176391,299063,180212,299063,184316,299063,13800,36218],"public_key":[50361,622213],"model_sha1":"4cdb11e6e87b18577dc2cc2352e0066d41e80bdb","unzipped_size":614360,"zipped_size":416023},"analytics":{"shares_count":0,"download_count":0,"search_appearances":90,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"]},"category":"food","manufacturing_date":"2020-05-05","owner_id":"org6150","purchase_status":1,"format":".glb","license":"Open source","description":"On May 8, 1886, Dr. John Pemberton served the world\u2019s first Coca-Cola at Jacobs' Pharmacy in Atlanta, Ga. From that one iconic drink, we\u2019ve evolved into a total beverage company. ","additional_info":"{'company': 'the coca cola company', 'started': 'may 8 1886', 'founder': 'Dr. John Pemberton', 'origin': \"Jacobs' Pharmacy in Atlanta, Ga\"}","annotations":{},"multi_node":0,"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"comment_feed":[],"meta_tags":"","model_index":4,"vision_message":"For optimal vision experience project this object near.","cpg_key":1,"sub_category":[],"product_id":"5eb1a6f1607f0a0787b0f2b0:cat_1","model_id":"5eb1a6f1607f0a0787b0f2b0","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":[]}]},{"basic_info":{"name":"Horizon Milk","manufacturer":"LocusAR","model":"Milk"},"price_related":{"price":0,"currency":"USD","price_status":"free"},"scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"tested_status":1,"hyperlink":"https://horizon.com/","document":"","sample_video":"","promotional_video":"","images":[],"license_source":"free3d.com","multi_texture":0,"actions":{"action_title":"Read More","action_type":"link","action_data":"https://horizon.com/"},"languages_supported":{},"model_files":{"3d_model_file":"https://nyc3.digitaloceanspaces.com/jediar/android_models/horizon_milk_android/horizon_milk_android.zip","thumbnail":"https://nyc3.digitaloceanspaces.com/jediar/android_models/horizon_milk_android/horizon_milk_android_thumbnail.jpg","textures_type":"single","textures":[],"audio_file":"","multitexture_models_list":[]},"video":"","cryptographic_info":{"digital_sig":[447855,478983,118698,478983,22910,22910,320232,80894,80894,483510,478983,118698,227157,123034,169924,320232,80894,310332,163070,80894,428993,428993,123034,310332,118698,428993,483510,22910,447855,320232,447855,358144,461799,123034,483510,478983,22910,123034,22910,123034],"public_key":[427593,501499],"model_sha1":"acf13a90743f660b2148544ee49e56a07502fe75","unzipped_size":2048040,"zipped_size":1526023},"analytics":{"shares_count":0,"download_count":0,"search_appearances":90,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"]},"category":"food","manufacturing_date":"2020-05-05","owner_id":"org6150","purchase_status":1,"format":".glb","license":"Open source","description":"It\u2019s a given that our milk and dairy products meet USDA organic standards, but we don\u2019t stop there. We\u2019re always looking for ways to evolve organic, as well as to broaden our own sustainability initiatives.","additional_info":"{'Taste': 'Organic Taste', 'Farmers': '600', 'Product': 'Organic Cottage chees', 'product': 'Organic Butter'}","annotations":{},"multi_node":0,"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"comment_feed":[],"meta_tags":"","model_index":5,"vision_message":"","cpg_key":1,"sub_category":[],"product_id":"5eb1a8e7607f0a0787b0f2b1:cat_1","model_id":"5eb1a8e7607f0a0787b0f2b1","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":[]}]},{"basic_info":{"name":"Pepsi","manufacturer":"LocusAR","model":"pepsi"},"price_related":{"price":0,"currency":"USD","price_status":"free"},"scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"tested_status":1,"hyperlink":"https://en.wikipedia.org/wiki/PepsiCo","document":"","sample_video":"","promotional_video":"","images":[],"license_source":"free3d.com","multi_texture":0,"actions":{"action_title":"Read More","action_type":"link","action_data":"https://en.wikipedia.org/wiki/PepsiCo"},"languages_supported":{},"model_files":{"3d_model_file":"https://nyc3.digitaloceanspaces.com/jediar/android_models/pepsi_android/pepsi_android.zip","thumbnail":"https://nyc3.digitaloceanspaces.com/jediar/android_models/pepsi_android/pepsi_android_thumbnail.jpg","textures_type":"single","textures":[],"audio_file":"","multitexture_models_list":[]},"video":"","cryptographic_info":{"digital_sig":[85001,157716,218942,2618,157716,133635,512219,39037,512219,560918,218942,133635,560918,223045,512219,157716,512219,85001,560918,85001,344947,2618,223045,223045,578335,295648,23441,269339,259070,427108,2618,85001,39037,560918,218942,269339,157716,578335,23441,578335],"public_key":[505463,648103],"model_sha1":"3eef84840a7f9c24b5339b2e6f3dba9eee19f0f6","unzipped_size":538960,"zipped_size":333870},"analytics":{"shares_count":0,"download_count":0,"search_appearances":90,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"]},"category":"food","manufacturing_date":"2020-05-05","owner_id":"org6150","purchase_status":1,"format":".glb","license":"Open source","description":"PepsiCo, Inc. is an American multinational food, snack, and beverage corporation headquartered in Harrison, New York, in the hamlet of Purchase. PepsiCo has interests in the manufacturing, marketing, and distribution of grain-based snack foods, beverages, and other products.","additional_info":"{'Type': 'Public', 'Industry': 'Beverages ', 'Founded': 'August 28, 1898', 'Founder': 'Caleb Bradham', 'Area served': 'World wide'}","annotations":{},"multi_node":0,"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"comment_feed":[],"meta_tags":"","model_index":6,"vision_message":"","cpg_key":1,"sub_category":[],"product_id":"5eb1aa29607f0a0787b0f2b2:cat_1","model_id":"5eb1aa29607f0a0787b0f2b2","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":[]}]},{"basic_info":{"name":"Aft Lounge","manufacturer":"LocusAR","model":"Lounge"},"price_related":{"price":0,"currency":"USD","price_status":"free"},"scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"hyperlink":"","document":"","sample_video":"","promotional_video":"","images":[],"multi_texture":0,"license_source":"LocusAR","actions":{"action_type":"link","action_title":"Read More","action_data":"https://en.wikipedia.org/wiki/Lounge"},"languages_supported":{},"model_files":{"audio_file":"","textures_type":"single","textures":[],"multitexture_models_list":[],"3d_model_file":"https://nyc3.digitaloceanspaces.com/jediar/android_models/aft_lounge1605174889/aft_lounge1605174889.zip","thumbnail":"https://nyc3.digitaloceanspaces.com/jediar/android_models/aft_lounge1605174889/aft_lounge_thumbnail.png"},"video":"","cryptographic_info":{"digital_sig":[414247,554904,128508,128785,506801,128785,128508,202572,128508,202572,386477,212133,337595,386477,202572,357327,128785,202572,95860,554904,128508,337595,554904,95860,302817,6178,128785,212133,128785,414247,6178,338344,128508,554904,429262,128785,414247,202572,128508,95860],"public_key":[248861,555773],"model_sha1":"6bf78b7f5d2f46aa1fbd398d6c1c56243b4b6c84","unzipped_size":3789064,"zipped_size":3449765},"analytics":{"shares_count":0,"download_count":0,"search_appearances":9,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"]},"category":"food","manufacturing_date":"2020-11-12","owner_id":"org6150","purchase_status":1,"format":".glb","license":"Open source","description":"The aft lounge featured a central section with free-standing seating and modern, fixed circular tables on a laminate wood floor, as seen above. The wooden-backed chairs have alternate dark blue or creamy/beige seats.","additional_info":"{}","annotations":{},"multi_node":0,"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"comment_feed":[],"meta_tags":"","vision_message":"","sub_category":[],"tested_status":1,"model_index":227,"product_id":"5fad06753af0510bb184dbd8:cat_1","model_id":"5fad06753af0510bb184dbd8","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":[]}]},{"basic_info":{"name":"Bush Restaurant","manufacturer":"LocusAR","model":"Restaurant"},"price_related":{"price":0,"currency":"USD","price_status":"free"},"scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"hyperlink":"","document":"","sample_video":"","promotional_video":"","images":[],"multi_texture":0,"license_source":"LocusAR","actions":{"action_type":"link","action_title":"Read More","action_data":"https://en.wikipedia.org/wiki/Restaurant"},"languages_supported":{},"model_files":{"audio_file":"","textures_type":"single","textures":[],"multitexture_models_list":[],"3d_model_file":"https://nyc3.digitaloceanspaces.com/jediar/android_models/bush_restaurant1605175182/bush_restaurant1605175182.zip","thumbnail":"https://nyc3.digitaloceanspaces.com/jediar/android_models/bush_restaurant1605175182/bush_restaurant_thumbnail.png"},"video":"","cryptographic_info":{"digital_sig":[268904,416508,416508,225319,416508,59416,59416,126884,268904,416508,59416,78341,215856,268904,416508,416508,225319,252501,416508,126884,252501,15195,252501,78341,260027,129454,99124,116784,126884,416508,116784,225319,126884,268904,252501,116784,222162,116784,215856,129454],"public_key":[378631,462523],"model_sha1":"c859fa13e1beb97cbd0d5ec1c32ecd28cc981971","unzipped_size":3645956,"zipped_size":3306643},"analytics":{"shares_count":0,"download_count":0,"search_appearances":9,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"]},"category":"food","manufacturing_date":"2020-11-12","owner_id":"org6150","purchase_status":1,"format":".glb","license":"Open source","description":"A restaurant, or an eatery, is a business that prepares and serves food and drinks to customers. Meals are generally served and eaten on the premises, but many restaurants also offer take-out and food delivery services. Restaurants vary greatly in appearance and offerings, including a wide variety of cuisines and service models ranging from inexpensive fast food restaurants and cafeterias, to mid-priced family restaurants, to high-priced luxury establishments.","additional_info":"{}","annotations":{},"multi_node":0,"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"comment_feed":[],"meta_tags":"","vision_message":"","sub_category":[],"tested_status":1,"model_index":228,"product_id":"5fad07923af0510bb184dbd9:cat_1","model_id":"5fad07923af0510bb184dbd9","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":[]}]},{"basic_info":{"name":"Italian Restaurant","manufacturer":"LocusAR","model":"Restaurant"},"price_related":{"price":0,"currency":"USD","price_status":"free"},"scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"hyperlink":"","document":"","sample_video":"","promotional_video":"","images":[],"multi_texture":0,"license_source":"LocusAR","actions":{"action_type":"link","action_title":"Read More","action_data":"https://en.wikipedia.org/wiki/Restaurant"},"languages_supported":{},"model_files":{"audio_file":"","textures_type":"single","textures":[],"multitexture_models_list":[],"3d_model_file":"https://nyc3.digitaloceanspaces.com/jediar/android_models/italian_restaurant1605175804/italian_restaurant1605175804.zip","thumbnail":"https://nyc3.digitaloceanspaces.com/jediar/android_models/italian_restaurant1605175804/italian_restaurant_thumbnail.png"},"video":"","cryptographic_info":{"digital_sig":[220983,336555,354869,220983,433212,498283,134624,336555,483184,66096,401531,476492,328605,328605,171480,66096,354869,234056,171480,171480,549018,171480,404475,220983,220983,591845,498283,220983,336555,354869,234056,220983,354869,549018,591845,404475,234056,476492,483184,498283],"public_key":[427305,592171],"model_sha1":"5066b96b0909050660d80b097ab703e4d93bd028","unzipped_size":1842016,"zipped_size":1504538},"analytics":{"shares_count":0,"download_count":0,"search_appearances":9,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"]},"category":"food","manufacturing_date":"2020-11-12","owner_id":"org6150","purchase_status":1,"format":".glb","license":"Open source","description":"A restaurant, or an eatery, is a business that prepares and serves food and drinks to customers. Meals are generally served and eaten on the premises, but many restaurants also offer take-out and food delivery services. Restaurants vary greatly in appearance and offerings, including a wide variety of cuisines and service models ranging from inexpensive fast food restaurants and cafeterias, to mid-priced family restaurants, to high-priced luxury establishments.","additional_info":"{}","annotations":{},"multi_node":0,"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"comment_feed":[],"meta_tags":"","vision_message":"","sub_category":[],"tested_status":1,"model_index":229,"product_id":"5fad0a093af0510bb184dbdc:cat_1","model_id":"5fad0a093af0510bb184dbdc","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":[]}]},{"basic_info":{"name":"Retro Restaurant","manufacturer":"LocusAR","model":"Restaurant"},"price_related":{"price":0,"currency":"USD","price_status":"free"},"scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"hyperlink":"","document":"","sample_video":"","promotional_video":"","images":[],"multi_texture":0,"license_source":"LocusAR","actions":{"action_type":"link","action_title":"Read More","action_data":"https://en.wikipedia.org/wiki/Restaurant"},"languages_supported":{},"model_files":{"audio_file":"","textures_type":"single","textures":[],"multitexture_models_list":[],"3d_model_file":"https://nyc3.digitaloceanspaces.com/jediar/android_models/retro_restaurant1605176734/retro_restaurant1605176734.zip","thumbnail":"https://nyc3.digitaloceanspaces.com/jediar/android_models/retro_restaurant1605176734/retro_restaurant_thumbnail.png"},"video":"","cryptographic_info":{"digital_sig":[705333,161752,310194,419964,559291,343292,143471,727991,95507,484688,161752,705333,484688,376645,343292,60147,310194,419964,727991,160549,312012,484688,376645,372621,376645,727991,705333,419964,559291,727991,310194,310194,559291,310194,559291,376645,160549,312012,484688,559291],"public_key":[671239,802673],"model_sha1":"e253f75e37ef774c83ba309c2164fcf639d248ed","unzipped_size":1898164,"zipped_size":1560623},"analytics":{"shares_count":0,"download_count":0,"search_appearances":9,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"]},"category":"food","manufacturing_date":"2020-11-12","owner_id":"org6150","purchase_status":1,"format":".glb","license":"Open source","description":"A restaurant, or an eatery, is a business that prepares and serves food and drinks to customers. Meals are generally served and eaten on the premises, but many restaurants also offer take-out and food delivery services. Restaurants vary greatly in appearance and offerings, including a wide variety of cuisines and service models ranging from inexpensive fast food restaurants and cafeterias, to mid-priced family restaurants, to high-priced luxury establishments.","additional_info":"{}","annotations":{},"multi_node":0,"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"comment_feed":[],"meta_tags":"","vision_message":"","sub_category":[],"tested_status":1,"model_index":230,"product_id":"5fad0da33af0510bb184dbdd:cat_1","model_id":"5fad0da33af0510bb184dbdd","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":[]}]}]
     */

    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

    private String message;
    private List<Data> data;
    private List<Avatars> avatars;
    private List<Animations> animations;

    public List<Avatars> getAvatars() {
        return avatars;
    }

    public void setAvatars(List<Avatars> avatars) {
        this.avatars = avatars;
    }

    public List<Animations> getAnimations() {
        return animations;
    }

    public void setAnimations(List<Animations> animations) {
        this.animations = animations;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        /**
         * basic_info : {"name":"A2 Milk Box","manufacturer":"LocusAR","model":"A2 Milk Box"}
         * price_related : {"price":0,"currency":"USD","price_status":"free"}
         * scaling : {"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}}
         * tested_status : 1.0
         * hyperlink : https://www.amazon.com/fairlife-Ultra-Filtered-Vanilla-Natural-Packaging/dp/B01FUI2HHU/ref=redir_mobile_desktop?ie=UTF8&aaxitk=G8DLDA4zwisTMASZO3e92w&hsa_cr_id=5347013920801&ref_=sb_s_sparkle
         * document :
         * sample_video :
         * promotional_video :
         * images : []
         * license_source : free3d.com
         * multi_texture : 0
         * actions : {"action_title":"Read More","action_type":"link","action_data":"https://www.amazon.com/fairlife-Ultra-Filtered-Vanilla-Natural-Packaging/dp/B01FUI2HHU/ref=redir_mobile_desktop?ie=UTF8&aaxitk=G8DLDA4zwisTMASZO3e92w&hsa_cr_id=5347013920801&ref_=sb_s_sparkle"}
         * languages_supported : {}
         * model_files : {"3d_model_file":"https://nyc3.digitaloceanspaces.com/jediar/android_models/a_two_milk_box/a_two_milk_box.zip","thumbnail":"https://nyc3.digitaloceanspaces.com/jediar/android_models/a_two_milk_box/a_two_milk_box_thumbnail.jpg","textures_type":"single","textures":[],"audio_file":"","multitexture_models_list":[]}
         * video :
         * cryptographic_info : {"digital_sig":[514208,453697,481771,42036,286009,42036,481771,659860,453697,355727,514208,355727,514208,239178,193678,566778,239178,42036,481771,168664,332666,656757,453697,453697,193678,168664,298963,168664,332666,355727,566778,239178,286009,298963,122334,193678,239178,42036,168664,481771],"public_key":[203719,693271],"model_sha1":"58a47af90c4bfe0141b177e1a4bca2932a13c8b2","unzipped_size":517656,"zipped_size":320997}
         * analytics : {"shares_count":0,"download_count":3,"search_appearances":90,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"]}
         * category : food
         * manufacturing_date : 2020-05-05
         * owner_id : org6150
         * purchase_status : 1
         * format : .glb
         * license : Open source
         * description : YUP Scoop O' Vanilla is a deliciously flavored ready to drink milk beverage made with 2% low fat ultra filtered milk
         A ready to drink milk beverage with nutritional goodness including 16g of protein calcium and vitamin A and D
         * additional_info : {'fat': 'Low Fat', 'filter': 'Ultra-Filtered Milk', 'Flavor': 'Smooth Vanilla Flavor', 'Packaging': '14 fl'}
         * annotations : {}
         * multi_node : 0
         * questionnaire : ["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"]
         * comment_feed : []
         * meta_tags :
         * model_index : 3
         * vision_message :
         * cpg_key : 1.0
         * sub_category : []
         * product_id : 5eb1a54c607f0a0787b0f2af:cat_1
         * model_id : 5eb1a54c607f0a0787b0f2af
         * isFavorite : 0
         * texture_file : [{"title":"default","default":1,"texturesList":[]}]
         */

        private BasicInfo basic_info;
        private PriceRelated price_related;
        private Scaling scaling;
        private double tested_status;
        private String hyperlink;
        private String document;
        private String sample_video;
        private String promotional_video;
        private List<?> images;
        private String license_source;
        private int multi_texture;
        private Actions actions;
        private LanguagesSupported languages_supported;
        private ModelFiles model_files;
        private String video;
        private CryptographicInfo cryptographic_info;
        private Analytics analytics;
        private String category;
        private String manufacturing_date;
        private String owner_id;
        private int purchase_status;
        private String format;
        private String license;
        private String description;
        private String additional_info;
        private Annotations annotations;
        private int multi_node;
        private List<String> questionnaire;
        private List<?> comment_feed;
        private String meta_tags;
        private int model_index;
        private String vision_message;
        private double cpg_key;
        private List<?> sub_category;
        private String product_id;
        private String model_id;
        private int isFavorite;
        private List<TextureFile> texture_file;
        private boolean default_shadows;
        @Override
        public boolean equals(Object o) {

            // If the object is compared with itself then return true
            if (o == this) {
                return true;
            }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
            if (!(o instanceof ModelsDataClass.Data)) {
                return false;
            }

            // typecast o to Complex so that we can compare data members
            ModelsDataClass.Data c = (ModelsDataClass.Data) o;

            return (this.product_id == null) ? (c.product_id == null) : this.product_id.equals(c.product_id);
        }

        @Override
        public int hashCode() {
            return (43 + 777);
        }

        public BasicInfo getBasic_info() {
            return basic_info;
        }

        public void setBasic_info(BasicInfo basic_info) {
            this.basic_info = basic_info;
        }

        public PriceRelated getPrice_related() {
            return price_related;
        }

        public void setPrice_related(PriceRelated price_related) {
            this.price_related = price_related;
        }

        public Scaling getScaling() {
            return scaling;
        }

        public void setScaling(Scaling scaling) {
            this.scaling = scaling;
        }

        public double getTested_status() {
            return tested_status;
        }

        public void setTested_status(double tested_status) {
            this.tested_status = tested_status;
        }

        public String getHyperlink() {
            return hyperlink;
        }

        public void setHyperlink(String hyperlink) {
            this.hyperlink = hyperlink;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public String getSample_video() {
            return sample_video;
        }

        public void setSample_video(String sample_video) {
            this.sample_video = sample_video;
        }

        public String getPromotional_video() {
            return promotional_video;
        }

        public void setPromotional_video(String promotional_video) {
            this.promotional_video = promotional_video;
        }

        public List<?> getImages() {
            return images;
        }

        public void setImages(List<?> images) {
            this.images = images;
        }

        public String getLicense_source() {
            return license_source;
        }

        public void setLicense_source(String license_source) {
            this.license_source = license_source;
        }

        public int getMulti_texture() {
            return multi_texture;
        }

        public void setMulti_texture(int multi_texture) {
            this.multi_texture = multi_texture;
        }

        public Actions getActions() {
            return actions;
        }

        public void setActions(Actions actions) {
            this.actions = actions;
        }

        public LanguagesSupported getLanguages_supported() {
            return languages_supported;
        }

        public void setLanguages_supported(LanguagesSupported languages_supported) {
            this.languages_supported = languages_supported;
        }

        public ModelFiles getModel_files() {
            return model_files;
        }

        public void setModel_files(ModelFiles model_files) {
            this.model_files = model_files;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public CryptographicInfo getCryptographic_info() {
            return cryptographic_info;
        }

        public void setCryptographic_info(CryptographicInfo cryptographic_info) {
            this.cryptographic_info = cryptographic_info;
        }

        public Analytics getAnalytics() {
            return analytics;
        }

        public void setAnalytics(Analytics analytics) {
            this.analytics = analytics;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getManufacturing_date() {
            return manufacturing_date;
        }

        public void setManufacturing_date(String manufacturing_date) {
            this.manufacturing_date = manufacturing_date;
        }

        public String getOwner_id() {
            return owner_id;
        }

        public void setOwner_id(String owner_id) {
            this.owner_id = owner_id;
        }

        public int getPurchase_status() {
            return purchase_status;
        }

        public void setPurchase_status(int purchase_status) {
            this.purchase_status = purchase_status;
        }

        public String getFormat() {
            return format;
        }

        public void setFormat(String format) {
            this.format = format;
        }

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAdditional_info() {
            return additional_info;
        }

        public void setAdditional_info(String additional_info) {
            this.additional_info = additional_info;
        }

        public Annotations getAnnotations() {
            return annotations;
        }

        public void setAnnotations(Annotations annotations) {
            this.annotations = annotations;
        }

        public int getMulti_node() {
            return multi_node;
        }

        public void setMulti_node(int multi_node) {
            this.multi_node = multi_node;
        }

        public List<String> getQuestionnaire() {
            return questionnaire;
        }

        public void setQuestionnaire(List<String> questionnaire) {
            this.questionnaire = questionnaire;
        }

        public List<?> getComment_feed() {
            return comment_feed;
        }

        public void setComment_feed(List<?> comment_feed) {
            this.comment_feed = comment_feed;
        }

        public String getMeta_tags() {
            return meta_tags;
        }

        public void setMeta_tags(String meta_tags) {
            this.meta_tags = meta_tags;
        }

        public int getModel_index() {
            return model_index;
        }

        public void setModel_index(int model_index) {
            this.model_index = model_index;
        }

        public String getVision_message() {
            return vision_message;
        }

        public void setVision_message(String vision_message) {
            this.vision_message = vision_message;
        }

        public double getCpg_key() {
            return cpg_key;
        }

        public void setCpg_key(double cpg_key) {
            this.cpg_key = cpg_key;
        }

        public List<?> getSub_category() {
            return sub_category;
        }

        public void setSub_category(List<?> sub_category) {
            this.sub_category = sub_category;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getModel_id() {
            return model_id;
        }

        public void setModel_id(String model_id) {
            this.model_id = model_id;
        }

        public int getIsFavorite() {
            return isFavorite;
        }

        public void setIsFavorite(int isFavorite) {
            this.isFavorite = isFavorite;
        }

        public List<TextureFile> getTexture_file() {
            return texture_file;
        }

        public void setTexture_file(List<TextureFile> texture_file) {
            this.texture_file = texture_file;
        }

        public boolean isDefault_shadows() {
            return default_shadows;
        }

        public void setDefault_shadows(boolean default_shadows) {
            this.default_shadows = default_shadows;
        }

        public static class BasicInfo {
            /**
             * name : A2 Milk Box
             * manufacturer : LocusAR
             * model : A2 Milk Box
             */

            private String name;
            private String manufacturer;
            private String model;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getManufacturer() {
                return manufacturer;
            }

            public void setManufacturer(String manufacturer) {
                this.manufacturer = manufacturer;
            }

            public String getModel() {
                return model;
            }

            public void setModel(String model) {
                this.model = model;
            }
        }

        public static class PriceRelated {
            /**
             * price : 0.0
             * currency : USD
             * price_status : free
             */

            private double price;
            private String currency;
            private String price_status;

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getPrice_status() {
                return price_status;
            }

            public void setPrice_status(String price_status) {
                this.price_status = price_status;
            }
        }

        public static class Scaling {
            /**
             * scaling_unit : inches
             * max_scale : {"x-axis":70,"y-axis":70,"z-axis":70}
             * min_scale : {"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}
             */

            private String scaling_unit;
            private MaxScale max_scale;
            private MinScale min_scale;

            public String getScaling_unit() {
                return scaling_unit;
            }

            public void setScaling_unit(String scaling_unit) {
                this.scaling_unit = scaling_unit;
            }

            public MaxScale getMax_scale() {
                return max_scale;
            }

            public void setMax_scale(MaxScale max_scale) {
                this.max_scale = max_scale;
            }

            public MinScale getMin_scale() {
                return min_scale;
            }

            public void setMin_scale(MinScale min_scale) {
                this.min_scale = min_scale;
            }

            public static class MaxScale {
                /**
                 * x-axis : 70
                 * y-axis : 70
                 * z-axis : 70
                 */

                @SerializedName("x-axis")
                private int xaxis;
                @SerializedName("y-axis")
                private int yaxis;
                @SerializedName("z-axis")
                private int zaxis;

                public int getXaxis() {
                    return xaxis;
                }

                public void setXaxis(int xaxis) {
                    this.xaxis = xaxis;
                }

                public int getYaxis() {
                    return yaxis;
                }

                public void setYaxis(int yaxis) {
                    this.yaxis = yaxis;
                }

                public int getZaxis() {
                    return zaxis;
                }

                public void setZaxis(int zaxis) {
                    this.zaxis = zaxis;
                }
            }

            public static class MinScale {
                /**
                 * x-axis : 0.5
                 * y-axis : 0.5
                 * z-axis : 0.5
                 */

                @SerializedName("x-axis")
                private double xaxis;
                @SerializedName("y-axis")
                private double yaxis;
                @SerializedName("z-axis")
                private double zaxis;

                public double getXaxis() {
                    return xaxis;
                }

                public void setXaxis(double xaxis) {
                    this.xaxis = xaxis;
                }

                public double getYaxis() {
                    return yaxis;
                }

                public void setYaxis(double yaxis) {
                    this.yaxis = yaxis;
                }

                public double getZaxis() {
                    return zaxis;
                }

                public void setZaxis(double zaxis) {
                    this.zaxis = zaxis;
                }
            }
        }

        public static class Actions {
            private String action_data;
            private String action_title;
            private String action_type;

            public String getAction_data() {
                return action_data;
            }

            public void setAction_data(String action_data) {
                this.action_data = action_data;
            }

            public String getAction_title() {
                return action_title;
            }

            public void setAction_title(String action_title) {
                this.action_title = action_title;
            }

            public String getAction_type() {
                return action_type;
            }

            public void setAction_type(String action_type) {
                this.action_type = action_type;
            }
        }
        public static class LanguagesSupported {
        }

        public static class ModelFiles {
            /**
             * 3d_model_file : https://nyc3.digitaloceanspaces.com/jediar/android_models/a_two_milk_box/a_two_milk_box.zip
             * thumbnail : https://nyc3.digitaloceanspaces.com/jediar/android_models/a_two_milk_box/a_two_milk_box_thumbnail.jpg
             * textures_type : single
             * textures : []
             * audio_file :
             * multitexture_models_list : []
             */

            @SerializedName("3d_model_file")
            private String _$3d_model_file;
            private String thumbnail;
            private String textures_type;
            private List<?> textures;
            private String audio_file;
            private List<?> multitexture_models_list;

            public String get_$3d_model_file() {
                return _$3d_model_file;
            }

            public void set_$3d_model_file(String _$3d_model_file) {
                this._$3d_model_file = _$3d_model_file;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getTextures_type() {
                return textures_type;
            }

            public void setTextures_type(String textures_type) {
                this.textures_type = textures_type;
            }

            public List<?> getTextures() {
                return textures;
            }

            public void setTextures(List<?> textures) {
                this.textures = textures;
            }

            public String getAudio_file() {
                return audio_file;
            }

            public void setAudio_file(String audio_file) {
                this.audio_file = audio_file;
            }

            public List<?> getMultitexture_models_list() {
                return multitexture_models_list;
            }

            public void setMultitexture_models_list(List<?> multitexture_models_list) {
                this.multitexture_models_list = multitexture_models_list;
            }
        }

        public static class CryptographicInfo {
            /**
             * digital_sig : [514208,453697,481771,42036,286009,42036,481771,659860,453697,355727,514208,355727,514208,239178,193678,566778,239178,42036,481771,168664,332666,656757,453697,453697,193678,168664,298963,168664,332666,355727,566778,239178,286009,298963,122334,193678,239178,42036,168664,481771]
             * public_key : [203719,693271]
             * model_sha1 : 58a47af90c4bfe0141b177e1a4bca2932a13c8b2
             * unzipped_size : 517656
             * zipped_size : 320997
             */

            private List<Integer> digital_sig;
            private List<Integer> public_key;
            private String model_sha1;
            private int unzipped_size;
            private int zipped_size;

            public List<Integer> getDigital_sig() {
                return digital_sig;
            }

            public void setDigital_sig(List<Integer> digital_sig) {
                this.digital_sig = digital_sig;
            }

            public List<Integer> getPublic_key() {
                return public_key;
            }

            public void setPublic_key(List<Integer> public_key) {
                this.public_key = public_key;
            }

            public String getModel_sha1() {
                return model_sha1;
            }

            public void setModel_sha1(String model_sha1) {
                this.model_sha1 = model_sha1;
            }

            public int getUnzipped_size() {
                return unzipped_size;
            }

            public void setUnzipped_size(int unzipped_size) {
                this.unzipped_size = unzipped_size;
            }

            public int getZipped_size() {
                return zipped_size;
            }

            public void setZipped_size(int zipped_size) {
                this.zipped_size = zipped_size;
            }
        }

        public static class Analytics {
        }

        public static class Annotations {
        }

        public static class TextureFile {
            /**
             * title : default
             * default : 1
             * texturesList : []
             */

            private String title;
            @SerializedName("default")
            private int defaultX;
            private List<?> texturesList;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getDefaultX() {
                return defaultX;
            }

            public void setDefaultX(int defaultX) {
                this.defaultX = defaultX;
            }

            public List<?> getTexturesList() {
                return texturesList;
            }

            public void setTexturesList(List<?> texturesList) {
                this.texturesList = texturesList;
            }
        }
    }

    public static class Avatars {
        public String getChracter_key() {
            return chracter_key;
        }

        public void setChracter_key(String chracter_key) {
            this.chracter_key = chracter_key;
        }

        public String getChracter_thumbnail() {
            return chracter_thumbnail;
        }

        public void setChracter_thumbnail(String chracter_thumbnail) {
            this.chracter_thumbnail = chracter_thumbnail;
        }

        String chracter_key;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        String id;
        String chracter_thumbnail;
    }

    public static class Animations{
        public String getAnimation_key() {
            return animation_key;
        }

        public void setAnimation_key(String animation_key) {
            this.animation_key = animation_key;
        }

        public String getAnimation_thumbnail() {
            return animation_thumbnail;
        }

        public void setAnimation_thumbnail(String animation_thumbnail) {
            this.animation_thumbnail = animation_thumbnail;
        }

        String animation_key;
        String animation_thumbnail;
    }
}
