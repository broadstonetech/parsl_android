package io.parsl.apps.ModelClass;

import java.util.List;

public class GetMyParslsModelClass {

    /**
     * message : Success
     * data : [{"namespace":"pr2431","parsl_otp":"C66242","app_id":"parsl_android","redeemed_status":true,"sender_name":"BST Tester4","timestamp":1641483430971}]
     * status : true
     */

    private String message;
    private boolean status;
    /**
     * namespace : pr2431
     * parsl_otp : C66242
     * app_id : parsl_android
     * redeemed_status : true
     * sender_name : BST Tester4
     * timestamp : 1641483430971
     */

    private List<Data> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        private String namespace;
        private String parsl_otp;
        private String app_id;
        private boolean redeemed_status;
        private String sender_name;
        private long timestamp;

        public String getNamespace() {
            return namespace;
        }

        public void setNamespace(String namespace) {
            this.namespace = namespace;
        }

        public String getParsl_otp() {
            return parsl_otp;
        }

        public void setParsl_otp(String parsl_otp) {
            this.parsl_otp = parsl_otp;
        }

        public String getApp_id() {
            return app_id;
        }

        public void setApp_id(String app_id) {
            this.app_id = app_id;
        }

        public boolean isRedeemed_status() {
            return redeemed_status;
        }

        public void setRedeemed_status(boolean redeemed_status) {
            this.redeemed_status = redeemed_status;
        }

        public String getSender_name() {
            return sender_name;
        }

        public void setSender_name(String sender_name) {
            this.sender_name = sender_name;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }
    }
}
