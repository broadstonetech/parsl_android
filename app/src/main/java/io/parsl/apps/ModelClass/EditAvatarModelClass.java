package io.parsl.apps.ModelClass;

public class EditAvatarModelClass {
    /**
     * success : true
     * message : success
     */

    private boolean status;
    private String message;

    public boolean isSuccess() {
        return status;
    }

    public void setSuccess(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
