package io.parsl.apps.ModelClass;

import java.util.List;

public class GetPArselDetailsModelClass {
    /**
     * message : Success
     * data : {"gift_model":{"basic_info":{"name":"Test v1 Gift Box Animation","manufacturer":"LocusAR","model":"Sunglasses"},"price_related":{"price":0,"currency":"USD","price_status":"free"},"scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"hyperlink":"","document":"","sample_video":"","promotional_video":"","images":[],"multi_texture":0,"license_source":"LocusAR","actions":{"action_type":"link","action_title":"Shop Now","action_data":"https://www.johnlewis.com/gucci-gc001245-men's-aviator-sunglasses-silver-brown/p4875641"},"languages_supported":{},"model_files":{"audio_file":"","textures_type":"single","textures":[],"multitexture_models_list":[],"3d_model_file":"https://cdn.locusar.com/models/test_v1_gift_box_animation1612802091/test_v1_gift_box_animation1612802091.usdz","thumbnail":"https://cdn.locusar.com/models/test_v1_gift_box_animation1612802091/test_v1_gift_box_animation_thumbnail.png"},"video":"","cryptographic_info":{"digital_sig":[177234,285381,111459,223489,87509,111459,34380,142093,279746,279746,279746,87509,29003,142093,294763,203207,29003,1552,34380,254106,29003,203207,254106,144679,142093,1552,285381,319717,34380,294763,319717,57803,57803,144679,254106,279746,111459,142093,254106,203207],"public_key":[258169,347371],"model_sha1":"e949a6f638fd92e407b4b8c17540f9400ce1a2ac","unzipped_size":3565865,"zipped_size":2584101},"analytics":{"shares_count":0,"download_count":0,"search_appearances":0,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"]},"category":"fashion","manufacturing_date":"2021-01-22","owner_id":"org6150","purchase_status":1,"format":".usdz","license":"Open source","description":"Carrying the sleek jet-setter vision of the brand, these Gucci sunglasses offer the best balance between style and functionality.The slim metal frame is there to provide resilient and durable wear, while the high-filter lenses will keep your vision clear on a bright day. The green and red stripe, the iconic feature of the brand, adorn the arms.","additional_info":"{}","annotations":{},"multi_node":0,"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"comment_feed":[],"meta_tags":"","vision_message":"","sub_category":["gift_fashion"],"tested_status":1,"model_index":250,"product_id":"602168333f3e15d44e7902c1:cat_3","model_id":"602168333f3e15d44e7902c1","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":[]}]},"sender_namespace":"org6150","parsl_stripe_token":"tok_1ISdnHBvUsVxAP8u308I8RmT","reciever_data":{"reciever_email":"bbakar4aws@gmail.com","reciever_name":"bilal","reciever_phone_no":"03354533444","text_msg":"this is gift","video_msg":""},"price_related":{"price":1,"currency":"usd"},"parsl_timestamp":1615189583584,"parsl_otp":"87E413C6","parsl_available_flag":false,"parsl_redeemed":false,"parsl_id":"6045d650207cf21979e4e225","parsl_models":[{"category":"food","manufacturing_date":"dec 28, 2018","description":"Bread is a staple food prepared from a dough of flour and water, usually by baking.","license":"open source","format":".scn","price_related":{"currency":"USD","price":0},"basic_info":{"model":"chicken","name":"Bread","manufacturer":"LocusAR"},"analytics":{"download_count":294,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"search_appearances":4460,"shares_count":14},"comment_feed":[],"model_files":{"textures":["https://cdn.locusar.com/models/bread/plate.jpg","https://cdn.locusar.com/models/bread/breadbmp.jpg","https://cdn.locusar.com/models/bread/bread_main.jpg"],"3d_model_file":"https://cdn.locusar.com/models/bread/bread.zip","thumbnail":"https://cdn.locusar.com/models/bread/thumbnail.png","audio_file":"","multitexture_models_list":[]},"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"additional_info":"{'calories': '100', 'fat': '3g', 'carbohydrates': '17g', 'protein': '1g', 'cholesterol': '5mg'}","cryptographic_info":{"public_key":[739,3233],"private_key":[1579,3233],"zipped_size":1135375,"digital_sig":[2015,1711,1019,2015,72,72,2015,2173,2635,564,2244,228,228,492,492,564,1971,2798,492,564,228,1019,1430,200,2173,1430,1711,1971,1971,1711,1430,2244,1019,1971,72,564,228,2015,564,1019],"model_sha1":"8f79a80f824e98876b68066fc04465c15e102608","unzipped_size":1556261},"owner_id":"org6150","app_name":"jedi","scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"purchase_status":1,"promotional_video":"https://cdn.locusar.com/promotionalvideo.mp4","tested_status":1,"document":"","hyperlink":"","images":[],"license_source":"free3d.com","multi_texture":0,"multi_node":0,"sample_video":"https://cdn.locusar.com/models/cookie_with_coffee/cakechroma.mp4","video":"","actions":{"action_title":"","action_type":"","action_data":""},"model_index":1,"languages_supported":{},"meta_tags":"breadbasket bread breadstick brown_bread bread_sauce bread_dough cuckoo_bread bread_mold","vision_message":"","sub_category":[],"product_id":"5cd3fb37eb0ceb4ab8674089:cat_1","model_id":"5cd3fb37eb0ceb4ab8674089","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":["https://cdn.locusar.com/models/bread/plate.jpg","https://cdn.locusar.com/models/bread/breadbmp.jpg","https://cdn.locusar.com/models/bread/bread_main.jpg"]}]},{"category":"food","manufacturing_date":"May 21, 2019","description":"Quick, Easy and Delicious-Homemade pizza crust and tomato sauce has never been easier-and this recipe for pepperoni pizza produces a delicious classic!","license":"Open source","format":".scn","price_related":{"currency":"USD","price":0},"basic_info":{"model":"Pepperoni","name":"Pepperoni pizza","manufacturer":"Pizza Parlour"},"analytics":{"download_count":843,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"search_appearances":3107,"shares_count":3},"comment_feed":[],"model_files":{"textures":["https://cdn.locusar.com/models/PepperoniPizza/cardboard.jpg","https://cdn.locusar.com/models/PepperoniPizza/pizza_top.png"],"3d_model_file":"https://cdn.locusar.com/models/PepperoniPizza/PepperoniPizza.zip","thumbnail":"https://cdn.locusar.com/models/PepperoniPizza/pepo.jpg","audio_file":"","multitexture_models_list":[]},"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"additional_info":"{'Total Fat': '10 g', 'Cholesterol': '18 mg', 'Sodium': '743 mg', 'Potassium': '212 mg', 'Total Carbohydrate': '31 g', 'Protein': '12 g'}","cryptographic_info":{"public_key":[1523,3233],"private_key":[4427,3233],"zipped_size":114372,"digital_sig":[1905,1841,1968,2113,1968,1905,2113,2113,1431,576,1983,1462,2242,2242,576,24,1968,529,858,1968,858,2920,2113,2113,529,1905,2920,478,2113,3018,576,24,529,1462,576,3018,858,2113,576,478],"model_sha1":"5214d7ebf07647e34be4443412318e60acd65a23","unzipped_size":309315},"owner_id":"org4929","app_name":"jedi","scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"purchase_status":1,"promotional_video":"https://cdn.locusar.com/promotionalvideo.mp4","tested_status":1,"document":"","hyperlink":"","images":[],"license_source":"free3d.com","multi_texture":0,"multi_node":0,"sample_video":"https://cdn.locusar.com/models/ChickenBread/modelvideo.mp4","video":"https://cdn.locusar.com/models/wooden_tabel_with_chairs/modelvideo.mp4","actions":{"action_title":"","action_type":"","action_data":""},"model_index":3,"languages_supported":{},"meta_tags":"lipizzan pizza_shop pizza_parlor pizza pizza_pie sausage_pizza cheese_pizza anchovy_pizza Sicilian_pizza pepperoni_pizza pizza_cutter","vision_message":"","sub_category":[],"product_id":"5ce79d55eb0ceb5a154d7dd8:cat_1","model_id":"5ce79d55eb0ceb5a154d7dd8","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":["https://cdn.locusar.com/models/PepperoniPizza/cardboard.jpg","https://cdn.locusar.com/models/PepperoniPizza/pizza_top.png"]}]}]}
     */

    private String message;
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        /**
         * gift_model : {"basic_info":{"name":"Test v1 Gift Box Animation","manufacturer":"LocusAR","model":"Sunglasses"},"price_related":{"price":0,"currency":"USD","price_status":"free"},"scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"hyperlink":"","document":"","sample_video":"","promotional_video":"","images":[],"multi_texture":0,"license_source":"LocusAR","actions":{"action_type":"link","action_title":"Shop Now","action_data":"https://www.johnlewis.com/gucci-gc001245-men's-aviator-sunglasses-silver-brown/p4875641"},"languages_supported":{},"model_files":{"audio_file":"","textures_type":"single","textures":[],"multitexture_models_list":[],"3d_model_file":"https://cdn.locusar.com/models/test_v1_gift_box_animation1612802091/test_v1_gift_box_animation1612802091.usdz","thumbnail":"https://cdn.locusar.com/models/test_v1_gift_box_animation1612802091/test_v1_gift_box_animation_thumbnail.png"},"video":"","cryptographic_info":{"digital_sig":[177234,285381,111459,223489,87509,111459,34380,142093,279746,279746,279746,87509,29003,142093,294763,203207,29003,1552,34380,254106,29003,203207,254106,144679,142093,1552,285381,319717,34380,294763,319717,57803,57803,144679,254106,279746,111459,142093,254106,203207],"public_key":[258169,347371],"model_sha1":"e949a6f638fd92e407b4b8c17540f9400ce1a2ac","unzipped_size":3565865,"zipped_size":2584101},"analytics":{"shares_count":0,"download_count":0,"search_appearances":0,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"]},"category":"fashion","manufacturing_date":"2021-01-22","owner_id":"org6150","purchase_status":1,"format":".usdz","license":"Open source","description":"Carrying the sleek jet-setter vision of the brand, these Gucci sunglasses offer the best balance between style and functionality.The slim metal frame is there to provide resilient and durable wear, while the high-filter lenses will keep your vision clear on a bright day. The green and red stripe, the iconic feature of the brand, adorn the arms.","additional_info":"{}","annotations":{},"multi_node":0,"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"comment_feed":[],"meta_tags":"","vision_message":"","sub_category":["gift_fashion"],"tested_status":1,"model_index":250,"product_id":"602168333f3e15d44e7902c1:cat_3","model_id":"602168333f3e15d44e7902c1","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":[]}]}
         * sender_namespace : org6150
         * parsl_stripe_token : tok_1ISdnHBvUsVxAP8u308I8RmT
         * reciever_data : {"reciever_email":"bbakar4aws@gmail.com","reciever_name":"bilal","reciever_phone_no":"03354533444","text_msg":"this is gift","video_msg":""}
         * price_related : {"price":1,"currency":"usd"}
         * parsl_timestamp : 1615189583584
         * parsl_otp : 87E413C6
         * parsl_available_flag : false
         * parsl_redeemed : false
         * parsl_id : 6045d650207cf21979e4e225
         * parsl_models : [{"category":"food","manufacturing_date":"dec 28, 2018","description":"Bread is a staple food prepared from a dough of flour and water, usually by baking.","license":"open source","format":".scn","price_related":{"currency":"USD","price":0},"basic_info":{"model":"chicken","name":"Bread","manufacturer":"LocusAR"},"analytics":{"download_count":294,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"search_appearances":4460,"shares_count":14},"comment_feed":[],"model_files":{"textures":["https://cdn.locusar.com/models/bread/plate.jpg","https://cdn.locusar.com/models/bread/breadbmp.jpg","https://cdn.locusar.com/models/bread/bread_main.jpg"],"3d_model_file":"https://cdn.locusar.com/models/bread/bread.zip","thumbnail":"https://cdn.locusar.com/models/bread/thumbnail.png","audio_file":"","multitexture_models_list":[]},"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"additional_info":"{'calories': '100', 'fat': '3g', 'carbohydrates': '17g', 'protein': '1g', 'cholesterol': '5mg'}","cryptographic_info":{"public_key":[739,3233],"private_key":[1579,3233],"zipped_size":1135375,"digital_sig":[2015,1711,1019,2015,72,72,2015,2173,2635,564,2244,228,228,492,492,564,1971,2798,492,564,228,1019,1430,200,2173,1430,1711,1971,1971,1711,1430,2244,1019,1971,72,564,228,2015,564,1019],"model_sha1":"8f79a80f824e98876b68066fc04465c15e102608","unzipped_size":1556261},"owner_id":"org6150","app_name":"jedi","scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"purchase_status":1,"promotional_video":"https://cdn.locusar.com/promotionalvideo.mp4","tested_status":1,"document":"","hyperlink":"","images":[],"license_source":"free3d.com","multi_texture":0,"multi_node":0,"sample_video":"https://cdn.locusar.com/models/cookie_with_coffee/cakechroma.mp4","video":"","actions":{"action_title":"","action_type":"","action_data":""},"model_index":1,"languages_supported":{},"meta_tags":"breadbasket bread breadstick brown_bread bread_sauce bread_dough cuckoo_bread bread_mold","vision_message":"","sub_category":[],"product_id":"5cd3fb37eb0ceb4ab8674089:cat_1","model_id":"5cd3fb37eb0ceb4ab8674089","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":["https://cdn.locusar.com/models/bread/plate.jpg","https://cdn.locusar.com/models/bread/breadbmp.jpg","https://cdn.locusar.com/models/bread/bread_main.jpg"]}]},{"category":"food","manufacturing_date":"May 21, 2019","description":"Quick, Easy and Delicious-Homemade pizza crust and tomato sauce has never been easier-and this recipe for pepperoni pizza produces a delicious classic!","license":"Open source","format":".scn","price_related":{"currency":"USD","price":0},"basic_info":{"model":"Pepperoni","name":"Pepperoni pizza","manufacturer":"Pizza Parlour"},"analytics":{"download_count":843,"survey":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"search_appearances":3107,"shares_count":3},"comment_feed":[],"model_files":{"textures":["https://cdn.locusar.com/models/PepperoniPizza/cardboard.jpg","https://cdn.locusar.com/models/PepperoniPizza/pizza_top.png"],"3d_model_file":"https://cdn.locusar.com/models/PepperoniPizza/PepperoniPizza.zip","thumbnail":"https://cdn.locusar.com/models/PepperoniPizza/pepo.jpg","audio_file":"","multitexture_models_list":[]},"questionnaire":["What you will recommend us?","How would you like to rate our services?","What you will recommend us?","How would you like to rate our services?"],"additional_info":"{'Total Fat': '10 g', 'Cholesterol': '18 mg', 'Sodium': '743 mg', 'Potassium': '212 mg', 'Total Carbohydrate': '31 g', 'Protein': '12 g'}","cryptographic_info":{"public_key":[1523,3233],"private_key":[4427,3233],"zipped_size":114372,"digital_sig":[1905,1841,1968,2113,1968,1905,2113,2113,1431,576,1983,1462,2242,2242,576,24,1968,529,858,1968,858,2920,2113,2113,529,1905,2920,478,2113,3018,576,24,529,1462,576,3018,858,2113,576,478],"model_sha1":"5214d7ebf07647e34be4443412318e60acd65a23","unzipped_size":309315},"owner_id":"org4929","app_name":"jedi","scaling":{"scaling_unit":"inches","max_scale":{"x-axis":70,"y-axis":70,"z-axis":70},"min_scale":{"x-axis":0.5,"y-axis":0.5,"z-axis":0.5}},"purchase_status":1,"promotional_video":"https://cdn.locusar.com/promotionalvideo.mp4","tested_status":1,"document":"","hyperlink":"","images":[],"license_source":"free3d.com","multi_texture":0,"multi_node":0,"sample_video":"https://cdn.locusar.com/models/ChickenBread/modelvideo.mp4","video":"https://cdn.locusar.com/models/wooden_tabel_with_chairs/modelvideo.mp4","actions":{"action_title":"","action_type":"","action_data":""},"model_index":3,"languages_supported":{},"meta_tags":"lipizzan pizza_shop pizza_parlor pizza pizza_pie sausage_pizza cheese_pizza anchovy_pizza Sicilian_pizza pepperoni_pizza pizza_cutter","vision_message":"","sub_category":[],"product_id":"5ce79d55eb0ceb5a154d7dd8:cat_1","model_id":"5ce79d55eb0ceb5a154d7dd8","isFavorite":0,"texture_file":[{"title":"default","default":1,"texturesList":["https://cdn.locusar.com/models/PepperoniPizza/cardboard.jpg","https://cdn.locusar.com/models/PepperoniPizza/pizza_top.png"]}]}]
         */

        private ModelsDataClass.Data gift_model;
        private String sender_namespace;
        private String parsl_stripe_token;
        private RecieverData reciever_data;
        private SenderData sender_data;
        private PriceRelated price_related;
        private long parsl_timestamp;
        private String parsl_otp;
        private boolean parsl_available_flag;
        private boolean parsl_redeemed;
        private String parsl_id;
        private List<ModelsDataClass.Data> parsl_models;
        private String avatar_animation;
        private String avatar_chracter;

        public double getParsl_amount() {
            return parsl_amount;
        }

        public void setParsl_amount(double parsl_amount) {
            this.parsl_amount = parsl_amount;
        }

        double parsl_amount;
        private boolean default_video;


        public boolean isDefault_video() {
            return default_video;
        }

        public void setDefault_video(boolean default_video) {
            this.default_video = default_video;
        }

        public String getAvatar_animation() {
            return avatar_animation;
        }

        public void setAvatar_animation(String avatar_animation) {
            this.avatar_animation = avatar_animation;
        }

        public String getAvatar_chracter() {
            return avatar_chracter;
        }

        public void setAvatar_chracter(String avatar_chracter) {
            this.avatar_chracter = avatar_chracter;
        }


        public SenderData getSender_data() {
            return sender_data;
        }

        public void setSender_data(SenderData sender_data) {
            this.sender_data = sender_data;
        }



        public ModelsDataClass.Data getGift_model() {
            return gift_model;
        }

        public void setGift_model(ModelsDataClass.Data gift_model) {
            this.gift_model = gift_model;
        }

        public String getSender_namespace() {
            return sender_namespace;
        }

        public void setSender_namespace(String sender_namespace) {
            this.sender_namespace = sender_namespace;
        }

        public String getParsl_stripe_token() {
            return parsl_stripe_token;
        }

        public void setParsl_stripe_token(String parsl_stripe_token) {
            this.parsl_stripe_token = parsl_stripe_token;
        }

        public RecieverData getReciever_data() {
            return reciever_data;
        }

        public void setReciever_data(RecieverData reciever_data) {
            this.reciever_data = reciever_data;
        }

        public PriceRelated getPrice_related() {
            return price_related;
        }

        public void setPrice_related(PriceRelated price_related) {
            this.price_related = price_related;
        }

        public long getParsl_timestamp() {
            return parsl_timestamp;
        }

        public void setParsl_timestamp(long parsl_timestamp) {
            this.parsl_timestamp = parsl_timestamp;
        }

        public String getParsl_otp() {
            return parsl_otp;
        }

        public void setParsl_otp(String parsl_otp) {
            this.parsl_otp = parsl_otp;
        }

        public boolean isParsl_available_flag() {
            return parsl_available_flag;
        }

        public void setParsl_available_flag(boolean parsl_available_flag) {
            this.parsl_available_flag = parsl_available_flag;
        }

        public boolean isParsl_redeemed() {
            return parsl_redeemed;
        }

        public void setParsl_redeemed(boolean parsl_redeemed) {
            this.parsl_redeemed = parsl_redeemed;
        }

        public String getParsl_id() {
            return parsl_id;
        }

        public void setParsl_id(String parsl_id) {
            this.parsl_id = parsl_id;
        }

        public List<ModelsDataClass.Data> getParsl_models() {
            return parsl_models;
        }

        public void setParsl_models(List<ModelsDataClass.Data> parsl_models) {
            this.parsl_models = parsl_models;
        }

        public static class RecieverData {
            /**
             * reciever_email : bbakar4aws@gmail.com
             * reciever_name : bilal
             * reciever_phone_no : 03354533444
             * text_msg : this is gift
             * video_msg :
             */

            private String reciever_email;
            private String reciever_name;
            private String reciever_phone_no;
            private String text_msg;
            private String video_msg;
            private Address address;

            public Address getAddress() {
                return address;
            }

            public void setAddress(Address address) {
                this.address = address;
            }

            public String getReciever_email() {
                return reciever_email;
            }

            public void setReciever_email(String reciever_email) {
                this.reciever_email = reciever_email;
            }

            public String getReciever_name() {
                return reciever_name;
            }

            public void setReciever_name(String reciever_name) {
                this.reciever_name = reciever_name;
            }

            public String getReciever_phone_no() {
                return reciever_phone_no;
            }

            public void setReciever_phone_no(String reciever_phone_no) {
                this.reciever_phone_no = reciever_phone_no;
            }

            public String getText_msg() {
                return text_msg;
            }

            public void setText_msg(String text_msg) {
                this.text_msg = text_msg;
            }

            public String getVideo_msg() {
                return video_msg;
            }

            public void setVideo_msg(String video_msg) {
                this.video_msg = video_msg;
            }
        }

        public static class SenderData {
            public String getSender_email() {
                return sender_email;
            }

            public void setSender_email(String sender_email) {
                this.sender_email = sender_email;
            }





            public String getSender_name() {
                return sender_name;
            }

            public void setSender_name(String sender_name) {
                this.sender_name = sender_name;
            }

            public String getSender_phone_no() {
                return sender_phone_no;
            }

            public void setSender_phone_no(String sender_phone_no) {
                this.sender_phone_no = sender_phone_no;
            }

            private String sender_name;
            private String sender_phone_no;
            private String sender_email;
            private Address address;

            public Address getAddress() {
                return address;
            }

            public void setAddress(Address address) {
                this.address = address;
            }



        }

        public static class Address{

            public String getLine1() {
                return line1;
            }

            public void setLine1(String line1) {
                this.line1 = line1;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getPostal_code() {
                return postal_code;
            }

            public void setPostal_code(String postal_code) {
                this.postal_code = postal_code;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            private String line1;
            private String city;
            private String state;
            private String postal_code;
            private String country;
        }
        public static class PriceRelated {
            /**
             * price : 1
             * currency : usd
             */

            private double price;
            private String currency;

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }
        }

    }
}
