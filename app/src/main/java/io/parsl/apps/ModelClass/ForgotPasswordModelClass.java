package io.parsl.apps.ModelClass;

public class ForgotPasswordModelClass {

    /**
     * message : We have sent a verification code to bil*******042@gmail.com.Enter the code to verify.
     * data : {}
     * status : true
     */

    private String message;
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
