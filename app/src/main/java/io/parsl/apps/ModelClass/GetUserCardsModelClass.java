package io.parsl.apps.ModelClass;

import java.util.List;

public class GetUserCardsModelClass {
    /**
     * message : Success
     * data : {"cards":[{"name_on_card":"9/XW0142d2DpCzU5Xpzz6g==","enc_card_num":"UYQX2unZDz39kWhXp2UUsZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"beNDZCHnJtzbb0TWxHu7/g==","card_network":"Visa","balance":"WmabG3ddF0tokioNgq/GBA==","issue_date":"Oiud2mANknau4kYPdIS1ww==","redeem_code":"fA0vJ4+ThIL1pMrQWH+FkQ==","currency":"$"},{"name_on_card":"IwfHPPmAzuwkfgIyhw4j7g==","enc_card_num":"cYg13KyUwY+0JWRDVrxr0pPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"beNDZCHnJtzbb0TWxHu7/g==","card_network":"Visa","balance":"WmabG3ddF0tokioNgq/GBA==","issue_date":"Za4ZGO/FmMXPKCDyDuasFw==","redeem_code":"4QsI+6osSxS+Q+uYMjGg4Q==","currency":"$"},{"name_on_card":"IwfHPPmAzuwkfgIyhw4j7g==","enc_card_num":"UhtWEkhqIyulplJvHZHdMZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"beNDZCHnJtzbb0TWxHu7/g==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"kVfzcl6VxbLyGMf/d3ykAw==","redeem_code":"2P9CFSGCImNvmWFeofsDug==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"I3FmOGIDERmMZTj/2/OjpZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"GTuhZDJGOCLxCQTOhvaMOw==","redeem_code":"J0Mb78YfBx5ZZB2UYWwpaA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"SfuLvGYRnDmEDWobqcgSBZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"NnRPyGg6QZAUe4JXUj4PjA==","redeem_code":"I+hnatFVGr7zfGuezDqqAQ==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"NPoc9HMBTeKDyFjFLTplxZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"5Cf08Bk74E8PXGUFlG0ITA==","redeem_code":"Fe17ut4iZX+LOlp7WkoTKg==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"AR+QZdgoW1zBqwQ+4nqtaZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"877FNDqkCsw5hwOHAMKtbw==","redeem_code":"7XvJdTX/B8ucoxZukIt6GA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"p+Q8yJ5gIXNxZzPaPVLpbpPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"V/6ESmGv2CVXSs2PVQreKQ==","redeem_code":"SIy3mQTyT79+fiV3W0uRmA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"avf0SKha33USok5YecW9V5PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"eebD0N9Z+crTocjzT7T4gg==","redeem_code":"zNxrxVSK2Ts0+wPDBxdqLA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"WU44kukOQ9T3AYkg6ziloJPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"Imfh8Yw6AC0Jh81dbH2nyw==","redeem_code":"etUOmMSaNcjF1iCJprlBKw==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"FE3q3whlMNgaC/JcjwkvZpPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"iJqKZo2sro3djjTw4rC73A==","redeem_code":"R6mw989xI/CoMiIUagLUqw==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"Vz3ET//51bL0FWV3Kvwf05PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"56vlNSASM+wIiBztScdZ+Q==","redeem_code":"KlLaqKSnGsoNRnpHvhbbZw==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"WRFp65+N90FvBoX4gNyaRJPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"aJLmzlfg05o7gtmlUdX8CA==","redeem_code":"wrfOax+qodPZYFLbMgKGFQ==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"jFQL5AywKwRPzWB0VdDCU5PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"LtFLAjZYWY2+kvx008suag==","redeem_code":"FdHoreLjLT1glUL288oXbA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"wJSEwiybRrOmLdehZwEMHZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"bZ5f1uJ8ROpT/E2D8mjS2w==","redeem_code":"kn+jrdpDCn2smFxIddaXjg==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"WlIw0Yl0T8Nfmh6nbhO0JpPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"KmXTTBprirdw6Wf1N+8wcw==","redeem_code":"9T8HWh54vJwe9vPi5k7jIg==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"IIqDLE2grWefaAMQcM8wk5PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"gj2YB9DurMdNAEIz7CZlPA==","redeem_code":"SgcydBSZLztWWEVgNguLtA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"9q9w6HZxYwXoooJAlh8sLJPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"uyUsXU9adrQG4N2kBrwdlA==","redeem_code":"bGwDPMHug1/vLKJebkmrHw==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"Mxl1z9Lx7HWJ+R3KBcJfbpPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"7VyVahNXqdS4Yu8J+4jj5w==","redeem_code":"sS1rmKBOmQ7HsMu7nEanUQ==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"tBPw5xcScKNXRKqVSzE5HZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"CwXnY24i/fhzBjYJuUa7kA==","redeem_code":"MDOmgyvtqJ6+QcXlXWOMsg==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"375fPYZF4909wbjkMCljq5PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"vO8WrXqePw1qudAazvfWWg==","redeem_code":"gDRfkB/dhsX2rnUnTDEQIA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"TQfSYe4nuUVd4qu5wt9Kw5PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"Ie8HzwjcGq8VgfLpF7PsAQ==","redeem_code":"Z6EPJrxvjeoBOsrsKji+CQ==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"laYAVYmsGQybTxowLIe9I5PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"/UzaJCbzqRiY3l37hgaHxQ==","redeem_code":"mdHihddsDi6HUAVBTnpPNQ==","currency":"$"}],"timestamp":1647345354708,"check_sum":"7ef5d67ccfee761a95b1ad3c2c8f046b867abe244124efe525b02fd192be9b4b89ed6aa0477d6c02623c3f0f33a697204c26d86c87c5b19fd47246d99910553c"}
     * status : true
     */

    private String message;
    /**
     * cards : [{"name_on_card":"9/XW0142d2DpCzU5Xpzz6g==","enc_card_num":"UYQX2unZDz39kWhXp2UUsZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"beNDZCHnJtzbb0TWxHu7/g==","card_network":"Visa","balance":"WmabG3ddF0tokioNgq/GBA==","issue_date":"Oiud2mANknau4kYPdIS1ww==","redeem_code":"fA0vJ4+ThIL1pMrQWH+FkQ==","currency":"$"},{"name_on_card":"IwfHPPmAzuwkfgIyhw4j7g==","enc_card_num":"cYg13KyUwY+0JWRDVrxr0pPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"beNDZCHnJtzbb0TWxHu7/g==","card_network":"Visa","balance":"WmabG3ddF0tokioNgq/GBA==","issue_date":"Za4ZGO/FmMXPKCDyDuasFw==","redeem_code":"4QsI+6osSxS+Q+uYMjGg4Q==","currency":"$"},{"name_on_card":"IwfHPPmAzuwkfgIyhw4j7g==","enc_card_num":"UhtWEkhqIyulplJvHZHdMZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"beNDZCHnJtzbb0TWxHu7/g==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"kVfzcl6VxbLyGMf/d3ykAw==","redeem_code":"2P9CFSGCImNvmWFeofsDug==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"I3FmOGIDERmMZTj/2/OjpZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"GTuhZDJGOCLxCQTOhvaMOw==","redeem_code":"J0Mb78YfBx5ZZB2UYWwpaA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"SfuLvGYRnDmEDWobqcgSBZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"NnRPyGg6QZAUe4JXUj4PjA==","redeem_code":"I+hnatFVGr7zfGuezDqqAQ==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"NPoc9HMBTeKDyFjFLTplxZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"5Cf08Bk74E8PXGUFlG0ITA==","redeem_code":"Fe17ut4iZX+LOlp7WkoTKg==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"AR+QZdgoW1zBqwQ+4nqtaZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"877FNDqkCsw5hwOHAMKtbw==","redeem_code":"7XvJdTX/B8ucoxZukIt6GA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"p+Q8yJ5gIXNxZzPaPVLpbpPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"V/6ESmGv2CVXSs2PVQreKQ==","redeem_code":"SIy3mQTyT79+fiV3W0uRmA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"avf0SKha33USok5YecW9V5PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"eebD0N9Z+crTocjzT7T4gg==","redeem_code":"zNxrxVSK2Ts0+wPDBxdqLA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"WU44kukOQ9T3AYkg6ziloJPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"Imfh8Yw6AC0Jh81dbH2nyw==","redeem_code":"etUOmMSaNcjF1iCJprlBKw==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"FE3q3whlMNgaC/JcjwkvZpPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"iJqKZo2sro3djjTw4rC73A==","redeem_code":"R6mw989xI/CoMiIUagLUqw==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"Vz3ET//51bL0FWV3Kvwf05PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"56vlNSASM+wIiBztScdZ+Q==","redeem_code":"KlLaqKSnGsoNRnpHvhbbZw==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"WRFp65+N90FvBoX4gNyaRJPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"aJLmzlfg05o7gtmlUdX8CA==","redeem_code":"wrfOax+qodPZYFLbMgKGFQ==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"jFQL5AywKwRPzWB0VdDCU5PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"LtFLAjZYWY2+kvx008suag==","redeem_code":"FdHoreLjLT1glUL288oXbA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"wJSEwiybRrOmLdehZwEMHZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"bZ5f1uJ8ROpT/E2D8mjS2w==","redeem_code":"kn+jrdpDCn2smFxIddaXjg==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"WlIw0Yl0T8Nfmh6nbhO0JpPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"KmXTTBprirdw6Wf1N+8wcw==","redeem_code":"9T8HWh54vJwe9vPi5k7jIg==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"IIqDLE2grWefaAMQcM8wk5PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"gj2YB9DurMdNAEIz7CZlPA==","redeem_code":"SgcydBSZLztWWEVgNguLtA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"9q9w6HZxYwXoooJAlh8sLJPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"uyUsXU9adrQG4N2kBrwdlA==","redeem_code":"bGwDPMHug1/vLKJebkmrHw==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"Mxl1z9Lx7HWJ+R3KBcJfbpPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"7VyVahNXqdS4Yu8J+4jj5w==","redeem_code":"sS1rmKBOmQ7HsMu7nEanUQ==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"tBPw5xcScKNXRKqVSzE5HZPzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"CwXnY24i/fhzBjYJuUa7kA==","redeem_code":"MDOmgyvtqJ6+QcXlXWOMsg==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"375fPYZF4909wbjkMCljq5PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"vO8WrXqePw1qudAazvfWWg==","redeem_code":"gDRfkB/dhsX2rnUnTDEQIA==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"TQfSYe4nuUVd4qu5wt9Kw5PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"Ie8HzwjcGq8VgfLpF7PsAQ==","redeem_code":"Z6EPJrxvjeoBOsrsKji+CQ==","currency":"$"},{"name_on_card":"dCFfHgiF8ZuNDLuosQPR7Q==","enc_card_num":"laYAVYmsGQybTxowLIe9I5PzQqJelHz/DN6B7ZVpQuc=","enc_cvc":"rYRGvxR9sP+WWxnza9RFnA==","enc_expiry":"v3yUgqdpn8EBttZq88PXBQ==","card_network":"Visa","balance":"B8m/fyvAoODNVXrrNK7A4w==","issue_date":"/UzaJCbzqRiY3l37hgaHxQ==","redeem_code":"mdHihddsDi6HUAVBTnpPNQ==","currency":"$"}]
     * timestamp : 1647345354708
     * check_sum : 7ef5d67ccfee761a95b1ad3c2c8f046b867abe244124efe525b02fd192be9b4b89ed6aa0477d6c02623c3f0f33a697204c26d86c87c5b19fd47246d99910553c
     */

    private Data data;
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        private long timestamp;
        private String check_sum;
        /**
         * name_on_card : 9/XW0142d2DpCzU5Xpzz6g==
         * enc_card_num : UYQX2unZDz39kWhXp2UUsZPzQqJelHz/DN6B7ZVpQuc=
         * enc_cvc : rYRGvxR9sP+WWxnza9RFnA==
         * enc_expiry : beNDZCHnJtzbb0TWxHu7/g==
         * card_network : Visa
         * balance : WmabG3ddF0tokioNgq/GBA==
         * issue_date : Oiud2mANknau4kYPdIS1ww==
         * redeem_code : fA0vJ4+ThIL1pMrQWH+FkQ==
         * currency : $
         */

        private List<Cards> cards;

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        public String getCheck_sum() {
            return check_sum;
        }

        public void setCheck_sum(String check_sum) {
            this.check_sum = check_sum;
        }

        public List<Cards> getCards() {
            return cards;
        }

        public void setCards(List<Cards> cards) {
            this.cards = cards;
        }

        public static class Cards {
            private String name_on_card;
            private String enc_card_num;
            private String enc_cvc;
            private String enc_expiry;
            private String card_network;
            private String balance;
            private String issue_date;
            private String redeem_code;
            private String currency;
            private String sender_name;

            public Cards() {
            }

            public Cards(String name_on_card, String enc_card_num, String enc_cvc, String enc_expiry, String card_network, String balance, String issue_date, String redeem_code, String currency, String sender_name) {
                this.name_on_card = name_on_card;
                this.enc_card_num = enc_card_num;
                this.enc_cvc = enc_cvc;
                this.enc_expiry = enc_expiry;
                this.card_network = card_network;
                this.balance = balance;
                this.issue_date = issue_date;
                this.redeem_code = redeem_code;
                this.currency = currency;
                this.sender_name = sender_name;
            }

            public String getName_on_card() {
                return name_on_card;
            }

            public void setName_on_card(String name_on_card) {
                this.name_on_card = name_on_card;
            }

            public String getEnc_card_num() {
                return enc_card_num;
            }

            public void setEnc_card_num(String enc_card_num) {
                this.enc_card_num = enc_card_num;
            }

            public String getEnc_cvc() {
                return enc_cvc;
            }

            public void setEnc_cvc(String enc_cvc) {
                this.enc_cvc = enc_cvc;
            }

            public String getEnc_expiry() {
                return enc_expiry;
            }

            public void setEnc_expiry(String enc_expiry) {
                this.enc_expiry = enc_expiry;
            }

            public String getCard_network() {
                return card_network;
            }

            public void setCard_network(String card_network) {
                this.card_network = card_network;
            }

            public String getBalance() {
                return balance;
            }

            public void setBalance(String balance) {
                this.balance = balance;
            }

            public String getIssue_date() {
                return issue_date;
            }

            public void setIssue_date(String issue_date) {
                this.issue_date = issue_date;
            }

            public String getRedeem_code() {
                return redeem_code;
            }

            public void setRedeem_code(String redeem_code) {
                this.redeem_code = redeem_code;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getSender_name() {
                return sender_name;
            }

            public void setSender_name(String sender_name) {
                this.sender_name = sender_name;
            }
        }
    }
}
