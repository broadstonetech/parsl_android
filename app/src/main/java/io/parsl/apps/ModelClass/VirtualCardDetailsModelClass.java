package io.parsl.apps.ModelClass;

import java.util.List;

public class VirtualCardDetailsModelClass {

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    /**
     * message : Success
     * data : {"id":"ic_1ITPumIw0tfQNfofp59IGfAp","object":"issuing.card","brand":"Visa","cancellation_reason":null,"cardholder":{"id":"ich_1ITPulIw0tfQNfofxpI8KKJ2","object":"issuing.cardholder","billing":{"address":{"city":"San Francisco","country":"US","line1":"1234 Main Street","line2":null,"postal_code":"94111","state":"CA"}},"company":null,"created":1615374559,"email":"bilawalkhan042@gmail.com","individual":null,"livemode":false,"metadata":{},"name":"bilawal","phone_number":"+923454061043","requirements":{"disabled_reason":null,"past_due":[]},"spending_controls":{"allowed_categories":[],"blocked_categories":[],"spending_limits":[],"spending_limits_currency":null},"status":"active","type":"individual"},"created":1615374560,"currency":"usd","cvc":"123","exp_month":2,"exp_year":2024,"last4":"0120","livemode":false,"metadata":{},"number":"4000009990000120","replaced_by":null,"replacement_for":null,"replacement_reason":null,"shipping":null,"spending_controls":{"allowed_categories":null,"blocked_categories":null,"spending_limits":[{"amount":30,"categories":[],"interval":"all_time"}],"spending_limits_currency":"usd"},"status":"active","type":"virtual"}
     */

    private String message;
    private Data data;

    public static class Data {

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getObject() {
            return object;
        }

        public void setObject(String object) {
            this.object = object;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public Object getCancellation_reason() {
            return cancellation_reason;
        }

        public void setCancellation_reason(Object cancellation_reason) {
            this.cancellation_reason = cancellation_reason;
        }



        public int getCreated() {
            return created;
        }

        public void setCreated(int created) {
            this.created = created;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getCvc() {
            return cvc;
        }

        public void setCvc(String cvc) {
            this.cvc = cvc;
        }

        public int getExp_month() {
            return exp_month;
        }

        public void setExp_month(int exp_month) {
            this.exp_month = exp_month;
        }

        public int getExp_year() {
            return exp_year;
        }

        public void setExp_year(int exp_year) {
            this.exp_year = exp_year;
        }

        public String getLast4() {
            return last4;
        }

        public void setLast4(String last4) {
            this.last4 = last4;
        }

        public boolean isLivemode() {
            return livemode;
        }

        public void setLivemode(boolean livemode) {
            this.livemode = livemode;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public Object getReplaced_by() {
            return replaced_by;
        }

        public void setReplaced_by(Object replaced_by) {
            this.replaced_by = replaced_by;
        }

        public Object getReplacement_for() {
            return replacement_for;
        }

        public void setReplacement_for(Object replacement_for) {
            this.replacement_for = replacement_for;
        }

        public Object getReplacement_reason() {
            return replacement_reason;
        }

        public void setReplacement_reason(Object replacement_reason) {
            this.replacement_reason = replacement_reason;
        }

        public Object getShipping() {
            return shipping;
        }

        public void setShipping(Object shipping) {
            this.shipping = shipping;
        }

        public SpendingControls getSpending_controls() {
            return spending_controls;
        }

        public void setSpending_controls(SpendingControls spending_controls) {
            this.spending_controls = spending_controls;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        /**
         * id : ic_1ITPumIw0tfQNfofp59IGfAp
         * object : issuing.card
         * brand : Visa
         * cancellation_reason : null
         * cardholder : {"id":"ich_1ITPulIw0tfQNfofxpI8KKJ2","object":"issuing.cardholder","billing":{"address":{"city":"San Francisco","country":"US","line1":"1234 Main Street","line2":null,"postal_code":"94111","state":"CA"}},"company":null,"created":1615374559,"email":"bilawalkhan042@gmail.com","individual":null,"livemode":false,"metadata":{},"name":"bilawal","phone_number":"+923454061043","requirements":{"disabled_reason":null,"past_due":[]},"spending_controls":{"allowed_categories":[],"blocked_categories":[],"spending_limits":[],"spending_limits_currency":null},"status":"active","type":"individual"}
         * created : 1615374560
         * currency : usd
         * cvc : 123
         * exp_month : 2
         * exp_year : 2024
         * last4 : 0120
         * livemode : false
         * metadata : {}
         * number : 4000009990000120
         * replaced_by : null
         * replacement_for : null
         * replacement_reason : null
         * shipping : null
         * spending_controls : {"allowed_categories":null,"blocked_categories":null,"spending_limits":[{"amount":30,"categories":[],"interval":"all_time"}],"spending_limits_currency":"usd"}
         * status : active
         * type : virtual
         */

        private String id;
        private String object;
        private String brand;
        private Object cancellation_reason;
        private int created;
        private String currency;
        private String cvc;
        private int exp_month;
        private int exp_year;
        private String last4;
        private boolean livemode;
        //private Metadata metadata;
        private String number;
        private Object replaced_by;
        private Object replacement_for;
        private Object replacement_reason;
        private Object shipping;
        private SpendingControls spending_controls;
        private String status;
        private String type;

        public Cardholder getCardholder() {
            return cardholder;
        }

        public void setCardholder(Cardholder cardholder) {
            this.cardholder = cardholder;
        }

        private Cardholder cardholder;

    }

    public static class Cardholder {

        /**
         * id : ich_1ITPulIw0tfQNfofxpI8KKJ2
         * object : issuing.cardholder
         * billing : {"address":{"city":"San Francisco","country":"US","line1":"1234 Main Street","line2":null,"postal_code":"94111","state":"CA"}}
         * company : null
         * created : 1615374559
         * email : bilawalkhan042@gmail.com
         * individual : null
         * livemode : false
         * metadata : {}
         * name : bilawal
         * phone_number : +923454061043
         * requirements : {"disabled_reason":null,"past_due":[]}
         * spending_controls : {"allowed_categories":[],"blocked_categories":[],"spending_limits":[],"spending_limits_currency":null}
         * status : active
         * type : individual
         */

        private String id;
        private String object;
        private Billing billing;
        private Object company;
        private int created;
        private String email;
        private Object individual;
        private boolean livemode;
        private Metadata metadata;
        private String name;
        private String phone_number;
        private Requirements requirements;
        private SpendingControls spending_controls;
        private String status;
        private String type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getObject() {
            return object;
        }

        public void setObject(String object) {
            this.object = object;
        }

        public Billing getBilling() {
            return billing;
        }

        public void setBilling(Billing billing) {
            this.billing = billing;
        }

        public Object getCompany() {
            return company;
        }

        public void setCompany(Object company) {
            this.company = company;
        }

        public int getCreated() {
            return created;
        }

        public void setCreated(int created) {
            this.created = created;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getIndividual() {
            return individual;
        }

        public void setIndividual(Object individual) {
            this.individual = individual;
        }

        public boolean isLivemode() {
            return livemode;
        }

        public void setLivemode(boolean livemode) {
            this.livemode = livemode;
        }

        public Metadata getMetadata() {
            return metadata;
        }

        public void setMetadata(Metadata metadata) {
            this.metadata = metadata;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public Requirements getRequirements() {
            return requirements;
        }

        public void setRequirements(Requirements requirements) {
            this.requirements = requirements;
        }

        public SpendingControls getSpending_controls() {
            return spending_controls;
        }

        public void setSpending_controls(SpendingControls spending_controls) {
            this.spending_controls = spending_controls;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public static class Billing {
            public static class Address {
            }
        }

        public static class Metadata {
        }

        public static class Requirements {
            /**
             * disabled_reason : null
             * past_due : []
             */

            private Object disabled_reason;
            private List<?> past_due;

            public Object getDisabled_reason() {
                return disabled_reason;
            }

            public void setDisabled_reason(Object disabled_reason) {
                this.disabled_reason = disabled_reason;
            }

            public List<?> getPast_due() {
                return past_due;
            }

            public void setPast_due(List<?> past_due) {
                this.past_due = past_due;
            }
        }

        public static class SpendingControls {
            /**
             * allowed_categories : []
             * blocked_categories : []
             * spending_limits : []
             * spending_limits_currency : null
             */

            private List<?> allowed_categories;
            private List<?> blocked_categories;
            private List<?> spending_limits;
            private Object spending_limits_currency;

            public List<?> getAllowed_categories() {
                return allowed_categories;
            }

            public void setAllowed_categories(List<?> allowed_categories) {
                this.allowed_categories = allowed_categories;
            }

            public List<?> getBlocked_categories() {
                return blocked_categories;
            }

            public void setBlocked_categories(List<?> blocked_categories) {
                this.blocked_categories = blocked_categories;
            }

            public List<?> getSpending_limits() {
                return spending_limits;
            }

            public void setSpending_limits(List<?> spending_limits) {
                this.spending_limits = spending_limits;
            }

            public Object getSpending_limits_currency() {
                return spending_limits_currency;
            }

            public void setSpending_limits_currency(Object spending_limits_currency) {
                this.spending_limits_currency = spending_limits_currency;
            }
        }
    }

    public static class SpendingControls {

        /**
         * allowed_categories : null
         * blocked_categories : null
         * spending_limits : [{"amount":30,"categories":[],"interval":"all_time"}]
         * spending_limits_currency : usd
         */

        private Object allowed_categories;
        private Object blocked_categories;
        private List<SpendingLimits> spending_limits;
        private String spending_limits_currency;

        public Object getAllowed_categories() {
            return allowed_categories;
        }

        public void setAllowed_categories(Object allowed_categories) {
            this.allowed_categories = allowed_categories;
        }

        public Object getBlocked_categories() {
            return blocked_categories;
        }

        public void setBlocked_categories(Object blocked_categories) {
            this.blocked_categories = blocked_categories;
        }

        public List<SpendingLimits> getSpending_limits() {
            return spending_limits;
        }

        public void setSpending_limits(List<SpendingLimits> spending_limits) {
            this.spending_limits = spending_limits;
        }

        public String getSpending_limits_currency() {
            return spending_limits_currency;
        }

        public void setSpending_limits_currency(String spending_limits_currency) {
            this.spending_limits_currency = spending_limits_currency;
        }

        public static class SpendingLimits {
            /**
             * amount : 30
             * categories : []
             * interval : all_time
             */

            private int amount;
            private List<?> categories;
            private String interval;

            public int getAmount() {
                return amount;
            }

            public void setAmount(int amount) {
                this.amount = amount;
            }

            public List<?> getCategories() {
                return categories;
            }

            public void setCategories(List<?> categories) {
                this.categories = categories;
            }

            public String getInterval() {
                return interval;
            }

            public void setInterval(String interval) {
                this.interval = interval;
            }
        }
    }

}
