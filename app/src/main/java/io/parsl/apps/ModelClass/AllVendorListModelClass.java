package io.parsl.apps.ModelClass;

import java.util.List;

public class AllVendorListModelClass {
    /**
     * message : Success
     * data : {"vendors_list":{"food":[{"vendor_id":"ven8150","vendor_name":"Walmart"},{"vendor_id":"ven7150","vendor_name":"McDonald's"},{"vendor_id":"ven0005","vendor_name":"Costa Coffee"},{"vendor_id":"ven0006","vendor_name":"Starbucks"}],"architecture":[],"fashion":[{"vendor_id":"ven8150","vendor_name":"Walmart"}],"equipment":[],"decor":[{"vendor_id":"ven9150","vendor_name":"Luxe Decor"}],"vehicle":[],"plant":[],"electronics":[{"vendor_id":"ven4150","vendor_name":"Samsung"},{"vendor_id":"ven4050","vendor_name":"Apple"},{"vendor_id":"ven0000","vendor_name":"Broadstone Technologies"}],"aircraft":[],"sports":[],"meditation":[],"medicine":[],"mindfulness":[],"kids":[],"special_occasions":[{"vendor_id":"ven0001","vendor_name":"Mother's Day"},{"vendor_id":"ven0002","vendor_name":"Halloween"},{"vendor_id":"ven0003","vendor_name":"Christmas"},{"vendor_id":"ven0004","vendor_name":"Father's Day"}],"animal":[],"knowledgeable":[]},"cat_list":[{"category_title":"Food","category_key":"food"},{"category_title":"Fashion","category_key":"fashion"},{"category_title":"Decor","category_key":"decor"},{"category_title":"Electronics","category_key":"electronics"},{"category_title":"Special Occasions","category_key":"special_occasions"}]}
     */

    private String message;

    private boolean status;

    private Data data;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        /**
         * vendors_list : {"food":[{"vendor_id":"ven8150","vendor_name":"Walmart"},{"vendor_id":"ven7150","vendor_name":"McDonald's"},{"vendor_id":"ven0005","vendor_name":"Costa Coffee"},{"vendor_id":"ven0006","vendor_name":"Starbucks"}],"architecture":[],"fashion":[{"vendor_id":"ven8150","vendor_name":"Walmart"}],"equipment":[],"decor":[{"vendor_id":"ven9150","vendor_name":"Luxe Decor"}],"vehicle":[],"plant":[],"electronics":[{"vendor_id":"ven4150","vendor_name":"Samsung"},{"vendor_id":"ven4050","vendor_name":"Apple"},{"vendor_id":"ven0000","vendor_name":"Broadstone Technologies"}],"aircraft":[],"sports":[],"meditation":[],"medicine":[],"mindfulness":[],"kids":[],"special_occasions":[{"vendor_id":"ven0001","vendor_name":"Mother's Day"},{"vendor_id":"ven0002","vendor_name":"Halloween"},{"vendor_id":"ven0003","vendor_name":"Christmas"},{"vendor_id":"ven0004","vendor_name":"Father's Day"}],"animal":[],"knowledgeable":[]}
         * cat_list : [{"category_title":"Food","category_key":"food"},{"category_title":"Fashion","category_key":"fashion"},{"category_title":"Decor","category_key":"decor"},{"category_title":"Electronics","category_key":"electronics"},{"category_title":"Special Occasions","category_key":"special_occasions"}]
         */

        private VendorsList vendors_list;
        private List<CatList> cat_list;

        public VendorsList getVendors_list() {
            return vendors_list;
        }

        public void setVendors_list(VendorsList vendors_list) {
            this.vendors_list = vendors_list;
        }

        public List<CatList> getCat_list() {
            return cat_list;
        }

        public void setCat_list(List<CatList> cat_list) {
            this.cat_list = cat_list;
        }

        public static class VendorsList {
            private List<DataObj> food;
            private List<?> architecture;
            private List<DataObj> fashion;
            private List<?> equipment;
            private List<DataObj> decor;
            private List<?> vehicle;
            private List<?> plant;
            private List<DataObj> electronics;
            private List<?> aircraft;
            private List<?> sports;
            private List<?> meditation;
            private List<?> medicine;
            private List<?> mindfulness;
            private List<?> kids;
            private List<DataObj> special_occasions;
            private List<?> animal;
            private List<?> knowledgeable;

            public List<DataObj> getFood() {
                return food;
            }

            public void setFood(List<DataObj> food) {
                this.food = food;
            }

            public List<?> getArchitecture() {
                return architecture;
            }

            public void setArchitecture(List<?> architecture) {
                this.architecture = architecture;
            }

            public List<DataObj> getFashion() {
                return fashion;
            }

            public void setFashion(List<DataObj> fashion) {
                this.fashion = fashion;
            }

            public List<?> getEquipment() {
                return equipment;
            }

            public void setEquipment(List<?> equipment) {
                this.equipment = equipment;
            }

            public List<DataObj> getDecor() {
                return decor;
            }

            public void setDecor(List<DataObj> decor) {
                this.decor = decor;
            }

            public List<?> getVehicle() {
                return vehicle;
            }

            public void setVehicle(List<?> vehicle) {
                this.vehicle = vehicle;
            }

            public List<?> getPlant() {
                return plant;
            }

            public void setPlant(List<?> plant) {
                this.plant = plant;
            }

            public List<DataObj> getElectronics() {
                return electronics;
            }

            public void setElectronics(List<DataObj> electronics) {
                this.electronics = electronics;
            }

            public List<?> getAircraft() {
                return aircraft;
            }

            public void setAircraft(List<?> aircraft) {
                this.aircraft = aircraft;
            }

            public List<?> getSports() {
                return sports;
            }

            public void setSports(List<?> sports) {
                this.sports = sports;
            }

            public List<?> getMeditation() {
                return meditation;
            }

            public void setMeditation(List<?> meditation) {
                this.meditation = meditation;
            }

            public List<?> getMedicine() {
                return medicine;
            }

            public void setMedicine(List<?> medicine) {
                this.medicine = medicine;
            }

            public List<?> getMindfulness() {
                return mindfulness;
            }

            public void setMindfulness(List<?> mindfulness) {
                this.mindfulness = mindfulness;
            }

            public List<?> getKids() {
                return kids;
            }

            public void setKids(List<?> kids) {
                this.kids = kids;
            }

            public List<DataObj> getSpecial_occasions() {
                return special_occasions;
            }

            public void setSpecial_occasions(List<DataObj> special_occasions) {
                this.special_occasions = special_occasions;
            }

            public List<?> getAnimal() {
                return animal;
            }

            public void setAnimal(List<?> animal) {
                this.animal = animal;
            }

            public List<?> getKnowledgeable() {
                return knowledgeable;
            }

            public void setKnowledgeable(List<?> knowledgeable) {
                this.knowledgeable = knowledgeable;
            }

        }

        public static class DataObj {
            /**
             * vendor_id : ven6150
             * vendor_name : Pizza Hut
             */

            private String vendor_id;
            private String vendor_name;

            public String getVendor_id() {
                return vendor_id;
            }

            public void setVendor_id(String vendor_id) {
                this.vendor_id = vendor_id;
            }

            public String getVendor_name() {
                return vendor_name;
            }

            public void setVendor_name(String vendor_name) {
                this.vendor_name = vendor_name;
            }
        }

        public static class CatList {
            /**
             * category_title : Food
             * category_key : food
             */

            private String category_title;
            private String category_key;

            public String getCategory_title() {
                return category_title;
            }

            public void setCategory_title(String category_title) {
                this.category_title = category_title;
            }

            public String getCategory_key() {
                return category_key;
            }

            public void setCategory_key(String category_key) {
                this.category_key = category_key;
            }
        }
    }
    /**
     * message : success
     * data : {"food":[{"vendor_id":"ven6150","vendor_name":"Pizza Hut"},{"vendor_id":"ven5150","vendor_name":"La Patisserie"},{"vendor_id":"ven8150","vendor_name":"Walmart"},{"vendor_id":"ven7150","vendor_name":"McDonald's"}],"architecture":[],"fashion":[{"vendor_id":"ven8150","vendor_name":"Walmart"}],"equipment":[],"decor":[{"vendor_id":"ven9150","vendor_name":"Luxe Decor"}],"vehicle":[],"plant":[],"electronics":[{"vendor_id":"ven4150","vendor_name":"Samsung"},{"vendor_id":"ven4050","vendor_name":"Apple"},{"vendor_id":"ven4250","vendor_name":"Dawlance"},{"vendor_id":"ven0000","vendor_name":"Broadstone Technologies"}],"aircraft":[],"sports":[],"meditation":[],"medicine":[],"mindfulness":[],"kids":[],"special_occasions":[],"animal":[],"knowledgeable":[]}
     */

//    private String message;
//    private Data data;
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public Data getData() {
//        return data;
//    }
//
//    public void setData(Data data) {
//        this.data = data;
//    }
//
//    public static class Data {
//        private List<DataObj> food;
//        private List<String> architecture;
//        private List<DataObj> fashion;
//        private List<String> equipment;
//        private List<DataObj> decor;
//        private List<String> vehicle;
//        private List<String> plant;
//        private List<DataObj> electronics;
//        private List<String> aircraft;
//        private List<String> sports;
//        private List<String> meditation;
//        private List<String> medicine;
//        private List<String> mindfulness;
//        private List<String> kids;
//        private List<DataObj> special_occasions;
//        private List<String> animal;
//        private List<String> knowledgeable;
//
//        public List<DataObj> getFood() {
//            return food;
//        }
//
//        public void setFood(List<DataObj> food) {
//            this.food = food;
//        }
//
//        public List<String> getArchitecture() {
//            return architecture;
//        }
//
//        public void setArchitecture(List<String> architecture) {
//            this.architecture = architecture;
//        }
//
//        public List<DataObj> getFashion() {
//            return fashion;
//        }
//
//        public void setFashion(List<DataObj> fashion) {
//            this.fashion = fashion;
//        }
//
//        public List<String> getEquipment() {
//            return equipment;
//        }
//
//        public void setEquipment(List<String> equipment) {
//            this.equipment = equipment;
//        }
//
//        public List<DataObj> getDecor() {
//            return decor;
//        }
//
//        public void setDecor(List<DataObj> decor) {
//            this.decor = decor;
//        }
//
//        public List<String> getVehicle() {
//            return vehicle;
//        }
//
//        public void setVehicle(List<String> vehicle) {
//            this.vehicle = vehicle;
//        }
//
//        public List<String> getPlant() {
//            return plant;
//        }
//
//        public void setPlant(List<String> plant) {
//            this.plant = plant;
//        }
//
//        public List<DataObj> getElectronics() {
//            return electronics;
//        }
//
//        public void setElectronics(List<DataObj> electronics) {
//            this.electronics = electronics;
//        }
//
//        public List<String> getAircraft() {
//            return aircraft;
//        }
//
//        public void setAircraft(List<String> aircraft) {
//            this.aircraft = aircraft;
//        }
//
//        public List<String> getSports() {
//            return sports;
//        }
//
//        public void setSports(List<String> sports) {
//            this.sports = sports;
//        }
//
//        public List<String> getMeditation() {
//            return meditation;
//        }
//
//        public void setMeditation(List<String> meditation) {
//            this.meditation = meditation;
//        }
//
//        public List<String> getMedicine() {
//            return medicine;
//        }
//
//        public void setMedicine(List<String> medicine) {
//            this.medicine = medicine;
//        }
//
//        public List<String> getMindfulness() {
//            return mindfulness;
//        }
//
//        public void setMindfulness(List<String> mindfulness) {
//            this.mindfulness = mindfulness;
//        }
//
//        public List<String> getKids() {
//            return kids;
//        }
//
//        public void setKids(List<String> kids) {
//            this.kids = kids;
//        }
//
//        public List<DataObj> getSpecial_occasions() {
//            return special_occasions;
//        }
//
//        public void setSpecial_occasions(List<DataObj> special_occasions) {
//            this.special_occasions = special_occasions;
//        }
//
//        public List<String> getAnimal() {
//            return animal;
//        }
//
//        public void setAnimal(List<String> animal) {
//            this.animal = animal;
//        }
//
//        public List<String> getKnowledgeable() {
//            return knowledgeable;
//        }
//
//        public void setKnowledgeable(List<String> knowledgeable) {
//            this.knowledgeable = knowledgeable;
//        }
//


   // }


}
