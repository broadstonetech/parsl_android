package io.parsl.apps.ModelClass;

import java.util.List;

public class CategoriesModelClass {
    /**
     * message : success
     * data : [{"category_title":"Food","category_key":"food","list_priority":1,"thumbnails":"https://jediar.s3.amazonaws.com/category_thumbnails/main_category_thumbnails/food_thumbnail.JPG"},{"category_title":"Fashion","category_key":"fashion","list_priority":2,"thumbnails":"https://jediar.s3.amazonaws.com/category_thumbnails/vehicle_subcategory_thumbnails/vehicle_motorbike_thumbnail.jpeg"},{"category_title":"Interior","category_key":"interior","list_priority":3,"thumbnails":"https://jediar.s3.amazonaws.com/category_thumbnails/main_category_thumbnails/interior_thumbnail.jpg"},{"category_title":"Electronics","category_key":"electronics","list_priority":4,"thumbnails":"https://jediar.s3.amazonaws.com/category_thumbnails/main_category_thumbnails/electronics_thumbnail.jpg"},{"category_title":"Special Occasions","category_key":"special_occasions","list_priority":5,"thumbnails":"https://jediar.s3.amazonaws.com/category_thumbnails/main_category_thumbnails/special_occasions_thumnail.jpg"}]
     */

    private String message;
    private List<Data> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        /**
         * category_title : Food
         * category_key : food
         * list_priority : 1
         * thumbnails : https://jediar.s3.amazonaws.com/category_thumbnails/main_category_thumbnails/food_thumbnail.JPG
         */

        private String category_title;
        private String category_key;
        private int list_priority;
        private String thumbnails;

        public String getCategory_title() {
            return category_title;
        }

        public void setCategory_title(String category_title) {
            this.category_title = category_title;
        }

        public String getCategory_key() {
            return category_key;
        }

        public void setCategory_key(String category_key) {
            this.category_key = category_key;
        }

        public int getList_priority() {
            return list_priority;
        }

        public void setList_priority(int list_priority) {
            this.list_priority = list_priority;
        }

        public String getThumbnails() {
            return thumbnails;
        }

        public void setThumbnails(String thumbnails) {
            this.thumbnails = thumbnails;
        }
    }
}
