package io.parsl.apps.ModelClass;

public class UserLogin {

    /**
     * message : Loged in successfuly
     * data : {"namespace":"pr6170"}
     * status : true
     */

    private String message;
    /**
     * namespace : pr6170
     */

    private Data data;
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        private String namespace;

        public String getNamespace() {
            return namespace;
        }

        public void setNamespace(String namespace) {
            this.namespace = namespace;
        }
    }
}
