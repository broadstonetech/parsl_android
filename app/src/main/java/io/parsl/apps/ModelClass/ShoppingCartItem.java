package io.parsl.apps.ModelClass;

public class ShoppingCartItem {

    public ModelsDataClass.Data getObj() {
        return obj;
    }

    public void setObj(ModelsDataClass.Data obj) {
        this.obj = obj;
    }

    @Override
    public boolean equals(Object o) {

        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof ModelsDataClass.Data)) {
            return false;
        }

        // typecast o to Complex so that we can compare data members
        ModelsDataClass.Data c = (ModelsDataClass.Data) o;

        // Compare the data members and return accordingly
        return Double.compare(Double.parseDouble(obj.getProduct_id()), Double.parseDouble(c.getProduct_id())) == 0;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getDisCounts() {
        return disCounts;
    }

    public void setDisCounts(double disCounts) {
        this.disCounts = disCounts;
    }

    private ModelsDataClass.Data obj;
    private int quantity=0;
    private double disCounts=0;

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    String discountType="";

    public String getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(String thumbnails) {
        this.thumbnails = thumbnails;
    }

    private String thumbnails = "";
}
