package io.parsl.apps.ModelClass;

import java.util.List;

public class EphermalKeysModelCLass {

  private String message;
  private Data data;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  public static class Data {
    private String id;
    private String object;
    private List<AssociatedObjects> associated_objects;
    private int created;
    private int expires;
    private boolean livemode;
    private String secret;

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getObject() {
      return object;
    }

    public void setObject(String object) {
      this.object = object;
    }

    public List<AssociatedObjects> getAssociated_objects() {
      return associated_objects;
    }

    public void setAssociated_objects(List<AssociatedObjects> associated_objects) {
      this.associated_objects = associated_objects;
    }

    public int getCreated() {
      return created;
    }

    public void setCreated(int created) {
      this.created = created;
    }

    public int getExpires() {
      return expires;
    }

    public void setExpires(int expires) {
      this.expires = expires;
    }

    public boolean isLivemode() {
      return livemode;
    }

    public void setLivemode(boolean livemode) {
      this.livemode = livemode;
    }

    public String getSecret() {
      return secret;
    }

    public void setSecret(String secret) {
      this.secret = secret;
    }

    public static class AssociatedObjects {
      private String type;
      private String id;

      public String getType() {
        return type;
      }

      public void setType(String type) {
        this.type = type;
      }

      public String getId() {
        return id;
      }

      public void setId(String id) {
        this.id = id;
      }
    }
  }
}
