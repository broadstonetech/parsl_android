package io.parsl.apps.ModelClass;

import java.util.List;

public class CategoryVendorsClass {
    /**
     * message : success
     * data : {"vendors_list":[{"vendor_id":"ven6150","vendor_name":"Pizza Hut"},{"vendor_id":"ven5150","vendor_name":"La Patisserie"},{"vendor_id":"ven8150","vendor_name":"Al Fatah"},{"vendor_id":"ven7150","vendor_name":"Macdonalds"}]}
     */

    private String message;
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        private List<VendorsList> vendors_list;

        public List<VendorsList> getVendors_list() {
            return vendors_list;
        }

        public void setVendors_list(List<VendorsList> vendors_list) {
            this.vendors_list = vendors_list;
        }

        public static class VendorsList {
            /**
             * vendor_id : ven6150
             * vendor_name : Pizza Hut
             */

            private String vendor_id;
            private String vendor_name;

            public boolean isSelected() {
                return selected;
            }

            public void setSelected(boolean selected) {
                this.selected = selected;
            }

            private boolean selected;
            public String getVendor_id() {
                return vendor_id;
            }

            public void setVendor_id(String vendor_id) {
                this.vendor_id = vendor_id;
            }

            public String getVendor_name() {
                return vendor_name;
            }

            public void setVendor_name(String vendor_name) {
                this.vendor_name = vendor_name;
            }
        }
    }
}
