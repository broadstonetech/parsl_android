package io.parsl.apps.ModelClass;

public class SaveParselCartModelClass {
    /**
     * message : Success
     * data : {"parsl_url":"https://parsl.io/6043613c207cf21979e4e21d","parsl_otp":"C3507F8A","parsl_id":"6043613c207cf21979e4e21d","cardholder_id":"","virtual_card_id":""}
     */

    private String message;
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        /**
         * parsl_url : https://parsl.io/6043613c207cf21979e4e21d
         * parsl_otp : C3507F8A
         * parsl_id : 6043613c207cf21979e4e21d
         * cardholder_id :
         * virtual_card_id :
         */

        private String parsl_url;
        private String parsl_otp;
        private String parsl_id;
        private String cardholder_id;
        private String virtual_card_id;

        public String getCard_number() {
            return card_number;
        }

        public void setCard_number(String card_number) {
            this.card_number = card_number;
        }

        public String getTotal_price() {
            return total_price;
        }

        public void setTotal_price(String total_price) {
            this.total_price = total_price;
        }

        private String card_number;
        private String total_price;

        public String getTransection_id() {
            return transection_id;
        }

        public void setTransection_id(String transection_id) {
            this.transection_id = transection_id;
        }

        private String transection_id;

        public String getParsl_url() {
            return parsl_url;
        }

        public void setParsl_url(String parsl_url) {
            this.parsl_url = parsl_url;
        }

        public String getParsl_otp() {
            return parsl_otp;
        }

        public void setParsl_otp(String parsl_otp) {
            this.parsl_otp = parsl_otp;
        }

        public String getParsl_id() {
            return parsl_id;
        }

        public void setParsl_id(String parsl_id) {
            this.parsl_id = parsl_id;
        }

        public String getCardholder_id() {
            return cardholder_id;
        }

        public void setCardholder_id(String cardholder_id) {
            this.cardholder_id = cardholder_id;
        }

        public String getVirtual_card_id() {
            return virtual_card_id;
        }

        public void setVirtual_card_id(String virtual_card_id) {
            this.virtual_card_id = virtual_card_id;
        }
    }
}
