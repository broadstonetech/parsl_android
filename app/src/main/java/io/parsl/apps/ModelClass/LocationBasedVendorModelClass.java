package io.parsl.apps.ModelClass;

import java.util.List;

public class LocationBasedVendorModelClass {
    private String message;
    private CategoryVendorsClass.Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CategoryVendorsClass.Data getData() {
        return data;
    }

    public void setData(CategoryVendorsClass.Data data) {
        this.data = data;
    }

    public static class Data {
        private List<AllVendorListModelClass.Data.DataObj> food;
        private List<String> architecture;
        private List<AllVendorListModelClass.Data.DataObj> fashion;
        private List<String> equipment;
        private List<AllVendorListModelClass.Data.DataObj> decor;
        private List<String> vehicle;
        private List<String> plant;
        private List<AllVendorListModelClass.Data.DataObj> electronics;
        private List<String> aircraft;
        private List<String> sports;
        private List<String> meditation;
        private List<String> medicine;
        private List<String> mindfulness;
        private List<String> kids;
        private List<AllVendorListModelClass.Data.DataObj> special_occasions;
        private List<String> animal;
        private List<String> knowledgeable;

        public List<AllVendorListModelClass.Data.DataObj> getFood() {
            return food;
        }

        public void setFood(List<AllVendorListModelClass.Data.DataObj> food) {
            this.food = food;
        }

        public List<String> getArchitecture() {
            return architecture;
        }

        public void setArchitecture(List<String> architecture) {
            this.architecture = architecture;
        }

        public List<AllVendorListModelClass.Data.DataObj> getFashion() {
            return fashion;
        }

        public void setFashion(List<AllVendorListModelClass.Data.DataObj> fashion) {
            this.fashion = fashion;
        }

        public List<String> getEquipment() {
            return equipment;
        }

        public void setEquipment(List<String> equipment) {
            this.equipment = equipment;
        }

        public List<AllVendorListModelClass.Data.DataObj> getDecor() {
            return decor;
        }

        public void setDecor(List<AllVendorListModelClass.Data.DataObj> decor) {
            this.decor = decor;
        }

        public List<String> getVehicle() {
            return vehicle;
        }

        public void setVehicle(List<String> vehicle) {
            this.vehicle = vehicle;
        }

        public List<String> getPlant() {
            return plant;
        }

        public void setPlant(List<String> plant) {
            this.plant = plant;
        }

        public List<AllVendorListModelClass.Data.DataObj> getElectronics() {
            return electronics;
        }

        public void setElectronics(List<AllVendorListModelClass.Data.DataObj> electronics) {
            this.electronics = electronics;
        }

        public List<String> getAircraft() {
            return aircraft;
        }

        public void setAircraft(List<String> aircraft) {
            this.aircraft = aircraft;
        }

        public List<String> getSports() {
            return sports;
        }

        public void setSports(List<String> sports) {
            this.sports = sports;
        }

        public List<String> getMeditation() {
            return meditation;
        }

        public void setMeditation(List<String> meditation) {
            this.meditation = meditation;
        }

        public List<String> getMedicine() {
            return medicine;
        }

        public void setMedicine(List<String> medicine) {
            this.medicine = medicine;
        }

        public List<String> getMindfulness() {
            return mindfulness;
        }

        public void setMindfulness(List<String> mindfulness) {
            this.mindfulness = mindfulness;
        }

        public List<String> getKids() {
            return kids;
        }

        public void setKids(List<String> kids) {
            this.kids = kids;
        }

        public List<AllVendorListModelClass.Data.DataObj> getSpecial_occasions() {
            return special_occasions;
        }

        public void setSpecial_occasions(List<AllVendorListModelClass.Data.DataObj> special_occasions) {
            this.special_occasions = special_occasions;
        }

        public List<String> getAnimal() {
            return animal;
        }

        public void setAnimal(List<String> animal) {
            this.animal = animal;
        }

        public List<String> getKnowledgeable() {
            return knowledgeable;
        }

        public void setKnowledgeable(List<String> knowledgeable) {
            this.knowledgeable = knowledgeable;
        }
    }
}
