package io.parsl.apps.ModelClass;

public class LocalTestModelClass {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVenderName() {
        return venderName;
    }

    public void setVenderName(String venderName) {
        this.venderName = venderName;
    }

    private String name;
    private String venderName;
}
