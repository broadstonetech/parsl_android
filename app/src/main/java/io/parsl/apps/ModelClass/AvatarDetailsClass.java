package io.parsl.apps.ModelClass;

import java.util.List;

public class AvatarDetailsClass {

    /**
     * message : Success
     * data : [{"title":"live_test_avatar","description":"it is live_test ................","timestamp":"1640625541","app_id":"parsl_ios","namespace":"pr9873","avatar_id":"6f3719a5-84ec-4ed9-b6e7-c7a980c11345","status":"processing"},{"title":"live_test_avatar_postman","description":"it is live_test ................","timestamp":"1640627449","app_id":"parsl_ios","namespace":"pr9873","avatar_id":"7ffff17c-4146-4f37-924b-72b9119d1309","status":"processing"},{"title":"live_test_avatar_postman","description":"it is live_test ................","timestamp":"1640676796","app_id":"parsl_ios","namespace":"pr9873","avatar_id":"63323ce3-d156-412a-a308-4fc24f5d820c","status":"processing"},{"title":"live_test_avatar","description":"it is live_test ................","timestamp":"1640709557","app_id":"parsl_ios","namespace":"pr9873","avatar_id":"1640709557_997433424c66596","status":"processing"},{"title":"live_test_avatar","description":"it is live_test ................","timestamp":"1640710351","app_id":"parsl_ios","namespace":"pr9873","avatar_id":"1640710351_7513113ea9da682","url":"https://jediar.nyc3.digitaloceanspaces.com/AssetBundles/Ios_live_test_avatarapi","status":"live"},{"title":"live_test_avatar","description":"it is live_test ................","timestamp":"1640765089","app_id":"parsl_ios","namespace":"pr9873","avatar_id":"1640765089_84218836f3cf5f","status":"processing"},{"title":"titleField","description":"descriptionField","timestamp":"1640765984","app_id":"parsl_ios","namespace":"pr9873","avatar_id":"1640765984_086511406edf4f","status":"processing"},{"title":"live_test_avatar","description":"it is live_test ................","timestamp":"1640765991","app_id":"parsl_ios","namespace":"pr9873","avatar_id":"1640765991_0497613c20b1f98","status":"processing"},{"title":"titleField","description":"descriptionField","timestamp":"1640766199","app_id":"parsl_ios","namespace":"pr9873","avatar_id":"1640766199_4103467201a7305","status":"processing"},{"title":"titleField","description":"descriptionField","timestamp":"1640766250","app_id":"parsl_ios","namespace":"pr9873","avatar_id":"1640766250_1751096a4e5ea63","status":"processing"},{"title":"zaryab","description":"descriptionField","timestamp":"1640766464","app_id":"parsl_ios","namespace":"pr9873","avatar_id":"1640766464_6839814e9829510","status":"processing"},{"title":"zaryab","description":"descriptionField","timestamp":"1640766483","app_id":"parsl_ios","namespace":"pr9873","avatar_id":"1640766483_62663563fdf0121","status":"processing"}]
     */

    private String message;
    private boolean status;
    /**
     * title : live_test_avatar
     * description : it is live_test ................
     * timestamp : 1640625541
     * app_id : parsl_ios
     * namespace : pr9873
     * avatar_id : 6f3719a5-84ec-4ed9-b6e7-c7a980c11345
     * status : processing
     */

    private List<Data> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        private String title;
        private String description;
        private long timestamp;
        private String app_id;
        private String namespace;
        private String avatar_id;
        private String status;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        public String getApp_id() {
            return app_id;
        }

        public void setApp_id(String app_id) {
            this.app_id = app_id;
        }

        public String getNamespace() {
            return namespace;
        }

        public void setNamespace(String namespace) {
            this.namespace = namespace;
        }

        public String getAvatar_id() {
            return avatar_id;
        }

        public void setAvatar_id(String avatar_id) {
            this.avatar_id = avatar_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
