package io.parsl.apps.ModelClass;

import java.util.List;

public class NotificationsClass {

    /**
     * message : Success
     * data : [{"notification_reciever_name":"Bilawal","diff_token":"+923454061043","device_id":"546660e03ecc4b8c","device_token":"c3FB--r7TOyKaYYjlCE3qv:APA91bEU79X5_8SuJ1N5ZQ4ru3sH-8kdM7BblmfvIn2Tv_sCVBdfYW9RRDLSsJKs0dXx7c_EL2Va0iVjj7Z6Vl3baLVElbb3nbgjJkWGz_046Gf7dl1S-5_uGm3ba3dginPvyzzxlJKS","not_type":"redeemed","not_message":"You have redeemed your parsl successfully.","not_title":"Congratulations!","sent_time":"1634198273807","parsl_otp":"D17802"},{"notification_reciever_name":"Bilawal","diff_token":"bilawalkhan042@gmail.com","device_id":"546660e03ecc4b8c","device_token":"c3FB--r7TOyKaYYjlCE3qv:APA91bEU79X5_8SuJ1N5ZQ4ru3sH-8kdM7BblmfvIn2Tv_sCVBdfYW9RRDLSsJKs0dXx7c_EL2Va0iVjj7Z6Vl3baLVElbb3nbgjJkWGz_046Gf7dl1S-5_uGm3ba3dginPvyzzxlJKS","not_type":"redeemed_success","not_message":"Your sent pasrsl is redeemed successfully.","not_title":"Congratulations!","sent_time":"1634198273807","parsl_otp":"D17802"}]
     * status : true
     */

    private String message;
    private boolean status;
    /**
     * notification_reciever_name : Bilawal
     * diff_token : +923454061043
     * device_id : 546660e03ecc4b8c
     * device_token : c3FB--r7TOyKaYYjlCE3qv:APA91bEU79X5_8SuJ1N5ZQ4ru3sH-8kdM7BblmfvIn2Tv_sCVBdfYW9RRDLSsJKs0dXx7c_EL2Va0iVjj7Z6Vl3baLVElbb3nbgjJkWGz_046Gf7dl1S-5_uGm3ba3dginPvyzzxlJKS
     * not_type : redeemed
     * not_message : You have redeemed your parsl successfully.
     * not_title : Congratulations!
     * sent_time : 1634198273807
     * parsl_otp : D17802
     */

    private List<Data> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        private String notification_reciever_name="";
        private String diff_token;
        private String device_id;
        private String device_token;
        private String not_type;
        private String not_message;
        private String not_title;
        private long sent_time;
        private String parsl_otp;

        public String getDate_today() {
            return date_today;
        }

        public void setDate_today(String date_today) {
            this.date_today = date_today;
        }

        private String date_today;

        public String getNotification_reciever_name() {
            return notification_reciever_name;
        }

        public void setNotification_reciever_name(String notification_reciever_name) {
            this.notification_reciever_name = notification_reciever_name;
        }

        public String getDiff_token() {
            return diff_token;
        }

        public void setDiff_token(String diff_token) {
            this.diff_token = diff_token;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public String getNot_type() {
            return not_type;
        }

        public void setNot_type(String not_type) {
            this.not_type = not_type;
        }

        public String getNot_message() {
            return not_message;
        }

        public void setNot_message(String not_message) {
            this.not_message = not_message;
        }

        public String getNot_title() {
            return not_title;
        }

        public void setNot_title(String not_title) {
            this.not_title = not_title;
        }

        public long getSent_time() {
            return sent_time;
        }

        public void setSent_time(long sent_time) {
            this.sent_time = sent_time;
        }

        public String getParsl_otp() {
            return parsl_otp;
        }

        public void setParsl_otp(String parsl_otp) {
            this.parsl_otp = parsl_otp;
        }
    }
}
