package io.parsl.apps.ModelClass;

public class CreateAvatar {

    /**
     * message : Success
     */

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
