package io.parsl.apps.ModelClass;

import java.util.List;

public class CoupanCodeModelClass {
    /**
     * message : Details checked successfuly.
     * data : {"approved_products":["5ea42142cdb56b4df464d700:cat_3","5eabf5863c957283b5d063ae:cat_3","5eabf58e3c957283b5d063af:cat_3","5e821acafebaf793c893b93f:cat_5","5e84a7b57d2fc473ae4b7c65:cat_12","5ee39dd1733a65ffbdb70d60:cat_1"],"denied_products":[],"copoun_status":true,"copoun_relief":6,"copoun_relief_type":"price_relief"}
     */

    private String message;
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        /**
         * approved_products : ["5ea42142cdb56b4df464d700:cat_3","5eabf5863c957283b5d063ae:cat_3","5eabf58e3c957283b5d063af:cat_3","5e821acafebaf793c893b93f:cat_5","5e84a7b57d2fc473ae4b7c65:cat_12","5ee39dd1733a65ffbdb70d60:cat_1"]
         * denied_products : []
         * copoun_status : true
         * copoun_relief : 6
         * copoun_relief_type : price_relief
         */

        private List<String> approved_products;
        private List<String> denied_products;
        private boolean coupon_status;
        private double coupon_relief;
        private String coupon_relief_type;


        public boolean isCoupon_status() {
            return coupon_status;
        }

        public void setCoupon_status(boolean coupon_status) {
            this.coupon_status = coupon_status;
        }

        public double getCoupon_relief() {
            return coupon_relief;
        }

        public void setCoupon_relief(double coupon_relief) {
            this.coupon_relief = coupon_relief;
        }

        public String getCoupon_relief_type() {
            return coupon_relief_type;
        }

        public void setCoupon_relief_type(String coupon_relief_type) {
            this.coupon_relief_type = coupon_relief_type;
        }


        public List<String> getApproved_products() {
            return approved_products;
        }

        public void setApproved_products(List<String> approved_products) {
            this.approved_products = approved_products;
        }

        public List<String> getDenied_products() {
            return denied_products;
        }

        public void setDenied_products(List<String> denied_products) {
            this.denied_products = denied_products;
        }




    }
}
