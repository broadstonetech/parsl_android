package io.parsl.apps.ModelClass;

public class WalletPublicKeyModelClass {

    /**
     * message : Data found
     * data : {"public_key":"1281da77-f221-478c-bfc2-4044d2e39363"}
     * status : true
     */

    private String message;
    /**
     * public_key : 1281da77-f221-478c-bfc2-4044d2e39363
     */

    private Data data;
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        private String public_key;

        public String getPublic_key() {
            return public_key;
        }

        public void setPublic_key(String public_key) {
            this.public_key = public_key;
        }
    }
}
