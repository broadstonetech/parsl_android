package io.parsl.apps.Utilities.Constants;

public class GeneralConstants {

    //Constant tags for activities
    public static final String APP_ID = "parsl_android";
    public static final String FCM_USER_TOKEN_KEY = "FCM_USER_TOKEN_KEY";
    public static final String FCM_USER_TOKEN_DEFAULT_VALUE = "";

    public static final String USER_EMAIL_KEY = "USER_EMAIL_KEY";
    public static final String USER_EMAIL_DEFAULT_VALUE = "";

    public static final String USER_OLD_EMAIL_KEY = "USER_OLD_EMAIL_KEY";
    public static final String USER_OLD_EMAIL_DEFAULT_VALUE = "";

    public static final String USER_PHONE_KEY = "USER_PHONE_KEY";
    public static final String USER_PHONE_DEFAULT_VALUE = "";

    public static final String USER_NAME_SPACE_KEY = "USER_NAME_SPACE_KEY";
    public static final String USER_NAME_SPACE_DEFAULT_VALUE = "";

    public static  boolean IsFromWebView = false;

    public static  String SUBMIT_AVATAR = "SUBMIT_AVATAR";

    public static  String WALLET_CARD_CONSTANT = "553A67059CDBF40B45A";

}
