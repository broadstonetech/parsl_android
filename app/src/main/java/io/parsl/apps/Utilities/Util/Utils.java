package io.parsl.apps.Utilities.Util;

import static android.os.Build.VERSION.SDK_INT;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.rlChangePassword;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.rlMyParsls;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.rlUploadPhotos;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.viewChangePass;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.viewMyparsls;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.viewUploadPhotos;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.core.app.ActivityCompat;

import io.parsl.apps.Adaptors.AdapterAnimationsData;
import io.parsl.apps.Adaptors.AdapterAvatarsData;
import io.parsl.apps.Adaptors.AdapterCategoriesData;
import io.parsl.apps.Adaptors.AdapterExtrasModelsData;
import io.parsl.apps.Adaptors.AdapterRedeemData;
import io.parsl.apps.Adaptors.ShowSentParselDetailsDataAdapter;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static void showView() {
        rlChangePassword.setVisibility(View.VISIBLE);
        rlMyParsls.setVisibility(View.VISIBLE);
        viewChangePass.setVisibility(View.VISIBLE);
        viewMyparsls.setVisibility(View.VISIBLE);
        viewUploadPhotos.setVisibility(View.VISIBLE);
    }

    public static void openUrlInChrome(String urlString, Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setPackage("com.android.chrome");
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            // Chrome browser presumably not installed so allow user to choose instead
            intent.setPackage(null);
            context.startActivity(intent);
        }
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public static JSONObject getJSonParams(Context context) {
        JSONObject js = new JSONObject();

        try {
            js.put("app_id", GeneralConstants.APP_ID);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    public static JSONObject getNameSpaceParams(Context context) {
        JSONObject js = new JSONObject();

        try {
            js.put("namespace", UserPrefs.getNameSpace(context));
            //  js.put("namespace", "pr5025");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }


    public static String convertDate(long milliSeconds) {

        long time = milliSeconds * (long) 1000;
        Date date = new Date(time);
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        format.setTimeZone(TimeZone.getTimeZone("GMT"));

        return format.format(date);
    }


    public static JSONObject getParams(String category) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            js.put("app_id", "parsl_android");
            js.put("namespace", "");
            data.put("category", category);
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    public static String getDate(long milliSeconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy", Locale.US);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
    //convert file to hex
    public static String fileSHA1(final File file) throws NoSuchAlgorithmException, IOException {
        final MessageDigest messageDigest = MessageDigest.getInstance("SHA1");

        try (InputStream is = new BufferedInputStream(new FileInputStream(file))) {
            final byte[] buffer = new byte[1024];
            for (int read = 0; (read = is.read(buffer)) != -1; ) {
                messageDigest.update(buffer, 0, read);
            }
        }
        // Convert the byte to hex format
        try (Formatter formatter = new Formatter()) {
            for (final byte b : messageDigest.digest()) {
                formatter.format("%02x", b);
            }
            return formatter.toString();
        }
    }
    //rounded double
    public static String getRoundedDoubleForMoney(double input) {
        String roundedString = new BigDecimal(input).setScale(2, RoundingMode.HALF_UP).toString();
        String decimalChars = roundedString.substring(roundedString.lastIndexOf("."));
        String wholeChars = roundedString.substring(0, roundedString.lastIndexOf("."));
        String commaFormatted = NumberFormat.getNumberInstance(Locale.US).
                format(Integer.parseInt(wholeChars));

        return commaFormatted + decimalChars;
    }

    public static void setProjectionModelPosition(int pos, AdapterCategoriesData adapterData) {
        if (adapterData != null) {
            adapterData.setSelectedModelPosition(pos);
            adapterData.notifyDataSetChanged();
        }
    }

    public static void setProjectionModelRedeemDataPosition(int pos, AdapterRedeemData adapterData) {
        if (adapterData != null) {
            adapterData.setSelectedModelPosition(pos);
            adapterData.notifyDataSetChanged();
        }
    }

    public static void setExtrasProjectionPosition(int pos, AdapterExtrasModelsData adapterData) {
        if (adapterData != null) {
            adapterData.setSelectedModelPosition(pos);
            adapterData.notifyDataSetChanged();
        }
    }

    public static void setExtrasProjectionPosition(int pos, AdapterAnimationsData adapterData) {
        if (adapterData != null) {
            adapterData.setSelectedModelPosition(pos);
            adapterData.notifyDataSetChanged();
        }
    }

    public static void setExtrasProjectionPosition(int pos, AdapterAvatarsData adapterData) {
        if (adapterData != null) {
            adapterData.setSelectedModelPosition(pos);
            adapterData.notifyDataSetChanged();
        }
    }

    public static void setProjectionModelPosition(int pos, ShowSentParselDetailsDataAdapter adapterData) {
        if (adapterData != null) {
            adapterData.setSelectedModelPosition(pos);
            adapterData.notifyDataSetChanged();
        }
    }

    public static String getTimestamp() {
        return String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
    }

    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = true;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{split[1]};
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static void showCenteredToastmessage(Context mContext, String text) {
        Toast toast = Toast.makeText(mContext, text, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void requestPermission(Context cn, ActivityResultLauncher<Intent> mPermissions, String[] PERMISSIONS, int PERMISSION_ALL, Activity activity) {
        if (SDK_INT >= Build.VERSION_CODES.R) {
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setData(Uri.parse(String.format("package:%s", cn.getApplicationContext().getPackageName())));
                mPermissions.launch(intent);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ActivityCompat.requestPermissions(activity, PERMISSIONS, PERMISSION_ALL);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (SDK_INT == Build.VERSION_CODES.R) {
            return Environment.isExternalStorageManager();

        } else if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
