package io.parsl.apps.Utilities.Preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import io.parsl.apps.Utilities.Constants.GeneralConstants;

public class UserPrefs {

    public static void setFCMUserToken(Context context, String token){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(GeneralConstants.FCM_USER_TOKEN_KEY,token).apply();
    }

    public static String getFCMUserToken(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(GeneralConstants.FCM_USER_TOKEN_KEY,GeneralConstants.FCM_USER_TOKEN_DEFAULT_VALUE);
    }

    public static void setEmail(Context context, String email){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(GeneralConstants.USER_EMAIL_KEY,email).apply();
    }

    public static String getEmail(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(GeneralConstants.USER_EMAIL_KEY,GeneralConstants.USER_EMAIL_DEFAULT_VALUE);
    }

    public static void setOldEmail(Context context, String email){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(GeneralConstants.USER_OLD_EMAIL_KEY,email).apply();
    }

    public static String getOldEmail(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(GeneralConstants.USER_OLD_EMAIL_KEY,GeneralConstants.USER_OLD_EMAIL_DEFAULT_VALUE);
    }

    public static void setPhone(Context context, String phone){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(GeneralConstants.USER_PHONE_KEY,phone).apply();
    }

    public static String getPhone(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(GeneralConstants.USER_PHONE_KEY,GeneralConstants.USER_PHONE_DEFAULT_VALUE);
    }

    public static void setNameSpace(Context context, String nameSpace){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(GeneralConstants.USER_NAME_SPACE_KEY,nameSpace).apply();
    }

    public static String getNameSpace(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(GeneralConstants.USER_NAME_SPACE_KEY,GeneralConstants.USER_NAME_SPACE_DEFAULT_VALUE);
    }
}
