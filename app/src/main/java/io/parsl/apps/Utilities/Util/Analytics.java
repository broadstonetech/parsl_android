package io.parsl.apps.Utilities.Util;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

public class Analytics {

    private static FirebaseAnalytics firebaseAnalytics;
    public static final String USER_ID = "user_id";
    public static void init(Context context){
        if (firebaseAnalytics == null){
            firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        }
    }

    public static void sendScreenEvent(Activity activity, String ScreenName,Context cxt,String nameSpace){
        init(activity);
        firebaseAnalytics.setCurrentScreen(activity,"Screen",ScreenName);
        sendEventTrackingData(cxt,nameSpace,"","");
    }

    public static void sendScreenEvent(Activity activity, String ScreenName){
        init(activity);
        firebaseAnalytics.setCurrentScreen(activity,"Screen",ScreenName);
    }

    public static void sendEventTrackingData(Context context, String eventId, String eventName, String eventType){
        init(context);
        if (firebaseAnalytics != null){
            Bundle bundle = new Bundle();
            bundle.putString(USER_ID, eventId);
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, eventName);
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, eventType);
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        }

    }

}

