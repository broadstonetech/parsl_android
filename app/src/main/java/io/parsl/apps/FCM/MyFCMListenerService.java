package io.parsl.apps.FCM;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.parsl.apps.Activities.ARActivities.GltfActivity;
import io.parsl.apps.AgentChatActivity.UsersChatActivity;
import io.parsl.apps.R;
import io.parsl.apps.SplashActivity.SplashActivity;
import io.parsl.apps.Utilities.Preferences.UserPrefs;

import static io.parsl.apps.Activities.ARActivities.GltfActivity.emailLiveSupport;


public class MyFCMListenerService extends FirebaseMessagingService {
    public static List<String> alertsList = new ArrayList<String>();
    private static final String TAG = "MyFCMListenerService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        Map<String, String> data = remoteMessage.getData();
        String type = data.get("notification_type");

        String title = data.get("title");
        String msg = data.get("message");


        if (type != null && type.equalsIgnoreCase("chat_notification")) {
            emailLiveSupport = data.get("email");
            Intent intent = new Intent(this, UsersChatActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
            showNotification(this, title, msg, intent, pendingIntent);
        } else {
            Intent intent = new Intent(this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
            showNotification(this, title, msg, intent, pendingIntent);
        }
    }


    public void showNotification(Context context, String title, String body, Intent intent, PendingIntent pendingIntent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 0;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.app_icon)
                .setContentTitle(title)
                .setContentText(body)
                .setPriority(Notification.PRIORITY_HIGH)
                .setDefaults(Notification.FLAG_AUTO_CANCEL | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setContentIntent(pendingIntent);
        long[] pattern = {500, 500, 500};

        mBuilder.setVibrate(pattern);

        int count = 0;
        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();

        for (int i = 0; i < alertsList.size(); i++) {
            style.addLine(alertsList.get(i));

            count++;

        }


        style.setBigContentTitle("PARSL");
        if (count < 1) {
            mBuilder.setContentText(body);
        } else {
            style.setSummaryText("You have " + count + " unseen alerts.");
        }

        mBuilder.setStyle(style);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify(notificationId, mBuilder.build());
    }


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        UserPrefs.setFCMUserToken(this, s);
        Log.e("NEW_TOKEN", s);
    }
}