 package io.parsl.apps.Payments.Stripe;

import android.content.Context;

import io.parsl.apps.Payments.PaymentUtils;
import com.stripe.android.Stripe;



public class StripeSession {
    private Stripe stripe;


    public void initializeStripeSession(Context context){
        stripe = new Stripe(context, PaymentUtils.STRIPE_PUBLISHABLE_KEY);
    }

    public Stripe getStripeSession(){
        return stripe;
    }

    public void deleteSession() {
        stripe = null;
    }
}
