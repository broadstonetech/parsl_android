 package io.parsl.apps.Payments.GooglePay;

public class GooglePayTokenModelClass {

    /**
     * id : tok_1I1tRbBvUsVxAP8uqM5fEIbF
     * object : token
     * card : {"id":"card_1I1tRbBvUsVxAP8ugj8VJRba","object":"card","address_city":"Mountain View","address_country":"US","address_line1":"1600 Amphitheatre Parkway","address_line1_check":"unchecked","address_line2":null,"address_state":"CA","address_zip":"94043","address_zip_check":"unchecked","brand":"Discover","country":"US","cvc_check":null,"dynamic_last4":"4242","exp_month":12,"exp_year":2025,"funding":"credit","last4":"1117","metadata":{},"name":null,"tokenization_method":"android_pay"}
     * client_ip : 209.85.198.170
     * created : 1608814887
     * livemode : false
     * type : card
     * used : false
     */

    private String id;
    private String object;
    /**
     * id : card_1I1tRbBvUsVxAP8ugj8VJRba
     * object : card
     * address_city : Mountain View
     * address_country : US
     * address_line1 : 1600 Amphitheatre Parkway
     * address_line1_check : unchecked
     * address_line2 : null
     * address_state : CA
     * address_zip : 94043
     * address_zip_check : unchecked
     * brand : Discover
     * country : US
     * cvc_check : null
     * dynamic_last4 : 4242
     * exp_month : 12
     * exp_year : 2025
     * funding : credit
     * last4 : 1117
     * metadata : {}
     * name : null
     * tokenization_method : android_pay
     */

    private Card card;
    private String client_ip;
    private int created;
    private boolean livemode;
    private String type;
    private boolean used;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public String getClient_ip() {
        return client_ip;
    }

    public void setClient_ip(String client_ip) {
        this.client_ip = client_ip;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public boolean isLivemode() {
        return livemode;
    }

    public void setLivemode(boolean livemode) {
        this.livemode = livemode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public static class Card {
        private String id;
        private String object;
        private String address_city;
        private String address_country;
        private String address_line1;
        private String address_line1_check;
        private Object address_line2;
        private String address_state;
        private String address_zip;
        private String address_zip_check;
        private String brand;
        private String country;
        private Object cvc_check;
        private String dynamic_last4;
        private int exp_month;
        private int exp_year;
        private String funding;
        private String last4;
        private Object name;
        private String tokenization_method;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getObject() {
            return object;
        }

        public void setObject(String object) {
            this.object = object;
        }

        public String getAddress_city() {
            return address_city;
        }

        public void setAddress_city(String address_city) {
            this.address_city = address_city;
        }

        public String getAddress_country() {
            return address_country;
        }

        public void setAddress_country(String address_country) {
            this.address_country = address_country;
        }

        public String getAddress_line1() {
            return address_line1;
        }

        public void setAddress_line1(String address_line1) {
            this.address_line1 = address_line1;
        }

        public String getAddress_line1_check() {
            return address_line1_check;
        }

        public void setAddress_line1_check(String address_line1_check) {
            this.address_line1_check = address_line1_check;
        }

        public Object getAddress_line2() {
            return address_line2;
        }

        public void setAddress_line2(Object address_line2) {
            this.address_line2 = address_line2;
        }

        public String getAddress_state() {
            return address_state;
        }

        public void setAddress_state(String address_state) {
            this.address_state = address_state;
        }

        public String getAddress_zip() {
            return address_zip;
        }

        public void setAddress_zip(String address_zip) {
            this.address_zip = address_zip;
        }

        public String getAddress_zip_check() {
            return address_zip_check;
        }

        public void setAddress_zip_check(String address_zip_check) {
            this.address_zip_check = address_zip_check;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public Object getCvc_check() {
            return cvc_check;
        }

        public void setCvc_check(Object cvc_check) {
            this.cvc_check = cvc_check;
        }

        public String getDynamic_last4() {
            return dynamic_last4;
        }

        public void setDynamic_last4(String dynamic_last4) {
            this.dynamic_last4 = dynamic_last4;
        }

        public int getExp_month() {
            return exp_month;
        }

        public void setExp_month(int exp_month) {
            this.exp_month = exp_month;
        }

        public int getExp_year() {
            return exp_year;
        }

        public void setExp_year(int exp_year) {
            this.exp_year = exp_year;
        }

        public String getFunding() {
            return funding;
        }

        public void setFunding(String funding) {
            this.funding = funding;
        }

        public String getLast4() {
            return last4;
        }

        public void setLast4(String last4) {
            this.last4 = last4;
        }

        public Object getName() {
            return name;
        }

        public void setName(Object name) {
            this.name = name;
        }

        public String getTokenization_method() {
            return tokenization_method;
        }

        public void setTokenization_method(String tokenization_method) {
            this.tokenization_method = tokenization_method;
        }
    }
}
