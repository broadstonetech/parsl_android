package io.parsl.apps.Payments.GooglePay;

import android.app.Activity;

import com.google.android.gms.wallet.PaymentDataRequest;
import com.stripe.android.GooglePayConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.parsl.apps.Payments.PaymentUtils;

public class GPayPaymentDataRequest {
    private final Activity activity;

    public GPayPaymentDataRequest(Activity activity) {
        this.activity = activity;
    }

    public PaymentDataRequest getPaymentDataRequest(double totalAmount) {
        final JSONObject tokenizationSpec;
        String paymentDataRequest = null;
        try {
            tokenizationSpec = new GooglePayConfig(activity).getTokenizationSpecification();
            final JSONObject cardPaymentMethod = new JSONObject()
                    .put("type", "CARD")
                    .put(
                            "parameters",
                            new JSONObject()
                                    .put("allowedAuthMethods", PaymentUtils.getSupportedMethods())
                                    .put("allowedCardNetworks", PaymentUtils.getSupportedCards())
                                    .put("billingAddressRequired", true)
                                    .put("billingAddressParameters",
                                            new JSONObject()
                                                    .put("format", "MIN")
                                                    .put("phoneNumberRequired", true)))
                    .put("tokenizationSpecification", tokenizationSpec);

            // create PaymentDataRequest
            paymentDataRequest = new JSONObject()
                    .put("apiVersion", PaymentUtils.GPAY_API_VERSION)
                    .put("apiVersionMinor", PaymentUtils.GPAY_MINOR_API_VERSION)
                    .put("allowedPaymentMethods",
                            new JSONArray().put(cardPaymentMethod))
                    .put("transactionInfo", new JSONObject()
                            .put("totalPrice", String.valueOf(totalAmount))
                            .put("totalPriceStatus", PaymentUtils.TOTAL_PRICE_STATUS)
                            .put("currencyCode", PaymentUtils.CURRENCY_CODE)
                    )
                    .put("merchantInfo", new JSONObject()
                            .put("merchantName", PaymentUtils.MERCHANT_NAME))

                    // require email address
                    .put("emailRequired", true)
                    .toString();
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }

        assert paymentDataRequest != null;
        return PaymentDataRequest.fromJson(paymentDataRequest);
    }
}
