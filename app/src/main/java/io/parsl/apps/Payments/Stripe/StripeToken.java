 package io.parsl.apps.Payments.Stripe;

import android.content.Context;

import com.stripe.android.ApiResultCallback;
import com.stripe.android.Stripe;
import com.stripe.android.model.CardParams;
import com.stripe.android.model.Token;

import java.util.UUID;

import io.parsl.apps.Payments.PaymentSessions;

public class StripeToken {
    private final Stripe stripe;
    private final StripeCards stripeCards;
    public StripeToken(Context context, CardParams cardParams){
        stripe = PaymentSessions.getStripeSession(context);
        stripeCards = new StripeCards(context,cardParams);
    }

    public void generateStripeToken(ApiResultCallback<Token> apiResultCallback){
        if (stripeCards.isValidCard()){
            stripe.createCardToken(stripeCards.getCardParams(), UUID.randomUUID().toString(), apiResultCallback);
        }
    }

}
