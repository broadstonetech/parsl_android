package io.parsl.apps.Payments;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.wallet.PaymentsClient;
import com.stripe.android.Stripe;

import io.parsl.apps.Payments.GooglePay.GPaySession;
import io.parsl.apps.Payments.Stripe.StripeSession;

public class PaymentSessions {

    private static StripeSession stripeSession;
    private static GPaySession gPaySession;

    private static void initStipe(Context context){
        if (stripeSession == null){
            stripeSession = new StripeSession();
            stripeSession.initializeStripeSession(context);
        }
    }

    private static void initGPay(Activity activity){
        if (gPaySession == null){
            gPaySession = new GPaySession();
            gPaySession.initializedGPaySession(activity);
        }
    }

    public static Stripe getStripeSession(Context context){
        initStipe(context);
        return stripeSession.getStripeSession();
    }

    public static PaymentsClient getGPaySession(Activity activity){
        initGPay(activity);
        return gPaySession.getGPaySession();
     }

    public static void deleteStripeSession(Context context){
        initStipe(context);
        stripeSession.deleteSession();
    }
}
