 package io.parsl.apps.Payments.GooglePay;

import android.app.Activity;

import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentsClient;

import io.parsl.apps.Payments.PaymentSessions;
import io.parsl.apps.Payments.PaymentUtils;

public class GPayStartFlow {
    PaymentsClient paymentsClient;
    private final Activity activity;
    private final double totalAmount;

    public GPayStartFlow(Activity activity, double totalAmount){
        this.activity = activity;
        this.totalAmount = totalAmount;
        paymentsClient = PaymentSessions.getGPaySession(activity);
    }

    public void startAcceptingGPay(){
        GPayAvailability.getInstance().checkIfGPayAvailable(paymentsClient, task -> {
            if (task.isSuccessful()){

                GPayPaymentDataRequest dataRequest = new GPayPaymentDataRequest(activity);
                AutoResolveHelper.resolveTask(
                        paymentsClient.loadPaymentData(dataRequest.getPaymentDataRequest(totalAmount)),
                        activity, PaymentUtils.GPAY_REQUEST_CODE);
            }else{

            }
        });
    }

}
