 package io.parsl.apps.Payments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;


import androidx.annotation.Nullable;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentData;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import io.parsl.apps.BaseActivity;
import io.parsl.apps.Payments.GooglePay.GooglePayTokenModelClass;

public abstract class PaymentBaseActivity extends BaseActivity {

    private PaymentDataCallbacks paymentDataCallbacks;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PaymentUtils.GPAY_REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK: {
                    assert data != null;
                    PaymentData paymentData = PaymentData.getFromIntent(data);
                    assert paymentData != null;
                    handlePaymentSuccess(paymentData);

                    break;
                }
                case Activity.RESULT_CANCELED: {
                    break;
                }
                case AutoResolveHelper.RESULT_ERROR: {
                    final Status status = AutoResolveHelper.getStatusFromIntent(data);
                    assert status != null;
                    handleError(status.getStatusCode());
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }


    private void handlePaymentSuccess(PaymentData paymentData) {
        final String paymentInfo = paymentData.toJson();
        if (paymentInfo == null) {
            return;
        }

        try {
            JSONObject paymentMethodData = new JSONObject(paymentInfo).getJSONObject("paymentMethodData");

            final JSONObject tokenizationData = paymentMethodData.getJSONObject("tokenizationData");
            final String token = tokenizationData.getString("token");
            Gson gson = new Gson();
            GooglePayTokenModelClass googlePayTokenModelClass = new GooglePayTokenModelClass();
            googlePayTokenModelClass = gson.fromJson(token, GooglePayTokenModelClass.class);
            if (paymentDataCallbacks != null){
                paymentDataCallbacks.onDataReceive(googlePayTokenModelClass.getId(),googlePayTokenModelClass.getCard().getDynamic_last4());
            }

        } catch (JSONException e) {
            if (paymentDataCallbacks != null){
                paymentDataCallbacks.onError(Arrays.toString(e.getStackTrace()));
            }
        }
    }

    private void handleError(int statusCode) {
        if (paymentDataCallbacks != null){
            paymentDataCallbacks.onError(String.format("Error code: %d", statusCode));
        }
    }

    public PaymentDataCallbacks getPaymentDataCallbacks() {
        return paymentDataCallbacks;
    }

    public void setPaymentDataCallbacks(PaymentDataCallbacks paymentDataCallbacks) {
        this.paymentDataCallbacks = paymentDataCallbacks;
    }
}
