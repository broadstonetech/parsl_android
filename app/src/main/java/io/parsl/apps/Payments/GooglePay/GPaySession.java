 package io.parsl.apps.Payments.GooglePay;

import android.app.Activity;

import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.Wallet;
import com.stripe.android.PaymentConfiguration;

import io.parsl.apps.Payments.PaymentUtils;


public class GPaySession {
    private PaymentsClient paymentsClient;

    public void initializedGPaySession(Activity activity){
        PaymentConfiguration.init(activity, PaymentUtils.STRIPE_PUBLISHABLE_KEY);
        Wallet.WalletOptions walletOptions =
                new Wallet.WalletOptions.Builder().setEnvironment(PaymentUtils.PAYMENTS_ENVIRONMENT).build();
        paymentsClient =  Wallet.getPaymentsClient(activity, walletOptions);
    }

    public PaymentsClient getGPaySession(){
        return paymentsClient;
    }
}
