 package io.parsl.apps.Payments.GooglePay;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentsClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GPayAvailability {

    private static GPayAvailability sharedInstance;

    public static GPayAvailability getInstance(){
        if (sharedInstance == null){
            sharedInstance = new GPayAvailability();
        }

        return sharedInstance;
    }

    @NonNull
    private IsReadyToPayRequest createIsReadyToPayRequest() {
        final JSONArray allowedAuthMethods;
        JSONObject isReadyToPayRequestJson = null;
        try {
            allowedAuthMethods = new JSONArray();
            allowedAuthMethods.put("PAN_ONLY");
            allowedAuthMethods.put("CRYPTOGRAM_3DS");

            final JSONArray allowedCardNetworks = new JSONArray();
            allowedCardNetworks.put("AMEX");
            allowedCardNetworks.put("DISCOVER");
            allowedCardNetworks.put("MASTERCARD");
            allowedCardNetworks.put("VISA");

            final JSONObject cardParameters = new JSONObject();
            cardParameters.put("allowedAuthMethods", allowedAuthMethods);
            cardParameters.put("allowedCardNetworks", allowedCardNetworks);

            final JSONObject cardPaymentMethod = new JSONObject();
            cardPaymentMethod.put("type", "CARD");
            cardPaymentMethod.put("parameters", cardParameters);

            final JSONArray allowedPaymentMethods = new JSONArray();
            allowedPaymentMethods.put(cardPaymentMethod);

            isReadyToPayRequestJson = new JSONObject();
            isReadyToPayRequestJson.put("apiVersion", 2);
            isReadyToPayRequestJson.put("apiVersionMinor", 0);
            isReadyToPayRequestJson.put("allowedPaymentMethods", allowedPaymentMethods);
        }catch (JSONException e){
            e.printStackTrace();
        }
        assert isReadyToPayRequestJson != null;
        return IsReadyToPayRequest.fromJson(isReadyToPayRequestJson.toString());
    }

    public void checkIfGPayAvailable(PaymentsClient paymentsClient, OnCompleteListener<Boolean> listener){
        final IsReadyToPayRequest request = createIsReadyToPayRequest();
        paymentsClient.isReadyToPay(request)
                .addOnCompleteListener(listener);
    }
}
