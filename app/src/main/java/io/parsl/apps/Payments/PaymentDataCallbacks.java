 package io.parsl.apps.Payments;

public interface PaymentDataCallbacks {
    void onDataReceive(String token,String cardNumber);
    void onError(String error);
}
