package io.parsl.apps.Payments;

import static io.parsl.apps.Activities.ARActivities.GltfActivity.shoppingCartProductList;
import static io.parsl.apps.Activities.CartActivity.etEmail;
import static io.parsl.apps.Activities.CartActivity.etName;
import static io.parsl.apps.Activities.CartActivity.idList;
import static io.parsl.apps.Activities.CartActivity.isDefualtVideo;
import static io.parsl.apps.AgentChatActivity.ChatApi.LiveSupport.isTablet;
import static io.parsl.apps.Network.Parser.SaveParselCartParser.saveParselCartObj;
import static pub.devrel.easypermissions.EasyPermissions.hasPermissions;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.CardRequirements;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentMethodTokenizationParameters;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.TransactionInfo;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.model.CardParams;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import io.parsl.apps.Activities.ARActivities.SenderReciverDataFragment;
import io.parsl.apps.Activities.CartActivity;
import io.parsl.apps.Activities.PaymentResponseActivity;
import io.parsl.apps.AgentChatActivity.model.User;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.Payments.GooglePay.GPayStartFlow;
import io.parsl.apps.Payments.Stripe.StripeToken;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;


public class PaymentMethodSelectionFragment extends BottomSheetDialogFragment implements View.OnClickListener, PaymentDataCallbacks {
    private LinearLayout llPayByStripe;
    private LinearLayout llPayByGoogle;
    private LinearLayout llStripePayView;
    private Button btnContinueStripePay;
    private CardInputWidget stripeCard;
    private ImageButton btnGPayInfo, btnCardPayInfo;
    private double gPayAmount, stripeAmount = 0;
    public static double finalAmount;
    private String stripePaymentToken = "";
    String receiverEmail, receiverName, receiverPhone;
    JSONArray productIDList = new JSONArray();
    int isVideo = 0;
    PaymentsClient paymentsClient;
    String stripLastFourDigit;
    double productPrice;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_payment_method_selection, container, false);

        llPayByStripe = view.findViewById(R.id.ll_pay_by_card);
        llPayByGoogle = view.findViewById(R.id.ll_pay_by_google);
        llStripePayView = view.findViewById(R.id.ll_stripe_pay_view);


        btnContinueStripePay = view.findViewById(R.id.btn_continue_stipe_pay);
        btnCardPayInfo = view.findViewById(R.id.btn_info_card_pay);
        btnGPayInfo = view.findViewById(R.id.btn_info_gpay);

        stripeCard = view.findViewById(R.id.stripe_card);

        ((PaymentBaseActivity) requireActivity()).setPaymentDataCallbacks(this);

        llPayByStripe.setOnClickListener(this);
        llPayByGoogle.setOnClickListener(this);
        btnContinueStripePay.setOnClickListener(this);
        btnGPayInfo.setOnClickListener(this);
        btnCardPayInfo.setOnClickListener(this);

        paymentsClient = Wallet.getPaymentsClient(getContext(),
                new Wallet.WalletOptions.Builder()
                        .setEnvironment(WalletConstants.ENVIRONMENT_TEST)
                        .build());
        isReadyToPay();

        return view;
    }

    //region onclick
    @Override
    public void onClick(View v) {
        if (v.getId() == llPayByStripe.getId()) {
            llStripePayView.setVisibility(View.VISIBLE);
        } else if (v.getId() == llPayByGoogle.getId()) {

            payMentDataPayload();
            GPayStartFlow gPayStartFlow = new GPayStartFlow(getActivity(), finalAmount);
            gPayStartFlow.startAcceptingGPay();
        } else if (v.getId() == btnContinueStripePay.getId()) {
            payMentDataPayload();
            stripeTokenCall();
        } else if (v.getId() == btnCardPayInfo.getId() || v.getId() == btnGPayInfo.getId()) {

        }
    }
    //endregion

    private void payMentDataPayload() {
        if (!CartActivity.animatedModel) {
            ((BaseActivity) requireActivity()).showCenteredToastmessage(getActivity(), "Please select animated model");
            return;
        } else {
            finalAmount = Double.parseDouble(Utils.getRoundedDoubleForMoney(Double.parseDouble(CartActivity.tv_total.getText().toString().replace("$", ""))));

            for (int x = 0; x < shoppingCartProductList.size(); x++) {
                JSONObject js = new JSONObject();
                JSONObject discount = new JSONObject();
                double price = 0;
                try {
                    if (shoppingCartProductList.get(x).getDisCounts() > 0) {
                        price = shoppingCartProductList.get(x).getDisCounts() * shoppingCartProductList.get(x).getQuantity();

                    } else {
                        price = shoppingCartProductList.get(x).getObj().getPrice_related().getPrice() * shoppingCartProductList.get(x).getQuantity();
                    }
                    productPrice += price;
                    js.put("product_id", shoppingCartProductList.get(x).getObj().getProduct_id());
                    js.put("product_name", shoppingCartProductList.get(x).getObj().getBasic_info().getName());
                    js.put("product_category", shoppingCartProductList.get(x).getObj().getCategory());
                    js.put("product_price", shoppingCartProductList.get(x).getObj().getPrice_related().getPrice());
                    js.put("product_manufacturer", shoppingCartProductList.get(x).getObj().getBasic_info().getManufacturer());
                    js.put("quantity", shoppingCartProductList.get(x).getQuantity());
                    if (shoppingCartProductList.get(x).getDisCounts() > 0) {
                        discount.put("discounted_amount", shoppingCartProductList.get(x).getDisCounts());
                        discount.put("discounted_type", shoppingCartProductList.get(x).getDiscountType());
                        discount.put("discounted_off", shoppingCartProductList.get(x).getObj().getPrice_related().getPrice() - shoppingCartProductList.get(x).getDisCounts());
                        js.put("discount", discount);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                productIDList.put(js);
            }

           // ((BaseActivity) getActivity()).showProgressDialog(getActivity());
            if (CartActivity.videoFile != null || isDefualtVideo) {
                isVideo = 1;
            }
            receiverEmail = SenderReciverDataFragment.dataObj.getRecieverEmail();
            receiverName = SenderReciverDataFragment.dataObj.getRecieverName();
            receiverPhone = SenderReciverDataFragment.dataObj.getRecieverPhone();
        }
    }

    @Override
    public void onDataReceive(String token, String cardNumber) {
        stripLastFourDigit = cardNumber;
        PostPaymentTokenAPI(token, CartActivity.animation, CartActivity.charcter);
    }

    @Override
    public void onError(String error) {
    }

    private void PostPaymentTokenAPI(String token, String animation, String character) {
        Api.saveParselCart(getActivity(), getSaveParselParams(productIDList, token, CartActivity.msg.getText().toString(), isVideo, stripLastFourDigit, animation, character), saveParselListner);
    }

    //region api call
    AsyncTaskListener saveParselListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            ((BaseActivity) requireActivity()).dismissProgressDialog();
            if (saveParselCartObj.getData().getParsl_id() != null) {
                if (!isDefualtVideo) {
                    ((CartActivity) requireActivity()).uploadVideo();
                }
            }
            CartActivity.animatedModel = false;
            isDefualtVideo = false;
            idList = new JSONArray();
            ((BaseActivity) getActivity()).AddActivityAndRemoveIntentTransition(PaymentResponseActivity.class);

        }
    };

    private JSONObject getSaveParselParams(JSONArray selectedProducts, String token, String msg, int isVideo, String last4digits, String animation, String character) {
        PackageInfo pInfo = null;
        String type;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (isTablet(getActivity())) {
            type = "tablet";
        } else {
            type = "mobile";
        }

        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        JSONObject price = new JSONObject();
        JSONObject sender_data = new JSONObject();
        JSONObject creater_device_info = new JSONObject();
        try {
            js.put("app_id", GeneralConstants.APP_ID);
            js.put("namespace", UserPrefs.getNameSpace(getActivity()));
            js.put("old_mail", UserPrefs.getOldEmail(getActivity()));
            data.put("selected_products", selectedProducts);
            data.put("gift_model", CartActivity.giftModelID);

            sender_data.put("sender_device_token", UserPrefs.getFCMUserToken(getActivity()));
            sender_data.put("sender_device_id", Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
            sender_data.put("card_type", "Visa");
            sender_data.put("card_number", last4digits);
            sender_data.put("email", etEmail.getText().toString().trim());
            sender_data.put("sender_name", etName.getText().toString());

            creater_device_info.put("device_version", pInfo.versionName);
            creater_device_info.put("device_os", "Android");
            creater_device_info.put("device_name", Build.MODEL);
            creater_device_info.put("device_manufacturar", Build.MANUFACTURER);
            creater_device_info.put("device_type", type);

            data.put("sender_data", sender_data);
            data.put("sender_device_info", creater_device_info);
            data.put("text_msg", msg);
            data.put("parsl_timestamp", Utils.getTimestamp());
            data.put("parsl_stripe_token", token);
            data.put("have_video", isVideo);
            data.put("avatar_animation", animation);
            data.put("avatar_chracter", character);
            data.put("default_video", isDefualtVideo);
            price.put("price", productPrice);
            price.put("total_price", finalAmount);
            price.put("currency", "usd");
            data.put("price_related", price);

            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("payment payload", js.toString());
        return js;
    }
    //endregion

    private void stripeTokenCall() {
        final StripeToken[] stripeToken = {new StripeToken(getActivity(), stripeCard.getCardParams())};
        CardParams card = stripeCard.getCardParams();
        stripLastFourDigit = card.getNumber$stripe_release().substring(12, 16);
        stripeToken[0].generateStripeToken(new ApiResultCallback<Token>() {
            @Override
            public void onSuccess(@NotNull Token token) {
                stripePaymentToken = token.getId();

                    Log.e("stripePaymentToken", stripePaymentToken);
                //PostPaymentTokenAPI(stripePaymentToken, CartActivity.animation, CartActivity.charcter);
            }

            @Override
            public void onError(@NotNull Exception e) {
                e.printStackTrace();
                ((BaseActivity) requireActivity()).dismissProgressDialog();
                ((CartActivity) getActivity()).showCenteredToastmessage(getActivity(), "error occurred");
            }
        });
    }

    private PaymentMethodTokenizationParameters createTokenizationParameters() {
        return PaymentMethodTokenizationParameters.newBuilder()
                .setPaymentMethodTokenizationType(
                        WalletConstants.PAYMENT_METHOD_TOKENIZATION_TYPE_PAYMENT_GATEWAY)
                .addParameter("gateway", "stripe")
                .addParameter("stripe:publishableKey",
                        PaymentUtils.STRIPE_PUBLISHABLE_KEY)
                .addParameter("stripe:version", "2018-11-08")
                .build();
    }

    private void isReadyToPay() {
        IsReadyToPayRequest request = IsReadyToPayRequest.newBuilder()
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                .build();
        paymentsClient.isReadyToPay(request).addOnCompleteListener(
                new OnCompleteListener<Boolean>() {
                    public void onComplete(Task<Boolean> task) {
                        try {
                            final boolean result =
                                    task.getResult(ApiException.class);
                            if (result) {
                                // show Google as payment option
                            } else {
                                // hide Google as payment option
                            }
                        } catch (ApiException exception) {
                        }
                    }
                }
        );
    }


}