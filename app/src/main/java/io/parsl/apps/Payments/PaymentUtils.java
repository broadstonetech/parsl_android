
package io.parsl.apps.Payments;

import com.google.android.gms.wallet.WalletConstants;

import org.json.JSONArray;

public class PaymentUtils {
    public static final String STRIPE_PUBLISHABLE_KEY = "pk_test_51IPtixKfB8onMcB4J2OMKggeNB4i5CQqh76tppB3bnanGnteXVE0MozloHVFgUoaxCDdFb74MVsu1GTIAqbczJ1X003NuT3YPJ";
    public static final String STRIPE_LIVE_PUBLISHABLE_KEY = "pk_live_UuXIlFMRRFJ7JQA5gZAdTxhj001jODFndh";
    public static final int GPAY_REQUEST_CODE = 900;

    public static final int PAYMENTS_ENVIRONMENT = WalletConstants.ENVIRONMENT_TEST;

    public static JSONArray getSupportedMethods() {
        JSONArray jsonArray = new JSONArray();
        jsonArray.put("PAN_ONLY");
        jsonArray.put("CRYPTOGRAM_3DS");

        return jsonArray;
    }


    public static JSONArray getSupportedCards() {
        JSONArray jsonArray = new JSONArray();
        jsonArray.put("AMEX");
        jsonArray.put("DISCOVER");
        jsonArray.put("JCB");
        jsonArray.put("MASTERCARD");
        jsonArray.put("VISA");

        return jsonArray;
    }


    public static final String COUNTRY_CODE = "US";
    public static final String CURRENCY_CODE = "USD";

    public static final String TOTAL_PRICE_STATUS = "FINAL";
    public static final String MERCHANT_NAME = "stripe";
    public static final int GPAY_API_VERSION = 2;
    public static final int GPAY_MINOR_API_VERSION = 0;
}
