 package io.parsl.apps.Payments.Stripe;

import android.content.Context;

import com.stripe.android.model.CardParams;

public class StripeCards {
    private final CardParams cardParams;

    public StripeCards(Context context, CardParams cardParams){
        this.cardParams = cardParams;
    }

    public boolean isValidCard(){
        return cardParams != null;
    }

    public CardParams getCardParams(){
        return cardParams;
    }
}
