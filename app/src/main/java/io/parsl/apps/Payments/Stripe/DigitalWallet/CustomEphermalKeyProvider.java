package io.parsl.apps.Payments.Stripe.DigitalWallet;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.stripe.android.pushProvisioning.PushProvisioningEphemeralKeyProvider;

import org.json.JSONException;
import org.json.JSONObject;

import io.parsl.apps.ModelClass.EphermalKeysModelCLass;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;

import static io.parsl.apps.Network.Parser.EphermalKeysModelClassParser.ephermalKeysJsonString;

public class CustomEphermalKeyProvider implements PushProvisioningEphemeralKeyProvider {

    Context mContext;
    String jsonData;
    CustomEphermalKeyProvider objtest;

    public CustomEphermalKeyProvider(Context cxt) {
        this.mContext = cxt;
    }


    protected CustomEphermalKeyProvider(Parcel in) {
    }

    public static final Creator<CustomEphermalKeyProvider> CREATOR = new Creator<CustomEphermalKeyProvider>() {
        @Override
        public CustomEphermalKeyProvider createFromParcel(Parcel in) {
            return new CustomEphermalKeyProvider(in);
        }

        @Override
        public CustomEphermalKeyProvider[] newArray(int size) {
            return new CustomEphermalKeyProvider[size];
        }
    };

    @Override
    public void createEphemeralKey(@NonNull String s, @NonNull com.stripe.android.pushProvisioning.EphemeralKeyUpdateListener ephemeralKeyUpdateListener) {

        Api.getEphermalKeys(mContext, params(s), new AsyncTaskListener() {
            @Override
            public void onComplete(TaskResult result) {
                EphermalKeysModelCLass obj = new EphermalKeysModelCLass();
                Gson gson = new Gson();
                obj = gson.fromJson(String.valueOf(ephermalKeysJsonString), EphermalKeysModelCLass.class);

                Gson gson1 = new Gson();
                jsonData = gson1.toJson(obj.getData());


                ephemeralKeyUpdateListener.onKeyUpdate(jsonData);
            }
        });
    }

    //region api call
    private JSONObject params(String parm) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            data.put("parsl", parm);
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }
    //endregion

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jsonData);
    }
}
