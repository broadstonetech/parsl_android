//package io.parsl.apps.Payments;
//
//import static io.parsl.apps.Activities.ARActivities.GltfActivity.shoppingCartProductList;
//import static io.parsl.apps.Fragments.CartFragment.etEmail;
//import static io.parsl.apps.Fragments.CartFragment.etName;
//import static io.parsl.apps.Fragments.CartFragment.idList;
//import static io.parsl.apps.Network.Parser.SaveParselCartParser.saveParselCartObj;
//
//import android.content.ClipData;
//import android.content.ClipboardManager;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.provider.Settings;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ImageButton;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AlertDialog;
//
//import com.google.android.gms.common.api.ApiException;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.android.gms.wallet.CardRequirements;
//import com.google.android.gms.wallet.IsReadyToPayRequest;
//import com.google.android.gms.wallet.PaymentDataRequest;
//import com.google.android.gms.wallet.PaymentMethodTokenizationParameters;
//import com.google.android.gms.wallet.PaymentsClient;
//import com.google.android.gms.wallet.TransactionInfo;
//import com.google.android.gms.wallet.Wallet;
//import com.google.android.gms.wallet.WalletConstants;
//import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
//import com.google.android.material.textfield.TextInputEditText;
//import com.stripe.android.ApiResultCallback;
//import com.stripe.android.model.CardParams;
//import com.stripe.android.model.Token;
//import com.stripe.android.view.CardInputWidget;
//
//import org.jetbrains.annotations.NotNull;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.Arrays;
//
//import io.parsl.apps.Activities.ARActivities.GltfActivity;
//import io.parsl.apps.Activities.ARActivities.SenderReciverDataFragment;
//import io.parsl.apps.Fragments.CartFragment;
//import io.parsl.apps.Activities.PaymentResponseActivity;
//import io.parsl.apps.BaseActivity;
//import io.parsl.apps.Fragments.CartFragment;
//import io.parsl.apps.Network.API.Api;
//import io.parsl.apps.Network.API.TaskResult;
//import io.parsl.apps.Network.AsyncTaskListener;
//import io.parsl.apps.Payments.GooglePay.GPayStartFlow;
//import io.parsl.apps.Payments.Stripe.StripeToken;
//import io.parsl.apps.R;
//import io.parsl.apps.Utilities.Constants.GeneralConstants;
//import io.parsl.apps.Utilities.Util.Utils;
//
//
//public class PaymentMethodSelectionFragment2 extends  BottomSheetDialogFragment implements View.OnClickListener, PaymentDataCallbacks {
//    private LinearLayout llPayByStripe;
//    private LinearLayout llPayByGoogle;
//    private LinearLayout llStripePayView;
//    private Button btnContinueStripePay;
//    private CardInputWidget stripeCard;
//    private ImageButton btnGPayInfo, btnCardPayInfo;
//
//    private double gPayAmount, stripeAmount = 0;
//    public static double finalAmount;
//
//    private String stripePaymentToken = "";
//
//    String receiverEmail,receiverName,receiverPhone,parselPrice;
//    JSONArray  productIDList = new JSONArray();
//    private TextInputEditText email,phoneNo,name,price;
//    int isVideo = 0;
//    PaymentsClient paymentsClient;
//    private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 200;
//    String stripLastFourDigit,cardHolderName,cardExpiryDate;
//
//    double productPrice;
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.activity_payment_method_selection, container, false);
//
//        TextView tvStripeAmount = view.findViewById(R.id.tv_amount_card);
//        TextView tvGPayAmount = view.findViewById(R.id.tv_amount_gpay);
//        TextView tvStripePayDesc = view.findViewById(R.id.tv_card_pay_desc);
//        TextView tvGPayDesc = view.findViewById(R.id.tv_google_pay_desc);
//
//        llPayByStripe = view.findViewById(R.id.ll_pay_by_card);
//        llPayByGoogle = view.findViewById(R.id.ll_pay_by_google);
//        llStripePayView = view.findViewById(R.id.ll_stripe_pay_view);
//        LinearLayout llGPayDesc = view.findViewById(R.id.ll_gpay_desc);
//        LinearLayout llCardPayDesc = view.findViewById(R.id.ll_card_pay_desc);
//
//
//        btnContinueStripePay = view.findViewById(R.id.btn_continue_stipe_pay);
//        btnCardPayInfo = view.findViewById(R.id.btn_info_card_pay);
//        btnGPayInfo = view.findViewById(R.id.btn_info_gpay);
//
//        stripeCard = view.findViewById(R.id.stripe_card);
//
//    //    calculateFinalAmount();
//
//        ((GltfActivity)getActivity()).setPaymentDataCallbacks(this);
//
//        llPayByStripe.setOnClickListener(this);
//        llPayByGoogle.setOnClickListener(this);
//        btnContinueStripePay.setOnClickListener(this);
//        btnGPayInfo.setOnClickListener(this);
//        btnCardPayInfo.setOnClickListener(this);
//
//        paymentsClient = Wallet.getPaymentsClient(getContext(),
//                new Wallet.WalletOptions.Builder()
//                        .setEnvironment(WalletConstants.ENVIRONMENT_TEST)
//                        .build());
//        isReadyToPay();
//
//        return view;
//    }
//
//    @Override
//    public void onClick(View v) {
//        if (v.getId() == llPayByStripe.getId()) {
//            llStripePayView.setVisibility(View.VISIBLE);
//        } else if (v.getId() == llPayByGoogle.getId()) {
//
//            payMentDataPayload();
//            GPayStartFlow gPayStartFlow = new GPayStartFlow(getActivity(),finalAmount);
//            gPayStartFlow.startAcceptingGPay();
//        }else if (v.getId() == btnContinueStripePay.getId()){
//            payMentDataPayload();
//            stripeTokenCall();
//        }else if (v.getId() == btnCardPayInfo.getId() || v.getId() == btnGPayInfo.getId()){
//
//        }
//    }
//
//    private void payMentDataPayload(){
//        if(!CartFragment.animatedModel){
//            ((GltfActivity)getActivity()).showCenteredToastmessage(getActivity(),"Please select animated model");
//            return;
//        }else {
//            finalAmount = Double.parseDouble(Utils.getRoundedDoubleForMoney(Double.parseDouble(CartFragment.tv_total.getText().toString().replace("$", ""))));
//
//            for (int x = 0; x < shoppingCartProductList.size(); x++) {
//                JSONObject js = new JSONObject();
//                double price = 0;
//                try {
//                    if (shoppingCartProductList.get(x).getDisCounts() > 0) {
//                        price = shoppingCartProductList.get(x).getDisCounts() * shoppingCartProductList.get(x).getQuantity();
//
//                    } else {
//                        price = shoppingCartProductList.get(x).getObj().getPrice_related().getPrice() * shoppingCartProductList.get(x).getQuantity();
//                    }
//                    productPrice += price;
//                    js.put("product_id", shoppingCartProductList.get(x).getObj().getProduct_id());
//                    js.put("product_name", shoppingCartProductList.get(x).getObj().getBasic_info().getName());
//                    js.put("product_manufacturer", shoppingCartProductList.get(x).getObj().getBasic_info().getManufacturer());
//                    js.put("quantity", shoppingCartProductList.get(x).getQuantity());
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                productIDList.put(js);
//            }
//
//            ((GltfActivity) getActivity()).showProgressDialog(getActivity());
//            if (CartFragment.videoFile != null) {
//                isVideo = 1;
//            }
//            receiverEmail = SenderReciverDataFragment.dataObj.getRecieverEmail();
//            receiverName = SenderReciverDataFragment.dataObj.getRecieverName();
//            receiverPhone = SenderReciverDataFragment.dataObj.getRecieverPhone();
//        }
//    }
//
//    public void setAmount(double amount) {
//    }
//
//
//    public void setDistributionType(int distributionType) {
//    }
//
//    @Override
//    public void onDataReceive(String token,String cardNumber) {
//        stripLastFourDigit = cardNumber;
//        PostPaymentTokenAPI(token,CartFragment.animation,CartFragment.charcter);
//    }
//
//    @Override
//    public void onError(String error) {
//
//        //Logger.showToastMessage(getActivity(),error);
//    }
//
//    private void PostPaymentTokenAPI(String token,String animation,String character){
//        Api.saveParselCart(getActivity(), getSaveParselParams(productIDList,token ,CartFragment.msg.getText().toString(), isVideo,stripLastFourDigit,animation,character), saveParselListner);
//    }
//
//
//    private void loadResponseDialog() {
//        AlertDialog.Builder builderAddInfo = new AlertDialog.Builder (getActivity());
//        LayoutInflater inflater = getActivity().getLayoutInflater();
//        View dialogViewAddInfo = inflater.inflate(R.layout.reciever_info_layout, null);
//        builderAddInfo.setView(dialogViewAddInfo);
//        AlertDialog dialogAddInfo = builderAddInfo.create();
//        dialogAddInfo.show();
//        dialogAddInfo.setCancelable(false);
//
//        TextView tvResponse = dialogViewAddInfo.findViewById(R.id.tvResponse);
//        TextView tvShare = dialogViewAddInfo.findViewById(R.id.tvShare);
//        TextView tvCopy = dialogViewAddInfo.findViewById(R.id.tvCopy);
//
//        tvResponse.setText(SenderReciverDataFragment.dataObj.getSenderName()+" "+"sent you a parsel gift"+" "+"Visit"+" "+saveParselCartObj.getData().getParsl_url()+" "+ "or use code"+" "+saveParselCartObj.getData().getParsl_otp()+" "+"" +
//                "to unwrap your PARSL");
//
//        tvShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               // shareIntent(SenderReciverDataFragment.dataObj.getSenderName(),saveParselCartObj.getData().getParsl_url(),saveParselCartObj.getData().getParsl_otp());
//                CartFragment.animatedModel = false;
//             //   shoppingCartProductList.clear();
//                idList = new JSONArray();
//                ((BaseActivity)getActivity()).AddActivityAndRemoveIntentTransition(PaymentResponseActivity.class);
//            }
//        });
//
//        tvCopy.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String text = SenderReciverDataFragment.dataObj.getSenderName()+" "+"sent you a parsel gift"+" "+"Visit"+" "+saveParselCartObj.getData().getParsl_url()+" "+ "or use code"+" "+saveParselCartObj.getData().getParsl_otp()+" "+"" +
//                        "to unwrap your PARSL";
//                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
//                ClipData clip = ClipData.newPlainText("Copied Text", text);
//                clipboard.setPrimaryClip(clip);
//                getActivity().finish();
//            }
//        });
//    }
//
//    AsyncTaskListener saveParselListner = new AsyncTaskListener() {
//        @Override
//        public void onComplete(TaskResult result) {
//            ((GltfActivity)getActivity()).dismissProgressDialog();
//           // ((BaseActivity)requireActivity()).showCenteredToastmessage(getActivity(),"Your gift has been sended");
//            if(saveParselCartObj.getData().getParsl_id()!=null){
//                ((CartFragment)getParentFragment()).uploadVideo();
//            }
//           // loadResponseDialog();
//            CartFragment.animatedModel = false;
//        //    shoppingCartProductList.clear();
//            idList = new JSONArray();
//            ((GltfActivity)getActivity()).AddActivityAndRemoveIntentTransition(PaymentResponseActivity.class);
//
//        }
//    };
//
//
//
//    private JSONObject getSaveParselParams(JSONArray selectedProducts,String token,String msg,int isVideo,String last4digits,String animation,String character) {
//        JSONObject js = new JSONObject();
//        JSONObject data = new JSONObject();
//        JSONObject price = new JSONObject();
//        JSONObject sender_data = new JSONObject();
//        try {
//            js.put("app_id", GeneralConstants.APP_ID);
//            js.put("namespace", "");
//            data.put("selected_products",selectedProducts);
//            data.put("gift_model",CartFragment.giftModelID);
//
//            sender_data.put("sender_device_token","token");
//            sender_data.put("sender_device_id", Settings.Secure.getString(getContext().getContentResolver(),Settings.Secure.ANDROID_ID));
//            sender_data.put("card_type", "Visa");
//            sender_data.put("card_number", last4digits);
//            sender_data.put("email",etEmail.getText().toString());
//            sender_data.put("sender_name",etName.getText().toString());
//
//            data.put("sender_data",sender_data);
//            data.put("text_msg",msg);
//            data.put("parsl_timestamp",Utils.getTimestamp());
//            data.put("parsl_stripe_token",token);
//            data.put("have_video",isVideo);
//            data.put("avatar_animation",animation);
//            data.put("avatar_chracter",character);
//            price.put("price", productPrice);
//            price.put("total_price",finalAmount);
//            price.put("currency","usd");
//            data.put("price_related",price);
//
//            js.put("data", data);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//         Log.e("payment payload",js.toString());
//        return js;
//    }
//
//    private void stripeTokenCall(){
//        final StripeToken[] stripeToken = {new StripeToken(getActivity(), stripeCard.getCardParams())};
//        CardParams card = stripeCard.getCardParams();
//        stripLastFourDigit = card.getNumber$stripe_release().substring(12,16);
//        stripeToken[0].generateStripeToken(new ApiResultCallback<Token>() {
//            @Override
//            public void onSuccess(@NotNull Token token) {
//                stripePaymentToken = token.getId();
//
//                Log.e("token",stripePaymentToken);
//                PostPaymentTokenAPI(stripePaymentToken,CartFragment.animation,CartFragment.charcter);
//            }
//
//            @Override
//            public void onError(@NotNull Exception e) {
//                e.printStackTrace();
//                ((GltfActivity)getActivity()).showCenteredToastmessage(getActivity(),"error occurred");
////                    if (UserPrefs.isStripeTokenValid(requireActivity())){
////                        PostPaymentTokenAPI(UserPrefs.getStripeToken(requireActivity()));
////                    }else{
////                        Logger.showToastMessage(requireActivity(), getString(R.string.something_went_wrong));
////                    }
//
//            }
//        });
//    }
//
//    private void shareIntent(String senderName,String url,String promo){
//        Intent sendIntent = new Intent();
//        sendIntent.setAction(Intent.ACTION_SEND);
//        sendIntent.putExtra(Intent.EXTRA_TEXT, senderName+" "+"sent you a parsel gift"+" "+"Visit"+" "+url+" "+ "or use code"+" "+promo+" "+"" +
//                "to unwrap your PARSL");
//        sendIntent.setType("text/plain");
//
//        Intent shareIntent = Intent.createChooser(sendIntent, null);
//        startActivity(shareIntent);
//    }
//
//    public void setTransactionalFeeIncluded(boolean transactionalFeeIncluded) {
//    }
//
//    private PaymentDataRequest createPaymentDataRequest() {
//        return PaymentDataRequest.newBuilder()
//                .setTransactionInfo(
//                        TransactionInfo.newBuilder()
//                                .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
//                                .setTotalPrice("10.00")
//                                .setCurrencyCode("USD")
//                                .build())
//                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
//                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
//                .setCardRequirements(
//                        CardRequirements.newBuilder()
//                                .addAllowedCardNetworks(Arrays.asList(
//                                        WalletConstants.CARD_NETWORK_AMEX,
//                                        WalletConstants.CARD_NETWORK_DISCOVER,
//                                        WalletConstants.CARD_NETWORK_VISA,
//                                        WalletConstants.CARD_NETWORK_MASTERCARD))
//                                .build())
//                .setPaymentMethodTokenizationParameters(createTokenizationParameters())
//                .build();
//    }
//
//    private PaymentMethodTokenizationParameters createTokenizationParameters() {
//        return PaymentMethodTokenizationParameters.newBuilder()
//                .setPaymentMethodTokenizationType(
//                        WalletConstants.PAYMENT_METHOD_TOKENIZATION_TYPE_PAYMENT_GATEWAY)
//                .addParameter("gateway", "stripe")
//                .addParameter("stripe:publishableKey",
//                        PaymentUtils.STRIPE_PUBLISHABLE_KEY)
//                .addParameter("stripe:version", "2018-11-08")
//                .build();
//    }
//
//    private void isReadyToPay() {
//        IsReadyToPayRequest request = IsReadyToPayRequest.newBuilder()
//                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
//                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
//                .build();
//        paymentsClient.isReadyToPay(request).addOnCompleteListener(
//                new OnCompleteListener<Boolean>() {
//                    public void onComplete(Task<Boolean> task) {
//                        try {
//                            final boolean result =
//                                    task.getResult(ApiException.class);
//                            if (result) {
//                                // show Google as payment option
//                            } else {
//                                // hide Google as payment option
//                            }
//                        } catch (ApiException exception) { }
//                    }
//                }
//        );
//    }
//
//
//}