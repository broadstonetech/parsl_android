package io.parsl.apps.SplashActivity;

import static io.parsl.apps.VersionCheckApi.CheckEnvirenmentParser.environemntObj;
import static io.parsl.apps.VersionCheckApi.VersionCheckParser.versionCheckObj;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.widget.VideoView;

import io.parsl.apps.Activities.ARActivities.GltfActivity;
import io.parsl.apps.BaseActivity;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.R;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.VersionCheckApi.VersionCheck;

public class SplashActivity extends BaseActivity {

    VideoView videoView;
    private String url;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        videoView = findViewById(R.id.videoView);
        videoView.setMediaController(null);

        String token = UserPrefs.getFCMUserToken(this);
        String path = "android.resource://" + getPackageName() + "/" + R.raw.splash_video;
        videoView.setVideoURI(Uri.parse(path));
        videoView.setBackgroundColor(Color.WHITE);
        videoView.start();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                videoView.setBackgroundColor(Color.TRANSPARENT);
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                videoView.stopPlayback();
                AddActivityAndRemoveIntentTransition(GltfActivity.class);
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    public static void setNightMode(boolean state) {


        if (state) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


    }

    final AsyncTaskListener environmentCheckListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            VersionCheck.getVersionCheck(SplashActivity.this, versionCheckListner);
        }
    };

    final AsyncTaskListener versionCheckListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if (!versionCheckObj.isStatus()) {
                showVersionCheckAlert();
            } else {
                AddActivityAndRemoveIntentTransition(GltfActivity.class);
            }
        }
    };

    private void showVersionCheckAlert() {
        if (environemntObj.getData().getEnvironment().equalsIgnoreCase("testflight")) {
            url = "https://play.google.com/apps/internaltest/4700074077598993071";
        } else {
            url = "https://play.google.com/apps/internaltest/4700074077598993071";
        }
        new AlertDialog.Builder(this)
                .setTitle("PARSL")
                .setMessage("App update available")// + "(" + playStoreAppVersion + ")" + ".")
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, whichButton) -> {

                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url + appPackageName)));
                    }
                    finish();
                    dialog.dismiss();
                }).show();
    }
}