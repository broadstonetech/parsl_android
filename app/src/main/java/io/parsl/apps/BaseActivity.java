package io.parsl.apps;

import static io.parsl.apps.Activities.ARActivities.ProfileActivity.rlChangePassword;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.rlMyParsls;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.rlUploadPhotos;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.tvEmail;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.tv_signout_fragments_settings;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.viewChangePass;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.viewMyparsls;
import static io.parsl.apps.Activities.ARActivities.ProfileActivity.viewUploadPhotos;
import static io.parsl.apps.Activities.CartActivity.fragmentLogin;
import static io.parsl.apps.Activities.CartActivity.llSignIn;
import static io.parsl.apps.Network.Parser.UserLoginParser.userLoginObj;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import io.parsl.apps.Activities.ARActivities.GltfActivity;
import io.parsl.apps.Activities.ARActivities.ProfileActivity;
import io.parsl.apps.Activities.CartActivity;
import io.parsl.apps.Network.API.Api;
import io.parsl.apps.Network.API.TaskResult;
import io.parsl.apps.Network.AsyncTaskListener;
import io.parsl.apps.Utilities.Constants.GeneralConstants;
import io.parsl.apps.Utilities.Preferences.UserPrefs;
import io.parsl.apps.Utilities.Util.Utils;

public abstract class BaseActivity extends AppCompatActivity {

    private Dialog dialog, toastDialog;
    private GoogleSignInClient mGoogleSignInClient;
    public ProgressDialog progressDialog;
    AlertDialog.Builder builder;
    View dialogView;
    private AlertDialog.Builder builderAddInfo;
    private View dialogViewAddInfo;
    private AlertDialog dialogAddInfo;
    private FirebaseAuth mAuth;
    public static final int REQUEST_CODE_GOOGLE_LOGIN = 200;
    private FirebaseUser user;
    Context loginFragmentContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        initViews(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    //region base functions
    public void googleSignIn(Context con) {
        loginFragmentContext = con;
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, REQUEST_CODE_GOOGLE_LOGIN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == REQUEST_CODE_GOOGLE_LOGIN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                assert account != null;
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                e.printStackTrace();
            }
        }
    }
//firebase google auth
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        user = mAuth.getCurrentUser();
                        showProgressDialog(this);
                        try {
                            Api.userLogin(this, googleLoginParams(user), googleLoginListner);
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }
                    } else {
                        // If sign in fails, display a message to the user.
                    }

                    // ...
                });
    }

    public String getSHA256(String pas) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(pas.getBytes(StandardCharsets.UTF_8));
        byte[] digest = md.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            sb.append(String.format("%02x", digest[i] & 0xFF));
        }
        return sb.toString();
    }


    private JSONObject googleLoginParams(FirebaseUser obj) throws NoSuchAlgorithmException {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            js.put("app_id", GeneralConstants.APP_ID);
            data.put("email",obj.getEmail());
            data.put("password",obj.getEmail());
            data.put("sociallogin", "google");
            data.put("device_token", UserPrefs.getFCMUserToken(this));
            js.put("data", data);

        } catch (JSONException  e) {
            e.printStackTrace();
        }

        return js;
    }

    AsyncTaskListener googleLoginListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            dismissProgressDialog();
            if(userLoginObj.isStatus()) {
                UserPrefs.setNameSpace(BaseActivity.this, userLoginObj.getData().getNamespace());
                UserPrefs.setEmail(BaseActivity.this, user.getEmail());
                fragmentLogin.dismisFragment();
                if(loginFragmentContext instanceof ProfileActivity){
                    tv_signout_fragments_settings.setText("Sign out");
                    tvEmail.setText(user.getEmail());
                    Utils.showView();
                }else if (loginFragmentContext instanceof CartActivity){
                    llSignIn.setVisibility(View.GONE);
                }
            }else {
                Utils.showCenteredToastmessage(BaseActivity.this,"Please Try Again!!!");
            }
        }
    };

    public void initViews(Bundle savedInstanceState) {

    }

    public abstract int getLayoutId();


    @Override
    protected void onStop() {
        super.onStop();
    }



    public void AddActivityAndRemoveIntentTransition(Class activity) {
        Intent intent = new Intent(this, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(intent, 0);
        finish();
    }

    public void AddActivityAndRemoveIntentTransitionWithoutFinish(Class activity) {
        Intent intent = new Intent(this, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void showProgressDialog(Context context) {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = new ProgressDialog(context, R.style.ProgressDialogTheme);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.setCancelable(false);
            progressDialog.show();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void dismissProgressDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    public void showCenteredToastmessage(Context mContext, String text) {
        Toast toast = Toast.makeText(mContext, text, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
    //endregion


}