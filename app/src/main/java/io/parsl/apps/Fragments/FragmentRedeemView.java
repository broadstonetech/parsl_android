package io.parsl.apps.Fragments;

import static io.parsl.apps.Activities.ARActivities.GltfActivity.isModelDownloadedFromUnity;


import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import io.parsl.apps.Activities.ARActivities.GltfActivity;
import io.parsl.apps.R;


public class FragmentRedeemView extends Fragment implements View.OnClickListener {

    LottieAnimationView image_background;
    CardView cvCard1, cvCard2;
    Animation animation, animation2, animation3;
    FloatingActionButton fabBtn1test, fabBtn2;
    LinearLayout llCardView1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View V = inflater.inflate(R.layout.gift_redeem_screen, container, false);

        image_background = V.findViewById(R.id.image_background);

        llCardView1 = V.findViewById(R.id.llCardView1);
        cvCard1 = V.findViewById(R.id.cvCard1);
        fabBtn1test = V.findViewById(R.id.fabBtn1test);

        cvCard2 = V.findViewById(R.id.cvCard2);
        fabBtn2 = V.findViewById(R.id.fabBtn2);

        loadLottieAnimation(image_background);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                animation = AnimationUtils.loadAnimation(getActivity(), R.anim.pro_animation);
                cvCard1.setVisibility(View.VISIBLE);
                cvCard1.startAnimation(animation);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        animation3 = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                        fabBtn1test.setVisibility(View.VISIBLE);
                        fabBtn1test.setAnimation(animation3);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }, 1000);


        fabBtn1test.setOnClickListener(this::onClick);

        fabBtn2.setOnClickListener(this);
        return V;
    }

    //region onclicks
    @Override
    public void onClick(View v) {
        if (v.getId() == fabBtn1test.getId()) {
            hideView();
            animation2 = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            cvCard2.setVisibility(View.VISIBLE);
            cvCard2.setAnimation(animation2);
            animation2.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    animation2 = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                    fabBtn2.setVisibility(View.VISIBLE);
                    fabBtn2.setAnimation(animation2);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        } else if (v.getId() == fabBtn2.getId()) {

            // ((GltfActivity)getActivity()).showToast();
            getActivity().getSupportFragmentManager().beginTransaction().remove(FragmentRedeemView.this).commit();

        }
    }
    //endregion

    public void loadLottieAnimation(LottieAnimationView lottieAnimationView) {
        lottieAnimationView.setProgress(0);
        lottieAnimationView.setMinimumHeight(200);
        lottieAnimationView.pauseAnimation();
        lottieAnimationView.playAnimation();

    }

    private void hideView() {
        llCardView1.setVisibility(View.GONE);
    }
}
