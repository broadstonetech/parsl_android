// package io.parsl.apps.Fragments;
//
// import android.app.Activity;
// import android.content.Intent;
// import android.media.MediaPlayer;
// import android.net.Uri;
// import android.os.AsyncTask;
// import android.os.Bundle;
// import android.provider.MediaStore;
// import android.text.TextUtils;
// import android.util.Patterns;
// import android.view.LayoutInflater;
// import android.view.View;
// import android.view.ViewGroup;
// import android.widget.ImageView;
// import android.widget.MediaController;
// import android.widget.TextView;
// import android.widget.VideoView;
//
// import androidx.annotation.NonNull;
// import androidx.annotation.Nullable;
// import androidx.fragment.app.Fragment;
// import androidx.recyclerview.widget.LinearLayoutManager;
// import androidx.recyclerview.widget.RecyclerView;
//
// import com.google.android.material.textfield.TextInputEditText;
// import com.google.android.material.textfield.TextInputLayout;
// import com.khoiron.actionsheets.ActionSheet;
// import com.khoiron.actionsheets.callback.ActionSheetCallBack;
//
// import org.json.JSONArray;
// import org.json.JSONException;
// import org.json.JSONObject;
//
// import java.io.File;
// import java.io.IOException;
// import java.net.URISyntaxException;
// import java.text.SimpleDateFormat;
// import java.util.ArrayList;
// import java.util.Date;
// import java.util.Locale;
//
// import cz.msebera.android.httpclient.HttpEntity;
// import cz.msebera.android.httpclient.HttpResponse;
// import cz.msebera.android.httpclient.ParseException;
// import cz.msebera.android.httpclient.client.HttpClient;
// import cz.msebera.android.httpclient.client.methods.HttpPost;
// import cz.msebera.android.httpclient.entity.mime.HttpMultipartMode;
// import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;
// import cz.msebera.android.httpclient.entity.mime.content.FileBody;
// import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
// import io.parsl.apps.Activities.ARActivities.GltfActivity;
// import io.parsl.apps.Adaptors.AdapterExtrasModelsData;
// import io.parsl.apps.Adaptors.AdapterItemTextData;
// import io.parsl.apps.Adaptors.CartListAdapter;
// import io.parsl.apps.Network.API.Api;
// import io.parsl.apps.Network.API.TaskResult;
// import io.parsl.apps.Network.AsyncTaskListener;
// import io.parsl.apps.Network.Parser.CoupanCodeDataParser;
// import io.parsl.apps.Network.Parser.ParselClassDataParser;
// import io.parsl.apps.Network.Parser.SaveParselCartParser;
// import io.parsl.apps.Payments.PaymentBaseActivity;
// import io.parsl.apps.Payments.PaymentMethodSelectionFragment2;
// import io.parsl.apps.R;
// import io.parsl.apps.Utilities.Constants.GeneralConstants;
// import io.parsl.apps.Utilities.Util.Utils;
//
// public class CartFragment extends Fragment implements View.OnClickListener {
//
//    public static TextView tv_total;
//    RecyclerView recycler_itemlist;
//    public static RecyclerView rvDataInText;
//    CartListAdapter cartListAdapter;
//    TextInputLayout widgetMessage;
//    TextView btnPayment;
//    RecyclerView rvExtras;
//    TextView tvAddMessage,tvApplyCoupan;
//    int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 1000;
//    int REQUEST_CODE_GALLERY_VIDEO = 2000;
//    String path = "";
//    public static File videoFile;
//    private AdapterExtrasModelsData adapterData;
//    public static String giftModelID;
//    public  static  TextInputEditText msg,etCoupan;
//    public static boolean animatedModel = false;
//    double totalPrice,totalPriceWithCommisin;
//    public  static  JSONArray idList = new JSONArray();
//     private AdapterItemTextData adapterTextData;
//     ImageView imgVideoIcon,imgEditVideo;
//     public static TextInputEditText etEmail,etName;
//     VideoView videoView;
//     private String video_url;
//     private Uri videoUri;
//     public static double commissioned;
//     public static String charcter;
//     public static String animation;
//     TextView tvAvatarScreen;
//
//     @Nullable
//     @Override
//     public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//         View V = inflater.inflate(R.layout.activity_cart_layout,container,false);
//
//        tv_total = V.findViewById(R.id.tv_total);
//
//        widgetMessage = V.findViewById(R.id.widgetMessage);
//        btnPayment = V.findViewById(R.id.btnPayment);
//        tvAddMessage = V.findViewById(R.id.tvAddMessage);
//         tvAvatarScreen = V.findViewById(R.id.tvAvatarScreen);
//        msg = V.findViewById(R.id.etMsg);
//        etCoupan = V.findViewById(R.id.etCoupan);
//        tvApplyCoupan = V.findViewById(R.id.tvApplyCoupan);
//         imgVideoIcon = V.findViewById(R.id.imgVideoIcon);
//         etEmail = V.findViewById(R.id.etEmail);
//         etName = V.findViewById(R.id.etName);
//         videoView = V.findViewById(R.id.videoView);
//
//        rvExtras = V.findViewById(R.id.rvExtras);
//         imgEditVideo = V.findViewById(R.id.imgEditVideo);
//
//        recycler_itemlist = V.findViewById(R.id.recycler_cart);
//
//        rvDataInText = V.findViewById(R.id.recyclerItems);
//        recycler_itemlist.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//
//
//        cartListAdapter = new CartListAdapter(getActivity(), GltfActivity.shoppingCartProductList);
//        recycler_itemlist.setAdapter(cartListAdapter);
//
//
//
//        rvDataInText.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//
//        adapterTextData = new AdapterItemTextData(getActivity(), GltfActivity.shoppingCartProductList);
//        rvDataInText.setAdapter(adapterTextData);
//
//         ((GltfActivity)getActivity()).showProgressDialog(getActivity());
//        Api.getParselModels(getActivity(), getParams(), extrasListner);
//
//        btnPayment.setOnClickListener(this);
//       // tvAddMessage.setOnClickListener(this);           // disabling clicklister so that bottom sheet functinality not avaible
//        tvApplyCoupan.setOnClickListener(this);
//         imgEditVideo.setOnClickListener(this);
//         tvAvatarScreen.setOnClickListener(this);
//
//        for(int x = 0; x < GltfActivity.shoppingCartProductList.size(); x++){
//
//            totalPrice +=  GltfActivity.shoppingCartProductList.get(x).getObj().getPrice_related().getPrice() * GltfActivity.shoppingCartProductList.get(x).getQuantity();
//            idList.put(GltfActivity.shoppingCartProductList.get(x).getObj().getProduct_id());
//        }
//
//         video_url = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.promo;
//         videoUri = Uri.parse(video_url);
//         MediaController mediaController= new MediaController(getActivity());
//         mediaController.setAnchorView(videoView);
//         videoView.setMediaController(mediaController);
//         videoView.setVideoURI(videoUri);
//         //videoView.requestFocus();
//         videoView.seekTo( 1 );
//        // videoView.start();
//
//         commissioned = totalPrice *0.15;
//        totalPriceWithCommisin = totalPrice + commissioned;
//        tv_total.setText("$"+" "+ Utils.getRoundedDoubleForMoney(totalPriceWithCommisin));
//
//
//        return V;
//    }
//
//
//    private void setExtrasObjModel() {
//         adapterData = new AdapterExtrasModelsData(getActivity(), ParselClassDataParser.parselModelObj,true);
//        rvExtras.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, true));
//        rvExtras.setAdapter(adapterData);
//
//        adapterData.setOnItemClickListener(new AdapterExtrasModelsData.ClickListener() {
//            @Override
//            public void onItemClick(int position, View v) {
//                Utils.setExtrasProjectionPosition(position,adapterData);
//                animatedModel = true;
//                giftModelID = ParselClassDataParser.parselModelObj.getData().get(position).getProduct_id();
//            }
//        });
//    }
//
//    @Override
//    public void onClick(View v) {
//        if (v.getId() == btnPayment.getId()) {
//            if(animatedModel) {
//                if (validateFields()) {
//                    if (isValidEmail(etEmail.getText().toString())) {
//                        startPaymentBottomSheetFragment(1000);
//                    } else {
//                        ((GltfActivity)getActivity()).showCenteredToastmessage(getActivity(), "Please enter valid email address.");
//                    }
//                }
//            }else {
//                ((GltfActivity)getActivity()).showCenteredToastmessage(getActivity(), "Please select gift from above mentioned choice.");
//            }
//        } else if (v.getId() == tvAddMessage.getId()) {
//
//        }else if(v.getId() == tvApplyCoupan.getId()){
//            ((GltfActivity)getActivity()).showProgressDialog(getActivity());
//            Api.getApplyCoupan(getActivity(),getCoupanParams(etCoupan.getText().toString(),idList),getCoupanListner);
//        }else if (v.getId() == imgEditVideo.getId()){
//
//            ArrayList<String> data = new ArrayList<>();
//
//            data.add("Use default video");
//            data.add("Record video");
//            data.add("Upload from gallery");
//
//            new ActionSheet(getActivity(),data)
//                    .setTitle("What do you want to do with the file")
//                    .setCancelTitle("Cancel")
//                    .setColorTitle(getResources().getColor(R.color.title))
//                    .setColorTitleCancel(getResources().getColor(R.color.action))
//                    .setColorData(getResources().getColor(R.color.action))
//                    .create(new ActionSheetCallBack() {
//                        @Override
//                        public void data( String data, int position) {
//                            switch (position){
//                                case 0:
//                                    videoView.setVideoURI(videoUri);
//                                    videoView.seekTo( 1 );
//                                    videoFile = new File(videoUri.getPath());
//                                    break;
//                                case 1:
//                                    Intent intent = new Intent("android.media.action.VIDEO_CAPTURE");
//                                    intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
//                                    intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
//                                    startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
//                                    break;
//                                case 2:
//                                    Intent intent2 = new Intent();
//                                    intent2.setType("video/*");
//                                    intent2.setAction(Intent.ACTION_GET_CONTENT);
//                                    startActivityForResult(Intent.createChooser(intent2,"Select Video"),REQUEST_CODE_GALLERY_VIDEO);
//                                    break;
//                            }
//                        }
//                    });
//        }else if(v.getId()==tvAvatarScreen.getId()){
//            ((GltfActivity)getActivity()).moveToAvatarScreen();
//        }
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {
//            if (resultCode == Activity.RESULT_OK) {
//                Uri fileUri = data.getData();
//                try {
//                    path = Utils.getPath(getActivity(),fileUri);
//                } catch (URISyntaxException e) {
//                    e.printStackTrace();
//                }
//                videoFile = new File(fileUri.getPath());
//                videoView.setVideoURI(fileUri);
//                videoView.seekTo(5);
//                //imgVideoIcon.setVisibility(View.VISIBLE);
//                int x = 100;
//           //     new UploadFileAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//            }
//        }else if (requestCode == REQUEST_CODE_GALLERY_VIDEO) {
//            Uri selectedVieo = data.getData();
//            try {
//
//                    MediaPlayer mp = MediaPlayer.create(getActivity(), selectedVieo);
//                    int duration = mp.getDuration();
//                    mp.release();
//
//                    if ((duration / 1000) > 10) {
//                        ((GltfActivity)getActivity()).showCenteredToastmessage(getActivity(),"Video must not be greater than 10 seconds");
//                    } else {
//                        videoView.setVideoURI(selectedVieo);
//                        videoView.seekTo(5);
//
//                        videoFile = new File(data.getData().getPath());
//
//                        path = Utils.getPath(getActivity(),selectedVieo);
//                    }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    //region Api listners and payload
//
//
//    AsyncTaskListener extrasListner = new AsyncTaskListener() {
//        @Override
//        public void onComplete(TaskResult result) {
//            ((GltfActivity)getActivity()).dismissProgressDialog();
//            setExtrasObjModel();
//        }
//    };
//
//     AsyncTaskListener getCoupanListner = new AsyncTaskListener() {
//         @Override
//         public void onComplete(TaskResult result) {
//             ((GltfActivity)getActivity()).dismissProgressDialog();
//             if(result.code == 200 || result.code == 2) {
//                 if (CoupanCodeDataParser.coupanCodeDatatObj.getMessage().equals("Details checked successfuly.")) {
//                     for (int i = 0; i < GltfActivity.shoppingCartProductList.size(); i++) {
//
//                         for(int x = 0; x < CoupanCodeDataParser.coupanCodeDatatObj.getData().getApproved_products().size() ; x++) {
//                             if (GltfActivity.shoppingCartProductList.get(i).getObj().getProduct_id().equals(CoupanCodeDataParser.coupanCodeDatatObj.getData().getApproved_products().get(x))) {
//                                 if (CoupanCodeDataParser.coupanCodeDatatObj.getData().getCoupon_relief_type().equals("price_relief")) {
//                                     double price = GltfActivity.shoppingCartProductList.get(i).getObj().getPrice_related().getPrice() - CoupanCodeDataParser.coupanCodeDatatObj.getData().getCoupon_relief();
//                                     GltfActivity.shoppingCartProductList.get(i).setDisCounts(price);
//                                 } else {
//                                     double relief = CoupanCodeDataParser.coupanCodeDatatObj.getData().getCoupon_relief() / 100;
//                                     double price = GltfActivity.shoppingCartProductList.get(i).getObj().getPrice_related().getPrice() * relief;
//                                     double prices = GltfActivity.shoppingCartProductList.get(i).getObj().getPrice_related().getPrice() - price;
//                                     double totalPRice = Double.valueOf(Utils.getRoundedDoubleForMoney(prices));
//                                     GltfActivity.shoppingCartProductList.get(i).setDisCounts(totalPRice);
//                                 }
//                             }
//                         }
//                     }
//                     cartListAdapter = new CartListAdapter(getActivity(), GltfActivity.shoppingCartProductList);
//                     recycler_itemlist.setAdapter(cartListAdapter);
//
//                     adapterTextData = new AdapterItemTextData(getActivity(), GltfActivity.shoppingCartProductList);
//                     rvDataInText.setAdapter(adapterTextData);
//
//                     totalPrice = 0;
//                     for (int x = 0; x < CoupanCodeDataParser.coupanCodeDatatObj.getData().getApproved_products().size(); x++) {
//
//                         for(int i = 0; i < GltfActivity.shoppingCartProductList.size();i++ ) {
//                             if (CoupanCodeDataParser.coupanCodeDatatObj.getData().getApproved_products().get(x).equals(GltfActivity.shoppingCartProductList.get(i).getObj().getProduct_id())){
//                                 totalPrice += GltfActivity.shoppingCartProductList.get(i).getDisCounts() * GltfActivity.shoppingCartProductList.get(i).getQuantity();
//                             }
//
//                         }
//                     }
//
//
//                 } else {
//                     ((GltfActivity)getActivity()).showCenteredToastmessage(getActivity(), "Your coupan does not valid on above products");
//                 }
//                   double deniedProductPrice = 0;
//                 if(CoupanCodeDataParser.coupanCodeDatatObj.getData().getDenied_products().size() > 0){
//                     for (int x = 0; x < CoupanCodeDataParser.coupanCodeDatatObj.getData().getDenied_products().size(); x++) {
//
//                         for(int i = 0; i < GltfActivity.shoppingCartProductList.size();i++ ) {
//
//                             if (CoupanCodeDataParser.coupanCodeDatatObj.getData().getDenied_products().get(x).equals(GltfActivity.shoppingCartProductList.get(i).getObj().getProduct_id()))
//                                 deniedProductPrice += GltfActivity.shoppingCartProductList.get(i).getObj().getPrice_related().getPrice() * GltfActivity.shoppingCartProductList.get(i).getQuantity();
//                         }
//                     }
//
//                      commissioned = (totalPrice + deniedProductPrice) * 0.15;
//                     totalPriceWithCommisin = totalPrice + deniedProductPrice + commissioned;
//                     tv_total.setText("$" + " " + Utils.getRoundedDoubleForMoney(totalPriceWithCommisin));
//                 }else {
//                     commissioned = (totalPrice + deniedProductPrice) * 0.15;
//                     totalPriceWithCommisin = totalPrice + deniedProductPrice + commissioned;
//                     tv_total.setText("$" + " " + Utils.getRoundedDoubleForMoney(totalPriceWithCommisin));
//                 }
//             }else {
//                 ((GltfActivity)getActivity()).showCenteredToastmessage(getActivity(),"Inavlid coupon");
//             }
//         }
//     };
//
//    private JSONObject getParams() {
//        JSONObject js = new JSONObject();
//        JSONObject data = new JSONObject();
//
//        try {
//            js.put("app_id", GeneralConstants.APP_ID);
//            js.put("namespace", "");
//            js.put("data", data);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return js;
//    }
//
//     private JSONObject getCoupanParams(String coupanCode, JSONArray selectedProducts) {
//         JSONObject js = new JSONObject();
//         JSONObject data = new JSONObject();
//         String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
//         String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
//         try {
//             js.put("app_id", GeneralConstants.APP_ID);
//             data.put("coupon_code", coupanCode);
//             data.put("selected_products", selectedProducts);
//             data.put("date_time", currentDate+" "+currentTime);
//             js.put("data", data);
//         } catch (JSONException e) {
//             e.printStackTrace();
//         }
//
//         return js;
//     }
//
//    public void uploadVideo(){
//        ((GltfActivity)getActivity()).showProgressDialog(getActivity());
//        new UploadFileAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//    }
//
//    private  class UploadFileAsync extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected String doInBackground(String... params) {
//            //  int code = upLoad2Server(path);
//            try {
//                uploadVideo(path);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return "";
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//
//            ((GltfActivity)getActivity()).dismissProgressDialog();
//            ((GltfActivity)getActivity()).showCenteredToastmessage(getActivity(), result);
//        }
//
//        @Override
//        protected void onPreExecute() {
//        }
//
//        private void uploadVideo(String videoPath) throws ParseException, IOException {
//
//            HttpClient httpclient = new DefaultHttpClient();
//            HttpPost httppost = new HttpPost("https://parsl.io/save/parsl/video/msg");
//
//            FileBody filebodyVideo = new FileBody(new File(videoPath));
//
//            MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
//            reqEntity.addPart("video_msg", filebodyVideo).addTextBody("parsl_id", SaveParselCartParser.saveParselCartObj.getData().getParsl_id());
//          //  reqEntity.addPart("video_msg", filebodyVideo).addTextBody("parsl_id","60a7c17a3ba6704b696ce6c2");
//
//            HttpEntity entity = reqEntity.build();
//
//            httppost.setEntity(entity);
//
//            HttpResponse response = httpclient.execute( httppost );
//            HttpEntity resEntity = response.getEntity( );
//
//
//            if (resEntity != null) {
//                resEntity.consumeContent( );
//            } // end if
//
//            httpclient.getConnectionManager( ).shutdown( );
//        }
//
//
//    }
//
//
//
//    //endregion
//
//    private void startPaymentBottomSheetFragment(double amount) {
//        PaymentMethodSelectionFragment2 paymentMethodSelectionActivity =
//                new PaymentMethodSelectionFragment2();
//        paymentMethodSelectionActivity.setAmount(amount);
//        paymentMethodSelectionActivity.setDistributionType(1);
//        paymentMethodSelectionActivity.setTransactionalFeeIncluded(true);
//        paymentMethodSelectionActivity.show(getActivity().getSupportFragmentManager(), "");
//    }
//
//
//     public boolean isValidEmail(CharSequence target) {
//         return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
//     }
//
//     private boolean validateFields(){
//         if(etName.getText().toString().isEmpty() && etEmail.getText().toString().isEmpty() ){
//             etName.setError("Name was empty");
//             etEmail.setError("Email was empty");
//             return false;
//         }
//
//         if(etName.getText().toString().isEmpty()){
//             etName.setError("Name was empty");
//             return false;
//         }
//
//        if(etEmail.getText().toString().isEmpty()){
//            etEmail.setError("Email was empty");
//            return false;
//        }
//        return true;
//     }
// }
